

INSERT INTO public.mca_content_analysis_config (id, doc_type, validation_type, byte_pattern, sample, string_pattern, unwanted_content) VALUES (1, 'DICOM', 'DICOM', '\x00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000004449434d', NULL, NULL, NULL);
INSERT INTO public.mca_content_analysis_config (id, doc_type, validation_type, byte_pattern, sample, string_pattern, unwanted_content) VALUES (2, 'HL7v2', 'HL7V2', NULL, NULL, NULL, NULL);
INSERT INTO public.mca_content_analysis_config (id, doc_type, validation_type, byte_pattern, sample, string_pattern, unwanted_content) VALUES (3, 'Certificate', 'TLS', NULL, NULL, NULL, NULL);

