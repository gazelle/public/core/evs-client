package net.ihe.gazelle.evsclient.interlay.adapters.validationservice;

import junit.framework.TestCase;
import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.domain.validation.ValidationType;
import net.ihe.gazelle.evsclient.domain.validation.report.ValidationReport;
import net.ihe.gazelle.evsclient.domain.validation.report.ValidationTestResult;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.SystemValidationServiceConf;
import net.ihe.gazelle.evsclient.interlay.adapters.validationservice.factory.DcmCheckValidationServiceFactory;
import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.File;
import java.io.IOException;

import static net.ihe.gazelle.files.FilesUtils.loadFile;

@RunWith(PowerMockRunner.class)
public class DcmCheckReportBuilderIT extends TestCase {

    @Mock
    ApplicationPreferenceManager applicationPreferenceManager;

    DcmCheckValidationService service;

    @Before
    public void setUp() {
        SystemValidationServiceConf conf = new SystemValidationServiceConf();
        conf.setValidationType(ValidationType.DICOM);
        conf.setAvailable(true);
        conf.setName("DcmCheck Validation Service");
        conf.setId(1);
        conf.setBinaryPath("dcmcheck");
        conf.setFactoryClassname(DcmCheckValidationServiceFactory.class.getCanonicalName());
        service = new DcmCheckValidationService(applicationPreferenceManager, conf);
    }

    @Test
    public void buildReportTest() {
        final File f = loadFile("/dcm/dcmcheck.txt");
        String fileContent;
        try {
            fileContent = FileUtils.readFileToString(f);
        } catch (IOException e) {
            fail("Unable to read file");
            return;
        }
        ValidationReport report = new DcmcheckReportBuilder().build(service, "dcmcheck", "", fileContent);
        Assert.assertEquals(ValidationTestResult.FAILED,report.getValidationOverallResult());
        Assert.assertEquals(10,report.getCounters().getNumberOfErrors().longValue());
        Assert.assertEquals(4,report.getSubReports().size());
    }

}
