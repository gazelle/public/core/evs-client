package net.ihe.gazelle.evsclient.application.validation;

import net.ihe.gazelle.evsclient.application.interfaces.adapters.ProcessingDao;
import net.ihe.gazelle.evsclient.domain.statistics.IntervalForStatistics;
import net.ihe.gazelle.evsclient.domain.statistics.StatisticsDataRow;
import net.ihe.gazelle.evsclient.domain.validation.Validation;
import net.ihe.gazelle.evsclient.domain.validation.ValidationStatus;
import net.ihe.gazelle.evsclient.interlay.dao.Repository;

import java.util.List;

public interface ValidationDao extends ProcessingDao<Validation> {

    int countByValidationStatus(ValidationStatus validationStatus);

    int countByValidationStatusAndReferencedStandardId(ValidationStatus validationStatus, int standardId);

    Repository getRepositoryType(Validation validation);

    StatisticsDataRow countTotalDataRow();

    List<StatisticsDataRow> countTotalDataRowGroupByReferencedStandard();

    List<Validation> findAllValidationByStandardId(int id);

    List<Object[]> countPerValidationServiceByReferencedStandardId(int referencedStandardId);

    List<Object[]> countPerValidatorByReferencedStandardId(int standardId);

    List<Object[]> countPerDateIntervalByReferencedStandardId(int referencedStandardId, IntervalForStatistics interval);

    List<Object[]> countPerCountry();
}
