/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evsclient.interlay.gui;

import net.ihe.gazelle.common.pages.Authorization;
import net.ihe.gazelle.common.pages.Page;

public enum Pages implements Page {

   HOME("/home.xhtml", Icons.FA_WRENCH, "gazelle.evs.client.home", Authorizations.ALL),

   VALIDATE("/validate.xhtml", Icons.FA_WRENCH, "gazelle.evs.client.validate"),

   SCHEMATRONS("/administration/schematrons.xhtml", Icons.FA_FILE_TEXT, "gazelle.evs.client.cda.schematrons.title",
         Authorizations.SCHEMATRON),

   STATISTICS("/administration/statistics.xhtml", Icons.FA_BAR_CHART, "gazelle.evs.client.statistics"),

   STATISTICS_BY_TYPE("/administration/statisticsByType.xhtml", Icons.FA_BAR_CHART, "gazelle.evs.client.statistics"),

   ERROR("/error.xhtml", Icons.FA_BAR_CHART, null),

   MESSAGE_CONTENT_ANALYZER("/messageContentAnalyzer.xhtml", Icons.FA_BAR_CHART, "net.ihe.gazelle.mca.MessageContentAnalyzer"),

   MESSAGE_CONTENT_ANALYZER_RESULT("/messageContentAnalyzerDetailedResult.xhtml", Icons.FA_BAR_CHART,
         "gazelle.evs.client.statistics", Authorizations.IS_LOGGED_IF_NEEDED),

   MESSAGE_CONTENT_ANALYZER_LOGS("/common/messageContentAnalyzerResultLogs.xhtml", Icons.FA_BOOK,
         "net.ihe.gazelle.mca.MessageContentAnalyzerResultLogs", Authorizations.IS_LOGGED_IF_NEEDED),

   MCA_CONFIG("/config/messageContentAnalyzerConfig.xhtml", Icons.FA_WRENCH,
         "net.ihe.gazelle.mca.MessageContentAnalyzerConfigurations", Authorizations.IS_LOGGED_IF_NEEDED),

   VALIDATION_SERVICES("/administration/validationServices.xhtml", Icons.FA_SERVER,
         "gazelle.evs.client.admin.validation.services", Authorizations.ADMIN),

   VALIDATION_SERVICES_DISPLAY("/administration/validationServiceDisplay.xhtml", Icons.FA_SERVER,
         "gazelle.evs.client.admin.validation.services", Authorizations.ADMIN),

   REFERENCED_STANDARDS("/administration/referencedStandards.xhtml", Icons.FA_BOOK,
         "gazelle.evs.client.admin.referenced.standards", Authorizations.ADMIN),

   REFERENCE_STANDARD_DISPLAY("/administration/referencedStandardDisplay.seam", Icons.FA_BOOK,
         "gazelle.evs.client.admin.referenced.standards", Authorizations.ADMIN),

   MENU_CONFIG("/administration/menuConfiguration.xhtml", Icons.FA_MAP_SIGNS,
         "gazelle.evs.client.admin.menu.menuConfiguration", Authorizations.ADMIN),

   CALLING_TOOLS("/administration/callingTools.xhtml", Icons.FA_PHONE,
         "gazelle.evs.client.CallingTools", Authorizations.ADMIN),

   ADMIN_TEMPLATES("/administration/adminTemplates.xhtml", Icons.FA_CROP,
         "net.ihe.gazelle.CDATemplates", Authorizations.ADMIN),

   CONFIGURATION("/administration/applicationConfiguration.xhtml", Icons.FA_WRENCH,
         "net.ihe.gazelle.evs.ApplicationPreferences", Authorizations.ADMIN),

   DISPLAY_APPLICATION_PREFERENCE("/administration/displayApplicationPreference.seam", Icons.FA_WRENCH,
         "net.ihe.gazelle.evs.ApplicationPreferences", Authorizations.ADMIN),

   API_KEY_MANAGEMENT("/administration/apiKeyManagement.xhtml", Icons.FA_KEY,
         "net.ihe.gazelle.evsclient.APIKeyManagement", Authorizations.LOGGED),

   DETAILED_RESULT("/report.xhtml", Icons.FA_WRENCH, "gazelle.evs.client.result"),

   HL7V2_RESULT("/hl7v2Result.xhtml", Icons.FA_WRENCH, "gazelle.evs.client.hl7v2result"),

   PDF_RESULT("/report.xhtml", Icons.FA_WRENCH, "gazelle.evs.client.pdfresult"),

   INITIALIZATION_ERROR("/initializationError.xhtml", Icons.FA_WRENCH, "gazelle.evs.client.initializationerror"),

   LOGGED_IN("/loggedIn.xhtml", Icons.FA_WRENCH, "gazelle.evs.client.loggedin"),

   WORLD_MAP("/worldMap.xhtml", Icons.FA_WRENCH, "WorldMap"),

   XVAL_RESULT("/xvalResult.xhtml", Icons.FA_SITEMAP, "gazelle.evs.client.xvalresult"),

   ATNA_LOGS("/atna/allLogs.xhtml", Icons.FA_BOOK, Constants.VALIDATIONS, Authorizations.IS_LOGGED_IF_NEEDED),
   ATNA_VALIDATOR("/atna/validator.xhtml", Icons.FA_CHECK_SQUARE_O, Constants.VALIDATE, Authorizations.IS_LOGGED_IF_NEEDED),
   CDA_LOGS("/cda/allLogs.xhtml", Icons.FA_BOOK, Constants.VALIDATIONS, Authorizations.IS_LOGGED_IF_NEEDED),
   CDA_VALIDATOR("/cda/validator.xhtml", Icons.FA_CHECK_SQUARE_O, Constants.VALIDATE, Authorizations.IS_LOGGED_IF_NEEDED),
   DEFAULT_LOGS("/default/allLogs.xhtml", Icons.FA_BOOK, Constants.VALIDATIONS, Authorizations.IS_LOGGED_IF_NEEDED),
   DEFAULT_VALIDATOR("/default/validator.xhtml", Icons.FA_CHECK_SQUARE_O, Constants.VALIDATE, Authorizations.IS_LOGGED_IF_NEEDED),
   DICOM_LOGS("/dicom/allLogs.xhtml", Icons.FA_BOOK, Constants.VALIDATIONS, Authorizations.IS_LOGGED_IF_NEEDED),
   DICOM_VALIDATOR("/dicom/validator.xhtml", Icons.FA_CHECK_SQUARE_O, Constants.VALIDATE, Authorizations.IS_LOGGED_IF_NEEDED),
   DICOM_WEB_LOGS("/dicom_web/allLogs.xhtml", Icons.FA_BOOK, Constants.VALIDATIONS, Authorizations.IS_LOGGED_IF_NEEDED),
   DICOM_WEB_VALIDATOR("/dicom_web/validator.xhtml", Icons.FA_CHECK_SQUARE_O, Constants.VALIDATE, Authorizations.IS_LOGGED_IF_NEEDED),
   DSUB_LOGS("/dsub/allLogs.xhtml", Icons.FA_BOOK, Constants.VALIDATIONS, Authorizations.IS_LOGGED_IF_NEEDED),
   DSUB_VALIDATOR("/dsub/validator.xhtml", Icons.FA_CHECK_SQUARE_O, Constants.VALIDATE, Authorizations.IS_LOGGED_IF_NEEDED),
   FHIR_LOGS("/fhir/allLogs.xhtml", Icons.FA_BOOK, Constants.VALIDATIONS, Authorizations.IS_LOGGED_IF_NEEDED),
   FHIR_VALIDATOR("/fhir/validator.xhtml", Icons.FA_CHECK_SQUARE_O, Constants.VALIDATE, Authorizations.IS_LOGGED_IF_NEEDED),
   HL7V2_LOGS("/hl7v2/allLogs.xhtml", Icons.FA_BOOK, Constants.VALIDATIONS, Authorizations.IS_LOGGED_IF_NEEDED),
   HL7V2_VALIDATOR("/hl7v2/validator.xhtml", Icons.FA_CHECK_SQUARE_O, Constants.VALIDATE, Authorizations.IS_LOGGED_IF_NEEDED),
   HL7V3_LOGS("/hl7v3/allLogs.xhtml", Icons.FA_BOOK, Constants.VALIDATIONS, Authorizations.IS_LOGGED_IF_NEEDED),
   HL7V3_VALIDATOR("/hl7v3/validator.xhtml", Icons.FA_CHECK_SQUARE_O, Constants.VALIDATE, Authorizations.IS_LOGGED_IF_NEEDED),
   HPD_LOGS("/hpd/allLogs.xhtml", Icons.FA_BOOK, Constants.VALIDATIONS, Authorizations.IS_LOGGED_IF_NEEDED),
   HPD_VALIDATOR("/hpd/validator.xhtml", Icons.FA_CHECK_SQUARE_O, Constants.VALIDATE, Authorizations.IS_LOGGED_IF_NEEDED),
   PDF_LOGS("/pdf/allLogs.xhtml", Icons.FA_BOOK, Constants.VALIDATIONS, Authorizations.IS_LOGGED_IF_NEEDED),
   PDF_VALIDATOR("/pdf/validator.xhtml", Icons.FA_CHECK_SQUARE_O, Constants.VALIDATE, Authorizations.IS_LOGGED_IF_NEEDED),
   SAML_LOGS("/saml/allLogs.xhtml", Icons.FA_BOOK, Constants.VALIDATIONS, Authorizations.IS_LOGGED_IF_NEEDED),
   SAML_VALIDATOR("/saml/validator.xhtml", Icons.FA_CHECK_SQUARE_O, Constants.VALIDATE, Authorizations.IS_LOGGED_IF_NEEDED),
   SVS_LOGS("/svs/allLogs.xhtml", Icons.FA_BOOK, Constants.VALIDATIONS, Authorizations.IS_LOGGED_IF_NEEDED),
   SVS_VALIDATOR("/svs/validator.xhtml", Icons.FA_CHECK_SQUARE_O, Constants.VALIDATE, Authorizations.IS_LOGGED_IF_NEEDED),
   TLS_LOGS("/tls/allLogs.xhtml", Icons.FA_BOOK, Constants.VALIDATIONS, Authorizations.IS_LOGGED_IF_NEEDED),
   TLS_VALIDATOR("/tls/validator.xhtml", Icons.FA_CHECK_SQUARE_O, Constants.VALIDATE, Authorizations.IS_LOGGED_IF_NEEDED),
   XDS_LOGS("/xds/allLogs.xhtml", Icons.FA_BOOK, Constants.VALIDATIONS, Authorizations.IS_LOGGED_IF_NEEDED),
   XDS_VALIDATOR("/xds/validator.xhtml", Icons.FA_CHECK_SQUARE_O, Constants.VALIDATE, Authorizations.IS_LOGGED_IF_NEEDED),
   XDW_LOGS("/xdw/allLogs.xhtml", Icons.FA_BOOK, Constants.VALIDATIONS, Authorizations.IS_LOGGED_IF_NEEDED),
   XDW_VALIDATOR("/xdw/validator.xhtml", Icons.FA_CHECK_SQUARE_O, Constants.VALIDATE, Authorizations.IS_LOGGED_IF_NEEDED),
   XML_LOGS("/xml/allLogs.xhtml", Icons.FA_BOOK, Constants.VALIDATIONS, Authorizations.IS_LOGGED_IF_NEEDED),
   XML_VALIDATOR("/xml/validator.xhtml", Icons.FA_CHECK_SQUARE_O, Constants.VALIDATE, Authorizations.IS_LOGGED_IF_NEEDED),
   XVAL_LOGS("/xval/allLogs.xhtml", Icons.FA_BOOK, Constants.VALIDATIONS, Authorizations.IS_LOGGED_IF_NEEDED),
   XVAL_VALIDATOR("/xval/validator.xhtml", Icons.FA_CHECK_SQUARE_O, Constants.VALIDATE, Authorizations.IS_LOGGED_IF_NEEDED),
   STATISTICS_BY_STANDARD("/common/statisticsByStandard.xhtml", Icons.FA_BAR_CHART, null, Authorizations.IS_LOGGED_IF_NEEDED),
   PRETTY_RESULT("/common/prettyResult.xhtml", Icons.FA_CODE, null, Authorizations.IS_LOGGED_IF_NEEDED);

   private String link;

   private final Authorization[] authorizations;

   private String label;

   private final String icon;

   Pages(final String link, final String icon, final String label) {
      this(link, icon, label, Authorizations.IS_LOGGED_IF_NEEDED);
   }

   Pages(final String link, final String icon, final String label, final Authorization... authorizations) {
      this.link = link;
      this.authorizations = authorizations.clone();
      this.label = label;
      this.icon = icon;
   }

   @Override
   public String getId() {
      return this.name();
   }

   @Override
   public String getLink() {
      return this.link;
   }


   @Override
   public String getIcon() {
      return this.icon;
   }

   @Override
   public Authorization[] getAuthorizations() {

      if (this.authorizations != null) {
         return this.authorizations.clone();
      } else {
         return new Authorization[]{};
      }
   }

   @Override
   public String getLabel() {
      return this.label;
   }

   @Override
   public String getMenuLink() {
      return this.link.replace(".xhtml", ".seam");
   }


   public static class Icons {
      public static final String FA_BAR_CHART = "fa-bar-chart";
      public static final String FA_BOOK = "fa-book";
      public static final String FA_CHECK_SQUARE_O = "fa-check-square-o";
      public static final String FA_CODE = "fa-code";
      public static final String FA_CROP = "fa-crop";
      public static final String FA_FILE_TEXT = "fa-file-text";
      public static final String FA_KEY = "fa-key";
      public static final String FA_MAP_SIGNS = "fa-map-signs";
      public static final String FA_PHONE = "fa-phone";
      public static final String FA_SERVER = "fa-server";
      public static final String FA_SITEMAP = "fa-sitemap";
      public static final String FA_SLIDERS = "fa-sliders";
      public static final String FA_WRENCH = "fa-wrench";

      private Icons() {
      }
   }

   private static class Constants {
      public static final String VALIDATIONS = "validations";
      public static final String VALIDATE = "validate";
   }
}
