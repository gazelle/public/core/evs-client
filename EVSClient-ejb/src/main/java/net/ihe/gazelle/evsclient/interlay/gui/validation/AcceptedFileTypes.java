package net.ihe.gazelle.evsclient.interlay.gui.validation;

import java.util.ArrayList;
import java.util.List;

public enum AcceptedFileTypes {

    HL7V3("xml", "cda", "saml"),
    HL7V2("hl7", "er7", "txt", "LOGGER", "xml"),
    FHIR("xml", "json", "txt"),
    XVAL("xml", "json", "txt"),
    DICOM_WEB("url", "txt"),
    DEFAULT("xml"),
    TLS("pem", "txt", "PEM", "TXT", "CRT", "crt");

    private String[] fileTypes;

    AcceptedFileTypes(String... fileTypes) {
        this.fileTypes = fileTypes;
    }

    public List<String> getAcceptedFileTypes() {
        List<String> acceptedFileTypes = new ArrayList<>();
        for (String fileType: fileTypes) {
            acceptedFileTypes.add(fileType);
            acceptedFileTypes.add(fileType.toUpperCase());
        }
        return acceptedFileTypes;
    }

    public String getAcceptedFileTypesAsString(){
        List<String> acceptedFileTypes = getAcceptedFileTypes();
        StringBuilder stringBuilder = new StringBuilder("\"");
        for (String acceptedFiletype : acceptedFileTypes) {
            stringBuilder.append(acceptedFiletype);
            stringBuilder.append(",");
        }
        stringBuilder.setLength(stringBuilder.length() - 1); // remove last ","
        stringBuilder.append("\"");
        return getAcceptedFileTypes().toString();
    }
}
