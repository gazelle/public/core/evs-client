package net.ihe.gazelle.evsclient.application.validationservice;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.domain.validationservice.ProcessingService;
import net.ihe.gazelle.evsclient.domain.validationservice.ProcessingServiceFactory;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.ProcessingConf;
import sun.reflect.generics.reflectiveObjects.ParameterizedTypeImpl;

public abstract class AbstractServiceFactory<T extends ProcessingService, C extends ProcessingConf>
      implements ProcessingServiceFactory<T, C> {

   protected ApplicationPreferenceManager applicationPreferenceManager;
   protected C configuration;
   protected Class<T> serviceClass;

   // FIXME configuration for ValidationService Instance should be a parameter of getValidationService and not of the
   //  factory constructor.
   protected AbstractServiceFactory(ApplicationPreferenceManager applicationPreferenceManager, C configuration,
                                 Class<T> serviceClass) {
      this.applicationPreferenceManager = applicationPreferenceManager;
      this.configuration = configuration;
      this.serviceClass = serviceClass;
   }

   protected Class<C> getConfigurationClass() {
      return (Class<C>) ((ParameterizedTypeImpl) this.getClass().getGenericSuperclass()).getActualTypeArguments()[1];
   }

   @Override
   public Class<T> getServiceClass() {
      return serviceClass;
   }

}
