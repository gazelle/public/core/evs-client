/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.ihe.gazelle.evsclient.domain.validation.extension;

import net.ihe.gazelle.evsclient.domain.processing.EVSCallerMetadata;
import net.ihe.gazelle.evsclient.domain.processing.HandledObject;
import net.ihe.gazelle.evsclient.domain.processing.OwnerMetadata;
import net.ihe.gazelle.evsclient.domain.validation.Validation;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;

@Entity
@Table(name = "evsc_validation_digital_signature")
public class DigitalSignature extends ValidationExtension implements Serializable {

    private static final long serialVersionUID = -3204150422262761864L;

    public static HandledObject getHandleObject(Validation validation) {
        return new HandledObject(
                        validation.getObjects().get(0).getContent()
        );
    }
    public DigitalSignature() {
        super();
    }

    public DigitalSignature(Validation validation, EVSCallerMetadata caller, OwnerMetadata owner) {
        super(validation,new ArrayList<>(Arrays.asList(getHandleObject(validation))), caller, owner);
    }
}