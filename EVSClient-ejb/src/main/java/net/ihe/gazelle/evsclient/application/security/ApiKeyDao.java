package net.ihe.gazelle.evsclient.application.security;

import net.ihe.gazelle.evsclient.domain.security.ApiKey;

import java.util.List;

public interface ApiKeyDao {

    ApiKey createKey(ApiKey apiKey);

    List<ApiKey> getAllKeysOfUser(String owner);

    ApiKey findByValue(String apiKey) throws ApiKeyNotFoundException;
}
