package net.ihe.gazelle.evsclient.interlay.adapters.validationservice;

import com.pixelmed.dicom.AttributeList;
import com.pixelmed.dicom.AttributeTag;
import com.pixelmed.dicom.DicomInputStream;
import com.pixelmed.dicom.VersionAndConstants;
import com.pixelmed.validate.DicomSRValidator;
import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.application.validation.ReportParser;
import net.ihe.gazelle.evsclient.domain.processing.HandledObject;
import net.ihe.gazelle.evsclient.domain.validation.ValidationType;
import net.ihe.gazelle.evsclient.domain.validation.report.*;
import net.ihe.gazelle.evsclient.domain.validation.types.ValidationProperties;
import net.ihe.gazelle.evsclient.domain.validationservice.EmbeddedValidationService;
import net.ihe.gazelle.evsclient.domain.validationservice.ValidationServiceOperationException;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.EmbeddedValidationServiceConf;
import net.ihe.gazelle.evsclient.interlay.adapters.validation.PlainReportMapper;
import net.ihe.gazelle.evsclient.interlay.dto.rest.validation.report.XMLValidationReportMarshaller;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;

@ValidationProperties(
        name="PixelMed",
        types =ValidationType.DICOM)
@ReportParser(PlainReportMapper.class)
public class PixelMedValidationService  extends AbstractConfigurableValidationService<EmbeddedValidationServiceConf> implements EmbeddedValidationService {


    public static final String DICOM_SR_CONFORMANCE = "DICOM SR Conformance";

    public PixelMedValidationService(ApplicationPreferenceManager applicationPreferenceManager, EmbeddedValidationServiceConf configuration) {
        super(applicationPreferenceManager, configuration);
    }

    @Override
    public String about() {
        return "Validate SR instances against the standard IOD for the corresponding SOP Class.\n" +
                "Typically used by reading the list of attributes that comprise an object, validating them and displaying the resulting string results to the user.";
    }

    @Override
    public String getName() {
        return configuration.getName();
    }

    @Override
    protected List<String> getValidatorNames(String validatorFilter) {
        return new ArrayList<>(Arrays.asList(DICOM_SR_CONFORMANCE));
    }

    @Override
    public List<String> getSupportedMediaTypes() {
        return Arrays.asList("application/dicom");
    }

    @Override
    public byte[] validate(HandledObject[] objects, String validator) throws ValidationServiceOperationException {
        try {
            DicomSRValidator dval = new DicomSRValidator();
            AttributeList list = new AttributeList();
            list.read(
                    new DicomInputStream(
                            new BufferedInputStream(
                                new ByteArrayInputStream(
                                        objects[0].getContent()
                                )
                            ),
                            (String)null, true
                    ),
                    (AttributeTag)null,
                    (AttributeList.ReadTerminationStrategy)null
            );

            ValidationReport report = new DicomReportBuilder().build(this, validator, this.getServiceVersion(),
                    dval.validate(list)
            );

            return new XMLValidationReportMarshaller(SeverityLevel.INFO)
                    .marshal(report).getBytes(StandardCharsets.UTF_8);
        } catch (Exception e) {
            throw new ValidationServiceOperationException(e);
        }
    }

    @Override
    public String getServiceVersion() {
        return VersionAndConstants.getBuildDate();
    }

}
