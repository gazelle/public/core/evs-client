package net.ihe.gazelle.evsclient.domain.validationservice.configuration;

public class ServiceConfException extends Exception {

   private static final long serialVersionUID = -6950010940002971763L;

   public ServiceConfException() {
   }

   public ServiceConfException(String s) {
      super(s);
   }

   public ServiceConfException(String s, Throwable throwable) {
      super(s, throwable);
   }

   public ServiceConfException(Throwable throwable) {
      super(throwable);
   }
}
