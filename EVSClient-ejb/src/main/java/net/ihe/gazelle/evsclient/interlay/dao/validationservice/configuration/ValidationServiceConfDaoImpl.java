package net.ihe.gazelle.evsclient.interlay.dao.validationservice.configuration;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.evsclient.application.validationservice.configuration.ValidationServiceConfDao;
import net.ihe.gazelle.evsclient.domain.validation.ValidationType;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.ReferencedStandard;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.ReferencedStandardQuery;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.ValidationServiceConf;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.ValidationServiceConfQuery;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.reports.ValidationReportType;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.reports.ValidationReportTypeQuery;
import net.ihe.gazelle.evsclient.interlay.adapters.validationservice.AbstractConfigurableValidationService;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;

import javax.faces.model.DataModel;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.List;

public class ValidationServiceConfDaoImpl implements ValidationServiceConfDao {
    
    private final EntityManagerFactory entityManagerFactory;

    protected Filter<ReferencedStandard> referenceStandardFilter;

    public ValidationServiceConfDaoImpl(EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
    }

    /**
     * @param inKeyword : keyword string that identifies the validation service to use
     */
    public ValidationServiceConf getValidationServiceByKeyword(final String inKeyword) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        ValidationServiceConfQuery query = new ValidationServiceConfQuery(entityManager);
        query.name().eq(inKeyword);
        return query.getUniqueResult();
    }

    @Override
    public ValidationServiceConf merge(ValidationServiceConf selectedValidationService) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        selectedValidationService = entityManager.merge(selectedValidationService);
        entityManager.flush();
        AbstractConfigurableValidationService.clearValidatorsCache(); // reset validators cache because service configuration has changed.
        return selectedValidationService;
    }

    /**
     * @param standard : Standard for which to return the list of validation services
     * @return : Returns the list of available validation services for the parameter inStandard
     */
    public List<ValidationServiceConf> getListOfAvailableValidationServicesForStandard(final ReferencedStandard standard) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        ValidationServiceConfQuery query = new ValidationServiceConfQuery(entityManager);
        query.name().eq(standard.getName());
        query.available().eq(Boolean.TRUE);
        return query.getList();
    }


    public List<ReferencedStandard> getAllReferencedStandard() {
        return getReferencedStandardFiltered(null, null, null);
    }

    public DataModel<ReferencedStandard> getAllReferencedStandardDataModel() {
        return new FilterDataModel<ReferencedStandard>(getReferenceStandardFilter()) {
            private static final long serialVersionUID = -653719939501439370L;

            @Override
            protected Object getId(final ReferencedStandard t) {
                return t.getId();
            }
        };
    }

    private Filter<ReferencedStandard> getReferenceStandardFilter() {
        if (referenceStandardFilter == null) {
            final ReferencedStandardQuery query = new ReferencedStandardQuery();
            final HQLCriterionsForFilter<ReferencedStandard> result = query.getHQLCriterionsForFilter();
            referenceStandardFilter = new Filter<>(result);
        }
        return referenceStandardFilter;
    }

    public List<ReferencedStandard> getReferencedStandardFiltered(final String name, final ValidationType validationType, final String validatorFilter) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        final ReferencedStandardQuery query = new ReferencedStandardQuery(entityManager);
        if (name != null && !name.isEmpty()) {
            query.name().eq(name);
        }
        if (validatorFilter != null && !validatorFilter.isEmpty()) {
            query.validatorFilter().eq(validatorFilter);
        }
        if (validationType != null) {
            query.validationType().eq(validationType);
        }
        query.validatorFilter().order(true);
        return query.getList();
    }

    @Override
    public ReferencedStandard merge(ReferencedStandard selectedReferencedStandard) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        selectedReferencedStandard = entityManager.merge(selectedReferencedStandard);
        entityManager.flush();
        return selectedReferencedStandard;
    }

    @Override
    public ReferencedStandard getReferencedStandardById(Integer id) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        final ReferencedStandardQuery query = new ReferencedStandardQuery(entityManager);
        query.id().eq(id);
        return query.getUniqueResult();
    }

    @Override
    public ValidationServiceConf getValidationServiceById(Integer id) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        ValidationServiceConfQuery query = new ValidationServiceConfQuery(entityManager);
        query.id().eq(id);
        return query.getUniqueResult();
    }

    @Override
    public ValidationReportType getValidationReportTypeById(Integer id) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        ValidationReportTypeQuery query = new ValidationReportTypeQuery(entityManager);
        query.id().eq(id);
        return query.getUniqueResult();
    }

    @Override
    public List<ValidationServiceConf> getAllValidationService() {
        return getValidationServiceFiltered(null, null, null);
    }

    @Override
    public List<ValidationServiceConf> getValidationServiceFiltered(final String name, final ValidationType validationType, final Boolean available) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        final ValidationServiceConfQuery query = new ValidationServiceConfQuery(entityManager);
        if (name != null && !name.isEmpty()) {
            query.name().eq(name);
        }
        if (validationType != null) {
            query.validationType().eq(validationType);
        }
        if (available != null) {
            query.available().eq(available);
        }
        query.name().order(true);
        return query.getList();
    }

    @Override
    public List<ReferencedStandard> getReferencedStandardsByValidationServiceId(int validationServiceId) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        final ReferencedStandardQuery query = new ReferencedStandardQuery(entityManager);
        query.configurations().id().eq(validationServiceId);
        return query.getList();
    }

}
