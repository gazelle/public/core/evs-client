package net.ihe.gazelle.evsclient.domain.statistics;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "evsc_ip_loc")
@SequenceGenerator(name = "ip_loc_sequence", sequenceName = "evsc_ip_loc_id_seq", allocationSize = 1)
public class IpLoc implements Serializable {
    @Id
    @GeneratedValue(generator = "ip_loc_sequence", strategy = GenerationType.SEQUENCE)
    @NotNull
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    @NotNull
    private String ip;

    @NotNull
    private String country;

    @NotNull
    @Column(name = "last_check", nullable = false)
    private Date lastCheck;

    public IpLoc() {
    }

    public IpLoc(String ip, String country,Date lastCheck) {
        this.ip = ip;
        this.country = country;
        this.lastCheck = lastCheck;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getLastCheck() {
        return lastCheck;
    }

    public void setLastCheck(Date lastCheck) {
        this.lastCheck = lastCheck;
    }
}
