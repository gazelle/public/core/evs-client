package net.ihe.gazelle.evsclient.interlay.gui;

public class QueryParamEvs extends QueryParam {

    // menuConfiguration.xhtml
    public static final String REF_STANDARD_ID = "standard";
    // validationServices.xhtml
    public static final String VAL_SERVICE_ID = "valId";
    public static final String STYLESHEET_ID = "stylesheetId";
    public static final String VALIDATOR_TYPE = "type";
    public static final String FILE_ID = "fileId";
    public static final String EXTENSION = "extension";
    public static final String STANDARD = "standard";
}
