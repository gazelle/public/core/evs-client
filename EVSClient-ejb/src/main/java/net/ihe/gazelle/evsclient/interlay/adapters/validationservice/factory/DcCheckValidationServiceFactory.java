package net.ihe.gazelle.evsclient.interlay.adapters.validationservice.factory;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.application.validationservice.AbstractValidationServiceFactory;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.SystemValidationServiceConf;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.WebValidationServiceConf;
import net.ihe.gazelle.evsclient.interlay.adapters.validationservice.DcCheckValidationService;

public class DcCheckValidationServiceFactory extends AbstractValidationServiceFactory<DcCheckValidationService, WebValidationServiceConf> {

    public DcCheckValidationServiceFactory(ApplicationPreferenceManager applicationPreferenceManager, WebValidationServiceConf configuration) {
        super(applicationPreferenceManager, configuration, DcCheckValidationService.class);
    }
}
