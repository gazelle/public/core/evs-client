/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evsclient.interlay.gui.menu;

import net.ihe.gazelle.common.pages.Authorization;
import net.ihe.gazelle.common.pages.Page;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.ReferencedStandard;
import net.ihe.gazelle.evsclient.interlay.gui.Authorizations;
import net.ihe.gazelle.evsclient.interlay.gui.validation.GuiComponentType;

public class PageStatistics implements Page {

    private String url;

    private final ReferencedStandard standard;

    public PageStatistics(final ReferencedStandard standard) {
        this.standard = standard;
    }

    @Override
    public String getId() {
        return this.url;
    }

    @Override
    public String getLabel() {
        return "gazelle.evs.client.statistics";
    }

    @Override
    public String getLink() {
        if (this.url == null && standard != null) {
            final StringBuilder builder = new StringBuilder(GuiComponentType.getStatViewByValidationType(standard.getValidationType()));
            EVSMenu.appendUrlParameters(builder, String.valueOf(standard.getId()));
            this.url = builder.toString();
        }
        return this.url;
    }

    @Override
    public String getIcon() {
        return "fa fa-bar-chart";
    }

    @Override
    public Authorization[] getAuthorizations() {
        return new Authorization[] { Authorizations.IS_LOGGED_IF_NEEDED };
    }

    @Override
    public String getMenuLink() {
        return this.getLink().replace(".xhtml", ".seam");
    }

}
