package net.ihe.gazelle.evsclient.interlay.dto.rest.validation.request.xml;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "validationService")
@XmlAccessorType(XmlAccessType.FIELD)
public class ValidationServiceDto {

    @XmlAttribute(name = "name")
    private String name;

    @XmlAttribute(name = "validator")
    private String validator;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValidator() {
        return validator;
    }

    public void setValidator(String validator) {
        this.validator = validator;
    }

    @Override
    public String toString() {
        return "ValidationServiceDto{" +
                "name='" + name + '\'' +
                ", validator='" + validator + '\'' +
                '}';
    }
}
