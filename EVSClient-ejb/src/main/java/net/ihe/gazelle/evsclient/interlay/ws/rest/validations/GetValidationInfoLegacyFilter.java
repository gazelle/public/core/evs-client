package net.ihe.gazelle.evsclient.interlay.ws.rest.validations;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import org.apache.commons.io.IOUtils;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Install;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.intercept.BypassInterceptors;
import org.jboss.seam.annotations.web.Filter;
import org.jboss.seam.mock.EnhancedMockHttpServletResponse;
import org.jboss.seam.web.AbstractFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Name("getValidationInfoLegacyFilter")
@Scope(ScopeType.APPLICATION)
@Install
@BypassInterceptors
@Filter
public class GetValidationInfoLegacyFilter extends AbstractFilter {

   private static final Logger LOG = LoggerFactory.getLogger(GetValidationInfoLegacyFilter.class);
   private static final List<String> filteredPaths = Arrays.asList(
         "/GetValidationStatus",
         "/GetValidationDate",
         "/GetValidationPermanentLink",
         "/GetLastResultStatusByExternalId",
         "/GetValidationPermanentLinkByExternalId"
   );

   private ApplicationPreferenceManager applicationPreferenceManager;

   public GetValidationInfoLegacyFilter() {
      applicationPreferenceManager = (ApplicationPreferenceManager) Component.getInstance(
            "applicationPreferenceManager");
   }

   public GetValidationInfoLegacyFilter(ApplicationPreferenceManager applicationPreferenceManager) {
      this.applicationPreferenceManager = applicationPreferenceManager;
   }

   @Override
   public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
         throws IOException, ServletException {
      final HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;

      if (isFilterEnabled() && isOneOfFilteredPath(httpServletRequest.getRequestURI())) {
         filterValidationInfoResponse(httpServletRequest, (HttpServletResponse) servletResponse, filterChain);
      } else {
         filterChain.doFilter(servletRequest, servletResponse);
      }
   }

   protected boolean isFilterEnabled() {
      return applicationPreferenceManager.getBooleanValue("legacy_results_redirection_enabled");
   }

   private boolean isOneOfFilteredPath(String requestURI) {
      return filteredPaths.contains(requestURI.substring(requestURI.lastIndexOf('/')));
   }

   private void filterValidationInfoResponse(HttpServletRequest httpServletRequest, HttpServletResponse servletResponse,
                                             FilterChain filterChain)
         throws IOException, ServletException {
      EnhancedMockHttpServletResponse bufferedServletResponse = new EnhancedMockHttpServletResponse();

      filterChain.doFilter(httpServletRequest, bufferedServletResponse);

      if (bufferedServletResponse.getStatus() == HttpServletResponse.SC_NO_CONTENT) {
         try {
            URL legacyResultQueryURL = buildLegacyResultQuery(
                  getLegacyResultsEvsUrl(),
                  httpServletRequest.getRequestURI(),
                  httpServletRequest.getParameterMap());

            HttpResponse legacyResultResponse = performHttpGet(legacyResultQueryURL);
            if (HttpServletResponse.SC_OK == legacyResultResponse.getStatus()) {
               flushLegacyResultResponse(legacyResultResponse, servletResponse);
               return;
            }
         } catch (IOException e) {
            LOG.error("Unable to access legacy results.", e);
         }
      }
      flushBufferedResponse(bufferedServletResponse, servletResponse);
   }

   protected HttpResponse performHttpGet(URL archivedEvsQueryURL) throws IOException {
      HttpURLConnection httpConnection = (HttpURLConnection) archivedEvsQueryURL.openConnection();
      httpConnection.setUseCaches(true);
      httpConnection.setRequestMethod("GET");
      try (InputStream connectionIS = httpConnection.getInputStream()) {
         return new HttpResponse(
               httpConnection.getResponseCode(),
               IOUtils.toString(connectionIS, StandardCharsets.UTF_8.name()));
      }
   }

   private void flushLegacyResultResponse(HttpResponse httpResponse, HttpServletResponse servletResponse)
         throws IOException {
      servletResponse.setStatus(httpResponse.getStatus());
      if (httpResponse.getResponse() != null) {
         try (OutputStream servletStream = servletResponse.getOutputStream()) {
            servletStream.write(httpResponse.getResponse().getBytes(StandardCharsets.UTF_8));
            servletStream.flush();
         }
      }
   }

   private URL buildLegacyResultQuery(String legacyResultsEvsUrl, String initialRequestURI,
                                      Map<String, Object> initialParameters) throws MalformedURLException {
      if (legacyResultsEvsUrl != null) {
         StringBuilder ressourcePathBuilder = new StringBuilder(
               initialRequestURI.substring(initialRequestURI.lastIndexOf("rest")));
         if (!initialParameters.isEmpty()) {
            ressourcePathBuilder.append("?");
            Iterator<Map.Entry<String, Object>> paramIterator = initialParameters.entrySet().iterator();
            while (paramIterator.hasNext()) {
               Map.Entry<String, Object> param = paramIterator.next();
               ressourcePathBuilder.append(param.getKey()).append("=").append(getParamValue(param.getValue()));
               if (paramIterator.hasNext()) {
                  ressourcePathBuilder.append("&");
               }
            }
         }
         String ressourcePath = ressourcePathBuilder.toString();
         URL legacyResultsQueryUrl = new URL(legacyResultsEvsUrl + ressourcePath);
         LOG.warn("No content found for {}, will redirect query to {}", ressourcePath, legacyResultsQueryUrl);

         return legacyResultsQueryUrl;
      } else {
         throw new MalformedURLException("legacy_results_evs_url is not defined");
      }
   }

   private String getParamValue(Object param) {
      // At runtime we can alternativly get a String or a String[] as value... I don't understand why... So this bit
      // of code:
      if (param instanceof String) {
         return (String) param;
      } else {
         String[] paramA = (String[]) param;
         return paramA != null && paramA.length > 0 ? paramA[0] : null;
      }
   }

   protected String getLegacyResultsEvsUrl() {
      return applicationPreferenceManager.getStringValue("legacy_results_evs_url");
   }

   private void flushBufferedResponse(EnhancedMockHttpServletResponse servletResponseBuffer,
                                      HttpServletResponse servletResponse) throws IOException {
      servletResponse.setStatus(servletResponseBuffer.getStatus());
      servletResponse.setCharacterEncoding(servletResponseBuffer.getCharacterEncoding());
      servletResponse.setContentType(servletResponseBuffer.getContentType());
      for (String headerName : (Set<String>) servletResponseBuffer.getHeaderNames()) {
         servletResponse.setHeader(headerName, servletResponseBuffer.getHeader(headerName));
      }
      try (OutputStream servletStream = servletResponse.getOutputStream()) {
         servletStream.write(servletResponseBuffer.getContentAsByteArray());
         servletStream.flush();
      }
   }


   static final class HttpResponse {

      private int status;
      private String response;

      public HttpResponse(int status, String response) {
         this.status = status;
         this.response = response;
      }

      public int getStatus() {
         return status;
      }

      public String getResponse() {
         return response;
      }
   }
}
