package net.ihe.gazelle.evsclient.interlay.dto.rest.analysis;

import io.swagger.v3.oas.annotations.media.Schema;
import net.ihe.gazelle.evsclient.interlay.dto.rest.validation.HandledObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Schema(description="**Analysis** minimale à envoyer à EVSClient pour créer une ressource et détecter son contenu.")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaResteasyServerCodegen", date = "2021-08-27T13:39:11.206Z")
public class AnalysisForCreation extends Analysis  {

  public void setAutoValidation(Boolean autoValidation) {
    this.autoValidation = autoValidation;
  }

  public void setObjects(List<HandledObject> objects) {
    List<net.ihe.gazelle.evsclient.domain.processing.HandledObject> domains = new ArrayList<>();
    if (objects!=null) {
      for (HandledObject object:objects) {
        domains.add(object.toDomain());
      }
    }
    domain.setObjects(domains);
  }

  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AnalysisForCreation analysisForCreation = (AnalysisForCreation) o;
    return true;
  }

  @Override
  public int hashCode() {
    return Objects.hash();
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AnalysisForCreation {\n");
    sb.append("    ").append(toIndentedString(super.toString())).append("\n");
    sb.append("}");
    return sb.toString();
  }

}

