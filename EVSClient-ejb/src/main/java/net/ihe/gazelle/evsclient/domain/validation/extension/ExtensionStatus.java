package net.ihe.gazelle.evsclient.domain.validation.extension;

/**
 * <b>Class Description : </b>ScorecardStatus<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 08/12/16
 * @class ScorecardStatus
 * @package net.ihe.gazelle.evs.client.legacy.xml.model
 * @see anne-gaelle.berge@ihe-europe.net - http://gazelle.ihe.net
 */
public enum ExtensionStatus {
    NEVER_COMPUTED, // 0 - Could be obtained but never computed
    FAILED_ON_SERVER_SIDE, // 1 - CDAGenerator did not complete the scorecard
    AVAILABLE, // 2 - scorecard computed and stored on disc
    NOT_AVAILABLE, // 3 - used when only schematron validation has been performed
    COMPUTE_AGAIN, IN_PROGRESS, FAILED_ON_CLIENT_SIDE
}
