package net.ihe.gazelle.evsclient.domain.validation.report;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Constraint Validation Class
 *
 * @author fde
 * @version $Id : $Id
 */
public class ConstraintValidation implements Serializable {
   private static final long serialVersionUID = 4975773838269682264L;
   
   private String constraintID;
   private String constraintType;
   private String constraintDescription;
   private String formalExpression;
   private String locationInValidatedObject;
   private String valueInValidatedObject;
   private List<String> assertionIDs = new ArrayList<>();
   private final List<UnexpectedError> unexpectedErrors = new ArrayList<>();
   private ValidationTestResult testResult;
   private SeverityLevel severity;
   private ConstraintPriority priority;

   /**
    * Constructor with mandatory parameters
    *
    * @param constraintDescription a {@link String} object, mandatory parameter can not be null
    * @param priority              a {@link ConstraintPriority} object, mandatory parameter can not be null
    * @param testResult            a {@link ValidationTestResult} object, mandatory parameter can not be null
    *
    * @throws IllegalArgumentException if one of mandatory parameters is null
    */
   public ConstraintValidation(String constraintDescription, ConstraintPriority priority,
                               ValidationTestResult testResult) {
      setConstraintDescription(constraintDescription);
      setPriority(priority);
      setTestResult(testResult);
   }

   /**
    * Constructor with all parameters
    *
    * @param constraintID              a {@link String} object, Recommended parameter
    * @param constraintType            a {@link String} object, Recommended parameter
    * @param constraintDescription     a {@link String} object, mandatory parameter can not be null
    * @param locationInValidatedObject a {@link String} object, Recommended parameter
    * @param valueInValidatedObject    a {@link String} object,Recommended parameter
    * @param assertionIDs              a {@link String} object, Recommended parameter
    * @param priority                  a {@link ConstraintPriority} object, mandatory parameter can not be null
    * @param testResult                a {@link ValidationTestResult} object, mandatory parameter can not be null
    *
    * @throws IllegalArgumentException if one of mandatory parameters is null
    */
   public ConstraintValidation(String constraintID, String constraintType, String constraintDescription,
                               String formalExpression, String locationInValidatedObject, String valueInValidatedObject,
                               List<String> assertionIDs, List<UnexpectedError> unexpectedErrors,
                               ConstraintPriority priority, ValidationTestResult testResult) {

      setConstraintID(constraintID);
      setConstraintType(constraintType);
      setConstraintDescription(constraintDescription);
      setFormalExpression(formalExpression);
      setLocationInValidatedObject(locationInValidatedObject);
      setValueInValidatedObject(valueInValidatedObject);
      setAssertionIDs(assertionIDs);
      setPriority(priority);
      setUnexpectedErrorsForConstructor(unexpectedErrors);
      setTestResult(testResult);
   }
   /**
    * copy Constructor
    *
    * @param constraint {@link ConstraintValidation}
    */
   public ConstraintValidation(ConstraintValidation constraint) {
      setConstraintID(constraint.getConstraintID());
      setConstraintType(constraint.getConstraintType());
      setConstraintDescription(constraint.getConstraintDescription());
      setFormalExpression(constraint.getFormalExpression());
      setLocationInValidatedObject(constraint.getLocationInValidatedObject());
      setValueInValidatedObject(constraint.getValueInValidatedObject());
      setAssertionIDs(constraint.getAssertionIDs());
      setPriority(constraint.getPriority());
      addUnexpectedErrors(constraint.getUnexpectedErrors());
      setTestResult(constraint.getTestResult());
   }

   /**
    * <p>Getter of the field <code>constraintID</code></p>
    *
    * @return a {@link String} object
    */
   public String getConstraintID() {
      return constraintID;
   }

   /**
    * <p>Setter for the field <code>constraintID</code></p>
    *
    * @param constraintID a {@link String} object, can  be null
    */
   public void setConstraintID(String constraintID) {
      this.constraintID = constraintID;
   }

   /**
    * <p>Getter of the field <code>constraintType</code></p>
    *
    * @return a {@link String} object
    */
   public String getConstraintType() {
      return constraintType;
   }

   /**
    * <p>Setter for the field <code>constraintType</code></p>
    *
    * @param constraintType a {@link String} object, can  be null
    */
   public void setConstraintType(String constraintType) {
      this.constraintType = constraintType;
   }

   /**
    * <p>Getter of the field <code>constraintDescription</code></p>
    *
    * @return a {@link String} object
    */
   public String getConstraintDescription() {
      return constraintDescription;
   }

   /**
    * <p>Setter for the field <code>constraintDescription</code></p>
    *
    * @param constraintDescription a {@link String} object, can not be null
    *
    * @throws IllegalArgumentException if constraintDescription is null
    */
   public void setConstraintDescription(String constraintDescription) {
      if (constraintDescription == null) {
         throw new IllegalArgumentException("constraintDescription can not be null");
      }
      this.constraintDescription = constraintDescription;
   }

   /**
    * Get the formal expression of the constraint (in mathematical or a programmatical language). This is for report
    * only, it should not be used to be interpreted by another processor.
    *
    * @return a String of the formal expression.
    */
   public String getFormalExpression() {
      return formalExpression;
   }

   /**
    * Set the formal expression of the constraint (in mathematical or a programmatical language). This is for report
    * only, it should not be used to be interpreted by another processor.
    *
    * @param formalExpression formal expression of the constraint.
    */
   public void setFormalExpression(String formalExpression) {
      this.formalExpression = formalExpression;
   }

   /**
    * <p>Getter of the field <code>locationInValidatedObject</code></p>
    *
    * @return a {@link String} object
    */
   public String getLocationInValidatedObject() {
      return locationInValidatedObject;
   }

   /**
    * <p>Setter for the field <code>locationInValidatedObject</code></p>
    *
    * @param locationInValidatedObject a {@link String} object, can  be null
    *
    * @throws IllegalArgumentException if locationInValidatedObject is null in failed case
    */
   public void setLocationInValidatedObject(String locationInValidatedObject) {
      this.locationInValidatedObject = locationInValidatedObject;
   }

   /**
    * <p>Getter of the field <code>valueInValidatedObject</code></p>
    *
    * @return a {@link String} object
    */
   public String getValueInValidatedObject() {
      return valueInValidatedObject;
   }

   /**
    * <p>Setter for the field <code>valueInValidatedObject</code></p>
    *
    * @param valueInValidatedObject a {@link String} object, can  be null
    *
    * @throws IllegalArgumentException if valueInValidatedObject is null in failed case
    */
   public void setValueInValidatedObject(String valueInValidatedObject) {
      this.valueInValidatedObject = valueInValidatedObject;
   }

   /**
    * <p>Getter of the field <code>assertionIDs</code></p>
    *
    * @return a {@link List} object
    */
   public List<String> getAssertionIDs() {
      return new ArrayList<>(assertionIDs);
   }

   /**
    * <p>Setter of the field <code>assertionIDs</code></p>
    * if parameter is null the list is initialized
    *
    * @param assertionIDs a {@link List} object, can be null
    */
   public void setAssertionIDs(List<String> assertionIDs) {
      this.assertionIDs = (assertionIDs != null) ? new ArrayList<String>(assertionIDs) : new ArrayList<String>();
   }

   /**
    * <p>Getter of the field <code>testResult</code></p>
    *
    * @return a {@link ValidationTestResult} object
    */
   public ValidationTestResult getTestResult() {
      return testResult;
   }


   /**
    * <p>Setter for the field <code>testResult</code></p>
    * If the test result is PASSED, severity level shall be set to INFO If the test result is FAILED, severity level
    * shall be set following specs
    *
    * @param testResult a {@link ValidationTestResult} object, can not be null
    *
    * @throws IllegalArgumentException if parameter is null
    */
   public void setTestResult(ValidationTestResult testResult) {
      if (testResult == null) {
         throw new IllegalArgumentException("testResult can not be null");
      }
      this.testResult = testResult;
      if (ValidationTestResult.PASSED.equals(testResult)) {
         severity = SeverityLevel.INFO;
      } else {
         switch (priority) {
            case MANDATORY:
               severity = SeverityLevel.ERROR;
               break;
            case RECOMMENDED:
               severity = SeverityLevel.WARNING;
               break;
            default:
               severity = SeverityLevel.INFO;
               break;
         }
      }
   }


   /**
    * <p>Getter of the field <code>severity</code></p>
    *
    * @return a {@link SeverityLevel} object
    */
   public SeverityLevel getSeverity() {
      return severity;
   }


   /**
    * <p>Getter of the field <code>priority</code></p>
    *
    * @return a {@link ConstraintPriority} object
    */
   public ConstraintPriority getPriority() {
      return priority;
   }

   /**
    * <p>Setter for the field <code>priority</code></p>
    *
    * @param priority a {@link ConstraintPriority} object, can not be null
    *
    * @throws IllegalArgumentException if priority is null
    */
   private void setPriority(ConstraintPriority priority) {
      if (priority == null) {
         throw new IllegalArgumentException("priority can not be null");
      }
      this.priority = priority;
   }

   /**
    * <p>Getter of the field <code>unexpectedErrors</code></p>
    *
    * @return a list of {@link UnexpectedError} or an empty list.
    */
   public List<UnexpectedError> getUnexpectedErrors() {
      return new ArrayList<>(unexpectedErrors);
   }

   /**
    * <p>add a list of {@link UnexpectedError} to ValidationConstraint</p>
    *
    * @param unexpectedErrors A list of UnexpectedError to add
    *
    * @throws IllegalArgumentException if unexpectedErrors is null.
    */
   public void addUnexpectedErrors(List<UnexpectedError> unexpectedErrors) {
      if ((unexpectedErrors != null)) {
         for (UnexpectedError unexpectedError : unexpectedErrors) {
            addUnexpectedError(unexpectedError);
         }
      } else {
         throw new IllegalArgumentException("Exception list to add cannot be null");
      }
   }

   /**
    * Add an UnexpectedError to a ConstraintValidation
    *
    * @param unexpectedError {@link UnexpectedError} to add to the constraint validation.
    *
    * @throws IllegalArgumentException if unexpectedError is null
    */
   public void addUnexpectedError(UnexpectedError unexpectedError) {
      if (unexpectedError != null) {
         this.unexpectedErrors.add(unexpectedError);
         setTestResult(ValidationTestResult.UNDEFINED);
      } else {
         throw new IllegalArgumentException("cannot add a null ExpectedError to the constraint in the validation " +
               "report");
      }
   }

   /**
    * Add a Throwable as {@link UnexpectedError} to the ConstraintValidation.
    *
    * @param throwable, error to add.
    *
    * @throws IllegalArgumentException if the throwable is null.
    */
   public void addUnexpectedError(Throwable throwable) {
      addUnexpectedError(new UnexpectedError(throwable));
   }

   private void setUnexpectedErrorsForConstructor(List<UnexpectedError> unexpectedErrors) {
      if(unexpectedErrors != null) {
         addUnexpectedErrors(unexpectedErrors);
      }
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public boolean equals(Object o) {
      if (this == o) {
         return true;
      }
      if (o == null || getClass() != o.getClass()) {
         return false;
      }

      ConstraintValidation that = (ConstraintValidation) o;

      if (constraintID != null ? !constraintID.equals(that.constraintID) : that.constraintID != null) {
         return false;
      }
      if (constraintType != null ? !constraintType.equals(that.constraintType) : that.constraintType != null) {
         return false;
      }
      if (!constraintDescription.equals(that.constraintDescription)) {
         return false;
      }
      if (locationInValidatedObject != null ? !locationInValidatedObject
            .equals(that.locationInValidatedObject) : that.locationInValidatedObject != null) {
         return false;
      }
      if (valueInValidatedObject != null ? !valueInValidatedObject.equals(
            that.valueInValidatedObject) : that.valueInValidatedObject != null) {
         return false;
      }
      if (!assertionIDs.equals(that.assertionIDs)) {
         return false;
      }
      if (!unexpectedErrors.equals(that.unexpectedErrors)) {
         return false;
      }
      if (testResult != that.testResult) {
         return false;
      }
      if (severity != that.severity) {
         return false;
      }
      return priority == that.priority;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public int hashCode() {
      int result = constraintID != null ? constraintID.hashCode() : 0;
      result = 31 * result + (constraintType != null ? constraintType.hashCode() : 0);
      result = 31 * result + constraintDescription.hashCode();
      result = 31 * result + (locationInValidatedObject != null ? locationInValidatedObject.hashCode() : 0);
      result = 31 * result + (valueInValidatedObject != null ? valueInValidatedObject.hashCode() : 0);
      result = 31 * result + assertionIDs.hashCode();
      result = 31 * result + unexpectedErrors.hashCode();
      result = 31 * result + testResult.hashCode();
      result = 31 * result + severity.hashCode();
      result = 31 * result + priority.hashCode();
      return result;
   }
}