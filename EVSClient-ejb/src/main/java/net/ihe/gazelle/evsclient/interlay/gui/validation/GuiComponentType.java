package net.ihe.gazelle.evsclient.interlay.gui.validation;

import net.ihe.gazelle.evsclient.domain.validation.ValidationType;

public enum GuiComponentType {

    ATNA(ValidationType.ATNA, "Audit messages", "/atna/validator.seam", "/atna/allLogs.seam", "/common/statisticsByStandard.seam")
    ,AUDIT_MESSAGE(ValidationType.AUDIT_MESSAGE, "Audit messages", "/atna/validator.seam", "/atna/allLogs.seam", "/common/statisticsByStandard.seam")
    ,CDA(ValidationType.CDA, "CDA", "/cda/validator.seam", "/cda/allLogs.seam", "/common/statisticsByStandard.seam")
    ,DICOM_WEB(ValidationType.DICOM_WEB, "DICOMWeb", "/dicom_web/validator.seam", "/dicom_web/allLogs.seam", "/common/statisticsByStandard.seam")
    ,DICOM(ValidationType.DICOM, "DICOM", "/dicom/validator.seam", "/dicom/allLogs.seam", "/common/statisticsByStandard.seam")
    ,DSUB(ValidationType.DSUB, "DSUB", "/dsub/validator.seam", "/dsub/allLogs.seam", "/common/statisticsByStandard.seam")
    ,FHIR(ValidationType.FHIR, "FHIR", "/fhir/validator.seam", "/fhir/allLogs.seam", "/common/statisticsByStandard.seam")
    ,HL7V2(ValidationType.HL7V2, "HL7v2", "/hl7v2/validator.seam", "/hl7v2/allLogs.seam", "/common/statisticsByStandard.seam")
    ,HL7V3(ValidationType.HL7V3, "HL7v3", "/hl7v3/validator.seam", "/hl7v3/allLogs.seam", "/common/statisticsByStandard.seam")
    ,HPD(ValidationType.HPD, "Healthcare Provider Directory", "/hpd/validator.seam", "/hpd/allLogs.seam", "/common/statisticsByStandard.seam")
    ,PDF(ValidationType.PDF, "PDF", "/pdf/validator.seam", "/pdf/allLogs.seam", "/common/statisticsByStandard.seam")
    ,SAML(ValidationType.SAML, "Assertions", "/saml/validator.seam", "/saml/allLogs.seam", "/common/statisticsByStandard.seam")
    ,SVS(ValidationType.SVS, "Sharing Value Set", "/svs/validator.seam", "/svs/allLogs.seam", "/common/statisticsByStandard.seam")
    ,TLS(ValidationType.TLS, "Certificates", "/tls/validator.seam", "/tls/allLogs.seam", "/common/statisticsByStandard.seam")
    ,WS_TRUST(ValidationType.WS_TRUST, "WSTrust 1.3", "/wstrust/validator.seam", "/wstrust/allLogs.seam", "/common/statisticsByStandard.seam")
    ,XDS(ValidationType.XDS, "XD* metadata", "/xds/validator.seam", "/xds/allLogs.seam", "/common/statisticsByStandard.seam")
    ,XDW(ValidationType.XDW, "XDW", "/xdw/validator.seam", "/xdw/allLogs.seam", "/common/statisticsByStandard.seam")
    ,XML(ValidationType.XML, "XML", "/xml/validator.seam", "/xml/allLogs.seam", "/common/statisticsByStandard.seam")
    ,XVAL(ValidationType.XVAL, "XVAL", "/xval/validator.seam", "/xval/allLogs.seam", "/common/statisticsByStandard.seam")
    ,DEFAULT(ValidationType.DEFAULT, "DEFAULT", "/default/validator.seam", "/default/allLogs.seam", "/common/statisticsByStandard.seam")
    ;

    private String label;
    private String validatorView;
    private String logView;
    private String statView;
    private ValidationType validationType;

    GuiComponentType(ValidationType validationType, String label, String validatorView, String logView, String statView) {
        this.label = label;
        this.validatorView = validatorView;
        this.logView = logView;
        this.validationType = validationType;
        this.statView = statView;
    }

    public String getLabel() {
        return label;
    }

    public String getValidatorView() {
        return validatorView;
    }

    public String getLogView() {
        return logView;
    }

    public String getStatView() {
        return statView;
    }

    public ValidationType getValidationType() {
        return validationType;
    }

    public static String getValidatorViewByValidationType(ValidationType validationType) {
        return getGuiComponentTypeByValidationType(validationType).getValidatorView();
    }

    public static String getLogViewByValidationType(ValidationType validationType) {
        return getGuiComponentTypeByValidationType(validationType).getLogView();
    }

    public static String getStatViewByValidationType(ValidationType validationType) {
        return getGuiComponentTypeByValidationType(validationType).getStatView();
    }

    public static GuiComponentType getGuiComponentTypeByValidationType(ValidationType validationType){
        for (GuiComponentType componentType: GuiComponentType.values()) {
            if(componentType.getValidationType().equals(validationType)){
                return componentType;
            }
        }
        throw new EnumConstantNotPresentException(GuiComponentType.class,"No GuiComponentType found for the validator type : " + validationType);
    }
}
