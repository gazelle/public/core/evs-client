/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.ihe.gazelle.evsclient.domain.validation;

import net.ihe.gazelle.evsclient.domain.processing.AbstractProcessing;
import net.ihe.gazelle.evsclient.domain.processing.EVSCallerMetadata;
import net.ihe.gazelle.evsclient.domain.processing.HandledObject;
import net.ihe.gazelle.evsclient.domain.processing.OwnerMetadata;
import net.ihe.gazelle.evsclient.domain.validation.report.ValidationReport;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "evsc_validation")
public class Validation extends AbstractProcessing implements Serializable {

    private static final long serialVersionUID = -7482942984080329263L;

    @Embedded
    private ValidationServiceRef validationService;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private ValidationStatus validationStatus = ValidationStatus.PENDING;

    @OneToMany(cascade = CascadeType.ALL)
    private List<ReferencedStandardRef> referencedStandards = new ArrayList<>();

    @Enumerated(EnumType.STRING)
    @Column(name = "validation_type")
    private ValidationType validationType;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "original_report_id")
    private Report<byte[]> originalReport;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "validation_report_id")
    private Report<ValidationReport> validationReport;

    @Column(name = "lucky_validation")
    private boolean lucky;

    public Validation() {
    }

    public Validation(List<HandledObject> objects, EVSCallerMetadata caller) {
        this(objects, caller, null);
    }

    public Validation(List<HandledObject> objects, EVSCallerMetadata caller, OwnerMetadata ownerMetadata) {
        super(objects, caller, ownerMetadata);
    }

    public List<ReferencedStandardRef> getReferencedStandards() {
        return referencedStandards;
    }

    public void addReferencedStandardRef(ReferencedStandardRef referencedStandardRef) {
        this.referencedStandards.add(referencedStandardRef);
    }

    public void setReferencedStandards(List<ReferencedStandardRef> referencedStandards) {
        this.referencedStandards = referencedStandards;
    }

    public boolean isLucky() {
        return lucky;
    }

    public void setLucky(boolean lucky) {
        this.lucky = lucky;
    }

    public ValidationServiceRef getValidationService() {
        return validationService;
    }

    public void setValidationService(ValidationServiceRef validationService) {
        this.validationService = validationService;
    }

    public Report<byte[]> getOriginalReport() {
        return originalReport;
    }

    public void setOriginalReport(Report<byte[]> originalReport) {
        this.originalReport = originalReport;
    }

    public Report<ValidationReport> getValidationReport() {
        return validationReport;
    }

    public void setValidationReport(Report<ValidationReport> validationReport) {
        this.validationReport = validationReport;
    }

    public ValidationType getValidationType() {
        return validationType;
    }

    public void setValidationType(ValidationType validationType) {
        this.validationType = validationType;
    }

    public ValidationStatus getValidationStatus() {
        return validationStatus;
    }

    public void setValidationStatus(ValidationStatus validationStatus) {
        this.validationStatus = validationStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Validation that = (Validation) o;
        return lucky == that.lucky && Objects.equals(referencedStandards, that.referencedStandards) && Objects.equals(validationService, that.validationService) && Objects.equals(validationReport, that.validationReport) && validationStatus == that.validationStatus && validationType == that.validationType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(referencedStandards, lucky, validationService, validationReport, validationStatus, validationType);
    }

    public ValidationRef toValidationRef() {
        return new ValidationRef(this.getOid(),this.getValidationStatus()==null?null:this.getValidationStatus().name());
    }
}