/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evsclient.application.security;

import net.ihe.gazelle.evsclient.domain.security.ApiKey;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import org.apache.commons.lang.RandomStringUtils;

import java.security.SecureRandom;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class ApiKeyManager {

    private static final String API_KEY_VALIDITY_APPLICATION_PREFERENCE_KEY = "api_key_validity_days";
    static final int API_KEY_LENGTH = 128;

    private ApiKeyDao apiKeyDao;

    public ApiKeyManager(ApiKeyDao apiKeyDao) {
        this.apiKeyDao = apiKeyDao;
    }

    public ApiKey createNewApiKeyForCurrentUser() {
        Calendar cal = Calendar.getInstance();
        Date creationDate = cal.getTime();
        Integer duration = PreferenceService.getInteger(API_KEY_VALIDITY_APPLICATION_PREFERENCE_KEY);
        if (duration<0) {
            duration = 0;
        }
        cal.add(Calendar.DATE, duration);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        Date expirationDate = cal.getTime();

        return apiKeyDao.createKey(new ApiKey(
                generateNewApiKeyValue(),
                SsoManager.getLoggedUsername(),
                SsoManager.getLoggedOrganization(),
                creationDate,
                expirationDate));
    }

    public List<ApiKey> getAllKeysOfCurrentUser() {
        return apiKeyDao.getAllKeysOfUser(SsoManager.getLoggedUsername());
    }

    public boolean isAllKeysOfCurrentUserExpired(List<ApiKey> apiKeys) {
        boolean res = true;
        for (ApiKey apiKey : apiKeys) {
            res &= apiKey.isExpired();
        }
        return res;
    }

    public ApiKey findApiKey(String key) throws ApiKeyNotFoundException {
        return apiKeyDao.findByValue(key);
    }

    public GazelleIdentity authenticate(String apiKeyValue) throws InvalidApiKeyException {
        try {
            ApiKey apiKey = apiKeyDao.findByValue(apiKeyValue);
            if(!apiKey.isExpired()){
                return new ApiKeyIdentity(apiKey.getOwner(), apiKey.getOrganization());
            } else {
                throw new InvalidApiKeyException("Expired api key");
            }
        } catch (ApiKeyNotFoundException e) {
            throw new InvalidApiKeyException("Unknown api key");
        }
    }

    private String generateNewApiKeyValue() {
        return RandomStringUtils.random(API_KEY_LENGTH, 0, 0, true, true, null, new SecureRandom());
    }

    void setApiKeyDao(ApiKeyDao apiKeyDao) {
        this.apiKeyDao = apiKeyDao;
    }
}
