package net.ihe.gazelle.evsclient.interlay.dto.rest.validation;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import net.ihe.gazelle.evsclient.domain.processing.EVSCallerMetadata;
import net.ihe.gazelle.evsclient.domain.processing.EntryPoint;
import net.ihe.gazelle.evsclient.domain.processing.OtherCallerMetadata;
import net.ihe.gazelle.evsclient.domain.processing.ProxyCallerMetadata;
import net.ihe.gazelle.evsclient.interlay.dto.rest.AbstractDTO;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@Schema(description="Métadonnées sur la source de la demande de validation ou d'analyse. ")
@XmlRootElement(name = "caller")
@XmlAccessorType(XmlAccessType.FIELD)
public class CallerMetadata extends AbstractDTO<EVSCallerMetadata> {
    public CallerMetadata(EVSCallerMetadata domain) {
        super(domain);
    }

    public CallerMetadata() {
        this(new ProxyCallerMetadata());
    }

    @Schema(name= "entryPoint",example = "WS", description = "Type de demandeur.")
    @JsonProperty("entryPoint")
    @XmlAttribute(namespace = "http://evsobjects.gazelle.ihe.net/", name = "entryPoint")
    public EntryPoint getEntryPoint() {
        return domain.getEntryPoint();
    }

    @Schema(name= "toolOid",example = "", description = "Identifiant de l'outil demandeur.") // TODO
    @JsonProperty("toolOid")
    @XmlAttribute(name = "toolOid")
    public String getToolOid() {
        return domain instanceof OtherCallerMetadata
                ? ((OtherCallerMetadata)domain).getToolOid()
                : null;
    }

    public void setToolOid(String toolOid) {
        if (domain instanceof OtherCallerMetadata)
                ((OtherCallerMetadata)domain).setToolOid(toolOid);
    }

    @Schema(name= "toolObjectId",example = "", description = ".") // TODO
    @JsonProperty("toolObjectId")
    @XmlAttribute(name = "toolObjectId")
    public String getToolObjectId() {
        return domain instanceof OtherCallerMetadata
                ? ((OtherCallerMetadata)domain).getToolObjectId()
                : null;
    }

    public void setToolObjectId(String toolObjectId) {
        if (domain instanceof OtherCallerMetadata)
            ((OtherCallerMetadata)domain).setToolObjectId(toolObjectId);
    }

    @Schema(name= "proxyType",example = "", description = ".") // TODO
    @JsonProperty("proxyType")
    @XmlAttribute(name = "proxyType")
    public String getProxyType() {
        return domain instanceof ProxyCallerMetadata
                ? ((ProxyCallerMetadata)domain).getProxyType()
                : null;
    }

    public void setProxyType(String proxyType) {
        if (domain instanceof ProxyCallerMetadata)
            ((ProxyCallerMetadata)domain).setProxyType(proxyType);
    }
}
