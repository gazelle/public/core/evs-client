package net.ihe.gazelle.evsclient.interlay.gui.validation;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.application.CallerMetadataFactory;
import net.ihe.gazelle.evsclient.application.interfaces.GuiPermanentLink;
import net.ihe.gazelle.evsclient.application.interfaces.ProcessingManager;

import net.ihe.gazelle.evsclient.domain.processing.AbstractProcessing;
import net.ihe.gazelle.evsclient.interlay.gui.processing.AbstractProcessingBeanGui;
import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;

public abstract class AbstractValidationProcessingBeanGui<T extends AbstractProcessing, M extends ProcessingManager<T>> extends AbstractProcessingBeanGui<T,M> {

   protected AbstractValidationProcessingBeanGui(Class<?> cls, M processingManager, CallerMetadataFactory callerMetadataFactory, GazelleIdentity identity, ApplicationPreferenceManager applicationPreferenceManager, GuiPermanentLink<T> permanentLinkGui) {
      super(cls, processingManager, callerMetadataFactory, identity, applicationPreferenceManager, permanentLinkGui);
   }

   protected abstract String getValidatorUrl();

   public abstract String performAnotherValidation();

   public abstract String revalidate();
}
