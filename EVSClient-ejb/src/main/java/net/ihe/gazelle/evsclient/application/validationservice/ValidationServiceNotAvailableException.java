package net.ihe.gazelle.evsclient.application.validationservice;

public class ValidationServiceNotAvailableException extends Exception {
   private static final long serialVersionUID = -8970905690972193640L;

   public ValidationServiceNotAvailableException(String s) {
      super(s);
   }

   public ValidationServiceNotAvailableException(String s, Throwable throwable) {
      super(s, throwable);
   }
}
