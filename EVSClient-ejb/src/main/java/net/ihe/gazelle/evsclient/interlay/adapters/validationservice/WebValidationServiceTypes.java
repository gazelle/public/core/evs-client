package net.ihe.gazelle.evsclient.interlay.adapters.validationservice;

import net.ihe.gazelle.evsclient.domain.validationservice.WebValidationService;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.ValidationServiceType;

public enum WebValidationServiceTypes implements ValidationServiceType<WebValidationService> {

    CERTIFICATE_SERVICE("Certificate Service", CertificateValidationServiceClient.class),// "TLS"
    GITB_REST_SERVICE("GITB Rest", GitbRestValidationServiceClient.class), // "XML"
    HL7V2_SERVICE("HL7v2", Hl7v2ValidationServiceClient.class),// "HL7v2"
    HL7V3_SERVICE("HL7v3",Hl7v3ValidationServiceClient.class),// "HL7v3"
    MBV_SERVICE("Model Based", ModelBasedValidationServiceClient.class),// "ATNA", "AUDIT_MESSAGE", "FHIR"
    CDA_SERVICE("CDA Model Based", CDAGeneratorValidationServiceClient.class),// "CDA"
    SCHEMATRON_SERVICE("Schematron", SchematronValidationServiceClient.class),// "XML", "CDA"
    DCCHECK_SERVICE("DC Check", DcCheckValidationService.class),
    GAZELLE_VALIDATION_SERVICE("Gazelle Validation Service", GazelleValidationServiceClient.class)
    ;

    private final String value;

    private final Class<? extends WebValidationService> validationServiceClass;

    private boolean implemented = true;

    WebValidationServiceTypes(String value, Class<? extends WebValidationService> validationServiceClass) {
        this.value = value;
        this.validationServiceClass = validationServiceClass;
    }

    WebValidationServiceTypes(String value,
                              Class<? extends WebValidationService> validationServiceClass, boolean implemented) {
        this(value,validationServiceClass);
        this.implemented = implemented;
    }


    @Override
    public String getValue() {
        return this.value;
    }

    @Override
    public Class<? extends WebValidationService> getValidationServiceClass() {
        return validationServiceClass;
    }

    @Override
    public boolean isImplemented() {
        return this.implemented;
    }
}
