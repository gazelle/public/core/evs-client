package net.ihe.gazelle.evsclient.domain.validationservice.configuration;

import net.ihe.gazelle.evsclient.domain.validationservice.ValidationService;
import net.ihe.gazelle.evsclient.domain.validationservice.WebValidationService;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "evsc_web_validation_service_conf", schema = "public")
public class WebValidationServiceConf extends ValidationServiceConf implements WebServiceConf {
    private static final long serialVersionUID = -1299697695315817735L;

    @Column(name = "url")
    private String url;

    @Column(name = "zip_transport")
    private boolean zipTransport;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isZipTransport() {
        return zipTransport;
    }

    public void setZipTransport(boolean zipTransport) {
        this.zipTransport = zipTransport;
    }

    @Override
    public Class<? extends ValidationService> getValidationServiceInterface() {
        return WebValidationService.class;
    }
}
