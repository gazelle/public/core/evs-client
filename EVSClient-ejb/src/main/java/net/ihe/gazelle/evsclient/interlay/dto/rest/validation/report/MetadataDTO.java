package net.ihe.gazelle.evsclient.interlay.dto.rest.validation.report;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import net.ihe.gazelle.evsclient.domain.validation.report.Metadata;
import net.ihe.gazelle.evsclient.interlay.dto.rest.DataTransferObject;

import javax.xml.bind.annotation.*;

@Schema(description="Métadonnée du rapport de validation.")
@XmlRootElement(name = "metadata")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class MetadataDTO implements DataTransferObject<Metadata> {

   private String name;
   private String value;

   public MetadataDTO(Metadata domain) {
      name = domain.getName();
      value = domain.getValue();
   }

   public MetadataDTO() {
   }

   @Schema(name= "name", description = "") // TODO
   @JsonProperty("name")
   @XmlAttribute(name = "name")
   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }

   @Schema(name= "value", description = "") // TODO
   @JsonProperty("value")
   @XmlValue
   public String getValue() {
      return value;
   }

   public void setValue(String value) {
      this.value = value;
   }

   @Override
   public Metadata toDomain() {
      return new Metadata(name, value);
   }
}