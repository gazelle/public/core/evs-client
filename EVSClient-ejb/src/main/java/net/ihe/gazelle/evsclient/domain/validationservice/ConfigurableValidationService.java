package net.ihe.gazelle.evsclient.domain.validationservice;

import net.ihe.gazelle.evsclient.domain.validationservice.configuration.ProcessingConf;

public interface ConfigurableValidationService<T extends ProcessingConf> extends ValidationService,ConfigurableService<T> {

}
