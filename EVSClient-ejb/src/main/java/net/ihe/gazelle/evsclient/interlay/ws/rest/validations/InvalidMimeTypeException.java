package net.ihe.gazelle.evsclient.interlay.ws.rest.validations;

public class InvalidMimeTypeException extends Exception {
   private static final long serialVersionUID = 2687646147678793594L;

   public InvalidMimeTypeException(String s) {
      super(s);
   }
}
