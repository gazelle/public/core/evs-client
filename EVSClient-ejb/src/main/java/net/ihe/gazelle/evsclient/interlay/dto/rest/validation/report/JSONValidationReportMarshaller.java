package net.ihe.gazelle.evsclient.interlay.dto.rest.validation.report;

import net.ihe.gazelle.evsclient.domain.validation.report.SeverityLevel;
import net.ihe.gazelle.evsclient.domain.validation.report.ValidationReport;
import net.ihe.gazelle.evsclient.interlay.dto.rest.JSONMarshaller;

public class JSONValidationReportMarshaller extends JSONMarshaller<ValidationReport,ValidationReportDTO> {

   private SeverityLevel severity;

   protected JSONValidationReportMarshaller(Class<ValidationReportDTO> cls, SeverityLevel severity) {
      super(cls);
      this.severity = severity;
   }
   public JSONValidationReportMarshaller(SeverityLevel severity) {
      this(ValidationReportDTO.class, severity);
   }
   public JSONValidationReportMarshaller() {
      this(SeverityLevel.INFO);
   }

   @Override
   protected ValidationReportDTO toDTO(ValidationReport domain) {
      return new ValidationReportDTO(domain, severity);
   }

}