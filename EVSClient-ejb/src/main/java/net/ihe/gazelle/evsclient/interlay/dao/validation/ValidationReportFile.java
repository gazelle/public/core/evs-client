package net.ihe.gazelle.evsclient.interlay.dao.validation;

import net.ihe.gazelle.evsclient.domain.validation.Report;
import net.ihe.gazelle.evsclient.domain.validation.report.ValidationReport;
import net.ihe.gazelle.evsclient.interlay.dto.rest.MarshallingException;
import net.ihe.gazelle.evsclient.interlay.dto.rest.validation.report.XMLValidationReportMarshaller;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.io.IOException;

@Entity
@DiscriminatorValue("validation-report")
public class ValidationReportFile extends ReportFile<ValidationReport> {

   private static final long serialVersionUID = -6495757173093691699L;

   // For JPA Only
   ValidationReportFile() {
      super();
   }

   ValidationReportFile(Report<ValidationReport> report) {
      super(report);
   }

   @Override
   ValidationReport unmarshall(ReportZipInputStream inputStream) throws IOException {
      return new XMLValidationReportMarshaller().unmarshal(inputStream);
   }

   @Override
   void marshall(ValidationReport content, ReportZipOutputStream outputStream) throws IOException {
      new XMLValidationReportMarshaller()
            .setCompleteDocument(true)
            .setIndentation(true)
            .marshal(content, outputStream);
   }
}
