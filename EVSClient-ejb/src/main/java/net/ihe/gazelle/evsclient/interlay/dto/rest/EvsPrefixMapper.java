package net.ihe.gazelle.evsclient.interlay.dto.rest;

import com.sun.xml.bind.marshaller.NamespacePrefixMapper;

public class EvsPrefixMapper extends NamespacePrefixMapper {

    private static final String PREFIX = ""; // DEFAULT NAMESPACE
    private static final String URI = "http://evsobjects.gazelle.ihe.net/";

    @Override
    public String getPreferredPrefix(String namespaceUri, String suggestion, boolean requirePrefix) {
        if(URI.equals(namespaceUri)) {
            return PREFIX;
        }
        return suggestion;
    }

    @Override
    public String[] getPreDeclaredNamespaceUris() {
        return new String[] { URI };
    }

}
