package net.ihe.gazelle.evsclient.interlay.dto.rest.validation;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import net.ihe.gazelle.evsclient.domain.validation.ValidationServiceRef;

import javax.xml.bind.annotation.*;
import java.util.List;
import java.util.Objects;

@Schema(description = "Service de validation demandé ou identifié pour valider l'objet.  ")
@XmlRootElement(name = "validationService")
@XmlAccessorType(XmlAccessType.FIELD)
public class ValidationService extends AbstractService<ValidationServiceRef> {

    @Schema(name = "extensions", description = "extensions du service de validation")
    @JsonProperty("extensions")
    @XmlElement(name = "extensions")
    private List<ExtensionService> extensions;

    public ValidationService() {
        super();
    }

    public ValidationService(ValidationServiceRef domain) {
        super(domain);
    }

    public List<ExtensionService> getExtensions() {
        return extensions;
    }

    public void setExtensions(List<ExtensionService> extensions) {
        this.extensions = extensions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ValidationService validationService = (ValidationService) o;
        return Objects.equals(getName(), validationService.getName()) &&
                Objects.equals(getValidator(), validationService.getValidator()) &&
                Objects.equals(getExtensions(), validationService.getExtensions());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getValidator(), getExtensions());
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class ValidationService {\n");

        sb.append("    name: ").append(toIndentedString(getName())).append("\n");
        sb.append("    validator: ").append(toIndentedString(getValidator())).append("\n");
        sb.append("    extensions: ").append(toIndentedString(getExtensions())).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}

