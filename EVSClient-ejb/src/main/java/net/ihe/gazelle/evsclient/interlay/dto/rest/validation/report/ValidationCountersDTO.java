package net.ihe.gazelle.evsclient.interlay.dto.rest.validation.report;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import net.ihe.gazelle.evsclient.domain.validation.report.ValidationCounters;

import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@Schema(description = "Compteurs du rapport de validation.")
@XmlRootElement(name = "validationCounters")
@XmlAccessorType(XmlAccessType.PROPERTY)
public class ValidationCountersDTO {

   private Integer numberOfConstraints;
   private Integer failedWithInfoNumber;
   private Integer numberOfWarnings;
   private Integer numberOfErrors;

   public ValidationCountersDTO(ValidationCounters domain) {
      numberOfConstraints = domain.getNumberOfConstraints();
      failedWithInfoNumber = domain.getFailedWithInfoNumber();
      numberOfWarnings = domain.getNumberOfWarnings();
      numberOfErrors = domain.getNumberOfErrors();
   }

   public ValidationCountersDTO() {

   }

   @Schema(name = "numberOfConstraints", description = "") // TODO
   @JsonProperty("numberOfConstraints")
   @XmlAttribute(name = "numberOfConstraints")
   public Integer getNumberOfConstraints() {
      return numberOfConstraints;
   }

   public void setNumberOfConstraints(Integer numberOfConstraints) {
      this.numberOfConstraints = numberOfConstraints;
   }

   @Schema(name = "failedWithInfoNumber", description = "") // TODO
   @JsonProperty("failedWithInfoNumber")
   @XmlAttribute(name = "failedWithInfoNumber")
   public Integer getFailedWithInfoNumber() {
      return failedWithInfoNumber;
   }

   public void setFailedWithInfoNumber(Integer failedWithInfoNumber) {
      this.failedWithInfoNumber = failedWithInfoNumber;
   }

   @Schema(name = "numberOfWarnings", description = "") // TODO
   @JsonProperty("numberOfWarnings")
   @XmlAttribute(name = "numberOfWarnings")
   public Integer getNumberOfWarnings() {
      return numberOfWarnings;
   }

   public void setNumberOfWarnings(Integer numberOfWarnings) {
      this.numberOfWarnings = numberOfWarnings;
   }

   @Schema(name = "numberOfErrors", description = "") // TODO
   @JsonProperty("numberOfErrors")
   @XmlAttribute(name = "numberOfErrors")
   public Integer getNumberOfErrors() {
      return numberOfErrors;
   }

   public void setNumberOfErrors(Integer numberOfErrors) {
      this.numberOfErrors = numberOfErrors;
   }

   @Transient
   @JsonIgnore
   public Integer getNumberOfSuccess() {
      return numberOfConstraints - numberOfErrors - numberOfWarnings - failedWithInfoNumber;
   }

}