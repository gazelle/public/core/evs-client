package net.ihe.gazelle.evsclient.domain.statistics;

public enum IntervalForStatistics {

    MONTH("YYYY-MM"),
    YEAR("YYYY");

    private String value;

    IntervalForStatistics(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

}
