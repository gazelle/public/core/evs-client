package net.ihe.gazelle.evsclient.interlay.adapters.validationservice;

import net.ihe.gazelle.atna.action.pki.ws.CertificateValidatorErrorTrace;
import net.ihe.gazelle.atna.action.pki.ws.CertificateValidatorResult;
import net.ihe.gazelle.evsclient.application.validation.ValidationReportMapper;
import net.ihe.gazelle.evsclient.domain.validation.report.*;
import net.ihe.gazelle.evsclient.domain.validationservice.VersionedValidationService;
import net.ihe.gazelle.evsclient.interlay.adapters.validation.AbstractReportMapper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

class CertificateReportBuilder extends AbstractReportMapper {

    public CertificateReportBuilder() {
    }

    <S extends VersionedValidationService> ValidationReport build(S service, String validator, String version, final CertificateValidatorResult result, boolean revocation) {

        ValidationReport report = new ValidationReport(
                UUID.randomUUID().toString(),
                new ValidationOverview(
                        ValidationReportMapper.DEFAULT_DISCLAIMER,
                        service.getName(),
                        service.getServiceVersion(),
                        validator,
                        version,
                        Arrays.asList(new Metadata("CheckRevocation", Boolean.toString(revocation)))));

        ValidationSubReport vsr = new ValidationSubReport(
                validator,
                new ArrayList<String>()
        );

        parseDocument(vsr, result);

        report.addSubReport(vsr);
        return report;
    }


    private ConstraintValidation parseError(CertificateValidatorErrorTrace notification) {
        return new ConstraintValidation(parseConstraintId(notification),
                parseConstraintType(notification),
                parseConstraintDescription(notification),
                parseFormalExpression(notification),
                parseLocationInValidatedObject(notification),
                parseValueInValidatedObject(notification),
                parseAssertionIDs(notification),
                parseUnexpectedErrors(notification),
                ConstraintPriority.MANDATORY, ValidationTestResult.FAILED);
    }
    private ConstraintValidation parseWarning(CertificateValidatorErrorTrace notification) {
            return new ConstraintValidation(parseConstraintId(notification),
                    parseConstraintType(notification),
                    parseConstraintDescription(notification),
                    parseFormalExpression(notification),
                    parseLocationInValidatedObject(notification),
                    parseValueInValidatedObject(notification),
                    parseAssertionIDs(notification),
                    parseUnexpectedErrors(notification),
                    ConstraintPriority.RECOMMENDED, ValidationTestResult.FAILED);
    }

    private List<UnexpectedError> parseUnexpectedErrors(CertificateValidatorErrorTrace notification) {
        return null; // TODO
    }

    private List<String> parseAssertionIDs(CertificateValidatorErrorTrace notification) {
        return null; // TODO
    }

    private String parseValueInValidatedObject(CertificateValidatorErrorTrace notification) {
        return null; // TODO
    }

    private String parseLocationInValidatedObject(CertificateValidatorErrorTrace notification) {
        return null; // TODO
    }

    private String parseFormalExpression(CertificateValidatorErrorTrace notification) {
        return null; // TODO
    }

    private String parseConstraintDescription(CertificateValidatorErrorTrace notification) {
        return notification.getExceptionMessage();
    }

    private String parseConstraintId(CertificateValidatorErrorTrace notification) {
        return notification.getNormMessage();
    }

    private String parseConstraintType(CertificateValidatorErrorTrace notification) {
        return null; // TODO
    }


    private void parseDocument(ValidationSubReport vsr, CertificateValidatorResult result) {
        if (result.isErrorsSpecified()) {
            for (CertificateValidatorErrorTrace error : result.getErrors()) {
                vsr.addConstraintValidation(parseError(error));
            }
        }
        if (result.isWarningsSpecified()) {
            for (CertificateValidatorErrorTrace error : result.getWarnings()) {
                vsr.addConstraintValidation(parseWarning(error));
            }
        }

        if (vsr.getConstraints().isEmpty()) {
            vsr.addConstraintValidation(new ConstraintValidation("checked",ConstraintPriority.PERMITTED, ValidationTestResult.PASSED));
        }

    }
}
