package net.ihe.gazelle.evsclient.application.job;

import gnu.trove.map.hash.THashMap;
import net.ihe.gazelle.common.application.action.ApplicationPreferenceManagerImpl;
import net.ihe.gazelle.common.interfacegenerator.GenerateInterface;
import net.ihe.gazelle.evsclient.application.validation.ExtensionDao;
import net.ihe.gazelle.evsclient.application.validationservice.GatewayTimeoutException;
import net.ihe.gazelle.evsclient.domain.validation.Report;
import net.ihe.gazelle.evsclient.domain.validation.extension.ExtensionStatus;
import net.ihe.gazelle.evsclient.domain.validation.extension.ValidationExtension;
import net.ihe.gazelle.evsclient.domain.validationservice.extension.ExtensionService;
import net.ihe.gazelle.evsclient.interlay.dao.validation.OriginalReportFile;
import net.ihe.gazelle.evsclient.interlay.factory.DaoFactory;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.*;
import org.jboss.seam.annotations.async.Asynchronous;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.text.MessageFormat;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

@GenerateInterface("ExtensionJobLocal")
@Name("extensionJob")
@AutoCreate
@Scope(ScopeType.APPLICATION)
public class ExtensionJob implements Serializable, ExtensionJobLocal {
    private static final long serialVersionUID = 1;
    private static Logger log = LoggerFactory.getLogger(ExtensionJob.class);
    private final THashMap<String, BlockingQueue<Callable<Exception>>> queues = new THashMap<String, BlockingQueue<Callable<Exception>>>();

    @In(value = "evsDaoFactory")
    private DaoFactory daoFactory;

    private ExtensionDao extensionDao;

    public ExtensionJob() {
        super();
    }

    public ExtensionDao getExtensionDao() {
        if (extensionDao==null) {
            extensionDao = daoFactory.getExtensionDao();
        }
        return extensionDao;
    }

    @Override
    @Observer("executeExtensionJob")
    @Transactional
    @Asynchronous
    public void executeExtensionJob(final ValidationExtension extension, final ExtensionService service) {
        try {
            log.info(MessageFormat.format("executeExtensionJob {0} {1} {2}", extension.getId(), extension.getValidationRef().getOid(), service.getName()));
            Exception exception = regulatedRun(
                    extension, service,
                    new Callable<Exception>() {
                        @Override
                        public Exception call() throws Exception {
                            try {
                                OriginalReportFile report = extension.getExtensionReport();
                                if (report == null) {
                                    extension.setExtensionReport(
                                            new OriginalReportFile(
                                                    new Report<byte[]>(
                                                            service.process(extension)
                                                    )));
                                } else {
                                    report.setArchivePath(null);
                                    report.setContent(service.process(extension));
                                }
                                extension.setExtensionStatus(ExtensionStatus.AVAILABLE);
                            } catch (Exception e) {
                                return e;
                            }
                            return null;
                        }
                    });
            if (exception!=null) {
                log.error(exception.getMessage(), exception);
                extension.setExtensionStatus(ExtensionStatus.FAILED_ON_SERVER_SIDE);
            }
        } finally {
            getExtensionDao().merge(extension);
        }
    }

    public Exception regulatedRun(final ValidationExtension extension, ExtensionService service, Callable<Exception> execution) {
        try {
            if (log.isDebugEnabled()) log.debug(MessageFormat.format("####################### run {1} {0}", extension.getValidationRef().getOid(), service.getName()));
            BlockingQueue<Callable<Exception>> queue = getQueue(service);
            if (log.isDebugEnabled()) log.debug(MessageFormat.format("####################### offer {3} {0} {1}[{2}]", extension.getValidationRef().getOid(), "Q"+queue.hashCode(), queue.size(), service.getName()));
            boolean offered = queue.offer(execution,
                    getTimeout(service),
                    TimeUnit.MILLISECONDS);
            if (!offered) {
                log.error(MessageFormat.format("####################### aborted run {3} {0} {1}[{2}]", extension.getValidationRef().getOid(), "Q"+queue.hashCode(), queue.size(), service.getName()));
                return new GatewayTimeoutException(MessageFormat.format("{0} {1}",extension.getValidationRef().getOid(), service.getName()));
            }
            if (log.isDebugEnabled()) log.debug(MessageFormat.format("####################### call {3} {0} {1}[{2}]", extension.getValidationRef().getOid(), "Q"+queue.hashCode(), queue.size(), service.getName()));
            Exception exception = execution.call();
            if (log.isDebugEnabled()) log.debug(MessageFormat.format("####################### poll {3} {0} {1}[{2}]", extension.getValidationRef().getOid(), "Q"+queue.hashCode(), queue.size(), service.getName()));
            queue.poll();
            if (exception==null) {
                if (log.isDebugEnabled()) log.debug(MessageFormat.format("####################### successful run {3} {0} {1}[{2}]", extension.getValidationRef().getOid(), "Q" + queue.hashCode(), queue.size(), service.getName()));
            } else {
                log.error(MessageFormat.format("####################### unsuccessful run {3} {0} {1}[{2}] {4}", extension.getValidationRef().getOid(), "Q" + queue.hashCode(), queue.size(), service.getName(), exception.getMessage()));
            }
            return exception;
        } catch (final Exception e) {
            log.error(MessageFormat.format("####################### failed run {1} {0}", extension.getValidationRef().getOid(), service.getName()),e);
            return e;
        }
    }

    private BlockingQueue<Callable<Exception>> getQueue(ExtensionService<?> service) {
        BlockingQueue<Callable<Exception>> res = queues.get(service.getName());
        if (res == null) {
            res = new LinkedBlockingQueue<>(getLimit(service));
            queues.put(service.getName(), res);
        }
        return res;
    }
    private int getTimeout(ExtensionService<?> service) {
        return getPreference(service.getName()+"_validation_extension_timeout",
                getPreference("validation_extension_timeout",60000));
    }
    private int getLimit(ExtensionService<?> service) {
        return getPreference(service.getName()+"_concurrent_extension_validation_limit",
                getPreference("concurrent_extension_validation_limit",100));
    }

    private int getPreference(String key, int defaultValue) {
        int value = defaultValue;
        try {
            if (ApplicationPreferenceManagerImpl.instance().getValues().containsKey(key)) {
                try {
                    value = Integer.parseInt(ApplicationPreferenceManagerImpl.instance().getValues().get(key).toString());
                } catch (NumberFormatException nfe) {
                    log.warn("invalid-value {}",key);
                }
            }
        } catch (java.lang.IllegalStateException e) {
            // in UT : No application context active
        }
        return value;
    }

}