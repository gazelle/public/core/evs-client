package net.ihe.gazelle.evsclient.application.security;


import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import net.ihe.gazelle.ssov7.authn.interlay.adapter.GazelleIdentityImpl;

public class SsoManager {
    public static String getLoggedUsername() {
        //FIXME dependency violation. Application layer must not depends on SsoIdentityImpl interlay. This will
        // require static mock for unit tests.
        final GazelleIdentity instance = GazelleIdentityImpl.instance();
        if (instance.isLoggedIn()) {
            return instance.getUsername();
        }
        return null;
    }
    public static String getLoggedOrganization() {
        //FIXME dependency violation. application must not depends on SsoIdentityImpl interlay. This will
        // require static mock for unit tests.
        final GazelleIdentity instance = GazelleIdentityImpl.instance();
        if (instance.isLoggedIn()) {
            return instance.getOrganisationKeyword();
        }
        return null;
    }

    private SsoManager() {
    }
}
