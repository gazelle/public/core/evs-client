package net.ihe.gazelle.evsclient.interlay.dao.validationservice.validator;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.common.application.action.ApplicationPreferenceManagerImpl;
import net.ihe.gazelle.evsclient.domain.validationservice.validator.PackageNameForProfileOID;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import org.jboss.resteasy.client.ClientRequest;
import org.jboss.resteasy.client.ClientResponse;
import org.jboss.seam.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import java.util.List;

public class HL7ProfileDaoImpl implements HL7ProfileDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(HL7ProfileDaoImpl.class);

    public String findPackageNameWithProfileOID(final String profileOID) {
        if (profileOID == null) {
            return null;
        }
        final EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
        final HQLQueryBuilder<PackageNameForProfileOID> queryBuilder = new HQLQueryBuilder<>(entityManager,
                PackageNameForProfileOID.class);
        queryBuilder.addEq("profileOID", profileOID);
        final List<PackageNameForProfileOID> packages = queryBuilder.getList();
        if (packages != null && !packages.isEmpty()) {
            return packages.get(0).getPackageName();
        } else {
            ApplicationPreferenceManager applicationPreferenceManager = new ApplicationPreferenceManagerImpl();
            final String validatorUrl = applicationPreferenceManager.getStringValue("gazelle_hl7v2_validator_url");
            final ClientRequest request = new ClientRequest(validatorUrl.concat("/rest/GetPackageForProfile"));
            request.queryParameter("oid", profileOID);
            try {
                final ClientResponse<String> response = request.get(String.class);
                if (response.getStatus() == 200 && response.getEntity() != null && !response.getEntity().isEmpty()) {
                    final PackageNameForProfileOID javaPackage = new PackageNameForProfileOID();
                    javaPackage.setPackageName(response.getEntity());
                    javaPackage.setProfileOID(profileOID);
                    entityManager.merge(javaPackage);
                    entityManager.flush();
                    return javaPackage.getPackageName();
                } else {
                    return null;
                }
            } catch (final Exception e) {
                LOGGER.error(e.getMessage(),e);
                return null;
            }
        }
    }
}
