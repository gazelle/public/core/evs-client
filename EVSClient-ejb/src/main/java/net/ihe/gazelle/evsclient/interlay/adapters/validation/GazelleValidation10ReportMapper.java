package net.ihe.gazelle.evsclient.interlay.adapters.validation;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import net.ihe.gazelle.evsclient.application.validation.OriginalReportParser;
import net.ihe.gazelle.evsclient.application.validation.ReportTransformationException;
import net.ihe.gazelle.evsclient.application.validation.ValidationReportMapper;
import net.ihe.gazelle.evsclient.domain.validation.report.ValidationReport;
import net.ihe.gazelle.evsclient.interlay.dto.rest.validation.report.ValidationReportDTO;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

public class GazelleValidation10ReportMapper implements ValidationReportMapper, OriginalReportParser<byte[]> {

    private static final ObjectMapper mapper = new ObjectMapper();

    @Override
    public ValidationReport transform(byte[] originalReport) throws ReportTransformationException {
        try {
            return convertToRetroReport(new String(originalReport, StandardCharsets.UTF_8)).toDomain();
        } catch (IOException e) {
            throw new ReportTransformationException("Error while transforming to retro report", e);
        }
    }

    @Override
    public byte[] parseOriginalReport(byte[] originalReport)  {
        return originalReport;
    }

    ValidationReportDTO convertToRetroReport(String newReport) throws IOException {
        JsonNode inputNode = mapper.readTree(newReport);
        ObjectNode outputNode = mapper.createObjectNode();
        setStringFieldIfNotNull(outputNode, "uuid", inputNode, "uuid");
        outputNode.set("validationOverview", getValidationOverviewJsonObject(inputNode));
        outputNode.set("counters", getCountersJsonObject(inputNode,"counters"));
        outputNode.set("subReports", getSubReportsJsonObject(inputNode, "reports"));
        setStringFieldIfNotNull(outputNode, "result", inputNode, "overallResult");
        return mapper.treeToValue(outputNode, ValidationReportDTO.class);
    }


    ObjectNode getValidationOverviewJsonObject(JsonNode inputNode) {
        ObjectNode validationOverviewNode = mapper.createObjectNode();
        setStringFieldIfNotNull(validationOverviewNode, "disclaimer", inputNode, "disclaimer");
        setStringFieldIfNotNull(validationOverviewNode, "validationDateTime", inputNode, "dateTime");
        if(inputNode.has("validationMethod")){
            JsonNode validationMethodNode = inputNode.get("validationMethod");
            setStringFieldIfNotNull(validationOverviewNode, "validationServiceName", validationMethodNode, "validationServiceName");
            setStringFieldIfNotNull(validationOverviewNode, "validationServiceVersion", validationMethodNode, "validationServiceVersion");
            setStringFieldIfNotNull(validationOverviewNode, "validatorID", validationMethodNode, "validationProfileID");
            setStringFieldIfNotNull(validationOverviewNode, "validatorVersion", validationMethodNode, "validationProfileVersion");
            validationOverviewNode.set("additionalMetadata", getAdditionalMetadataJsonObject(inputNode));
            setStringFieldIfNotNull(validationOverviewNode, "validationOverallResult", inputNode, "overallResult");
        }
        return validationOverviewNode;
    }

    ObjectNode getCountersJsonObject(JsonNode inputNode, String inputFieldName) {
        ObjectNode countersNode = mapper.createObjectNode();
        if(inputNode.has(inputFieldName)){
            JsonNode countersInputNode = inputNode.get(inputFieldName);
            setLongFieldIfNotNull(countersNode, "numberOfConstraints", countersInputNode, "numberOfAssertions");
            setLongFieldIfNotNull(countersNode, "failedWithInfoNumber", countersInputNode, "numberOfFailedWithInfos");
            setLongFieldIfNotNull(countersNode, "numberOfWarnings", countersInputNode, "numberOfFailedWithWarnings");
            setLongFieldIfNotNull(countersNode, "numberOfErrors", countersInputNode, "numberOfFailedWithErrors");
        }
        return countersNode;
    }


    ArrayNode getAdditionalMetadataJsonObject(JsonNode inputNode) {
        ArrayNode additionalMetadataNode = mapper.createArrayNode();
        if(inputNode.has("additionalMetadata")){
            JsonNode additionalMetadataInputNode = inputNode.get("additionalMetadata");
            if(additionalMetadataInputNode.isArray()){
                for(JsonNode additionalMetadataItem : additionalMetadataInputNode){
                    ObjectNode additionalMetadataItemNode = mapper.createObjectNode();
                    setStringFieldIfNotNull(additionalMetadataItemNode, "name", additionalMetadataItem, "name");
                    setStringFieldIfNotNull(additionalMetadataItemNode, "value", additionalMetadataItem, "value");
                    additionalMetadataNode.add(additionalMetadataItemNode);
                }
            }
        }
        return additionalMetadataNode;
    }


    ArrayNode getSubReportsJsonObject(JsonNode inputNode, String inputFieldName) {
        ArrayNode subReportsArrayNode = mapper.createArrayNode();
        if (inputNode.has(inputFieldName)) {
            JsonNode subReportsInputNode = inputNode.get(inputFieldName);
            for (JsonNode subReportInputNode : subReportsInputNode) {
                ObjectNode subReportNode = mapper.createObjectNode();
                setStringFieldIfNotNull(subReportNode, "name", subReportInputNode, "name");
                subReportNode.set("standards", getArrayOfStringJsonObject(subReportInputNode, "standards"));
                subReportNode.set("subCounters", getCountersJsonObject(subReportInputNode, "subCounters"));
                subReportNode.set("unexpectedErrors", getUnexpectedErrorsJsonObject(subReportInputNode));
                subReportNode.set("constraints", getConstraintsJsonObject(subReportInputNode));
                subReportNode.set("subReports", getSubReportsJsonObject(subReportInputNode, "subReports"));
                setStringFieldIfNotNull(subReportNode, "subReportResult", subReportInputNode, "subReportResult");
                subReportsArrayNode.add(subReportNode);
            }
        }
        return subReportsArrayNode;
    }

    ArrayNode getUnexpectedErrorsJsonObject(JsonNode inputNode) {
        ArrayNode unexpectedErrorsArrayNode = mapper.createArrayNode();
        if (inputNode.has("unexpectedErrors")) {
            JsonNode unexpectedErrorsInputNode = inputNode.get("unexpectedErrors");
            for (JsonNode unexpectedErrorInputNode : unexpectedErrorsInputNode) {
                ObjectNode unexpectedErrorNode = getUnexpectedErrorJsonObject(unexpectedErrorInputNode);
                unexpectedErrorsArrayNode.add(unexpectedErrorNode);
            }
        }
        return unexpectedErrorsArrayNode;
    }


    ObjectNode getUnexpectedErrorJsonObject(JsonNode inputNode) {
        ObjectNode unexpectedErrorNode = mapper.createObjectNode();
        setStringFieldIfNotNull(unexpectedErrorNode, "name", inputNode, "name");
        setStringFieldIfNotNull(unexpectedErrorNode, "message", inputNode, "message");
        if (inputNode.get("cause") != null && !Objects.equals(inputNode.get("cause").asText(), "null")) {
            unexpectedErrorNode.set("cause", getUnexpectedErrorJsonObject(inputNode.get("cause")));
        }
        return unexpectedErrorNode;
    }

    ArrayNode getConstraintsJsonObject(JsonNode inputNode) {
        ArrayNode constraintsArrayNode = mapper.createArrayNode();
        if (inputNode.has("assertionReports")) {
            JsonNode constraintsInputNode = inputNode.get("assertionReports");
            for (JsonNode assertionInputNode : constraintsInputNode) {
                ObjectNode constraintNode = mapper.createObjectNode();
                setStringFieldIfNotNull(constraintNode, "constraintID", assertionInputNode, "assertionID");
                setStringFieldIfNotNull(constraintNode, "constraintType", assertionInputNode, "assertionType");
                setStringFieldIfNotNull(constraintNode, "constraintDescription", assertionInputNode, "description");
                // FIXME: 23/02/2023 formalExpression attribute doesn't exist in new report
                //setStringFieldIfNotNull(constraintNode, "formalExpression", assertionInputNode, "NPT_FOUND");
                setStringFieldIfNotNull(constraintNode, "locationInValidatedObject", assertionInputNode, "subjectLocation");
                setStringFieldIfNotNull(constraintNode, "valueInValidatedObject", assertionInputNode, "subjectValue");
                constraintNode.set("assertionID",getArrayOfStringJsonObject(assertionInputNode, "requirementIDs"));
                constraintNode.set("unexpectedErrors", getUnexpectedErrorsJsonObject(assertionInputNode));
                setStringFieldIfNotNull(constraintNode, "testResult", assertionInputNode, "result");
                setStringFieldIfNotNull(constraintNode, "severity", assertionInputNode, "severity");
                setStringFieldIfNotNull(constraintNode, "priority", assertionInputNode, "priority");
                constraintsArrayNode.add(constraintNode);
            }
        }
        return constraintsArrayNode;
    }


    ArrayNode getArrayOfStringJsonObject(JsonNode inputNode, String inputFieldName) {
        JsonNode arrayStringInputNode = inputNode.get(inputFieldName);
        ArrayNode arrayString = mapper.createArrayNode();
        if (arrayStringInputNode != null && arrayStringInputNode.isArray() && arrayStringInputNode.size() > 0) {
            for (JsonNode stringElement : arrayStringInputNode) {
                arrayString.add(stringElement.asText());
            }
        }
        return arrayString;
    }

    private void setStringFieldIfNotNull(ObjectNode node, String fieldName, JsonNode inputNode, String inputFieldName) {
        if (inputNode.has(inputFieldName) && inputNode.get(inputFieldName) != null && !Objects.equals(inputNode.get(inputFieldName).asText(), "null")) {
            node.put(fieldName, inputNode.get(inputFieldName).asText());
        }
    }

    private void setLongFieldIfNotNull(ObjectNode node, String fieldName, JsonNode inputNode, String inputFieldName) {
        if (inputNode.has(inputFieldName)) {
            node.put(fieldName, inputNode.get(inputFieldName).asLong());
        }
    }

}
