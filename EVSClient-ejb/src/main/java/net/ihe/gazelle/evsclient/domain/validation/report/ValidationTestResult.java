package net.ihe.gazelle.evsclient.domain.validation.report;

/**
 * ValidationTestResult Enum
 *
 * @author fde
 * @version $Id : $Id
 */
public enum ValidationTestResult {
    PASSED,
    FAILED,
    UNDEFINED
}
