package net.ihe.gazelle.evsclient.interlay.dto.rest.validation;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import net.ihe.gazelle.evsclient.domain.validation.ValidationStatus;
import net.ihe.gazelle.evsclient.domain.validation.ValidationType;
import net.ihe.gazelle.evsclient.interlay.dto.rest.AbstractProcessing;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Objects;

@Schema(description="Validation d'un objet")
@XmlRootElement(name = "validation")
public class ValidationDTO extends AbstractProcessing<net.ihe.gazelle.evsclient.domain.validation.Validation> {

  public ValidationDTO(net.ihe.gazelle.evsclient.domain.validation.Validation domain) {
    super(domain);
  }

  public ValidationDTO() {
    super();
  }

  @Schema(name= "status",description = "") // TODO
  @JsonProperty("status")
  @XmlElement(namespace = "http://evsobjects.gazelle.ihe.net/", name = "status")
  public ValidationStatus getStatus() {
    return domain.getValidationStatus();
  }

  @Schema(name= "validationService",description = "") // TODO
  @JsonProperty("validationService")
  @XmlElement(namespace = "http://evsobjects.gazelle.ihe.net/", name = "validationService")
  public ValidationService getValidationService() {
    return new ValidationService(domain.getValidationService());
  }
  public void setValidationService(ValidationService validationService) {
    domain.setValidationService(validationService.toDomain());
  }
  
  @Schema(name= "validationType",description = "")
  @JsonProperty("validationType")
  @XmlElement(name = "validationType")
  public ValidationType getValidationType() {
    return domain.getValidationType();
  }

  @Schema(name= "validationReportRef",description = "")
  @JsonProperty("validationReportRef")
  @XmlElement(name = "validationReportRef")
  public ValidationReportRef getValidationReportRef() {
    return new ValidationReportRef(domain);
  }


  @XmlAttribute(name = "objectType")
  @JsonProperty("objectType")
  @Schema(name = "objectType", example = "ANS-CR-BIO", description = "Type de l'objet. Peut être défini lors de la création d'un objet ou calculé par MCA.  *Le type de l'objet permet à EVSClient d'identifier quel service de validation et quel validateur appliquer. La liste des types d'objets disponible est visible depuis EVSClient.* ")
  public String getObjectType() {
    return null; // TODO in V3
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ValidationDTO validation = (ValidationDTO) o;
    return Objects.equals(getStatus(), validation.getStatus()) &&
        Objects.equals(getValidationService(), validation.getValidationService()) &&
        Objects.equals(getValidationType(), validation.getValidationType()) &&
        Objects.equals(getValidationReportRef(), validation.getValidationReportRef());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getStatus(),
          getValidationService(),
          getValidationType(),
          getValidationReportRef());
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Validation {\n");
    sb.append("    ").append(toIndentedString(super.toString())).append("\n");
    sb.append("    status: ").append(toIndentedString(getStatus())).append("\n");
    sb.append("    validationService: ").append(toIndentedString(getValidationService())).append("\n");
    sb.append("    validationType: ").append(toIndentedString(getValidationType())).append("\n");
    sb.append("    validationReportRef: ").append(toIndentedString(getValidationReportRef())).append("\n");
    sb.append("}");
    return sb.toString();
  }

}

