package net.ihe.gazelle.evsclient.interlay.adapters.validationservice;

import net.ihe.gazelle.evsclient.domain.validationservice.EmbeddedValidationService;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.ValidationServiceType;

public enum EmbeddedValidationServiceTypes implements ValidationServiceType<EmbeddedValidationService> {

    PIXELMED_SERVICE("PixelMed", PixelMedValidationService.class),
    PDF_SERVICE("PDF", PdfValidationService.class),
    XVAL_SERVICE("XVAL", CrossValidationService.class);

    private final String value;

    private final Class<? extends EmbeddedValidationService> validationServiceClass;

    private boolean implemented = true;

    EmbeddedValidationServiceTypes(String value, Class<? extends EmbeddedValidationService> validationServiceClass) {
        this.value = value;
        this.validationServiceClass = validationServiceClass;
    }

    EmbeddedValidationServiceTypes(String value,
                                   Class<? extends EmbeddedValidationService> validationServiceClass,
                                   boolean implemented) {
        this(value,validationServiceClass);
        this.implemented = implemented;
    }

    @Override
    public String getValue() {
        return this.value;
    }

    @Override
    public Class<? extends EmbeddedValidationService> getValidationServiceClass() {
        return this.validationServiceClass;
    }

    public boolean isImplemented() {
        return implemented;
    }
}
