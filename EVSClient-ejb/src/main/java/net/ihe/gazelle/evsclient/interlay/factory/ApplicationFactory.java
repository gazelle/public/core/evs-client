package net.ihe.gazelle.evsclient.interlay.factory;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.application.interfaces.OidGeneratorManager;
import net.ihe.gazelle.evsclient.application.interfaces.ProcessingCacheManager;
import net.ihe.gazelle.evsclient.application.job.ExtensionJob;
import net.ihe.gazelle.evsclient.application.preferences.ApplicationConfiguration;
import net.ihe.gazelle.evsclient.application.security.ApiKeyManager;
import net.ihe.gazelle.evsclient.application.statistics.IpCountryProvider;
import net.ihe.gazelle.evsclient.application.statistics.IpLocService;
import net.ihe.gazelle.evsclient.application.statistics.StatisticsManager;
import net.ihe.gazelle.evsclient.application.validation.ValidationCacheManagerImpl;
import net.ihe.gazelle.evsclient.application.validation.ValidationServiceFacade;
import net.ihe.gazelle.evsclient.application.validationservice.configuration.ValidationServiceConfManager;
import net.ihe.gazelle.evsclient.interlay.gui.menu.EVSMenu;
import org.apache.commons.lang.StringUtils;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.*;

@Name("evsApplicationFactory")
@Scope(ScopeType.APPLICATION)
@AutoCreate
public class ApplicationFactory {

    @In(value = "evsCommonApplicationFactory")
    private EvsCommonApplicationFactory evsCommonApplicationFactory;

   @In(value = "evsDaoFactory")
   private DaoFactory daoFactory;

   @In(value = "applicationPreferenceManager", create = true)
   private ApplicationPreferenceManager applicationPreferenceManager;

   @In(value = "oidGeneratorManager", create = true)
   private OidGeneratorManager oidGeneratorManager;

    @In(value = "extensionJob", create = true)
    private ExtensionJob extensionJob;

    private ImplementationFinder finder = new ImplementationFinder("net.ihe.gazelle.evsclient.interlay");


    @Factory( value = "applicationConfiguration", scope =ScopeType.PAGE)
    public ApplicationConfiguration getApplicationConfiguration() {
        return new ApplicationConfiguration(applicationPreferenceManager);
    }

   @Factory(value = "validationServiceFacade", scope = ScopeType.APPLICATION)
    public ValidationServiceFacade getValidationServiceFacade() {
        return new ValidationServiceFacade(daoFactory.getValidationDao(), applicationPreferenceManager,
                oidGeneratorManager, getServiceConfManager(), extensionJob, getIpLocService());
   }

   @Factory(value = "serviceConfManager", scope = ScopeType.APPLICATION)
   public ValidationServiceConfManager getServiceConfManager() {
        return new ValidationServiceConfManager(daoFactory.getServiceConfDao(), applicationPreferenceManager);
   }

   @Factory(value = "apiKeyManager", scope = ScopeType.APPLICATION)
   public ApiKeyManager getApiKeyManager() {
      return new ApiKeyManager(daoFactory.getApiKeyDao());
   }

   @Factory(value = "validationsCacheManager", scope = ScopeType.PAGE)
   public ProcessingCacheManager getValidationsCacheManager() {
      return new ValidationCacheManagerImpl(applicationPreferenceManager, daoFactory.getValidationDao());
   }

    @Factory( value = "evsMenu", scope = ScopeType.APPLICATION)
    public EVSMenu getEVSMenu() {
        return new EVSMenu();
    }

    @Factory( value = "statisticsManager", scope = ScopeType.APPLICATION)
    public StatisticsManager getStatisticsManager() {
        return new StatisticsManager(daoFactory.getValidationDao(), daoFactory.getServiceConfDao(), evsCommonApplicationFactory.getCsvMapper(), getEVSMenu());
    }

    @Factory( value = "ipLocService", scope = ScopeType.APPLICATION)
    public IpLocService getIpLocService() {
        try {
            Class<? extends IpCountryProvider> cls;
            IpCountryProvider provider;
            if (StringUtils.isEmpty(applicationPreferenceManager.getStringValue("ip_loc_provider"))) {
                cls = finder.find(IpCountryProvider.class);
                provider = finder.instantiate(cls);
            } else {
                provider = finder.instantiate(IpCountryProvider.class, applicationPreferenceManager.getStringValue("ip_loc_provider"));
            }
            return new IpLocService(daoFactory.getIpLocDao(),
                    applicationPreferenceManager.getIntegerValue("ip_loc_validity_period"),
                    provider
            );
        } catch (Exception e) {
            return null;
        }
    }
}
