package net.ihe.gazelle.evsclient.interlay.adapters.validationservice;

import net.ihe.gazelle.evsclient.application.validation.ValidationReportMapper;
import net.ihe.gazelle.evsclient.domain.validation.report.*;
import net.ihe.gazelle.evsclient.domain.validationservice.VersionedValidationService;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;

import javax.xml.xpath.XPathExpressionException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class DcmcheckReportBuilder extends DicomReportBuilder {

    private Pattern metadatum = Pattern.compile("^(?:--|\\*\\*\\s*)(.*?)(?:\\:| \\(C\\))\\s+(.*)");
    private Pattern subreport = Pattern.compile("^(?:\\*\\*\\s*)(.*?)\\s+\\*\\*$");
    private Pattern errors = Pattern.compile("^\\s*(?i)error");
    private Pattern warnings = Pattern.compile("^\\s*(?i)warn");
    private Pattern notificationDetails = Pattern.compile("^(?:\\s+)(.*?)$");

    public DcmcheckReportBuilder() {
    }

    <S extends VersionedValidationService> ValidationReport build(S service, String validator, String version, String result) {

        Iterator<String> lines = Arrays.asList(result.split("\n")).iterator();

        ValidationReport report = new ValidationReport(
                UUID.randomUUID().toString(),
                new ValidationOverview(
                        ValidationReportMapper.DEFAULT_DISCLAIMER,
                        service.getName(),
                        service.getServiceVersion(),
                        validator,
                        version,
                        parseMetadata(lines)));

        parseDocument(report, lines);

        return report;

    }

    private List<Metadata> parseMetadata(Iterator<String> lines) {
        List<Metadata> metadata = new ArrayList<>();
        boolean endOfMetadata = false;
        while(lines.hasNext()&&!endOfMetadata) {
            String line = lines.next();
            Matcher m = metadatum.matcher(line);
            if (m.matches()) {
                metadata.add(new Metadata(m.group(1),m.group(2)));
            }
            endOfMetadata = StringUtils.isEmpty(line);
        }
        return metadata;
    }

    private String parseSubReportName(Iterator<String> lines) {
        boolean endOfSubReportName = false;
        StringBuilder sb = new StringBuilder();
        while(lines.hasNext()&&!endOfSubReportName) {
            String line = lines.next();
            Matcher m = subreport.matcher(line);
            if (m.matches()) {
                sb.append(m.group(1)).append(" ");
            }
            endOfSubReportName = StringUtils.isEmpty(line);
        }
        return sb.toString().replaceFirst("\\s$","");
    }

    private boolean parseNotification(ValidationSubReport vsr, Iterator<String> lines) {
        boolean endOfNotification = false;
        String line = null;
        while (lines.hasNext() && !endOfNotification) {
            line = lines.next();
            while (lines.hasNext() && StringUtils.isEmpty(line)) {
                line = lines.next();
            }
            endOfNotification = StringUtils.isEmpty(line)||line.matches("^\\*+$");
            if (!endOfNotification) {
                String id = line;
                String details = parseNotificationDetails(lines);
                if (errors.matcher(line).find()) {
                    vsr.addConstraintValidation(new ConstraintValidation(parseConstraintId(id),
                            parseConstraintType(details),
                            parseConstraintDescription(details),
                            parseFormalExpression(details),
                            parseLocationInValidatedObject(details),
                            parseValueInValidatedObject(details),
                            parseAssertionIDs(details),
                            parseUnexpectedErrors(details),
                            ConstraintPriority.MANDATORY, ValidationTestResult.FAILED));
                } else if (warnings.matcher(line).find()) {
                    vsr.addConstraintValidation(new ConstraintValidation(parseConstraintId(id),
                            parseConstraintType(details),
                            parseConstraintDescription(details),
                            parseFormalExpression(details),
                            parseLocationInValidatedObject(details),
                            parseValueInValidatedObject(details),
                            parseAssertionIDs(details),
                            parseUnexpectedErrors(details),
                            ConstraintPriority.RECOMMENDED, ValidationTestResult.FAILED));
                } else {
                    vsr.addConstraintValidation(new ConstraintValidation(parseConstraintId(id),
                            parseConstraintType(details),
                            parseConstraintDescription(details),
                            parseFormalExpression(details),
                            parseLocationInValidatedObject(details),
                            parseValueInValidatedObject(details),
                            parseAssertionIDs(details),
                            parseUnexpectedErrors(details),
                            ConstraintPriority.PERMITTED, ValidationTestResult.PASSED));
                }
            }

        }
        return line==null?true:line.matches("^\\*+$");
    }

    private List<UnexpectedError> parseUnexpectedErrors(String details) {
        return null; // TODO
    }

    private List<String> parseAssertionIDs(String details) {
        return null; // TODO
    }

    private String parseValueInValidatedObject(String details) {
        return null; // TODO
    }

    private String parseLocationInValidatedObject(String details) {
        return null; // TODO
    }

    private String parseFormalExpression(String details) {
        return null; // TODO
    }

    private String parseConstraintDescription(String details) {
        return StringEscapeUtils.escapeHtml(details);
    }

    private String parseConstraintId(String id) {
        return StringEscapeUtils.escapeHtml(id.replaceFirst("^\\w+\\s+",""));
    }

    private String parseConstraintType(String details) {
        return null;
    }

    private String parseNotificationDetails(Iterator<String> lines) {
        boolean endOfNotification = false;
        String line = null;
        StringBuilder sb = new StringBuilder();
        while (lines.hasNext() && !endOfNotification) {
            line = lines.next();
            Matcher m = notificationDetails.matcher(line);
            if (m.matches()) {
                sb.append(m.group(1)).append("\n");
            }
            endOfNotification = StringUtils.isEmpty(line);
        }
        return sb.toString();
    }

    private void parseDocument(ValidationReport report, Iterator<String> lines) {
        while (lines.hasNext()) {
            ValidationSubReport vsr = new ValidationSubReport(
                    parseSubReportName(lines),
                    new ArrayList<String>()
            );
            boolean endOfSubReport = false;
            while (lines.hasNext()&&!endOfSubReport) {
                endOfSubReport = parseNotification(vsr,lines);
            }
            if (vsr.getConstraints().isEmpty()) {
                vsr.addConstraintValidation(new ConstraintValidation("checked",ConstraintPriority.PERMITTED, ValidationTestResult.PASSED));
            }
            report.addSubReport(vsr);
        }
    }
}
