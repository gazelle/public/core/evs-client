/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.ihe.gazelle.evsclient.domain.validationservice.configuration;

import net.ihe.gazelle.evsclient.domain.validationservice.configuration.stylesheets.NamedStylesheet;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * <b>Class Description : </b>ReferencedStandard<br>
 * <br>
 * This class describes the ReferencedStandard object. ReferencedStandard describes the standard that can be validated using such or such validation service. Examples are HL7v2.4 or HL7v2.3.1 or HL7V3
 * <p/>
 * Attributes of this object are:
 * <ul>
 * <li><b>id</b> : id of the object in the database</li>
 * <li><b>name</b> : name of the standard (eg. HL7)</li>
 * <li><b>extension</b> : possible extension (None if it is not defined)</li>
 * <li><b>description</b> : short description of the standard</li>
 * <li><b>label</b> : label that will be displayed in the user interface (namevversion if not defined)</li>
 * <li><b>validationServices</b> : the list of validation services that can be used to validated this standard</li>
 * </ul>
 *
 * @author Anne-Gaelle Berge / INRIA Rennes IHE development Project
 * @version 1.0 - 2010, June 10th
 */

@Entity
@Table(name = "evsc_referenced_standard", schema = "public")
@SequenceGenerator(name = "processing_conf_sequence", sequenceName = "evsc_processing_conf_id_seq", allocationSize = 1)
public class ReferencedStandard extends CompositeProcessingConf<ValidationServiceConf> implements Serializable, Comparable<ReferencedStandard> {
    private static final long serialVersionUID = 7296266536938828798L;

    @Column(name = "validator_filter")
    private String validatorFilter;

    @Column(name = "icon_style_class")
    private String iconStyleClass;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "referenced_standard_id")
    private List<NamedStylesheet> objectStylesheets = new ArrayList<>();


    /**
     * Constructor
     */
    public ReferencedStandard() {
        super();
    }

    public String getValidatorFilter() {
        return validatorFilter;
    }

    public void setValidatorFilter(String validatorFilter) {
        this.validatorFilter = validatorFilter;
    }

    public String getIconStyleClass() {
        return iconStyleClass;
    }

    public void setIconStyleClass(String iconStyleClass) {
        this.iconStyleClass = iconStyleClass;
    }

    public List<NamedStylesheet> getObjectStylesheets() {
        return objectStylesheets;
    }

    public void setObjectStylesheets(List<NamedStylesheet> objectStylesheets) {
        this.objectStylesheets = objectStylesheets;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReferencedStandard that = (ReferencedStandard) o;
        return Objects.equals(id, that.id) && Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }

    @Override
    public int compareTo(final ReferencedStandard o) {
        if (this.getValidatorFilter() != null && o != null && o.getValidatorFilter() != null) {
            return this.getValidatorFilter().compareTo(o.getValidatorFilter());
        } else {
            return 1;
        }
    }

    public List<ValidationServiceConf> getValidationServiceConfs() {
        return getConfigurations();
    }
}
