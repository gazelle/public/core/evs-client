package net.ihe.gazelle.evsclient.interlay.gui.validation;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.application.validation.ValidationServiceFacade;
import net.ihe.gazelle.evsclient.interlay.gui.I18n;

public class ValidationBrowserBeanGui extends AbstractValidationBrowserBeanGui {

    public ValidationBrowserBeanGui(final ValidationServiceFacade validationServiceFacade, ApplicationPreferenceManager applicationPreferenceManager) {
        super(validationServiceFacade, applicationPreferenceManager);
    }

    public String getLogsPageTitle() {
        return I18n.get("net.ihe.gazelle.evs.Validations", getReferencedStandardName());
    }

}
