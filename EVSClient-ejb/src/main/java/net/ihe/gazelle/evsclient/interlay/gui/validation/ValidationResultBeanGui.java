package net.ihe.gazelle.evsclient.interlay.gui.validation;

import gnu.trove.map.hash.THashMap;
import net.ihe.gazelle.callingtools.common.model.CallingTool;
import net.ihe.gazelle.com.templates.Template;
import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.common.report.ReportExporterManager;
import net.ihe.gazelle.common.tree.GazelleTreeNodeImpl;
import net.ihe.gazelle.evsclient.application.AbstractGuiPermanentLink;
import net.ihe.gazelle.evsclient.application.CallerMetadataFactory;
import net.ihe.gazelle.evsclient.application.interfaces.ForbiddenAccessException;
import net.ihe.gazelle.evsclient.application.interfaces.NotFoundException;
import net.ihe.gazelle.evsclient.application.interfaces.UnauthorizedException;
import net.ihe.gazelle.evsclient.application.validation.ReportTransformationException;
import net.ihe.gazelle.evsclient.application.validation.ValidationServiceFacade;
import net.ihe.gazelle.evsclient.application.validationservice.configuration.ServiceConfNotFoundException;
import net.ihe.gazelle.evsclient.domain.processing.OtherCallerMetadata;
import net.ihe.gazelle.evsclient.domain.validation.Validation;
import net.ihe.gazelle.evsclient.domain.validation.ValidationStatus;
import net.ihe.gazelle.evsclient.domain.validation.report.SeverityLevel;
import net.ihe.gazelle.evsclient.domain.validation.report.ValidationReport;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.ReferencedStandard;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.ValidationServiceConf;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.stylesheets.NamedStylesheet;
import net.ihe.gazelle.evsclient.interlay.dao.FileReadException;
import net.ihe.gazelle.evsclient.interlay.dto.rest.validation.report.ValidationReportDTO;
import net.ihe.gazelle.evsclient.interlay.dto.view.validation.ValidationResultDto;
import net.ihe.gazelle.evsclient.interlay.gui.*;
import net.ihe.gazelle.evsclient.interlay.gui.document.HandledDocument;
import net.ihe.gazelle.evsclient.interlay.gui.document.InvalidStylesheetException;
import net.ihe.gazelle.evsclient.interlay.gui.document.ReportConverter;
import net.ihe.gazelle.evsclient.interlay.gui.document.StyledDocumentRenderer;
import net.ihe.gazelle.evsclient.interlay.util.SyntaxDetector;
import net.ihe.gazelle.evsclient.interlay.util.SyntaxDetectorTika;
import net.ihe.gazelle.gen.common.DetailedResultMarshaller;
import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import net.ihe.gazelle.ssov7.gum.client.application.UserAttributeCommon;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import net.ihe.gazelle.validation.DetailedResult;
import org.apache.commons.lang.StringUtils;
import org.jboss.seam.Component;
import org.jboss.seam.core.ResourceBundle;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.LocaleSelector;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.faces.model.SelectItemGroup;
import javax.xml.bind.JAXBException;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;
import java.util.*;


public class ValidationResultBeanGui extends AbstractValidationProcessingBeanGui<Validation, ValidationServiceFacade>
        implements UserAttributeCommon {

    private static final Logger LOG = LoggerFactory.getLogger(ValidationResultBeanGui.class);
    public static final String EYE_NAVIGATION_ICON_REGEX = "<a[\\s]*.*gotoNode[^>]*>((?:.(?!</a>))*.)</a>";

    /* CHECK USELESSNESS */

    private String selectedStylesheetName;


    /* USED */

    private Template selectedTemplate;
    private boolean showTemplates;
    //FIXME class DetailedResult should not be used in GUI. Only the original report content as byte[] is allowed for
    // XSL transformation. Validation
    // information should be obtained from classes Validation and ValidationReport
    private DetailedResult detailedResult;
    private String detailedResultString;

    private ValidationResultDto validationResultDto;

    private Integer referencedStandardId;
    private List<SelectItem> options;

    private String current;
    private boolean singleMode;
    private List<ReferencedStandard> standards;
    private Map<String, NamedStylesheet> namedStylesheets;
    private Integer stylesheetId;
    private TemplateAchitecture templateArchitecture;
    private final RemotePermanentLinkSender remotePermanentLinkSender;

    private Reporter reporter;
    private String transformedResult;

    private boolean originalReportDownloadable = false;

    private final SyntaxDetector syntaxDetector;
    private final UserService userService = (UserService) Component.getInstance("gumUserService");


    public ValidationResultBeanGui(ValidationServiceFacade validationServiceFacade,
                                   CallerMetadataFactory callerMetadataFactory,
                                   GazelleIdentity identity, ApplicationPreferenceManager applicationPreferenceManager) {
        super(ValidationResultBeanGui.class, validationServiceFacade, callerMetadataFactory, identity, applicationPreferenceManager, null);
        this.permanentLinkGui = new AbstractGuiPermanentLink<Validation>(applicationPreferenceManager) {
            @Override
            public String getResultPageUrl() {
                try {
                    return Pages.valueOf(ValidationResultBeanGui.this.getValidationType().toUpperCase(Locale.ROOT) + "_RESULT").getMenuLink();
                } catch (Exception e) {
                    return Pages.DETAILED_RESULT.getMenuLink();
                }
            }
        };
        this.remotePermanentLinkSender = new RemotePermanentLinkSender(this);
        this.labelizer = new Labelizer("net.ihe.gazelle.evs.");
        this.reporter = new Reporter(labelizer, Validation.class);
        this.syntaxDetector = new SyntaxDetectorTika();

    }

    public void init(final ValidationResultDto validationResultDto, Integer referencedStandardId) {
        this.validationResultDto = validationResultDto;
        this.referencedStandardId = referencedStandardId;
        setSelectedObject(validationResultDto.getValidation());
        showTemplates = false;
        selectedTemplate = null;
        transformedResult = null;
        if (validationResultDto.isValidationDone()) {
            if (selectedObject != null && StringUtils.isNotEmpty(validationResultDto.getValidationType())) {
                initReports();
            } else {
                FacesMessages.instance()
                        .addFromResourceBundle(StatusMessage.Severity.ERROR, "net.ihe.gazelle.evs.xml.noObjectOIDProvided");
                setSelectedObject(null);
            }
        }
        templateArchitecture = new TemplateAchitecture();
      /*
      // TODO add StyledDocumentRenderer as normal renderer
      try {
         StyledDocumentRenderer r = getPresenter().getRenderer(StyledDocumentRenderer.class);
         r.setRenderingModes(getReferencedStandardStylesheets());
      } catch (NoSuchMethodException e) {
         e.printStackTrace();
      } catch (InvocationTargetException e) {
         e.printStackTrace();
      } catch (InstantiationException e) {
         e.printStackTrace();
      } catch (IllegalAccessException e) {
         e.printStackTrace();
      }
      */
    }

    public String getSelectedStylesheetName() {
        return selectedStylesheetName;
    }

    public void setSelectedStylesheetName(String selectedStylesheetName) {
        this.selectedStylesheetName = selectedStylesheetName;
    }

    public String getValidationServiceName() {
        return selectedObject.getValidationService().getName();
    }

    public String getValidationServiceVersion() {
        return selectedObject.getValidationService().getVersion();
    }

    public String getValidationProfileId() {
        return selectedObject.getValidationService().getValidatorKeyword();
    }

    public String getValidationProfileVersion() {
        return selectedObject.getValidationService().getValidatorVersion();
    }

    private Validation getCurrentValidation() {
        return selectedObject;
    }

    public boolean sendBackResultToTool() {
        if (selectedObject == null || !(selectedObject.getCaller() instanceof OtherCallerMetadata)) {
            return false;
        } else {
            return (!CallingTool.toolIsTM(((OtherCallerMetadata) selectedObject.getCaller()).getToolOid())
                    || ((OtherCallerMetadata) selectedObject.getCaller()).getToolObjectId().startsWith("sample_"));
        }
    }

    public boolean displayInfoPopup() {
        return selectedObject != null && selectedObject.getCaller() instanceof OtherCallerMetadata
                && ((OtherCallerMetadata) selectedObject.getCaller()).getToolObjectId().startsWith("stepdata_");
    }

    public String getValidatorName() {
        return validationResultDto.getValidatorName();
    }

    public String getValidationType() {
        return validationResultDto.getValidationType();
    }

    public DetailedResult getDetailedResult() {
        return detailedResult;
    }

    @Override
    public void initFromUrl() throws UnauthorizedException, NotFoundException, ForbiddenAccessException {
        try {
            super.initFromUrl();
        } catch (NotFoundException nfe) {
            GuiMessage.logMessage(StatusMessage.Severity.ERROR, "net.ihe.gazelle.evs.invalid-oid");
            throw nfe;
        } catch (UnauthorizedException | ForbiddenAccessException e) {
            GuiMessage.logMessage(StatusMessage.Severity.ERROR, "net.ihe.gazelle.evs.access-error");
            throw e;
        }
        final Map<String, String> urlParams = FacesContext.getCurrentInstance().getExternalContext()
                .getRequestParameterMap();
        if (urlParams != null && !urlParams.isEmpty()) {
            if (urlParams.containsKey(QueryParamEvs.REF_STANDARD_ID)) {
                this.referencedStandardId = Integer.valueOf(urlParams.get(QueryParamEvs.REF_STANDARD_ID));
            }
            if (urlParams.containsKey(QueryParamEvs.STYLESHEET_ID)) {
                this.stylesheetId = Integer.valueOf(urlParams.get(QueryParamEvs.STYLESHEET_ID));
            }
        }
        if (selectedObject != null) {
            validationResultDto = new ValidationResultDto(selectedObject, true, selectedObject.getValidationType().name(),
                    selectedObject.getValidationService().getValidatorKeyword());
            initReports();
        }
    }

    private void initReports() {
        try {
            Object r = processingManager.asOriginalReport(getCurrentValidation().getOriginalReport(),
                    getCurrentValidation().getValidationService());
            if(r != null){
                this.originalReportDownloadable = true;
                if (r instanceof ValidationReport) {
                    this.reporter.setValidationReport(new ValidationReportDTO((ValidationReport) r, SeverityLevel.INFO));
                } else {
                    if ( r instanceof DetailedResult ) {
                        detailedResult = (DetailedResult) r;
                    }
                    this.reporter.setValidationReport(new ValidationReportDTO((ValidationReport) selectedObject.getValidationReport().getContent(), SeverityLevel.INFO));
                    this.transformedResult = getTransformedDetailedResult();
                }
            }
        } catch (Exception e) {
            if (!ReportTransformationException.class.isInstance(e)
                    || !ValidationStatus.DONE_UNDEFINED.equals(selectedObject.getValidationStatus())) {
                setSelectedObject(null);
                FacesMessages.instance()
                        .addFromResourceBundle("net.ihe.gazelle.evs.xml.ObjectTypeInvalid",
                                validationResultDto.getValidationType());
            } else {
                this.reporter.setValidationReport(new ValidationReportDTO((ValidationReport) selectedObject.getValidationReport().getContent(), SeverityLevel.INFO));
            }
        }
    }

    @Override
    protected String getValidatorUrl() {
        return null;
    }

    public String performAnotherValidation() {
        GuiComponentType componentType = GuiComponentType.valueOf(getValidationType());
        return componentType.getValidatorView() +
                "?" + QueryParamEvs.REF_STANDARD_ID + "=" + referencedStandardId;
    }

    @Override
    public String revalidate() {
        GuiComponentType componentType = GuiComponentType.valueOf(getValidationType());
        return componentType.getValidatorView() +
                "?" + QueryParamEvs.REF_STANDARD_ID + "=" + referencedStandardId +
                "&" + QueryParam.PROCESSING_OID + "=" + selectedObject.getOid();
    }

    public String getValidationStatus() {
        return getCurrentValidation().getValidationStatus().name();
    }

    public void downloadValidatedObject() {
        try {
            downloadFileContent("text/xml");
        } catch (IOException e) {
            LOG.error("Can not download the file for the object " + selectedObject.getOid(), e);
            GuiMessage.logMessage(StatusMessage.Severity.ERROR, "Can not download the file : " + e.getMessage());
        }
    }

    private void downloadFileContent(String mimeType) throws IOException {
        ReportExporterManager.exportToFile(mimeType, selectedObject.getObject().getContent(),
                selectedObject.getObject().getOriginalFileName());
    }

    public void downloadResultAsXml() {
        if (getCurrentValidation().getOriginalReport() != null) {
            byte[] content = getCurrentValidation().getOriginalReport().getContent();
            SyntaxDetector.Syntax syntax = syntaxDetector.detectSyntax(content);
            String resultFileName = "originalReport-" + getCurrentValidation().getOid() + syntax.getExtension();
            ReportExporterManager.exportToFile(null, content, resultFileName);
        } else {
            GuiMessage.logMessage(StatusMessage.Severity.ERROR, "Unable to download, missing original XML report.");
        }
    }

    public boolean isOriginalReportDownloadable(){
        return originalReportDownloadable;
    }

    public void shareResult() {
        processingManager.shareResult(selectedObject);
    }

    public String getDefaultTab() {
        if (selectedObject.getValidationService() != null) {
            return selectedObject.getValidationService().getName();
        }
        return "";
    }

    public String getTransformedDetailedResult() {
        if (selectedObject == null || selectedObject.getOriginalReport() == null || detailedResult == null) {
            return "";
        }
        if (transformedResult != null) {
            return transformedResult;
        }
        try {
            String detailedResultString = new String(selectedObject.getOriginalReport().getContent(),
                    StandardCharsets.UTF_8);
            String xsl = null;
            if (selectedObject.getValidationService() != null) {
                xsl = getXSLPath(selectedObject.getValidationService().getName());
            }
            if (StringUtils.isNotEmpty(detailedResultString)) {
                if (detailedResultString.contains("?>")) {
                    detailedResultString = detailedResultString.substring(detailedResultString.indexOf("?>") + 2);
                }
                return ReportConverter.transformXMLStringToHTML(detailedResultString, xsl,
                        LocaleSelector.instance().getLanguage());
            }
        } catch (InvalidStylesheetException e) {
            GuiMessage.logMessage(StatusMessage.Severity.ERROR,
                    MessageFormat.format("invalid-stylesheet {0}", e.getMessage()));
        } catch (FileReadException e) {
            GuiMessage.logMessage(StatusMessage.Severity.ERROR,
                    MessageFormat.format("cant-load-validation-report-content {0}", e.getMessage()));
        }
        return null;
    }

    public String getTransformedDetailedResult(boolean eyeNavigatorEnabled) {
        String output = getTransformedDetailedResult();
        if (eyeNavigatorEnabled()) {
            if (!eyeNavigatorEnabled && output != null) {
                String message = I18n.get("net.ihe.gazelle.evs.EnablePrettyViewToUseTheNavigator");
                return output.replaceAll(EYE_NAVIGATION_ICON_REGEX,
                        "<a title=\"" + message + "\" ><span style=\"color: gray; cursor: not-allowed; \" class=\"gzl-icon-eye\"></span></a>");
            }
        } else {
            return output.replaceAll(EYE_NAVIGATION_ICON_REGEX, "");
        }
        return output;
    }

    public String renderValidationReport() {
        if (selectedObject == null || selectedObject.getValidationReport() == null) {
            return "";
        }
        return this.reporter.renderXMLReport(selectedObject.getValidationReport().getContent());
    }


    private String getXSLPath(String serviceName) {
        try {
            ValidationServiceConf avsc =
                    processingManager.getValidationServiceByKeyword(serviceName);
            return avsc.getValidationReportType().getStylesheet().getLocation();
        } catch (ServiceConfNotFoundException e) {
            String message = MessageFormat.format("Unable to retrieve " +
                    "configuration and stylesheet for service {0} : {1}", serviceName, e.getMessage());
            GuiMessage.logMessage(StatusMessage.Severity.ERROR, message);
            LOG.error(message);
            return null;
        }
    }

    public List<SelectItem> getReferencedStandardStylesheets() {
        if (options == null) {
            options = new ArrayList<>();
            initNamedStylesheets(selectedObject.getValidationService().getName());
            if (standards != null && !standards.isEmpty()) {
                SelectItem rootItem = new SelectItem();
                rootItem.setValue(null);
                rootItem.setNoSelectionOption(true);
                rootItem.setDisabled(true);
                rootItem.setLabel(ResourceBundle.instance().getString("gazelle.common.PleaseSelect"));
                options.add(rootItem);
                for (ReferencedStandard standard : standards) {
                    String filter = " (" + standard.getValidatorFilter() + ")";
                    SelectItemGroup group = new SelectItemGroup(
                            standard.getName() + (standard.getName().endsWith(filter) ? "" : filter));
                    List<SelectItem> selectItems = new ArrayList<>();
                    for (NamedStylesheet stylesheet : standard.getObjectStylesheets()) {
                        SelectItem selectItem = new SelectItem();
                        selectItem.setValue(stylesheet.getName());
                        selectItem.setLabel(stylesheet.getName());
                        selectItems.add(selectItem);
                    }
                    if (!selectItems.isEmpty()) {
                        group.setSelectItems(selectItems.toArray(new SelectItem[]{}));
                        options.add(group);
                    }
                }
            }
        }
        return options;
    }

    private Map<String, NamedStylesheet> initNamedStylesheets(String serviceName) {
        if (namedStylesheets == null) {
            try {
                standards = processingManager.getReferencedStandardsByValidationServiceConf(
                        processingManager.getValidationServiceByKeyword(serviceName));
            } catch (ServiceConfNotFoundException e) {
                String message = MessageFormat.format("Unable to retreive " +
                        "configuration for service {0} : {1}", serviceName, e.getMessage());
                GuiMessage.logMessage(StatusMessage.Severity.ERROR, message);
                LOG.error(message);
            }
            if (standards != null && !standards.isEmpty()) {
                namedStylesheets = new THashMap<>();
                for (ReferencedStandard standard : standards) {
                    for (NamedStylesheet stylesheet : standard.getObjectStylesheets()) {
                        namedStylesheets.put(stylesheet.getName(), stylesheet);
                    }
                }
            }
        }
        return namedStylesheets;
    }


    public String redirectToPrettyXmlContent() {
        StringBuilder stringBuilder = new StringBuilder(Pages.PRETTY_RESULT.getMenuLink()
                + "?" + QueryParam.PROCESSING_OID + "=" + selectedObject.getOid()
                + "&" + QueryParamEvs.VALIDATOR_TYPE + "=" + getValidationType()
                + "&" + QueryParamEvs.STYLESHEET_ID + "=" + getSelectedStylesheetId());
        if (selectedObject.getPrivacyKey() != null) {
            stringBuilder.append("&").append(QueryParam.PRIVACY_KEY).append("=").append(selectedObject.getPrivacyKey());
        }
        return stringBuilder.toString();
    }

   public String getPrettyXmlContentUrl() {
      return getAppUrlWithoutLastSlash() + redirectToPrettyXmlContent();
   }

   private Integer getSelectedStylesheetId() {
      if (stylesheetId != null) {
         return stylesheetId;
      }
      NamedStylesheet s = getSelectedStylesheet();
      if (s != null) {
         return s.getId();
      }
      return null;
   }

    private NamedStylesheet getSelectedStylesheet() {
        if (namedStylesheets == null && stylesheetId != null) {
            initNamedStylesheets(selectedObject.getValidationService().getName());
            for (NamedStylesheet stylesheet : namedStylesheets.values()) {
                if (Objects.equals(stylesheetId, stylesheet.getId())) {
                    selectedStylesheetName = stylesheet.getName();
                    return stylesheet;
                }
            }
        }
        if (namedStylesheets != null && selectedStylesheetName != null) {
            return namedStylesheets.get(selectedStylesheetName);
        }
        return null;
    }

    public Boolean getShowTemplates() {
        return showTemplates;
    }

    public void setShowTemplates(Boolean showTemplates) {
        this.showTemplates = showTemplates;
    }

    // FIXME this is specific CDA MBV. It should be moved in a child class GOCValidationResultBeanGui
    public boolean isTemplateTreeAvailable() {
        return (detailedResult != null && detailedResult.getTemplateDesc() != null);
    }

    public String getPrettyXmlContent() {
        if (this.selectedObject == null) {
            return "";
        }
        NamedStylesheet stylesheet = this.getSelectedStylesheet();
        if (stylesheet != null
                && stylesheet.getLocation() != null
                && stylesheet.getName() != null) {
            return new StyledDocumentRenderer(stylesheet.getLocation()).render(document);
        }
        return null;
    }

    public GazelleTreeNodeImpl<Template> getListTemplatesArchitecture() {
        GazelleTreeNodeImpl<Template> temp = new GazelleTreeNodeImpl<>();
        if (detailedResult == null) {
            return null;
        }
        for (ReferencedStandard standard : standards) {
            for (NamedStylesheet stylesheet : standard.getObjectStylesheets()) {
                temp =  templateArchitecture.getListTemplatesArchitecture(detailedResult);
            }
        }
        return temp;
    }


    public String getIconClassForTemplate(final Template template) {
        return templateArchitecture.getIconClassForTemplate(template);
    }

    public void sendPermaLink() {
        if (this.callerPrivacyKey != null && !this.callerPrivacyKey.isEmpty()) {
            Map<String, String> params = new THashMap<>();
            params.put(QueryParam.CALLER_PRIVACY_KEY, this.callerPrivacyKey);
            remotePermanentLinkSender.sendPermaLink(params);
        } else {
            remotePermanentLinkSender.sendPermaLink();
        }
    }

    public boolean showStyledReport() {
        return getTransformedDetailedResult() != null
                && !getTransformedDetailedResult().isEmpty()
                && this.applicationPreferenceManager.getBooleanValue("show_styled_report");
    }

    public boolean showXmlReport() {
        return this.applicationPreferenceManager.getBooleanValue("show_xml_report");
    }

    public boolean showAnalyzer() {
        return this.applicationPreferenceManager.getBooleanValue("show_message_analyzer");
    }

    public Reporter getReporter() {
        return reporter;
    }

    public String getLabel() {
        return reporter.getLabel();
    }

    public String getId() {
        return reporter.getId();
    }

    public String getCode() {
        return reporter.getCode();
    }

    public String getLabel(String value, Object... params) {
        return reporter.getLabel(value, params);
    }

    public String getLabel(String value) {
        return reporter.getLabel(value);
    }

    public String getCode(String value) {
        return reporter.getCode(value);
    }


    public void downloadReportAsPdf() {
        StringBuilder content = new StringBuilder();
        String reportSource = "EVSClientReport";
        String detailedResultStringReport = getDetailedResultString();
        content.append(detailedResultStringReport);
        Map<String, Object> parameters = initParams();
        // We make the validation report neutral so that the same jasper report can be used for the result transformation into pdf.
        String xmlReport = content.toString()
                .replaceAll("SchematronValidation", "Validation")
                .replaceAll("MDAValidation", "Validation");
        String subdirReportsPath = applicationPreferenceManager.getGazelleReportsPath();
        reportSource = subdirReportsPath + File.separatorChar + reportSource;
        String fileNameDestination = "report.pdf";
        ReportExporterManager.exportFromXmlToPDF(reportSource, subdirReportsPath, xmlReport, fileNameDestination, parameters);


    }

    public String getAppUrlWithoutLastSlash() {
        String appUrl = applicationPreferenceManager.getApplicationUrl();
        if (appUrl.endsWith("/")) {
            appUrl = appUrl.substring(0, appUrl.length() - 1);
        }
        return appUrl;
    }


    public Map<String, Object> initParams() {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("APPLICATION_URL", getAppUrlWithoutLastSlash());
        parameters.put("VALIDATION_TYPE", this.selectedObject.getValidationType().name());
        parameters.put("OID", this.selectedObject.getOid());
        parameters.put("VALIDATED_FILE_NAME", this.selectedObject.getValidationReport().getContent()
                .getValidationOverview().getValidationOverallResult().name());
        return parameters;
    }

    public String getDetailedResultString() {
        if (detailedResultString == null) {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            try {
                DetailedResultMarshaller.save(outputStream, detailedResult);
                detailedResultString = outputStream.toString("UTF-8");
            } catch (JAXBException | UnsupportedEncodingException e) {
                e.printStackTrace();
                return "";
            }
        }
        return detailedResultString;
    }

    public int getCurrentDocumentLength() {
        return document.getContent().length;
    }

    public float getDocumentSize() {
        int doc = getCurrentDocumentLength();
        return (doc / 1048576);
    }

    public boolean isDocumentTooLarge() {
        return getCurrentDocumentLength() > 5000000;
    }

    public boolean eyeNavigatorEnabled() {
        return SyntaxDetector.Syntax.XML.equals(super.document.getSyntax());
    }

    @Override
    public String getUserName(String userID) {
        return userService.getUserDisplayNameWithoutException(userID);
    }
}
