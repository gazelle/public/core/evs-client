package net.ihe.gazelle.evsclient.application.statistics;

import net.ihe.gazelle.evsclient.application.interfaces.ExportMapper;
import net.ihe.gazelle.evsclient.application.interfaces.GuiPermanentLink;
import net.ihe.gazelle.evsclient.application.validation.ValidationDao;
import net.ihe.gazelle.evsclient.application.validationservice.configuration.ValidationServiceConfDao;
import net.ihe.gazelle.evsclient.domain.processing.EntryPoint;
import net.ihe.gazelle.evsclient.domain.statistics.IntervalForStatistics;
import net.ihe.gazelle.evsclient.domain.statistics.StatisticsDataRow;
import net.ihe.gazelle.evsclient.domain.validation.Validation;
import net.ihe.gazelle.evsclient.domain.validation.ValidationStatus;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.ReferencedStandard;
import net.ihe.gazelle.evsclient.interlay.gui.menu.EVSMenu;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class StatisticsManager {

    private final ValidationDao validationDao;

    private final ValidationServiceConfDao validationServiceConfDao;

    private final ExportMapper csvExportMapper;

    private final EVSMenu evsMenu;

    public StatisticsManager(ValidationDao validationDao, ValidationServiceConfDao validationServiceConfDao, ExportMapper csvExportMapper, EVSMenu evsMenu) {
        this.validationDao = validationDao;
        this.csvExportMapper = csvExportMapper;
        this.validationServiceConfDao = validationServiceConfDao;
        this.evsMenu = evsMenu;
    }

    public int countAll(){
        return validationDao.countAll();
    }

    public int countByEntryPoint(EntryPoint entryPoint) {
        return validationDao.countByEntryPoint(entryPoint);
    }

    public int countByValidationStatus(ValidationStatus validationStatus) {
        return validationDao.countByValidationStatus(validationStatus);
    }

    public StatisticsDataRow countTotalDataRow(){
        return validationDao.countTotalDataRow();
    }

    public List<StatisticsDataRow> countTotalDataRowGroupByReferencedStandard(){
        return validationDao.countTotalDataRowGroupByReferencedStandard();
    }

    public List<Validation> findAllValidationByStandardId(int id) {
        return validationDao.findAllValidationByStandardId(id);
    }

    public String createCsvData(Date exportStartDate, Date exportEndDate, String[] headersToExport, GuiPermanentLink<Validation> guiPermanentLink) throws Exception {
        List<Validation> validations = findAllValidationByDateBetween(exportStartDate, exportEndDate);
        List<ReferencedStandard> referencedStandards = validationServiceConfDao.getAllReferencedStandard();
        return csvExportMapper.getDataToExport(getDataToExport(validations, referencedStandards, guiPermanentLink), headersToExport);
    }

    private List<Validation> findAllValidationByDateBetween(Date startDate, Date endDate){
        return validationDao.findALlByDateBetween(startDate, endDate);
    }

    private List<Object[]> getDataToExport(List<Validation> validations, List<ReferencedStandard> referencedStandards, GuiPermanentLink<Validation> guiPermanentLink) throws Exception {
        List<Object[]> dataToExport = new ArrayList<>();
        for (Validation validation : validations) {
            dataToExport.add(new Object[]{
                    validation.getOid(),
                    validation.getDate(),
                    getMenusAndStandardsByValidation(validation, referencedStandards),
                    validation.getValidationService().getName(),
                    validation.getValidationService().getVersion(),
                    validation.getValidationService().getValidatorKeyword(),
                    validation.getValidationService().getValidatorVersion(),
                    validation.getValidationStatus().name(),
                    validation.getCaller().getEntryPoint().name(),
                    validation.getOwner() !=null && validation.getOwner().getUsername() !=null ? validation.getOwner().getUsername() : "", // nullable
                    validation.getOwner() !=null && validation.getOwner().getOrganization() !=null ? validation.getOwner().getOrganization() : "", // nullable
                    validation.getSharing().getIsPrivate(),
                    guiPermanentLink.getPermanentLink(validation)
            });
        }
        return dataToExport;
    }

    private String getReferencedStandardNameByIdInList(List<ReferencedStandard> referencedStandards, int id) throws Exception {
        for(ReferencedStandard referencedStandard : referencedStandards){
            if(referencedStandard.getId().equals(id)){
                return referencedStandard.getName();
            }
        }
        throw new Exception("Referenced standard not found while creating export file");
    }

    private String getMenusAndStandardsByValidation(Validation validation, List<ReferencedStandard> referencedStandards) throws Exception {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < validation.getReferencedStandards().size(); i++) {
            String standardName = getReferencedStandardNameByIdInList(referencedStandards, validation.getReferencedStandards().get(i).getReferencedStandardId());
            List<String> menus = evsMenu.getMenuNameByReferencedStandard(validation.getReferencedStandards().get(i).getReferencedStandardId());
            if(menus.isEmpty()){
                builder.append("no active menu" + " - " + standardName);
            } else {
                for (int j = 0; j < menus.size(); j++) {
                    builder.append(menus.get(j) + " - " + standardName);
                    if(j != menus.size() -1){ // append comma to separate menu - standards couples
                        builder.append(", ");
                    }
                }
            }
            if(i != validation.getReferencedStandards().size() -1){ // append comma to separate menu - standards couples
                builder.append(", ");
            }
        }
        return builder.toString();
    }

    public List<Object[]> countPerValidationServiceByReferencedStandardId(int referencedStandardId) {
        return validationDao.countPerValidationServiceByReferencedStandardId(referencedStandardId);
    }

    public List<Object[]> countPerValidatorByReferencedStandardId(int standardId) {
        return validationDao.countPerValidatorByReferencedStandardId(standardId);
    }

    public List<Object[]> countPerStatusByReferencedStandardId(int standardId) {
        List<Object[]> datarows = new ArrayList<>();
        datarows.add(new Object[] {ValidationStatus.DONE_PASSED.name(),
                validationDao.countByValidationStatusAndReferencedStandardId(ValidationStatus.DONE_PASSED, standardId)});
        datarows.add(new Object[] {ValidationStatus.DONE_FAILED.name(),
                validationDao.countByValidationStatusAndReferencedStandardId(ValidationStatus.DONE_FAILED, standardId)});
        datarows.add(new Object[] {ValidationStatus.DONE_UNDEFINED.name(),
                validationDao.countByValidationStatusAndReferencedStandardId(ValidationStatus.DONE_UNDEFINED, standardId)});
        return  datarows;
    }

    public List<Object[]> countPerDateMonthIntervalByReferencedStandardId(int standardId){
        return validationDao.countPerDateIntervalByReferencedStandardId(standardId, IntervalForStatistics.MONTH);
    }

    public List<Object[]> countPerDateYearIntervalByReferencedStandardId(int standardId){
        return validationDao.countPerDateIntervalByReferencedStandardId(standardId, IntervalForStatistics.YEAR);
    }

    public List<Object[]> countPerCountry() {
        return validationDao.countPerCountry();
    }
}
