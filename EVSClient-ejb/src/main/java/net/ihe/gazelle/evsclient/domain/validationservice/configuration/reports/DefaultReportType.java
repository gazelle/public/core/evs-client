package net.ihe.gazelle.evsclient.domain.validationservice.configuration.reports;

public enum DefaultReportType {

    CERTIFICATE_REPORT,
    DC_CHECK_REPORT,
    DCM_CHECK_REPORT,
    DICOM_WEB_REPORT,
    GAZELLE_VALIDATION_REPORT,
    HL7V2_REPORT,
    MBV_REPORT,
    PDF_REPORT,
    SCHEMATRON_VAL_REPORT,
    DEFAULT_REPORT,
    XVAL_REPORT;

}
