package net.ihe.gazelle.evsclient.domain.validationservice;

public class ServiceOperationException extends Exception {
   private static final long serialVersionUID = 957099667243955901L;

   public ServiceOperationException() {
   }

   public ServiceOperationException(String message) {
      super(message);
   }

   public ServiceOperationException(String message, Throwable cause) {
      super(message, cause);
   }

   public ServiceOperationException(Throwable cause) {
      super(cause);
   }
}
