package net.ihe.gazelle.evsclient.interlay.adapters.validation;

import net.ihe.gazelle.evsclient.application.validation.ReportTransformationException;
import net.ihe.gazelle.evsclient.application.validation.ValidationReportMapper;
import net.ihe.gazelle.evsclient.domain.validation.report.*;
import net.ihe.gazelle.hl7.assertion.GazelleHL7Assertion;
import net.ihe.gazelle.hl7.exception.GazelleHL7Exception;
import net.ihe.gazelle.hl7.validator.report.Error;
import net.ihe.gazelle.hl7.validator.report.*;
import net.ihe.gazelle.validation.DetailedResult;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class Hl7ReportMapper extends MbvReportMapper {

   public Hl7ReportMapper() {
      dateFormat = new SimpleDateFormat("yyyy, MM dd");
      timeFormat = new SimpleDateFormat("hh:mm (aa)");
   }

   @Override
   public ValidationReport transform(byte[] originalReport) throws ReportTransformationException {
      HL7v2ValidationReport parsedOriginalReport = parseAsHL7v2Report(originalReport);
      ValidationReport outputValidationReport = new ValidationReport(generateUuid(),
            buildOverview(parsedOriginalReport));
      outputValidationReport.addSubReport(parseValidationResults(parsedOriginalReport));
      return outputValidationReport;
   }

   @Override
   public DetailedResult parseOriginalReport(byte[] originalReport) throws ReportTransformationException {
      return new HL7v2ToDetailedResultMapper().buildDetailedResult(parseAsHL7v2Report(originalReport));
   }

   public HL7v2ValidationReport parseAsHL7v2Report(byte[] originalReport) throws ReportTransformationException {
      if (originalReport != null && originalReport.length > 0) {
         try (ByteArrayInputStream reportInputStream = new ByteArrayInputStream(originalReport)) {
            final JAXBContext jax = JAXBContext.newInstance(HL7v2ValidationReport.class);
            final Unmarshaller unm = jax.createUnmarshaller();
            return (HL7v2ValidationReport) unm.unmarshal(reportInputStream);
         } catch (IOException | JAXBException e) {
            throw new ReportTransformationException("Unable to parse HL7v2 report", e);
         }
      } else {
         throw new ReportTransformationException("Empty original report");
      }
   }

   private ValidationOverview buildOverview(HL7v2ValidationReport parsedOriginalReport) {
      if (parsedOriginalReport.getOverview() != null) {

         ValidationResultsOverview originalOverview = parsedOriginalReport.getOverview();
         String validationServiceName = originalOverview.getValidationServiceName();
         String validationServiceVersion = originalOverview.getValidationServiceVersion();
         String validatorID = originalOverview.getProfileOid();
         String validatorVersion = originalOverview.getProfileRevision();

         ValidationOverview overview = new ValidationOverview(originalOverview.getDisclaimer(),
               originalOverview.getValidationDateTime(),
               validationServiceName != null ? validationServiceName : ValidationReportMapper.UNKNOWN,
               validationServiceVersion != null ? validationServiceVersion : ValidationReportMapper.UNKNOWN,
               validatorID != null ? validatorID : UNKNOWN
         );
         overview.setValidatorVersion(validatorVersion != null ? validatorVersion : UNKNOWN);
         overview.addAdditionalMetadata(
               new Metadata("MessageOID", originalOverview.getMessageOid()));
         if (originalOverview.getValidationAbortedReason() != null) {
            overview.addAdditionalMetadata(
                  new Metadata("ValidationAbortedReason", originalOverview.getValidationAbortedReason()));
         }
         return overview;
      } else {
         return new ValidationOverview(ValidationReportMapper.DEFAULT_DISCLAIMER, UNKNOWN, UNKNOWN, UNKNOWN);
      }
   }

   private ValidationSubReport parseValidationResults(HL7v2ValidationReport parsedOriginalReport) {
      ValidationSubReport subReport = new ValidationSubReport(
            "HL7 Validation",
            //Arrays.asList("HL7v2") may also be HL7v3
            new ArrayList<String>()
      );
      if (parsedOriginalReport.getResults() != null) {
         for (Object notification : parsedOriginalReport.getResults().getNotifications()) {
            subReport.addConstraintValidation(parseHL7v2Notification(notification));
         }
      }
      return subReport;
   }

   private ConstraintValidation parseHL7v2Notification(Object notification) {
      if (notification instanceof Error) {
         return buildFailedConstraintValidation((Error) notification, ConstraintPriority.MANDATORY);
      } else if (notification instanceof Warning) {
         return buildFailedConstraintValidation((Warning) notification, ConstraintPriority.RECOMMENDED);
      } else if (notification instanceof GazelleProfileException) {
         return buildUndefinedContraintValidation((GazelleProfileException) notification);
      } else if (notification instanceof GazelleHL7Assertion) {
         return buildPassedConstraintValidation((GazelleHL7Assertion) notification);
      } else {
         return new ConstraintValidation("Unknown HL7 ValidationResults: " + notification.toString(),
               ConstraintPriority.PERMITTED,
               ValidationTestResult.UNDEFINED);
      }
   }

   private ConstraintValidation buildFailedConstraintValidation(GazelleHL7Exception error,
                                                                ConstraintPriority priority) {
      ConstraintValidation constraintValidation = new ConstraintValidation(
            buildConstraintDescription(
                  error.getDescription(),
                  error.getHl7tableId()),
            priority,
            ValidationTestResult.FAILED);
      constraintValidation.setLocationInValidatedObject(error.getHl7Path());
      constraintValidation.setConstraintType(error.getFailureType());
      constraintValidation.setValueInValidatedObject(error.getValue());
      return constraintValidation;
   }

   private ConstraintValidation buildPassedConstraintValidation(GazelleHL7Assertion note) {
      ConstraintValidation constraintValidation = new ConstraintValidation(
            note.getAssertion(),
            ConstraintPriority.MANDATORY,
            ValidationTestResult.PASSED);
      constraintValidation.setLocationInValidatedObject(note.getPath());
      if (note.getType() != null) {
         constraintValidation.setConstraintType(note.getType().getLabel());
      }
      return constraintValidation;
   }

   private ConstraintValidation buildUndefinedContraintValidation(GazelleHL7Exception exception) {
      ConstraintValidation constraintValidation = new ConstraintValidation(
            exception.getDescription(),
            HL7ClassificationMapping.fromClassification(exception.getValue()).getPriority(),
            ValidationTestResult.UNDEFINED);
      constraintValidation.setLocationInValidatedObject(exception.getHl7Path());
      constraintValidation.setConstraintType(exception.getFailureType());
      constraintValidation.addUnexpectedError(new UnexpectedError("Exception", exception.getMessage()));
      return constraintValidation;
   }

   private String buildConstraintDescription(String hl7Description, String hl7Tableid) {
      if (hl7Tableid != null && !hl7Tableid.isEmpty()) {
         return hl7Description + ". See HL7 table '" + hl7Tableid + "'.";
      } else {
         return hl7Description;
      }
   }

   private enum HL7ClassificationMapping {
      ERROR("Error", ConstraintPriority.MANDATORY),
      AFFIRMATIVE("Affirmative", ConstraintPriority.MANDATORY),
      ALERTE("Alerte", ConstraintPriority.RECOMMENDED),
      WARNING("Warning", ConstraintPriority.RECOMMENDED);

      private final String value;
      private final ConstraintPriority priorityMapping;

      HL7ClassificationMapping(String value, ConstraintPriority priorityMapping) {
         this.value = value;
         this.priorityMapping = priorityMapping;
      }

      public String getClassification() {
         return value;
      }

      public ConstraintPriority getPriority() {
         return priorityMapping;
      }

      public static HL7ClassificationMapping fromClassification(String value) {
         for(HL7ClassificationMapping mapping : HL7ClassificationMapping.values()) {
            if(mapping.getClassification().equals(value)) {
               return mapping;
            }
         }
         return ERROR;
      }
   }

}