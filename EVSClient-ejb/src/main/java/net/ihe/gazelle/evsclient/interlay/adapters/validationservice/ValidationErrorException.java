package net.ihe.gazelle.evsclient.interlay.adapters.validationservice;

public class ValidationErrorException extends RuntimeException {
    public ValidationErrorException() {
    }

    public ValidationErrorException(String s) {
        super(s);
    }

    public ValidationErrorException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public ValidationErrorException(Throwable throwable) {
        super(throwable);
    }

    public ValidationErrorException(String s, Throwable throwable, boolean b, boolean b1) {
        super(s, throwable, b, b1);
    }
}
