package net.ihe.gazelle.evsclient.interlay.adapters;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.common.application.action.ApplicationPreferenceManagerImpl;
import net.ihe.gazelle.evsclient.application.interfaces.OidGeneratorManager;
import net.ihe.gazelle.evsclient.domain.OIDSequence;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import javax.persistence.EntityManager;

@Name("oidGenerator")
@Scope(ScopeType.STATELESS)
public class OIDGeneratorImpl implements OidGeneratorManager {

    private static final String ROOT_OID_PREFERENCE_NAME = "root_oid";

    @Override
    public String getNewOid() {
        final EntityManager em = EntityManagerService.provideEntityManager();
        OIDSequence oid = new OIDSequence();
        oid = em.merge(oid);
        em.flush();
        return OIDGeneratorImpl.getRootOid().concat(Integer.toString(oid.getId()));
    }

    private static String getRootOid() {
        ApplicationPreferenceManager applicationPreferenceManager = new ApplicationPreferenceManagerImpl();
        return applicationPreferenceManager.getStringValue(OIDGeneratorImpl.ROOT_OID_PREFERENCE_NAME);
    }
}
