/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evsclient.interlay.dao.security;

import net.ihe.gazelle.evsclient.application.security.ApiKeyDao;
import net.ihe.gazelle.evsclient.application.security.ApiKeyNotFoundException;
import net.ihe.gazelle.evsclient.domain.security.ApiKey;
import net.ihe.gazelle.evsclient.domain.security.ApiKeyQuery;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.List;

public class ApiKeyDaoImpl implements ApiKeyDao {

    private final EntityManagerFactory entityManagerFactory;

    public ApiKeyDaoImpl(EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
    }

    @Override
    public List<ApiKey> getAllKeysOfUser(String owner) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        ApiKeyQuery query = new ApiKeyQuery(entityManager);
        query.expirationDate().order(false);
        query.owner().eq(owner);
        return query.getList();
    }

    @Override
    public ApiKey createKey(ApiKey apiKey) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        return entityManager.merge(apiKey);
    }

    @Override
    public ApiKey findByValue(String apiKeyValue) throws ApiKeyNotFoundException {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        ApiKey apiKey = entityManager.find(ApiKey.class, apiKeyValue);
        if(apiKey != null) {
            return apiKey;
        } else {
            throw new ApiKeyNotFoundException(String.format("Unknown key with value '%s'.", apiKeyValue));
        }
    }
}
