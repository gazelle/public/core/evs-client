package net.ihe.gazelle.evsclient.interlay.adapters.validationservice;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.application.validation.ReportParser;
import net.ihe.gazelle.evsclient.domain.validation.ValidationType;
import net.ihe.gazelle.evsclient.domain.validation.types.ValidationProperties;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.WebValidationServiceConf;
import net.ihe.gazelle.evsclient.interlay.adapters.validation.CDAGeneratorReportMapper;

@ValidationProperties(
      name = "CDA Model Based",
      types = {
            ValidationType.CDA
      })
@ReportParser(CDAGeneratorReportMapper.class)
public class CDAGeneratorValidationServiceClient extends ModelBasedValidationServiceClient {
   public CDAGeneratorValidationServiceClient(ApplicationPreferenceManager applicationPreferenceManager, WebValidationServiceConf configuration) {
      super(applicationPreferenceManager, configuration);
   }
}
