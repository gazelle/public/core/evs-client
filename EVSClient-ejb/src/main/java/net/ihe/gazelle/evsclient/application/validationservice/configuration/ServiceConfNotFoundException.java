package net.ihe.gazelle.evsclient.application.validationservice.configuration;

public class ServiceConfNotFoundException extends Exception {
   private static final long serialVersionUID = 6710563794385546304L;

   public ServiceConfNotFoundException(String s) {
      super(s);
   }

   public ServiceConfNotFoundException(String s, Throwable throwable) {
      super(s, throwable);
   }
}
