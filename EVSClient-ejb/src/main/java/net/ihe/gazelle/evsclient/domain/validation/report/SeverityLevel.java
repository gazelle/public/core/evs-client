package net.ihe.gazelle.evsclient.domain.validation.report;

/**
 * SeverityLevel
 *  Error is set when a Mandatory constraint is failed
 *  Warning is set when a Recommended constraint is failed
 *  Info is set when a Permitted constraint is failed or a constraint is passed
 *
 * @author fde
 * @version $Id: $Id
 */
public enum SeverityLevel {
    INFO,
    WARNING,
    ERROR
}
