package net.ihe.gazelle.evsclient.application.validationservice.configuration;

import gnu.trove.map.hash.THashMap;
import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.application.validation.OriginalReportParser;
import net.ihe.gazelle.evsclient.application.validation.ReportParser;
import net.ihe.gazelle.evsclient.application.validation.ValidationReportMapper;
import net.ihe.gazelle.evsclient.domain.validation.ValidationServiceRef;
import net.ihe.gazelle.evsclient.domain.validation.ValidationType;
import net.ihe.gazelle.evsclient.domain.validationservice.ValidationService;
import net.ihe.gazelle.evsclient.domain.validationservice.ValidationServiceFactory;
import net.ihe.gazelle.evsclient.domain.validationservice.Validator;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.*;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.reports.ValidationReportType;
import net.ihe.gazelle.evsclient.interlay.gui.I18n;
import org.reflections.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sun.reflect.generics.reflectiveObjects.ParameterizedTypeImpl;

import javax.faces.model.DataModel;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ValidationServiceConfManager {
   private static final Logger LOGGER = LoggerFactory.getLogger(ValidationServiceConfManager.class);
   private static final Map<Class<? extends ValidationService>, Class<? extends ValidationReportMapper>> REPORT_PARSERS = assertReportParsersDefinitions();

   private final ValidationServiceConfDao validationServiceConfDao;
   private final ApplicationPreferenceManager applicationPreferenceManager;

   public ValidationServiceConfManager(ValidationServiceConfDao validationServiceConfDao, ApplicationPreferenceManager applicationPreferenceManager) {
      this.validationServiceConfDao = validationServiceConfDao;
      this.applicationPreferenceManager = applicationPreferenceManager;
   }

   public ValidationServiceConf getValidationServiceConfByKeyword(String inKeyword)
         throws ServiceConfNotFoundException {
      ValidationServiceConf vsc = validationServiceConfDao.getValidationServiceByKeyword(inKeyword);
      if (vsc != null) {
         return vsc;
      } else {
         throw new ServiceConfNotFoundException(inKeyword);
      }
   }

   public ValidationServiceConf getValidationServiceById(Integer id) {
      return validationServiceConfDao.getValidationServiceById(id);
   }

   public ValidationReportType getValidationReportTypeById(Integer id) {
      return validationServiceConfDao.getValidationReportTypeById(id);
   }

   public List<ValidationServiceConf> getAllValidationService() {
      return validationServiceConfDao.getAllValidationService();
   }

   public ValidationServiceConf merge(ValidationServiceConf selectedValidationService) {
      return validationServiceConfDao.merge(selectedValidationService);
   }

   public List<ValidationServiceConf> getListOfAvailableValidationServicesForStandard(ReferencedStandard inStandard) {
      return validationServiceConfDao.getListOfAvailableValidationServicesForStandard(inStandard);
   }

   public List<ReferencedStandard> getReferencedStandardFiltered(final String displayName,
                                                                 final ValidationType validationType,
                                                                 final String validatorFilter) {
      return validationServiceConfDao.getReferencedStandardFiltered(displayName, validationType, validatorFilter);
   }

   public ReferencedStandard getReferencedStandardById(Integer id) {
      return validationServiceConfDao.getReferencedStandardById(id);
   }

   public ReferencedStandard merge(ReferencedStandard selectedReferencedStandard) {
      return validationServiceConfDao.merge(selectedReferencedStandard);
   }

   public List<ReferencedStandard> getAllReferencedStandard() {
      return validationServiceConfDao.getAllReferencedStandard();
   }

   public DataModel<ReferencedStandard> getAllReferencedStandardDataModel() {
      return validationServiceConfDao.getAllReferencedStandardDataModel();
   }

   public void saveValidationServiceConf(ValidationServiceConf validationServiceConf) {
      merge(validationServiceConf);
   }

   public List<ReferencedStandard> getReferencedStandardsByValidationServiceId(int validationServiceId) {
      return validationServiceConfDao.getReferencedStandardsByValidationServiceId(validationServiceId);
   }

   public List<ValidationServiceConf> getValidationServiceFiltered(final String name,
                                                                   final ValidationType validationType,
                                                                   final Boolean available) {
      return validationServiceConfDao.getValidationServiceFiltered(name, validationType, available);
   }

   public List<ReferencedStandard> inferReferencedStandards(String validationServiceName, String validatorName)
         throws ReferencedStandardNotFoundException, ServiceConfNotFoundException {

      ValidationServiceConf validationServiceConf = getValidationServiceConfByKeyword(validationServiceName);

      if (validationServiceConf instanceof AtomicProcessingConf) {

         // STEP 1 : Find referenced standards associated with validation service
         List<ReferencedStandard> referencedStandards =
               getReferencedStandardsByValidationServiceId(validationServiceConf.getId());
         if (!referencedStandards.isEmpty()) {

            // STEP 2 : Reduce the number of referenced standards by comparing the validators available for each
            // of them
            return filterStandardsByValidatorName(referencedStandards, validatorName, validationServiceConf);
         } else {
            throw new ReferencedStandardNotFoundException(
                  "No reference standards has been found for " + validationServiceConf.getName() + " validation " +
                        "service");
         }
      } else {
         throw new UnsupportedOperationException("Inference of referencedStandard of Composite " +
               "Validation Service is not implemented");
      }
   }

   public ValidationReportMapper getReportMapper(ValidationServiceRef validationService)
         throws ServiceConfException {
      try {
         Class<? extends ValidationReportMapper> reportMapperClass = getReportMapperClass(validationService);
         return reportMapperClass.newInstance();
      } catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
         throw new ServiceConfException(
               "Unable to instantiate mapper for validation service " + validationService.getName(), e);
      }
   }

   /**
    * Get a parser for the original report.
    *
    * @param validationServiceRef Validation service at the origin of the report to parse.
    * @param <T>                  Original report type
    *
    * @return an instance of OriginalReportParser for this validation service.
    *
    * @throws ServiceConfException if the validation service is not found or wrongly configured.
    * @throws ClassCastException   if the ValidationReportMapper for this validation service does not support original
    *                              report parsing.
    *
    * @deprecated Remove this method when GUI will not need anymore to use origial reports.
    */
   @Deprecated
   public <T> OriginalReportParser<T> getOriginalReportParser(ValidationServiceRef validationServiceRef)
         throws ServiceConfException {
      return (OriginalReportParser<T>) getReportMapper(validationServiceRef);
   }

   private static Map<Class<? extends ValidationService>, Class<? extends ValidationReportMapper>> assertReportParsersDefinitions()
         throws ReportParserNotDefinedException {
      final Map<Class<? extends ValidationService>, Class<? extends ValidationReportMapper>> reportParsers
            = new THashMap<>();
      Reflections reflections = new Reflections("net.ihe.gazelle.evsclient");
      for (Class<? extends ValidationService> cls : reflections.getSubTypesOf(ValidationService.class)) {
         if (!cls.isInterface() && !Modifier.isAbstract(cls.getModifiers())) {
            ReportParser rpa = cls.getAnnotation(ReportParser.class);
            if (rpa == null) {
               throw new ReportParserNotDefinedException(cls.getSimpleName());
            }
            reportParsers.put(cls, rpa.value());
         }
      }
      return reportParsers;
   }

   private Class<? extends ValidationReportMapper> getReportMapperClass(ValidationServiceRef validationService)
         throws ClassNotFoundException {
      try {
         ValidationServiceConf conf = getValidationServiceConfByKeyword(validationService.getName());
         return getReportMapperClass(conf);
      } catch (ServiceConfNotFoundException e) {
         throw new ClassNotFoundException("no-report-mapper-for-unknown-service-conf", e);
      }
   }

   private Class<? extends ValidationReportMapper> getReportMapperClass(ValidationServiceConf validationServiceConf)
         throws ClassNotFoundException {
      Class<? extends ValidationReportMapper> vrpClass = REPORT_PARSERS.get(getServiceClass(validationServiceConf));
      if (vrpClass != null) {
         return vrpClass;
      } else {
         throw new ClassNotFoundException();
      }
   }

   private List<ReferencedStandard> filterStandardsByValidatorName(List<ReferencedStandard> referencedStandards,
                                                                   String validatorName,
                                                                   ValidationServiceConf validationServiceConf)
         throws ReferencedStandardNotFoundException {
      List<ReferencedStandard> filteredStandards = new ArrayList<>();
      try {
         ValidationService validationService = getFactory(validationServiceConf).getService();

         for (ReferencedStandard rs : referencedStandards) {
               if (validationService
                       .getValidators(
                               rs.getValidatorFilter()
                       ).contains(new Validator(validatorName))) {
                  filteredStandards.add(rs);
               }
         }
      } catch (Throwable t) {
         throw new ReferencedStandardNotFoundException(String.format("Unable to contact validationService " +
               "%s to discriminate the referencedStandards", validationServiceConf.getName()), t);
      }
      if (!filteredStandards.isEmpty()) {
         return filteredStandards;
      } else {
         throw new ReferencedStandardNotFoundException(validatorName + " does not exist");
      }
   }

   /*
    * @deprecated ValidationServiceType should disappear from future versions of Validation domain
    */
   @Deprecated
   public ValidationServiceType getServiceType(ValidationServiceConf validationServiceConf) {
      Class<?> cls = getServiceClass(validationServiceConf);
      if (validationServiceConf!=null) {
         for (ValidationServiceType vst : getValidationServiceTypes(validationServiceConf)) {
            if (vst.getValidationServiceClass() != null && vst.getValidationServiceClass().equals(cls)) {
               return vst;
            }
         }
      }
      return null;
   }

   /*
    * @deprecated ValidationServiceType should disappear from future versions of Validation domain
    */
   @Deprecated
   public void setServiceType(ValidationServiceConf validationServiceConf, ValidationServiceType serviceType) {
      validationServiceConf.setFactoryClassname(
            serviceType.getValidationServiceClass()
                    .getCanonicalName()
                    .replaceFirst("\\.([^\\.]+?)(?:Client)?$", ".factory.$1Factory"));
   }

   /*
    * @deprecated ValidationServiceType should disappear from future versions of Validation domain
    */
   @Deprecated
   protected ValidationServiceType[] getValidationServiceTypes(ValidationServiceConf validationServiceConf) {
      Reflections reflections = new Reflections("net.ihe.gazelle.evsclient.interlay.adapters.validationservice");
      for (Class<? extends ValidationServiceType> cls : reflections.getSubTypesOf(ValidationServiceType.class)) {
         if (Enum.class.isAssignableFrom(cls)) {
            Class<? extends ValidationService> vsc =
                  (Class<? extends ValidationService>) ((ParameterizedTypeImpl) cls.getGenericInterfaces()[0]).getActualTypeArguments()[0];
            if (validationServiceConf.getValidationServiceInterface().isAssignableFrom(vsc)) {
               try {
                  return (ValidationServiceType[]) cls.getDeclaredMethod("values", null).invoke(null);
               } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
                  e.printStackTrace();
               }
            }
         }
      }
      return new ValidationServiceType[]{};
   }

   public Class<? extends ValidationService> getServiceClass(ValidationServiceConf validationServiceConf) {
      try {
         return getFactory(validationServiceConf).getServiceClass();
      } catch (ServiceConfException e) {
         LOGGER.error(I18n.get("net.ihe.gazelle.evs.invalid-factory",validationServiceConf.getFactoryClassname()),e);
         return null;
      }
   }

   public ValidationServiceFactory<?, ?> getFactory(ValidationServiceConf validationServiceConf)
         throws ServiceConfException {
      try {
         Class<ValidationServiceFactory<?, ?>> fc = validationServiceConf.getFactoryClass();
         return fc.getDeclaredConstructor(ApplicationPreferenceManager.class, validationServiceConf.getClass())
               .newInstance(applicationPreferenceManager, validationServiceConf);
      } catch (NoSuchMethodException | InvocationTargetException | InstantiationException |
            IllegalAccessException e) {
         throw new ServiceConfException(String.format("Configuration error: Unable to instantiate factory %s for " +
               "service %s", validationServiceConf.getFactoryClassname(), validationServiceConf.getName()), e);
      }

   }

   public static class ReportParserNotDefinedException extends RuntimeException {
      private static final long serialVersionUID = 7556234767268461324L;

      public ReportParserNotDefinedException(String s) {
         super(s);
      }
   }

}
