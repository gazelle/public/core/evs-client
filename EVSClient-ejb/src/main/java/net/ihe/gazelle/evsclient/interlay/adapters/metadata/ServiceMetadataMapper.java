package net.ihe.gazelle.evsclient.interlay.adapters.metadata;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.google.gson.JsonArray;
import net.ihe.gazelle.evsclient.domain.validationservice.Validator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ServiceMetadataMapper {

    private static final ObjectMapper mapper = new ObjectMapper();

    public ServiceMetadata getServiceMetadata(String json) throws IOException {
        ServiceMetadata serviceMetadata = new ServiceMetadata();
        JsonNode jsonNode = mapper.readTree(json);
        if(!jsonNode.has("name")){
            throw new IllegalArgumentException("Service name not found");
        }
        serviceMetadata.setServiceName(jsonNode.get("name").asText());
        if(jsonNode.has("providedInterfaces") && jsonNode.get("providedInterfaces").isArray()){
            //Take only the first interface
            JsonNode providedInterfacesJson = jsonNode.get("providedInterfaces").get(0);
            if(providedInterfacesJson.has("validationProfiles") && providedInterfacesJson.get("validationProfiles").isArray()){
                List<Validator> validators = getValidators(providedInterfacesJson.get("validationProfiles"));
                serviceMetadata.getValidators().addAll(validators);
            }

            return serviceMetadata;
        }
        else{
            throw new IllegalArgumentException("No providedInterfaces found");
        }

    }

    public List<Validator> getValidators(String validationProfilesJson) throws IOException {
        JsonNode validationProfilesNode = mapper.readTree(validationProfilesJson);
        return getValidators(validationProfilesNode);
    }

    List<Validator> getValidators(JsonNode validationProfilesNode) throws IOException {
        if(!validationProfilesNode.isArray()){
            throw new IllegalArgumentException("validationProfilesJson is not an array");
        }
        ArrayNode validationProfiles = (ArrayNode) validationProfilesNode;
        List<Validator> validators = new ArrayList<>();
        for (JsonNode validationProfile : validationProfiles) {
            if (!validationProfile.has("profileID")) {
                throw new IllegalArgumentException("No name found");
            }
            Validator validator = new Validator(validationProfile.get("profileID").asText());
            if(validationProfile.has("profileName")){
                validator.setName(validationProfile.get("profileName").asText());
            }
            if(validationProfile.has("domain")){
                validator.setDomain(validationProfile.get("domain").asText());
            }
            // covered items doesn't exist in V7 report
            validators.add(validator);
        }
        return validators;
    }



}
