package net.ihe.gazelle.evsclient.application.validation;

import net.ihe.gazelle.validation.DetailedResult;

/**
 * Parse original report from ValidationService in their Java representation.
 *
 * @param <T> original report type to map from
 *
 * @deprecated FIXME once the GUI will not need anymore to use DetailedResult and other original reports, remove this
 * method
 */
@Deprecated
public interface OriginalReportParser<T> {
   /**
    * Return the original report as {@link DetailedResult} for GUI components that still depends on this deprecated
    * structure.
    *
    * @deprecated FIXME once the GUI will not need anymore to use DetailedResult, remove this method
    */
   @Deprecated
   T parseOriginalReport(byte[] originalReport) throws ReportTransformationException;
}
