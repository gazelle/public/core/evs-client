/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evsclient.domain.menu;

import net.ihe.gazelle.hql.HQLQueryBuilder;
import org.hibernate.annotations.Type;
import org.jboss.seam.Component;
import org.jboss.seam.annotations.Name;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

@Entity
@Name("cmnHome")
@Table(name = "cmn_home", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = "iso3_language"))
@SequenceGenerator(name = "cmn_home_sequence", sequenceName = "cmn_home_id_seq", allocationSize = 1)
public class Home implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1218389994561421605L;

    @Id
    @GeneratedValue(generator = "cmn_home_sequence", strategy = GenerationType.SEQUENCE)
    @NotNull
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    @Column(name = "iso3_language")
    private String iso3Language;

    @Column(name = "main_content")
    @Lob
    @Type(type = "text")
    private String mainContent;

    @Column(name = "home_title")
    private String homeTitle;

    public Home() {

    }

    public Home(final String iso3Language) {
        this.iso3Language = iso3Language;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    public String getIso3Language() {
        return this.iso3Language;
    }

    public void setIso3Language(final String iso3Language) {
        this.iso3Language = iso3Language;
    }

    public String getMainContent() {
        return this.mainContent;
    }

    public void setMainContent(final String mainContent) {
        this.mainContent = mainContent;
    }

    public String getHomeTitle() {
        return this.homeTitle;
    }

    public void setHomeTitle(final String homeTitle) {
        this.homeTitle = homeTitle;
    }

    /**
     */
    public Home save() {
        final EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
        final Home home = entityManager.merge(this);
        entityManager.flush();
        return home;
    }

    /**
     */
    public static Home getHomeForSelectedLanguage() {
        final String language = org.jboss.seam.core.Locale.instance().getISO3Language();
        if (language != null && !language.isEmpty()) {
            final HQLQueryBuilder<Home> queryBuilder = new HQLQueryBuilder<>(
                    (EntityManager) Component.getInstance("entityManager"), Home.class);
            queryBuilder.addEq("iso3Language", language);
            final List<Home> homes = queryBuilder.getList();
            if (homes != null && !homes.isEmpty()) {
                return homes.get(0);
            } else {
                return new Home(language);
            }
        } else {
            return null;
        }
    }
}
