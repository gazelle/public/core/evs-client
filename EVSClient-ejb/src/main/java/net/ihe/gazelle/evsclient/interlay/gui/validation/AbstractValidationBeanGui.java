package net.ihe.gazelle.evsclient.interlay.gui.validation;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import gnu.trove.map.hash.THashMap;
import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.common.application.action.ApplicationPreferenceManagerImpl;
import net.ihe.gazelle.common.report.ReportExporterManager;
import net.ihe.gazelle.evsclient.application.CallerMetadataFactory;
import net.ihe.gazelle.evsclient.application.interfaces.ForbiddenAccessException;
import net.ihe.gazelle.evsclient.application.interfaces.GuiPermanentLink;
import net.ihe.gazelle.evsclient.application.interfaces.NotFoundException;
import net.ihe.gazelle.evsclient.application.interfaces.UnauthorizedException;
import net.ihe.gazelle.evsclient.application.notification.EmailNotificationManager;
import net.ihe.gazelle.evsclient.application.validation.Directive;
import net.ihe.gazelle.evsclient.application.validation.ValidationServiceFacade;
import net.ihe.gazelle.evsclient.domain.processing.EVSCallerMetadata;
import net.ihe.gazelle.evsclient.domain.processing.HandledObject;
import net.ihe.gazelle.evsclient.domain.processing.OwnerMetadata;
import net.ihe.gazelle.evsclient.domain.validation.ReferencedStandardRef;
import net.ihe.gazelle.evsclient.domain.validation.Validation;
import net.ihe.gazelle.evsclient.domain.validationservice.ValidationService;
import net.ihe.gazelle.evsclient.domain.validationservice.Validator;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.ReferencedStandard;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.ValidationServiceConf;
import net.ihe.gazelle.evsclient.interlay.dao.FileReadException;
import net.ihe.gazelle.evsclient.interlay.dto.view.validation.ValidationResultDto;
import net.ihe.gazelle.evsclient.interlay.gui.GuiMessage;
import net.ihe.gazelle.evsclient.interlay.gui.I18n;
import net.ihe.gazelle.evsclient.interlay.gui.QueryParam;
import net.ihe.gazelle.evsclient.interlay.gui.QueryParamEvs;
import net.ihe.gazelle.evsclient.interlay.gui.document.HandledDocument;
import net.ihe.gazelle.evsclient.interlay.servlet.Upload;
import net.ihe.gazelle.evsclient.interlay.util.MustacheTemplate;
import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.jboss.seam.core.ResourceBundle;
import org.jboss.seam.faces.FacesManager;
import org.jboss.seam.international.StatusMessage;
import org.richfaces.event.FileUploadEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.faces.model.SelectItemGroup;
import javax.xml.bind.DatatypeConverter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.regex.Pattern;

public abstract class AbstractValidationBeanGui extends AbstractValidationProcessingBeanGui<Validation, ValidationServiceFacade> {
   private static final Logger LOGGER = LoggerFactory.getLogger(ValidationBeanGui.class);


   interface Discriminator {
      boolean mayValidate(byte[] documentContent);
      int getSpecificityLevel();
   }

   class DefaultDiscriminator implements Discriminator {
      private Pattern pattern;
      private String regexp;
      private int specificityLevel = 0;

      public DefaultDiscriminator() {
      }

      public String getRegexp() {
         return regexp;
      }

      public void setRegexp(String regexp) {
         this.regexp = regexp;
         this.pattern = Pattern.compile(regexp);
      }

      @Override
      public int getSpecificityLevel() {
         return specificityLevel;
      }

      public void setSpecificityLevel(int specificityLevel) {
         this.specificityLevel = specificityLevel;
      }

      @Override
      public boolean mayValidate(byte[] documentContent) {
         return pattern.matcher(new String(documentContent, StandardCharsets.UTF_8)).find();
      }
   }

   public interface DiscriminatorDao<T extends Discriminator> {
      T findByValidator(ValidationServiceConf service, String validator);
   }

   public class YamlDiscriminatorDao implements DiscriminatorDao<Discriminator>{
      private Map<String,Discriminator> discriminators;

      public YamlDiscriminatorDao() {
         super();
         load();
      }

      /**
       * example : {
       *     "CDA ModelBased (ASIP)" : {
       *         "ASIP" : {
       *             "Model-Based CDA Validator" : {
       *                 "HL7 - CDA Release 2" : {
       *                     "discriminator" : {
       *                         "regexp": ""
       *                         "specificityLevel":
       *                     }
       *                 }
       *                 "ASIP - Fiche de Réunion de Concertation Pluridisciplinaire (FRCP)" : {
       *
       *                 }
       *             }
       *         }
       *     }
       * }
       */
      private void load() {
         this.discriminators = new THashMap<>();
         try {
            String json = ApplicationPreferenceManagerImpl.instance().getStringValue("validators_discrimination_rules");
            if (StringUtils.isNotEmpty(json)) {
               JsonNode r = new ObjectMapper().readTree(json);
               Iterator<String> standards = r.fieldNames();
               while(standards.hasNext()) {
                  String standard = standards.next();
                  JsonNode f = r.get(standard);
                  Iterator<String> filters = f.fieldNames();
                  while(filters.hasNext()) {
                     String filter = filters.next();
                     JsonNode s = f.get(filter);
                     Iterator<String> services = s.fieldNames();
                     while(services.hasNext()) {
                        String service = services.next();
                        JsonNode v = s.get(service);
                        Iterator<String> validators = v.fieldNames();
                        while(validators.hasNext()) {
                           String validator = validators.next();
                           JsonNode d = s.get(validator);
                           StringBuilder sb = new StringBuilder("/").append(standard).append("/").append(filter).append("/").append(service).append("/").append(validator);
                           discriminators.put(sb.toString(), buildDiscriminator(d));
                        }
                     }
                  }
               }
            }
         } catch (Exception e) {
            LOGGER.warn("validators discrimination rules parsing error",e);
         }
      }

      private Discriminator buildDiscriminator(JsonNode d) throws ClassNotFoundException, JsonProcessingException {
         Class<? extends Discriminator> cls = getDiscriminatorClass(d);
         return new ObjectMapper().treeToValue(d.get("discriminator"),cls);
      }

      private Class<? extends Discriminator> getDiscriminatorClass(JsonNode d) throws ClassNotFoundException {
         if (d.get("implementation")==null) {
            return DefaultDiscriminator.class;
         } else {
            return (Class<? extends Discriminator>) Class.forName(d.get("implementation").asText());
         }
      }

      @Override
      public Discriminator findByValidator(ValidationServiceConf service, String validator) {
         if (discriminators==null) {
            return null;
         }

         StringBuilder sb = new StringBuilder("/")
                 .append(getReferencedStandard().getName())
                 .append("/")
                 .append(getReferencedStandard().getValidatorFilter())
                 .append("/")
                 .append(service.getName())
                 .append("/").append(validator);

         return discriminators.get(sb.toString());
      }
   }

   protected DiscriminatorDao discriminatorDao;

   protected boolean validationDone;

   protected final EmailNotificationManager emailNotificationManager;

   protected final Map<String, ValidationServiceConf> validatorNameValidationServiceConfMap = new HashMap<>();

   protected String selectedValidatorName;

   protected ReferencedStandard referencedStandard;

   protected List<SelectItem> options;

   protected AbstractValidationBeanGui(Class<?> cls,
                                       ValidationServiceFacade validationServiceFacade,
                                       CallerMetadataFactory callerMetadataFactory,
                                       GazelleIdentity identity,
                                       ApplicationPreferenceManager applicationPreferenceManager,
                                       GuiPermanentLink<Validation> permanentLinkGui,
                                       EmailNotificationManager emailNotificationManager) {
      super(cls, validationServiceFacade, callerMetadataFactory, identity, applicationPreferenceManager,
            permanentLinkGui);
      this.emailNotificationManager = emailNotificationManager;
      try {
         init();
      } catch (UnauthorizedException | ForbiddenAccessException | NotFoundException e) {
         FacesManager.instance().redirect("/error.seam");
         GuiMessage.logMessage(StatusMessage.Severity.ERROR, "Unable to init validation page", e);
      }
      discriminatorDao = new YamlDiscriminatorDao();
   }

   public void init() throws UnauthorizedException, NotFoundException, ForbiddenAccessException {
      try {
         initFromUrl(); // get by Id or Oid

         uploadFileFromRemoteValidationIfNeeded();
         initReferenceStandard();
      } catch (final NumberFormatException e) {
         FacesManager.instance().redirect("/error.seam");
         GuiMessage.logMessage(StatusMessage.Severity.ERROR, "Standard id is not an integer");
      } catch (final FileReadException | NotFoundException e){
         FacesManager.instance().redirect("/error.seam");
         GuiMessage.logMessage(StatusMessage.Severity.ERROR, "You cannot access the content of this report");
      }

      if (selectedObject!=null) { // revalidate
         setSelectedValidatorName(selectedObject.getValidationService().getValidatorKeyword());
      }

   }

   private void initReferenceStandard() {
      final Map<String, String> urlParams = FacesContext.getCurrentInstance().getExternalContext()
              .getRequestParameterMap();
      int id = 0;
      if (urlParams.containsKey(QueryParamEvs.REF_STANDARD_ID)) {
         id = Integer.parseInt(urlParams.get(QueryParamEvs.REF_STANDARD_ID));
      } else {
         id = selectedObject.getReferencedStandards().iterator().next().getReferencedStandardId();
      }
      setReferencedStandard(processingManager.getReferencedStandardById(id));

      List<ValidationServiceConf> validationServiceConfs = referencedStandard.getConfigurations();

      for (ValidationServiceConf vsc : validationServiceConfs) {
         try {
            if(vsc.isAvailable()) {
               ValidationService validationService = processingManager.getValidationService(vsc);
               for (Validator validator : validationService.getValidators(referencedStandard.getValidatorFilter())) {
                  validatorNameValidationServiceConfMap.put(validator.getKeyword(), vsc);
               }
            }
         } catch (Exception e) {
            String message = "Unable to retrieve the list of validators for configuration " + vsc.getName();
            GuiMessage.logMessage(StatusMessage.Severity.ERROR, message, e);
            emailNotificationManager.send(e, message, null);
         }
      }
   }

   public void uploadFileFromRemoteValidationIfNeeded() {
      final Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext()
            .getRequestParameterMap();
      if (params.get(QueryParam.KEY) != null && document==null) { // parameter set by remote validation
         try {
            setDocument(new HandledDocument(Upload.getFileAndRemoveFromKeyMap(params.get(QueryParam.KEY))));
            setUploadedFileName("uploadedFileFromRemote.xml");
         } catch (IOException e) {
            FacesManager.instance().redirect("/error.seam");
            GuiMessage.logMessage(StatusMessage.Severity.ERROR, "Unable to load file from remote validation", e);
         }
      } else if (selectedObject != null) {
         try {
            setDocument(new HandledDocument(selectedObject.getObject().getContent()));
            uploadedFileName = selectedObject.getObject().getOriginalFileName();
         } catch (FileReadException e) {
            FacesManager.instance().redirect("/error.seam");
            String message =
                  "Unable to retrieve the content of previously uploaded file: " + e.getCause().getMessage();
            GuiMessage.logMessage(StatusMessage.Severity.ERROR, message, e);
         }
      }
   }

   @Override
   public void reset() {
      super.reset();
      setValidationDone(false);
   }

   public String getValidationServiceDescription() {
      if (validatorNameValidationServiceConfMap.get(this.selectedValidatorName) != null) {
         return validatorNameValidationServiceConfMap.get(selectedValidatorName).getDescription();
      }
      return "";
   }

   public String getStandardDescription() {
      return this.referencedStandard.getDescription();
   }

   public boolean isValidationDone() {
      return validationDone;
   }

   public void setValidationDone(boolean validationDone) {
      this.validationDone = validationDone;
   }

   public void validate() {
      try {
         validate(
                 new ArrayList<>(Arrays.asList(
                         new HandledObject[]{new HandledObject(super.getUploadedFileContent(), super.getUploadedFileName())})),
                 super.getEvsCallerMetadata(),
                 super.getOwnerMetadata()
         );
      } catch (Exception e) {
            GuiMessage.logMessage(StatusMessage.Severity.ERROR,"net.ihe.gazelle.evs.cannotValidate", e);
      }
   }

   protected void validate(List<HandledObject> objects,
                           EVSCallerMetadata caller,
                           OwnerMetadata owner) {

      Directive directive = buildDirective();
      if (selectedObject == null) {
         Validation validation = null;
         try {
            validation = processingManager.validate(objects, caller, owner, directive, false);
         } catch (Exception e) {
            GuiMessage.logMessage(StatusMessage.Severity.ERROR,
                  "Failed to validate with " + selectedValidatorName + " validator.", e);
         } finally {
            if (validation != null) {
               validation.setReferencedStandards(
                     new ArrayList<>(Arrays.asList(new ReferencedStandardRef(referencedStandard.getId()))));
               selectedObject = validation;
            }
         }
      } else {
         try {
            selectedObject = processingManager.revalidate(selectedObject.getOid(), getOwnerMetadata(), directive,
                  false);
         } catch (Exception e) {
            GuiMessage.logMessage(StatusMessage.Severity.ERROR,
                  "Failed to revalidate with " + selectedValidatorName + " validator.", e);
         }
      }

      // TODO async ?
      setValidationDone(true);
   }

   protected Directive buildDirective() {
      List<Directive> extensions = buildExtensionDirectives();

      // We let here the possibility to start multiple validation with extension with Directive.extensions
      return new Directive(validatorNameValidationServiceConfMap.get(selectedValidatorName).getName(),
            selectedValidatorName, Arrays.asList(referencedStandard), extensions);
   }

   protected List<Directive> buildExtensionDirectives() {
      return new ArrayList<>();
   }

   @Override
   public void uploadListener(final FileUploadEvent event) {
      this.reset();
      super.uploadListener(event);
   }


   public void displayInvalidFileTypeMessage() {
      GuiMessage.logMessage(StatusMessage.Severity.ERROR, I18n.get("net.ihe.gazelle.document.invalid-file-type") + getAcceptedFileTypesAsString());
   }

   public String getAcceptedFileTypesAsString() {
      String result = applicationPreferenceManager.getStringValue(
            referencedStandard.getValidationType().name() + "_file_types");
      if (result != null) {
         return result;
      } else {
         return AcceptedFileTypes.valueOf(referencedStandard.getValidationType().name()).getAcceptedFileTypesAsString();
      }
   }

   public ValidationResultDto buildAndGetValidationResultDto() {
      return new ValidationResultDto(
              selectedObject,
              selectedObject==null?validationDone:selectedObject.getValidationStatus().name().startsWith("DONE_"),
              referencedStandard.getValidationType().name(),
              selectedValidatorName);
   }

   public String getSelectedValidatorName() {
      if (selectedValidatorName==null) {
        selectDefaultValidator(getValidatorsAsSelectItems());
      }
      return selectedValidatorName;
   }

   public void setSelectedValidatorName(String selectedValidatorName) {
      this.selectedValidatorName = selectedValidatorName;
   }

   public ReferencedStandard getReferencedStandard() {
      return referencedStandard;
   }

   public String getReferencedStandardName() {
      return getReferencedStandard() != null ? getReferencedStandard().getName() : "";
   }

   public void setReferencedStandard(ReferencedStandard referencedStandard) {
      this.referencedStandard = referencedStandard;
   }

   public void downloadWebServiceBody() {
      final String serviceName = validatorNameValidationServiceConfMap.get(selectedValidatorName).getName();
      @SuppressWarnings("findbugs:URF_UNREAD_FIELD") // properties in context object will be used by mustache engine
      String body = new MustacheTemplate("validation-for-creation")
            .execute(
                    new Object() {
               String service = serviceName;
               String validator = selectedValidatorName;
               List<Object> objects = Arrays.asList(new Object[]{new Object() {
                  String fileName = getUploadedFileName();
                  String content = DatatypeConverter.printBase64Binary(document.getContent());
               }
               });
            });
      StringBuilder fileNameBuilder = new StringBuilder();
      fileNameBuilder.append(getUploadedFileNameWithoutExtension());
      fileNameBuilder.append("_");
      fileNameBuilder.append(selectedValidatorName);
      fileNameBuilder.append(".xml");
      ReportExporterManager.exportToFile("text/xml", body, fileNameBuilder.toString());
   }


   private Discriminator getDiscriminator(String validator) {
      ValidationServiceConf service = validatorNameValidationServiceConfMap.get(validator);
      if (service==null) {
         return null;
      }
      return discriminatorDao.findByValidator(service, validator);
   }

   public List<SelectItem> getValidatorsAsSelectItems() {
      if (options==null) {
         options = new ArrayList<>();
         addRootSelectItem(options);
         addValidatorsByValidationServiceGroup(options);
      }
      return options;
   }

   private void selectDefaultValidator(List<SelectItem> options) {
      SelectItem defaultItem = defaultValidator(options);
      if (StringUtils.isEmpty(selectedValidatorName)&&defaultItem!=null) {
         setSelectedValidatorName(defaultItem.getValue().toString());
      }
   }

   private SelectItem defaultValidator(List<SelectItem> options) {
      // if only one validator, select it.
      int enabled = 0;
      SelectItem defaultItem = null;
      for (int i = 0; i< options.size()&&enabled<2 ; i++) {
         if (!options.get(i).isDisabled()) {
            enabled++;
            if (SelectItemGroup.class.isInstance(options.get(i))) {
               defaultItem = defaultValidator(Arrays.asList(((SelectItemGroup)options.get(i)).getSelectItems()));
            } else {
               defaultItem = options.get(i);
            }
            if (enabled!=1) {
               return null;
            }
         }
      }
//      if (defaultItem==null) {
//         // if more than one validator, discover if we can discriminate candidates
//         defaultItem = discoverValidator(options,getDocumentContent());
//      }
      return defaultItem;
   }

   private SelectItem discoverValidator(List<SelectItem> options, byte[] documentContent) {
      // if document is not empty
      if (ArrayUtils.isNotEmpty(documentContent)) {
         // in specificity order (from most specific to less specific)
         SortedMap<Discriminator,SelectItem> sorted = new TreeMap<>(new Comparator<Discriminator>() {
            @Override
            public int compare(Discriminator a, Discriminator b) {
               if (a==null&&b==null) {
                  return 0;
               }
               if (a==null&&b!=null) {
                  return 1;
               } else if (b==null) {
                  return -1;
               }
               return a.getSpecificityLevel() >  b.getSpecificityLevel() ? -1 : a.getSpecificityLevel() == b.getSpecificityLevel() ? 0 : 1;
            }
         });
         collectValidators(options, sorted);
         // select the first validator which discriminator says the document content is appropriate
         if (!sorted.isEmpty()) {
            return sorted.get(sorted.firstKey());
         }
      }
      return null;
   }

   private void collectValidators(List<SelectItem> options, SortedMap<Discriminator, SelectItem> sorted) {
      for (int i = 0; i < options.size(); i++) {
         // sort discriminators by specificity level
         if (options.get(i) != null && SelectItemGroup.class.isInstance(options.get(i)) && ((SelectItemGroup) options.get(i)).getSelectItems() != null) {
            collectValidators(Arrays.asList(((SelectItemGroup) options.get(i)).getSelectItems()), sorted);
         } else if (options.get(i) != null && options.get(i).getValue() != null) {
            Discriminator discriminator = getDiscriminator(options.get(i).getValue().toString());
            sorted.put(discriminator, options.get(i));
         }
      }
   }


   private void addValidatorsByValidationServiceGroup(List<SelectItem> options) {
      Set<String> distinctValidationServiceName = getDistinctValidationServiceName();

      for (String validationServiceName: distinctValidationServiceName) {
         SelectItemGroup validationServiceGroup = new SelectItemGroup(validationServiceName);
         List<SelectItem> selectItemsForCurrentValidationServiceGroup = new ArrayList<>();
         SortedSet<String> sortedValidatorName = new TreeSet<>(validatorNameValidationServiceConfMap.keySet());
         for (String validator : sortedValidatorName) {
            if(validatorNameValidationServiceConfMap.get(validator).getName().equals(validationServiceGroup.getLabel())){
               SelectItem selectItem = new SelectItem();
               selectItem.setValue(validator);
               selectItem.setLabel(validator);
               selectItemsForCurrentValidationServiceGroup.add(selectItem);
            }
         }

         validationServiceGroup.setSelectItems(selectItemsForCurrentValidationServiceGroup.toArray(new SelectItem[0]));
         options.add(validationServiceGroup);
      }
   }

   protected Set<String> getDistinctValidationServiceName() {
      Set<String> distinctValidationServiceName = new HashSet<>();
      for (Map.Entry<String, ValidationServiceConf> entry: validatorNameValidationServiceConfMap.entrySet()) {
         distinctValidationServiceName.add(entry.getValue().getName());
      }
      return distinctValidationServiceName;
   }

   private void addRootSelectItem(List<SelectItem> options) {
      SelectItem rootItem = new SelectItem();
      rootItem.setValue(null);
      rootItem.setNoSelectionOption(true);
      rootItem.setDisabled(true);
      rootItem.setLabel(ResourceBundle.instance().getString("gazelle.common.PleaseSelect"));
      options.add(rootItem);
   }

}
