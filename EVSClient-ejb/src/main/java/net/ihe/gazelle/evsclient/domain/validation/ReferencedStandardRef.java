package net.ihe.gazelle.evsclient.domain.validation;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name = "evsc_referenced_standard_ref")
@SequenceGenerator(name = "referenced_standard_ref_sequence", sequenceName = "referenced_standard_ref_id_seq", allocationSize = 1)
public class ReferencedStandardRef implements Serializable {
    private static final long serialVersionUID = -8986027622524877764L;

    @Id
    @GeneratedValue(generator = "referenced_standard_ref_sequence", strategy = GenerationType.SEQUENCE)
    @NotNull
    @Column(name = "id", nullable = false, unique = true)
    protected Integer id;

    @Column(name = "referenced_standard_id")
    private int referencedStandardId;

    ReferencedStandardRef() {
    }

    public ReferencedStandardRef(int referencedStandardId) {
        this.referencedStandardId = referencedStandardId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getReferencedStandardId() {
        return referencedStandardId;
    }

    public void setReferencedStandardId(int referencedStandardId) {
        this.referencedStandardId = referencedStandardId;
    }

}
