/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.ihe.gazelle.evsclient.domain.validation.extension;

import net.ihe.gazelle.evsclient.domain.processing.AbstractProcessing;
import net.ihe.gazelle.evsclient.domain.processing.EVSCallerMetadata;
import net.ihe.gazelle.evsclient.domain.processing.HandledObject;
import net.ihe.gazelle.evsclient.domain.processing.OwnerMetadata;
import net.ihe.gazelle.evsclient.domain.validation.Validation;
import net.ihe.gazelle.evsclient.domain.validation.ValidationRef;
import net.ihe.gazelle.evsclient.interlay.dao.validation.OriginalReportFile;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "evsc_validation_extension")
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class ValidationExtension extends AbstractProcessing implements Serializable {

    private static final long serialVersionUID = -3204150422262761864L;

    @Embedded
    private ValidationRef validationRef;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private ExtensionStatus extensionStatus = ExtensionStatus.NEVER_COMPUTED;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "extension_report_id")
    private OriginalReportFile extensionReport;

    protected ValidationExtension() {
        super();
    }

    protected ValidationExtension(Validation validation, List<HandledObject> objects, EVSCallerMetadata caller, OwnerMetadata owner) {
        super(objects, caller, owner);
        this.validationRef = new ValidationRef(validation.getOid(),validation.getValidationStatus().name());
    }

    public OriginalReportFile getExtensionReport() {
        return extensionReport;
    }

    public void setExtensionReport(OriginalReportFile originalReport) {
        this.extensionReport = originalReport;
    }

    public ValidationRef getValidationRef() {
        return validationRef;
    }

    public void setValidationRef(ValidationRef validationRef) {
        this.validationRef = validationRef;
    }

    public ExtensionStatus getExtensionStatus() {
        return extensionStatus;
    }

    public void setExtensionStatus(ExtensionStatus validationStatus) {
        this.extensionStatus = validationStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ValidationExtension that = (ValidationExtension) o;
        return Objects.equals(validationRef, that.validationRef) && extensionStatus == that.extensionStatus;
    }

    @Override
    public int hashCode() {
        return Objects.hash(validationRef, extensionStatus);
    }
}