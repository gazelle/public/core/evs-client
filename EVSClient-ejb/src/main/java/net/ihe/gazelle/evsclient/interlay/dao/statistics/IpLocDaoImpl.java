package net.ihe.gazelle.evsclient.interlay.dao.statistics;

import net.ihe.gazelle.evsclient.application.statistics.IpLocDao;
import net.ihe.gazelle.evsclient.domain.statistics.IpLoc;
import org.jboss.seam.annotations.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import java.io.IOException;
import java.util.List;

public class IpLocDaoImpl implements IpLocDao {

    private EntityManagerFactory entityManagerFactory;

    public IpLocDaoImpl(EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
    }

    @Override
    public IpLoc findLastCheckedLocByIp(String ip) {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        final Query query = entityManager.createQuery(
                "select l from IpLoc l where l.ip = :param order by l.lastCheck desc");
        query.setParameter("param", ip);
        List<IpLoc> locs = query.getResultList();
        if (locs==null||locs.isEmpty()) {
            return null;
        }
        return locs.iterator().next();
    }

    @Override
    @Transactional
    public IpLoc save(IpLoc ipLoc) throws IOException {
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.persist(ipLoc);
        entityManager.flush();
        return ipLoc;
    }

}
