package net.ihe.gazelle.evsclient.interlay.adapters.validationservice.factory;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.application.validationservice.AbstractValidationServiceFactory;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.WebValidationServiceConf;
import net.ihe.gazelle.evsclient.interlay.adapters.validationservice.CDAGeneratorValidationServiceClient;

public class CDAGeneratorValidationServiceFactory extends AbstractValidationServiceFactory<CDAGeneratorValidationServiceClient,WebValidationServiceConf> {

    public CDAGeneratorValidationServiceFactory(ApplicationPreferenceManager applicationPreferenceManager, WebValidationServiceConf configuration) {
        super(applicationPreferenceManager, configuration, CDAGeneratorValidationServiceClient.class);
    }
}
