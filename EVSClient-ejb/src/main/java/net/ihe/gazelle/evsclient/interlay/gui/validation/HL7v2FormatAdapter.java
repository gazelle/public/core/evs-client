package net.ihe.gazelle.evsclient.interlay.gui.validation;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.parser.DefaultXMLParser;
import ca.uhn.hl7v2.parser.PipeParser;
import ca.uhn.hl7v2.parser.XMLParser;
import ca.uhn.hl7v2.validation.impl.NoValidation;
import net.ihe.gazelle.evsclient.interlay.gui.document.Er7FormatAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class HL7v2FormatAdapter extends Er7FormatAdapter {

    private static final Logger LOGGER = LoggerFactory.getLogger(HL7v2FormatAdapter.class);
    public static final String CANNOT_COMPUTE_ER7_FORM_XML = "Cannot compute ER7 content from XML message.";

    private HL7v2FormatAdapter() {
        super();
    }

    public static String trunkHL7MessageContentIfTooBig(String messageToTrunk) {
        int a = messageToTrunk.indexOf('\n');
        int b = 0;
        final int numberOfSegmentsToDisplay = 50;

        while (b < numberOfSegmentsToDisplay && b > -1 && a != -1) {
            a = messageToTrunk.indexOf('\n', a + 1);
            b++;
        }

        // In this case, it is possible that the messages don't have any
        // carriage return.
        if (b < 2 && a == -1) {
            if (messageToTrunk.length() > 30 * numberOfSegmentsToDisplay) {
                return messageToTrunk.substring(0, 30 * numberOfSegmentsToDisplay);
            } else {
                return messageToTrunk;
            }
        } else if (a != -1) {
            return messageToTrunk.substring(0, a);
        } else {
            return messageToTrunk;
        }
    }

    public static String getDisplayER7(String messageContent, boolean isXmlMessage) {
        String er7Message = getRawER7(messageContent, isXmlMessage);
        if (er7Message != null){
            er7Message = er7Message.replace("<", "&lt;");
            er7Message = er7Message.replace(">", "&gt;");
            er7Message = er7Message.replace("&", "&amp;");
            er7Message = er7Message.replace("#", "&#35;");
            er7Message = er7Message.replace("~", "&#126;");
            er7Message = er7Message.replace("|", "&#124;");
            er7Message = er7Message.replace("^", "&#94;");
            return er7Message.replace("\n", "<br/>");
        } else {
            return CANNOT_COMPUTE_ER7_FORM_XML;
        }
    }

    private static String getRawER7(String messageContent, boolean isXmlMessage){
        if (isXmlMessage){
            final XMLParser xmlParser = new DefaultXMLParser();
            xmlParser.setValidationContext(new NoValidation());
            final PipeParser pipeParser = new PipeParser();
            pipeParser.setValidationContext(new NoValidation());
            try {
                final Message message;
                message = xmlParser.parse(messageContent);
                String encodedMessage = pipeParser.encode(message);
                return encodedMessage.replace("\r","\n");
            } catch (HL7Exception|NumberFormatException e) {
                LOGGER.error(e.getMessage());
                return null;
            }
        } else {
            return messageContent;
        }
    }

    public static String getHighlightedER7(String messageContent, boolean isXmlMessage) {
        Integer b = Integer.valueOf(0);
        final List<String> listSegment = new ArrayList<>();

        String highlightedMessage = getRawER7(messageContent, isXmlMessage);

        if (highlightedMessage != null){
            Integer a = Integer.valueOf(highlightedMessage.indexOf('|'));
            highlightedMessage = highlightedMessage.replace("<", "&lt;");
            highlightedMessage = highlightedMessage.replace(">", "&gt;");

            if (a < highlightedMessage.length() && a != -1) {
                String subString = highlightedMessage.substring(b, a);
                highlightedMessage = highlightedMessage
                        .replaceFirst(subString, "<span class=\"hl-reserved\">" + subString + "</span>");

                while (a < highlightedMessage.length() && a > 0 && b >= 0) {
                    a = Integer.valueOf(highlightedMessage.indexOf('\n', a + 1));

                    if (a + 1 < highlightedMessage.length() && a != -1) {
                        b = Integer.valueOf(highlightedMessage.indexOf('|', a + 1));

                        if (b - a == 4) {
                            subString = highlightedMessage.substring(a, b);

                            if (!listSegment.contains(subString)) {
                                listSegment.add(subString);
                                highlightedMessage = highlightedMessage.replaceAll(subString,
                                        "\n<span class=\"hl-reserved\">" + subString.substring(1, 4) + "</span>");
                            }
                        }
                    } else {
                        a = Integer.valueOf(-1);
                    }
                }
            }
            highlightedMessage = highlightedMessage.replace("&", "<span class=\"hl-special\">&amp;</span>");
            highlightedMessage = highlightedMessage.replace("#", "<span class=\"hl-special\">&#35;</span>");
            highlightedMessage = highlightedMessage.replace("~", "<span class=\"hl-special\">&#126;</span>");
            highlightedMessage = highlightedMessage.replace("|", "<span class=\"hl-special\">&#124;</span>");
            highlightedMessage = highlightedMessage.replace("^", "<span class=\"hl-brackets\">&#94;</span>");
            highlightedMessage = highlightedMessage.replace("\n", "<span class=\"hl-crlf\">[CR]</span><br/>");

            return highlightedMessage;
        }

        return CANNOT_COMPUTE_ER7_FORM_XML;
    }

}
