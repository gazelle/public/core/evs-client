package net.ihe.gazelle.evsclient.interlay.adapters.validationservice.factory;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.application.validationservice.AbstractValidationServiceFactory;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.EmbeddedValidationServiceConf;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.SystemValidationServiceConf;
import net.ihe.gazelle.evsclient.interlay.adapters.validationservice.PixelMedValidationService;

public class PixelMedValidationServiceFactory extends AbstractValidationServiceFactory<PixelMedValidationService, EmbeddedValidationServiceConf> {

    public PixelMedValidationServiceFactory(ApplicationPreferenceManager applicationPreferenceManager, EmbeddedValidationServiceConf configuration) {
        super(applicationPreferenceManager, configuration, PixelMedValidationService.class);
    }
}
