/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evsclient.interlay.gui.menu;

import net.ihe.gazelle.common.pages.Authorization;
import net.ihe.gazelle.common.pages.Page;

public class PageMenu implements Page {

    private int iMenu;

    private final String title;
    private final String img;
    private final String id;

    private Authorization[] authorizations;

    public PageMenu(final String title, final String img, final Authorization... authorizations) {
        this.title = title;
        this.img = img;
        this.id = "menu_" + this.iMenu;
        this.iMenu++;
        this.authorizations = authorizations.clone();
    }

    @Override
    public String getId() {
        return this.id;
    }

    @Override
    public String getLabel() {
        return this.title;
    }

    @Override
    public String getLink() {
        return "";
    }

    @Override
    public String getIcon() {
        return this.img;
    }

    @Override
    public Authorization[] getAuthorizations() {
        if(this.authorizations != null) {
            return this.authorizations.clone();
        }
        else {
            return null;
        }
    }

    @Override
    public String getMenuLink() {
        return this.getLink().replace(".xhtml", ".seam");
    }
}
