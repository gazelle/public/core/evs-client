package net.ihe.gazelle.evsclient.application.security;

import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

/**
 * Class for a GazelleIdentity corresponding to a ApiKey
 * Some of the methods return a default value to avoid throwing an exception. This is due to the fact that the ApiKeyIdentity
 * is incomplete.
 */
public class ApiKeyIdentity implements GazelleIdentity, Serializable {
    private static final Logger LOG = LoggerFactory.getLogger(ApiKeyIdentity.class);

    private static final long serialVersionUID = -8784894045842514453L;
    private static final String FALSE = "false";
    private final boolean loggedIn;
    private final String username;
    private final String organization;

    public ApiKeyIdentity() {
        this.loggedIn = false;
        this.username = null;
        this.organization = null;
    }

    public ApiKeyIdentity(String username, String organization) {
        this.username = sanitize(username, "ApiKeyIdentity.username");
        this.organization = sanitize(organization, "ApiKeyIdentity.organization");
        this.loggedIn = true;
    }

    @Override
    public boolean isLoggedIn() {
        return loggedIn;
    }

    @Override
    public String getOrganisationKeyword() {
        return organization;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public String getFirstName() {
        logUnsupportedMethodMessage("getFirstName()", "ApiKeyIdentity.firstName");
        return "ApiKeyIdentity.firstName";
    }


    @Override
    public String getLastName() {
        logUnsupportedMethodMessage("getLastName()", "ApiKeyIdentity.lastName");
        return "ApiKeyIdentity.lastName";
    }

    @Override
    public String getEmail() {
        logUnsupportedMethodMessage("getEmail()", "ApiKeyIdentity.email");
        return "ApiKeyIdentity.email";
    }

    @Override
    public boolean hasRole(String role) {
        logUnsupportedMethodMessage("hasRole(String role)", FALSE);
        return false;
    }

    @Override
    public boolean hasPermission(String name, String action, Object... contexts) {
        logUnsupportedMethodMessage("hasPermission(String name, String action, Object... contexts)", FALSE);
        return false;
    }

    @Override
    public boolean hasPermission(Object target, String action) {
        logUnsupportedMethodMessage("hasPermission(String name, String action, Object... contexts)", FALSE);
        return false;
    }

    @Override
    public String getDisplayName() {
        logUnsupportedMethodMessage("getDisplayName()", "ApiKeyIdentity.displayName");
        return "ApiKeyIdentity.displayName";
    }

    private String sanitize(String value, String valueName) {
        if (value != null && !value.isEmpty()) {
            return value;
        } else {
            throw new IllegalArgumentException(String.format("%s must be defined", valueName));
        }
    }

    private void logUnsupportedMethodMessage(String methodName, String defaultValueReturned) {
        LOG.error("ApiKeyIdentity doesn't support {} calls, returned {} by default", methodName, defaultValueReturned);
    }
}
