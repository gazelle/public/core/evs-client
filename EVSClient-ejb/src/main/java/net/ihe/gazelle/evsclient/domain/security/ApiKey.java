package net.ihe.gazelle.evsclient.domain.security;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

@Entity
@Table(name = "evsc_api_key", schema = "public")
public class ApiKey implements Serializable {

    private static final long serialVersionUID = 1627259107159098076L;

    @Id
    @Column(name = "value")
    private String value;

    @Column(name = "owner_username")
    private String owner;

    @Column(name = "owner_organization")
    private String organization;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "creation_date")
    private Date creationDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "expiration_date")
    private Date expirationDate;

    public ApiKey() {

    }

    public ApiKey(String value, String owner, String organization, Date creationDate, Date expirationDate) {
        this.value = value;
        this.owner = owner;
        this.organization = organization;
        this.creationDate = creationDate;
        this.expirationDate = expirationDate;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public boolean isExpired() {
        return expirationDate.before(Calendar.getInstance().getTime());
    }
}
