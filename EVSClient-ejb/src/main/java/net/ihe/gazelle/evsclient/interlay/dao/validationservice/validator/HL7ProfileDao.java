package net.ihe.gazelle.evsclient.interlay.dao.validationservice.validator;

public interface HL7ProfileDao {
    String findPackageNameWithProfileOID(final String profileOID);
}
