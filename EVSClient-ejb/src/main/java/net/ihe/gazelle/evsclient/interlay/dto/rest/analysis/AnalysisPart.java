package net.ihe.gazelle.evsclient.interlay.dto.rest.analysis;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import net.ihe.gazelle.evsclient.domain.validation.ValidationType;
import net.ihe.gazelle.evsclient.interlay.dto.rest.AbstractDTO;
import net.ihe.gazelle.evsclient.interlay.dto.rest.validation.HandledObject;
import net.ihe.gazelle.evsclient.interlay.dto.rest.validation.ValidationService;
import net.ihe.gazelle.mca.contentanalyzer.business.model.EncodedType;

import javax.validation.constraints.Pattern;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;
import java.util.Objects;

@Schema(description="Sous-Partie d'une analyse de contenu.  Etant donné la richesse des objets analysables (documents, archives, messages XML, requêtes, etc.) Il est possible qu'un objet corresponde à plusieurs type d'objets. L'analyse est donc découpée en parties (AnalysisPart) pour chaque contenu différent identifié. Ces parties peuvent elle-même contenir d'autres parties (childrenParts). ")
@XmlRootElement(namespace = "http://evsobjects.gazelle.ihe.net/", name="analysisPart")
@XmlAccessorType(XmlAccessType.FIELD)
public class AnalysisPart extends AbstractDTO<net.ihe.gazelle.mca.contentanalyzer.business.model.AnalysisPart> {

  public AnalysisPart(net.ihe.gazelle.mca.contentanalyzer.business.model.AnalysisPart domain) {
    super(domain);
  }

  public AnalysisPart() {
    super();
  }

  /**
   **/
  
  @Schema(name= "encodedType",description = "") // TODO
  @JsonProperty("encodedType")
  @XmlElement(namespace = "http://evsobjects.gazelle.ihe.net/", name = "encodedType")
  public EncodedType getEncodedType() {
    return domain.getEncodedType();
  }

  /**
   **/
  
  @Schema(name= "validationType",description = "") // TODO
  @JsonProperty("validationType")
  @XmlElement(namespace = "http://evsobjects.gazelle.ihe.net/", name = "validationType")
  public ValidationType getValidationType() {
    return domain.getValidationType();
  }

  /**
   **/
  
  @Schema(name= "objectType",description = "") // TODO
  @JsonProperty("objectType")
  @XmlElement(namespace = "http://evsobjects.gazelle.ihe.net/", name = "objectType")
  public String getObjectType() {
    return ""; // TODO
  }

  /**
   **/
  
  @Schema(name= "validationService",description = "") // TODO
  @JsonProperty("validationService")
  @XmlElement(namespace = "http://evsobjects.gazelle.ihe.net/", name = "validationService")
  public ValidationService getValidationService() {
    return null; // TODO return new ValidationService(domain.getValidation().);
  }

  /**
   **/
  
  @Schema(name= "xValInputType",description = "") // TODO
  @JsonProperty("xValInputType")
  @XmlElement(namespace = "http://evsobjects.gazelle.ihe.net/", name = "xValInputType")
  public String getXValInputType() {
    return domain.getxValInputType();
  }

  /**
   **/
  
  @Schema(name= "startOffset",description = "") // TODO
  @JsonProperty("startOffset")
  @XmlElement(namespace = "http://evsobjects.gazelle.ihe.net/", name = "startOffset")
  public Integer getStartOffset() {
    return domain.getStartOffset();
  }

  /**
   **/
  
  @Schema(name= "endOffset",description = "") // TODO
  @JsonProperty("endOffset")
  @XmlElement(namespace = "http://evsobjects.gazelle.ihe.net/", name = "endOffset")
  public Integer getEndOffset() {
    return domain.getEndOffset();
  }

  /**
   **/
  
  @Schema(name= "noDefinedNamespaces",description = "") // TODO
  @JsonProperty("noDefinedNamespaces")
  @XmlElement(namespace = "http://evsobjects.gazelle.ihe.net/", name = "noDefinedNamespaces")
  public Boolean isNoDefinedNamespaces() {
    return domain.getNoDefinedNamespace();
  }

  /**
   **/
  
  @Schema(name= "namespaces",description = "") // TODO
  @JsonProperty("namespaces")
  @XmlElement(namespace = "http://evsobjects.gazelle.ihe.net/", name = "namespaces")
  public List<HandledObject> getNamespaces() {
    return null; // TODO return domain.getNamespaces();
  }

  /**
   **/
  
  @Schema(name= "childrenParts",description = "") // TODO
  @JsonProperty("childrenParts")
  @XmlElement(namespace = "http://evsobjects.gazelle.ihe.net/", name = "childrenParts")
  public List<AnalysisPart> getChildrenParts() {
    return null; // TODO return domain.getChildPart();
  }

  /**
   **/
  
  @Schema(name= "partLog",description = "") // TODO
  @JsonProperty("partLog")
  @Pattern(regexp="^(?:[A-Za-z0-9+/]{4})*(?:[A-Za-z0-9+/]{2}==|[A-Za-z0-9+/]{3}=)?$")
  @XmlElement(namespace = "http://evsobjects.gazelle.ihe.net/", name = "partLog")
  public byte[] getPartLog() {
    return null; // TODO return domain.getPartLog();
  }

  /**
   **/
  
  @Schema(name= "validationRef",description = "") // TODO
  @JsonProperty("validationRef")
  @XmlElement(namespace = "http://evsobjects.gazelle.ihe.net/", name = "validationRef")
  public ValidationRef getValidationRef() {
    return new ValidationRef(domain.getValidation());
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AnalysisPart analysisPart = (AnalysisPart) o;
    return Objects.equals(getEncodedType(), analysisPart.getEncodedType()) &&
        Objects.equals(getValidationType(), analysisPart.getValidationType()) &&
        Objects.equals(getObjectType(), analysisPart.getObjectType()) &&
        Objects.equals(getValidationService(), analysisPart.getValidationService()) &&
        Objects.equals(getXValInputType(), analysisPart.getXValInputType()) &&
        Objects.equals(getStartOffset(), analysisPart.getStartOffset()) &&
        Objects.equals(getEndOffset(), analysisPart.getEndOffset()) &&
        Objects.equals(isNoDefinedNamespaces(), analysisPart.isNoDefinedNamespaces()) &&
        Objects.equals(getNamespaces(), analysisPart.getNamespaces()) &&
        Objects.equals(getChildrenParts(), analysisPart.getChildrenParts()) &&
        Objects.equals(getPartLog(), analysisPart.getPartLog()) &&
        Objects.equals(getValidationRef(), analysisPart.getValidationRef());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getEncodedType(), getValidationType(), getObjectType(), getValidationService(), getXValInputType(), getStartOffset(), getEndOffset(), isNoDefinedNamespaces(), getNamespaces(), getChildrenParts(), getPartLog(), getValidationRef());
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AnalysisPart {\n");
    
    sb.append("    encodedType: ").append(toIndentedString(getEncodedType())).append("\n");
    sb.append("    validationType: ").append(toIndentedString(getValidationType())).append("\n");
    sb.append("    objectType: ").append(toIndentedString(getObjectType())).append("\n");
    sb.append("    validationService: ").append(toIndentedString(getValidationService())).append("\n");
    sb.append("    xValInputType: ").append(toIndentedString(getXValInputType())).append("\n");
    sb.append("    startOffset: ").append(toIndentedString(getStartOffset())).append("\n");
    sb.append("    endOffset: ").append(toIndentedString(getEndOffset())).append("\n");
    sb.append("    noDefinedNamespaces: ").append(toIndentedString(isNoDefinedNamespaces())).append("\n");
    sb.append("    namespaces: ").append(toIndentedString(getNamespaces())).append("\n");
    sb.append("    childrenParts: ").append(toIndentedString(getChildrenParts())).append("\n");
    sb.append("    partLog: ").append(toIndentedString(getPartLog())).append("\n");
    sb.append("    validationRef: ").append(toIndentedString(getValidationRef())).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

