package net.ihe.gazelle.evsclient.interlay.adapters.validationservice;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.application.validation.ReportParser;
import net.ihe.gazelle.evsclient.domain.processing.HandledObject;
import net.ihe.gazelle.evsclient.domain.validation.ValidationType;
import net.ihe.gazelle.evsclient.domain.validation.types.ValidationProperties;
import net.ihe.gazelle.evsclient.domain.validationservice.ValidationServiceOperationException;
import net.ihe.gazelle.evsclient.domain.validationservice.WebValidationService;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.WebValidationServiceConf;
import net.ihe.gazelle.evsclient.interlay.adapters.validation.MbvReportMapper;
import net.ihe.gazelle.mb.validator.client.MBValidator;
import net.ihe.gazelle.mb.validator.client.ServiceConnectionError;
import net.ihe.gazelle.mb.validator.client.ServiceOperationError;

import javax.xml.bind.DatatypeConverter;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;

@ValidationProperties(
      name = "Model based",
      types = {
            ValidationType.ATNA,
            ValidationType.AUDIT_MESSAGE,
            ValidationType.DICOM_WEB,
            ValidationType.DSUB,
            ValidationType.FHIR,
            ValidationType.HPD,
            ValidationType.SAML,
            ValidationType.SVS,
            ValidationType.XDS,
            ValidationType.XDW,
            ValidationType.XML
      })
@ReportParser(MbvReportMapper.class)
public class ModelBasedValidationServiceClient extends AbstractValidationServiceClient implements WebValidationService {

   private final MBValidator mbValidator;

   public ModelBasedValidationServiceClient(ApplicationPreferenceManager applicationPreferenceManager, WebValidationServiceConf configuration) {
      super(applicationPreferenceManager, configuration);
      Long validationTimeout = getValidationTimeout();
      if (validationTimeout != null) {
         mbValidator = new MBValidator(getUrl(), validationTimeout);
      } else {
         mbValidator = new MBValidator(getUrl());
      }
   }

   @Override
   public String about() {
      return "Mbv";
   }

   @Override
   protected List<String> getValidatorNames(String validatorFilter) {
      try {
         return mbValidator.listAvailableValidators(validatorFilter == null ? "" : validatorFilter);
      } catch (ServiceConnectionError | ServiceOperationError e) {
         throw new ValidationErrorException(e);
      }
   }

   @Override
   public String getName() {
      return configuration.getName();
   }

   @Override
   public List<String> getSupportedMediaTypes() {
      return Arrays.asList("application/xml");
   }

   @Override
   public byte[] validate(HandledObject[] objects, String validator) throws ValidationServiceOperationException {
      try {
         if (!configuration.isZipTransport()) {
            return this.mbValidator
                  .validate(DatatypeConverter.printBase64Binary(objects[0].getContent()), validator, true)
                  .getBytes(StandardCharsets.UTF_8);

         } else {
            return this.mbValidator.zipAndvalidateXMLAndReturnXMLString(objects[0].getContent(), validator, "application/zip")
                  .getBytes(StandardCharsets.UTF_8);
         }
      } catch (ServiceConnectionError | ServiceOperationError e) {
         throw new ValidationServiceOperationException(e);
      }
   }
}
