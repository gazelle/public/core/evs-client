package net.ihe.gazelle.evsclient.interlay.dto.rest.validation.report;

import net.ihe.gazelle.evsclient.domain.validation.report.SeverityLevel;
import net.ihe.gazelle.evsclient.domain.validation.report.ValidationReport;
import net.ihe.gazelle.evsclient.interlay.dto.rest.AbstractEvsMarshaller;
import net.ihe.gazelle.evsclient.interlay.dto.rest.MarshallingException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.InputStream;
import java.io.OutputStream;

public class XMLValidationReportMarshaller extends AbstractEvsMarshaller<ValidationReport> {

   private static final JAXBContext JAXB_CONTEXT = instantiateContext();
   private SeverityLevel severity;

   public XMLValidationReportMarshaller(SeverityLevel severity) {
      this.severity = severity;
   }
   public XMLValidationReportMarshaller() {
      this(SeverityLevel.INFO);
   }

   private static JAXBContext instantiateContext() {
      try {
         return JAXBContext.newInstance(ValidationReportDTO.class);
      } catch (JAXBException e) {
         throw new RuntimeException("Unable to set up JAXB Context for ValidationReportDTO.class", e);
      }
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public void marshal(ValidationReport report, OutputStream outputStream) throws MarshallingException {
      try {
         Marshaller jaxbMarshaller = JAXB_CONTEXT.createMarshaller();
         jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, isIndentation());
         jaxbMarshaller.setProperty(Marshaller.JAXB_FRAGMENT, !isCompleteDocument());
         jaxbMarshaller.marshal(new ValidationReportDTO(report, severity), outputStream);
      } catch (JAXBException e) {
         throw new MarshallingException("Unable to marshall Validation Report.", e);
      }
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public ValidationReport unmarshal(InputStream inputStream) throws MarshallingException {
      try {
         final Unmarshaller unm = JAXB_CONTEXT.createUnmarshaller();
         return ((ValidationReportDTO) unm.unmarshal(inputStream)).toDomain();
      } catch (JAXBException e) {
         throw new MarshallingException("Unable to unmarshall Validation Report.", e);
      }
   }
}