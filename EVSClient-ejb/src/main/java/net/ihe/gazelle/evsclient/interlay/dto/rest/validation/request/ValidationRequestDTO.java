package net.ihe.gazelle.evsclient.interlay.dto.rest.validation.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import net.ihe.gazelle.evsclient.domain.validation.request.ValidationItem;
import net.ihe.gazelle.evsclient.domain.validation.request.ValidationRequest;

import java.util.ArrayList;
import java.util.List;

@JsonRootName(value = "validationRequest")
public class ValidationRequestDTO extends ValidationRequest {

    public ValidationRequestDTO(){
       super();
    }

    public ValidationRequestDTO(ValidationRequest validationRequest){
        this.setApiVersion(validationRequest.getApiVersion());
        this.setValidationServiceName(validationRequest.getValidationServiceName());
        this.setValidationProfileId(validationRequest.getValidationProfileId());
        this.setValidationItems(validationRequest.getValidationItems());
    }

    @JsonProperty(value = "apiVersion")
    @Override
    public String getApiVersion() {
        return super.getApiVersion();
    }

    @JsonProperty(value = "validationServiceName")
    @Override
    public String getValidationServiceName() {
        return super.getValidationServiceName();
    }

    @JsonProperty(value = "validationProfileId")
    @Override
    public String getValidationProfileId() {
        return super.getValidationProfileId();
    }

    @JsonProperty(value = "validationItems")
    @Override
    public List<ValidationItem> getValidationItems() {
        List<ValidationItem> validationItems = super.getValidationItems();
        List<ValidationItem> validationItemsDTO = new ArrayList<>();
        for (ValidationItem validationItem : validationItems) {
            validationItemsDTO.add(new ValidationItemDTO(validationItem));
        }
        return validationItemsDTO;
    }

    @JsonIgnore
    @Override
    public boolean isValidationProfileIdValid() {
        return super.isValidationProfileIdValid();
    }

    @JsonIgnore
    @Override
    public boolean isValidationServiceNameValid() {
        return super.isValidationServiceNameValid();
    }

    @JsonIgnore
    @Override
    public boolean isValidationItemsValid() {
        return super.isValidationItemsValid();
    }

    @JsonIgnore
    @Override
    public boolean isValid() {
        return super.isValid();
    }
}
