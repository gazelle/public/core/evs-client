package net.ihe.gazelle.evsclient.domain.validationservice;

import net.ihe.gazelle.evsclient.domain.validationservice.configuration.ProcessingConf;

public interface ValidationServiceFactory<S extends ValidationService, C extends ProcessingConf> extends ProcessingServiceFactory<S,C> {

}
