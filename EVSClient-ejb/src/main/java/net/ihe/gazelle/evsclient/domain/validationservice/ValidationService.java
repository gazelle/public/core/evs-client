package net.ihe.gazelle.evsclient.domain.validationservice;

import net.ihe.gazelle.evsclient.domain.processing.HandledObject;

import java.util.List;

/**
 * Validation Service API.
 * <p>
 * Interact with a validation service, get information on available validators, supported object-types and media-types and trigger a validation.
 * <p>
 * <i>A Validator can be seen as a validation profile or a set of rules, whereas a validation service is the engine providing the mechanism to apply the rules of the validator.</i>
 *
 * @author ceoche
 */
public interface ValidationService extends ProcessingService {

   /**
    * Get the list of available Validators
    *
    * @return list of available validators
    */
   List<Validator> getValidators();

   /**
    * Get the list of available Validators filtered by a domain (or a disciminator string)
    *
    * @param filter to filter Validators list on.
    *
    * @return the list of validators that match the given domain filter.
    */
   List<Validator> getValidators(String filter);

   /**
    * Get the list of supported media-types (MIME Types of the objects to validate). <i>Media types returned should be part of the official IANA
    * list.</i>
    *
    * @return the list of Media-Types the Validation Service is supporting.
    *
    * @see <a href="https://www.iana.org/assignments/media-types/media-types.xhtml">https://www.iana.org/assignments/media-types/media-types.xhtml</a>
    */
   List<String> getSupportedMediaTypes();

   /**
    * Validate an object according to the requested Validator and get a detailed report. The validator will try to perform all decoding and
    * verifications, even if one is failed, to return the most complete report possible.
    *
    * @param objects   the object to validate in raw bytes (document, message, archive or ressource, that may depends on the validation service).
    * @param validator the keyword of the validator to use to verify the object.
    *
    * @return a detailed report of the validation, usually a plain text, XML or JSON structured document but in raw bytes.
    *
    * @throws ValidationServiceOperationException if the requested validation raised an unexpected issue or if the service is unavailable at the time
    *                                             of the call.
    */
   byte[] validate(HandledObject[] objects, String validator) throws ValidationServiceOperationException;

}
