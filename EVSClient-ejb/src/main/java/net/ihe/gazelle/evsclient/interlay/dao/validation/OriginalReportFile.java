package net.ihe.gazelle.evsclient.interlay.dao.validation;

import net.ihe.gazelle.evsclient.domain.validation.Report;
import net.ihe.gazelle.evsclient.interlay.dao.FileReadException;
import org.apache.commons.io.IOUtils;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.io.IOException;

@Entity
@DiscriminatorValue("original-report")
public class OriginalReportFile extends ReportFile<byte[]> {

   private static final long serialVersionUID = 1317064647463854880L;

   // For JPA Only
   public OriginalReportFile() {
      super();
   }

   public OriginalReportFile(Report<byte[]> report) {
      super(report);
   }

   @Override
   public byte[] getContent() {
      try {
         return super.getContent();
      } catch (FileReadException e) {
         return new byte[]{};
      }
   }

   @Override
   byte[] unmarshall(ReportZipInputStream inputStream) throws IOException {
      return IOUtils.toByteArray(inputStream);
   }

   @Override
   void marshall(byte[] content, ReportZipOutputStream outputStream) throws IOException {
      outputStream.write(content);
   }
}
