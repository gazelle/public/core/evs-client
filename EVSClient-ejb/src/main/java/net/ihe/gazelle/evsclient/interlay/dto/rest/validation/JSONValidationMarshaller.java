package net.ihe.gazelle.evsclient.interlay.dto.rest.validation;

import net.ihe.gazelle.evsclient.domain.validation.Validation;
import net.ihe.gazelle.evsclient.interlay.dto.rest.JSONMarshaller;

public class JSONValidationMarshaller extends JSONMarshaller<Validation,ValidationDTO> {
   public JSONValidationMarshaller() {
      super(ValidationDTO.class);
   }
   @Override
   protected ValidationDTO toDTO(Validation domain) {
      return new ValidationDTO(domain);
   }
}
