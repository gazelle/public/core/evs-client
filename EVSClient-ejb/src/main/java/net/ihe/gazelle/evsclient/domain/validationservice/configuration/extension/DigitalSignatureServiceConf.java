package net.ihe.gazelle.evsclient.domain.validationservice.configuration.extension;

import net.ihe.gazelle.evsclient.domain.validation.Validation;
import net.ihe.gazelle.evsclient.domain.validation.extension.DigitalSignature;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.ValidationServiceConf;
import net.ihe.gazelle.evsclient.interlay.adapters.validationservice.DigitalSignatureExtensionServiceClient;
import net.ihe.gazelle.evsclient.interlay.adapters.validationservice.factory.DigitalSignatureExtensionServiceFactory;
import net.ihe.gazelle.evsclient.interlay.factory.XmlDocumentBuilderFactory;
import net.ihe.gazelle.evsclient.interlay.gui.I18n;
import net.ihe.gazelle.preferences.PreferenceService;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.xml.parsers.DocumentBuilder;
import java.io.ByteArrayInputStream;

@Entity
@Table(name = "evsc_digital_signature_service_conf", schema = "public")
public class DigitalSignatureServiceConf extends WebExtensionServiceConf<DigitalSignature, DigitalSignatureExtensionServiceClient, DigitalSignatureServiceConf, DigitalSignatureExtensionServiceFactory> {
    private static final long serialVersionUID = 6067224015483089438L;



    public DigitalSignatureServiceConf(ValidationServiceConf validationServiceConf, Validation validation) {
        super(null,
                I18n.get("net.ihe.gazelle.evs.digital-signature-description"),
                "dsig",
                DigitalSignatureExtensionServiceFactory.class.getCanonicalName(),
                isServiceAvailable(validationServiceConf,validation)&&PreferenceService.getBoolean("enable_dsig_validation"),
                buildUrl(validationServiceConf),
                isZipped(validationServiceConf));
    }

    public DigitalSignatureServiceConf() {
        super();
    }

    public static boolean isServiceAvailable(ValidationServiceConf validationServiceConf, Validation validation) {
        try {
            DocumentBuilder builder = new XmlDocumentBuilderFactory()
                    .setNamespaceAware(true)
                    .getBuilder();
            Document document = builder.parse(new ByteArrayInputStream(validation.getObject().getContent()));
            NodeList signatureNodes = document.getElementsByTagNameNS("http://www.w3.org/2000/09/xmldsig#", "Signature");
            if (signatureNodes != null && signatureNodes.getLength() > 0) {
                return true;
            }
        } catch (Exception e) {}
        return false;
    }


    private static boolean isZipped(ValidationServiceConf validationServiceConf) {
        return false;
    }

    private static String buildUrl(ValidationServiceConf validationServiceConf) {
        return PreferenceService.getString("default_dsig_service_wsdl");
    }

}
