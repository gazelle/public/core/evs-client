package net.ihe.gazelle.evsclient.interlay.adapters.validationservice;

import edu.harvard.hul.ois.jhove.HandlerBase;
import edu.harvard.hul.ois.jhove.JhoveBase;
import edu.harvard.hul.ois.jhove.JhoveException;
import edu.harvard.hul.ois.jhove.RepInfo;
import edu.harvard.hul.ois.jhove.handler.XmlHandler;
import edu.harvard.hul.ois.jhove.module.PdfModule;
import edu.harvard.hul.ois.jhove.module.pdf.PdfProfile;
import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.application.validation.ReportParser;
import net.ihe.gazelle.evsclient.application.validation.ValidationReportMapper;
import net.ihe.gazelle.evsclient.domain.processing.HandledObject;
import net.ihe.gazelle.evsclient.domain.validation.ValidationType;
import net.ihe.gazelle.evsclient.domain.validation.report.*;
import net.ihe.gazelle.evsclient.domain.validation.types.ValidationProperties;
import net.ihe.gazelle.evsclient.domain.validationservice.EmbeddedValidationService;
import net.ihe.gazelle.evsclient.domain.validationservice.ValidationServiceOperationException;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.EmbeddedValidationServiceConf;
import net.ihe.gazelle.evsclient.interlay.adapters.validation.AbstractReportMapper;
import net.ihe.gazelle.evsclient.interlay.adapters.validation.PlainReportMapper;
import net.ihe.gazelle.evsclient.interlay.dto.rest.validation.report.XMLValidationReportMarshaller;
import net.ihe.gazelle.evsclient.interlay.factory.DocumentBuilderFactoryException;
import net.ihe.gazelle.evsclient.interlay.factory.XmlDocumentBuilderFactory;
import org.apache.commons.lang.StringUtils;
import org.richfaces.json.XML;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.bootstrap.DOMImplementationRegistry;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSOutput;
import org.w3c.dom.ls.LSSerializer;
import org.xml.sax.SAXException;

import javax.activation.MimeType;
import javax.activation.MimeTypeParseException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;

@ValidationProperties(
        name="PDF Validation",
        types =ValidationType.PDF)
@ReportParser(PlainReportMapper.class)
public class PdfValidationService extends AbstractConfigurableValidationService<EmbeddedValidationServiceConf> implements EmbeddedValidationService {
    private static String VALIDATOR_ID = "JHOVE";
    private static MimeType XML_MIME_TYPE;
    static {
        try {
            XML_MIME_TYPE = new MimeType("application/xml");
        } catch (MimeTypeParseException e) {
            e.printStackTrace();
        }
    }
    private static class PdfModuleProfiles extends PdfModule {
        public List getProfiles() {
            return this._profile;
        }
    }

    private class PdfReportBuilder extends AbstractReportMapper {
        public PdfReportBuilder() {
        }

        ValidationReport build(PdfModuleProfiles module, RepInfo info, Document xml) throws XPathExpressionException {
            final List<String> profiles = info.getProfile();
            final List<PdfProfile> moduleProfiles = module.getProfiles();
            final List<String> moduleTxtProfiles = new ArrayList<>(moduleProfiles.size());
            for (final Object object : moduleProfiles) {
                final PdfProfile profile = (PdfProfile) object;
                moduleTxtProfiles.add(profile.getText());
            }
            Collections.sort(moduleTxtProfiles);
            Collections.sort(profiles);

            ValidationReport report = new ValidationReport(
                    UUID.randomUUID().toString(),
                    new ValidationOverview(
                            ValidationReportMapper.DEFAULT_DISCLAIMER,
                            PdfValidationService.this.getName(),
                            PdfValidationService.this.getServiceVersion(),
                            VALIDATOR_ID,
                            module.getRelease(),
                            Arrays.asList(
                                    new Metadata("note",info.getNote()),
                                    new Metadata("foundProfiles", StringUtils.join(profiles, "; ")),
                                    new Metadata("testedProfiles", StringUtils.join(moduleTxtProfiles, "; ")),
                                    new Metadata("documentProperties", serializeElement(xml.getDocumentElement().getElementsByTagName("properties"))
                                    ))));

            report.addSubReport(parseDocument(info,xml,profiles));

            report.addSubReport(buildPdfValiditySubReport(info));

            report.addSubReport(buildPdfWellFormedSubReport(info));

            return report;

        }

        private String serializeElement(NodeList properties) {
            try {
                DOMImplementationRegistry registry = DOMImplementationRegistry.newInstance();
                DOMImplementationLS domImplLS = (DOMImplementationLS) registry.getDOMImplementation("LS");

                LSSerializer ser = domImplLS.createLSSerializer();

                LSOutput out = domImplLS.createLSOutput();
                StringWriter sw = new StringWriter();
                out.setCharacterStream(sw);

                for (int i = 0; i < properties.getLength(); i++) {
                    Node xml = properties.item(i);
                    ser.write(xml, out);
                }
                return XML.toJSONObject(
                            sw.toString()
                        ).toString();
            } catch (Exception e) {
                return e.getMessage();
            }
        }

        private ValidationSubReport buildPdfValiditySubReport(RepInfo info) {
            ValidationSubReport vsr = new ValidationSubReport(
                    "Pdf Validity",
                    new ArrayList<String>()
            );
            ValidationTestResult validity;
            switch (info.getValid()) {
                case RepInfo.TRUE:
                    validity = ValidationTestResult.PASSED;
                    break;
                case RepInfo.FALSE:
                    validity = ValidationTestResult.FAILED;
                    break;
                default:
                    validity = ValidationTestResult.UNDEFINED;
                    break;
            }
            vsr.addConstraintValidation(new ConstraintValidation("validity status",ConstraintPriority.MANDATORY, validity));
            return vsr;
        }

        private ValidationSubReport buildPdfWellFormedSubReport(RepInfo info) {
            ValidationSubReport vsr = new ValidationSubReport(
                    "Syntactic validation",
                    new ArrayList<String>()
            );
            ValidationTestResult validity;
            switch (info.getWellFormed()) {
                case RepInfo.TRUE:
                    validity = ValidationTestResult.PASSED;
                    break;
                case RepInfo.FALSE:
                    validity = ValidationTestResult.FAILED;
                    break;
                default:
                    validity = ValidationTestResult.UNDEFINED;
                    break;
            }
            vsr.addConstraintValidation(new ConstraintValidation("well formed",ConstraintPriority.MANDATORY, validity));
            return vsr;
        }

        private ValidationSubReport parseDocument(RepInfo info,Document xml,List<String> profiles) throws XPathExpressionException {
            ValidationSubReport vsr = new ValidationSubReport(
                    "Pdf validation",
                    new ArrayList<String>()
            );
            String prefix = "PDF";
            for (final String profile : info.getProfile()) {
                if (profile.contains("PDF/A")) {
                    prefix = "PDF/A";
                }
            }
            vsr.addConstraintValidation(new ConstraintValidation("Document must fit PDF/A profile",
                    ConstraintPriority.MANDATORY,
                    "PDF/A".equals(prefix)
                            ?ValidationTestResult.PASSED
                            :ValidationTestResult.FAILED));

            vsr.addConstraintValidation(new ConstraintValidation("Document must be a valid well formed PDF/A",
                    ConstraintPriority.MANDATORY,
                    info.getWellFormed()==RepInfo.TRUE&&info.getValid()==RepInfo.TRUE&&"PDF/A".equals(prefix)
                        ?ValidationTestResult.PASSED
                        :ValidationTestResult.FAILED));

            XPath xPath = XPathFactory.newInstance().newXPath();
            NodeList nodeList = (NodeList) xPath.compile("//messages/message").evaluate(xml, XPathConstants.NODESET);
            for (int i=0 ; i<nodeList.getLength() ; i++) {
                Node node = nodeList.item(i);
                String severity = node.getAttributes().getNamedItem("severity").getNodeValue();
                ConstraintPriority priority;
                ValidationTestResult result;
                switch (severity) {
                    case "error":
                        priority=ConstraintPriority.MANDATORY;
                        result=ValidationTestResult.FAILED;
                        break;
                    default:
                        priority=ConstraintPriority.RECOMMENDED;
                        result=ValidationTestResult.PASSED;
                }
                vsr.addConstraintValidation(
                        new ConstraintValidation(
                                node.getTextContent(),
                                priority,
                                result));

            }
            return vsr;
        }
    }


    private String serviceVersion;

    public PdfValidationService(ApplicationPreferenceManager applicationPreferenceManager, EmbeddedValidationServiceConf configuration) {
        super(applicationPreferenceManager, configuration);
    }

    @Override
    protected List<String> getValidatorNames(String validatorFilter) {
        return Arrays.asList(VALIDATOR_ID);
    }

    @Override
    // TODO
    public String about() {
        return null;
    }

    @Override
    public String getName() {
        return configuration.getName();
    }

    @Override
    public List<String> getSupportedMediaTypes() {
        return Arrays.asList("application/pdf");
    }

    @Override
    public byte[] validate(HandledObject[] objects, String validator) throws ValidationServiceOperationException {
        try {
            final PdfModuleProfiles module = new PdfModuleProfiles();
            module.setDefaultParams(new ArrayList<String>());
            File pdfFile = File.createTempFile("jhove_", ".pdf");
            FileOutputStream outputStream = new FileOutputStream(pdfFile);
            outputStream.write(objects[0].getContent());
            outputStream.close();
            final RandomAccessFile raf = new RandomAccessFile(pdfFile, "r");
            final RepInfo info = new RepInfo("");
            module.parse(raf, info);
            raf.close();

            // Export
            DocumentBuilder builder = new XmlDocumentBuilderFactory().getBuilder();
            Document xml = builder.parse(new ByteArrayInputStream(this.getResult(info, new XmlHandler()).getBytes(StandardCharsets.UTF_8)));

            ValidationReport report = new PdfReportBuilder().build(module,info,xml);

            return new XMLValidationReportMarshaller(SeverityLevel.INFO)
                    .marshal(report).getBytes(StandardCharsets.UTF_8);
        } catch (JhoveException | IOException | XPathExpressionException | DocumentBuilderFactoryException | SAXException e) {
            LOGGER.error(String.format("an unexpected error occurred during the validation: %s", e.getMessage()));
            throw new ValidationServiceOperationException(e);
        }
    }

    private String getResult(final RepInfo info, final HandlerBase handler) throws JhoveException {
        final JhoveBase je = new JhoveBase();
        je.setSignatureFlag(false);
        handler.setBase(je);
        final StringWriter sw = new StringWriter();
        final PrintWriter pw = new PrintWriter(sw);
        handler.reset();
        handler.setWriter(pw);
        info.show(handler);
        pw.close();
        return sw.toString();
    }

    public String getServiceVersion() {
        if (serviceVersion==null) {
            serviceVersion = getVersion(VALIDATOR_ID,"Version");
        }
        return serviceVersion;
    }

}
