package net.ihe.gazelle.evsclient.interlay.dao.validation;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.application.validation.ValidationDao;
import net.ihe.gazelle.evsclient.domain.processing.EntryPoint;
import net.ihe.gazelle.evsclient.domain.processing.UnexpectedProcessingException;
import net.ihe.gazelle.evsclient.domain.statistics.IntervalForStatistics;
import net.ihe.gazelle.evsclient.domain.statistics.StatisticsDataRow;
import net.ihe.gazelle.evsclient.domain.validation.Validation;
import net.ihe.gazelle.evsclient.domain.validation.ValidationQuery;
import net.ihe.gazelle.evsclient.domain.validation.ValidationStatus;
import net.ihe.gazelle.evsclient.domain.validation.ValidationType;
import net.ihe.gazelle.evsclient.interlay.dao.AbstractProcessingDaoImpl;
import net.ihe.gazelle.evsclient.interlay.dao.FileRepository;
import net.ihe.gazelle.evsclient.interlay.dao.Repository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class ValidationDaoImpl extends AbstractProcessingDaoImpl<Validation> implements ValidationDao {

   private static final Logger LOGGER = LoggerFactory.getLogger(ValidationDaoImpl.class);

   private static final String ARCHIVE_NAME_PREFIX = "result_";
   private static final String ARCHIVE_NAME_EXTENSION = ".zip";
   private static final FileRepository DEFAULT_REPOSITORY = RepositoryTypes.XML;

   private final ValidationReportFileDAO validationReportFileDao;

   public ValidationDaoImpl(EntityManagerFactory entityManagerFactory,
                            ApplicationPreferenceManager applicationPreferenceManager) {
      super(entityManagerFactory, applicationPreferenceManager);
      validationReportFileDao = new ValidationReportFileDAO(applicationPreferenceManager);
   }

   @Override
   public Validation merge(Validation validation) {
      if (validation.getId() != null) {

         validationReportFileDao.saveOriginalReportInFile(validation, getRepositoryType(validation));
         validationReportFileDao.saveValidationReportInFile(validation, getRepositoryType(validation));

         EntityManager entityManager = getEntityManager();
         validation = entityManager.merge(validation);
         entityManager.flush();
         return validation;
      } else {
         throw new IllegalStateException(String.format("Validation must first be saved with a call to .create() " +
               "before being merged with .merge()"));
      }
   }

   @Override
   public int countByValidationStatus(ValidationStatus validationStatus) {
      EntityManager entityManager = getEntityManager();
      ValidationQuery query = new ValidationQuery(entityManager);
      query.validationStatus().eq(validationStatus);
      return query.getCount();
   }

   @Override
   public int countByValidationStatusAndReferencedStandardId(ValidationStatus validationStatus, int standardId) {
      EntityManager entityManager = getEntityManager();
      ValidationQuery query = new ValidationQuery(entityManager);
      query.validationStatus().eq(validationStatus);
      query.referencedStandards().referencedStandardId().eq(standardId);
      return query.getCount();
   }

   @Override
    public Repository getRepositoryType(Validation validation) {
      ValidationType type = validation.getValidationType();
      try {
            return (Repository)RepositoryTypes.class.getField(type.name()).get(null);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            LOGGER.warn(
                  String.format("Unable to resolve repository type %s for service %s. Fallback on %s repository.",
                        type.name(), validation.getValidationService().getName(), DEFAULT_REPOSITORY));
            return DEFAULT_REPOSITORY;
      }
   }

   @Override
   protected void deleteSpecificItems(Validation processing) throws UnexpectedProcessingException {
      //TODO (for real !) delete all validation report and extension !
   }

    @Override
    public StatisticsDataRow countTotalDataRow() {
        EntityManager entityManager = getEntityManager();
        Query query = entityManager.createNativeQuery("select count(*) as total, " +
                "count(*) filter (where entrypoint = '" + EntryPoint.GUI.name() + "') as gui, " +
                "count(*) filter (where entrypoint = '" + EntryPoint.WS.name() + "') as ws, " +
                "count(*) filter (where status = '" + ValidationStatus.DONE_PASSED.name() + "') as passed, " +
                "count(*) filter (where status = '" + ValidationStatus.DONE_FAILED.name() + "') as failed, " +
                "count(*) filter (where status = '" + ValidationStatus.DONE_UNDEFINED.name() + "') as undefined " +
                "from evsc_processing p " +
                "join evsc_caller_metadata c on c.id = p.caller_id " +
                "join evsc_validation v on p.id = v.id");
        List<Object[]> totalRaw = query.getResultList();
        return new StatisticsDataRow((BigInteger) totalRaw.get(0)[0],(BigInteger) totalRaw.get(0)[1],(BigInteger) totalRaw.get(0)[2],
                (BigInteger) totalRaw.get(0)[3],(BigInteger) totalRaw.get(0)[4],(BigInteger) totalRaw.get(0)[5]);
    }

    @Override
    public List<StatisticsDataRow> countTotalDataRowGroupByReferencedStandard() {
        EntityManager entityManager = getEntityManager();
        Query query = entityManager.createNativeQuery("select mg.label, rs.id as standard_id, rs.display_name as standard_name, " +
                "count(*) as total, " +
                "count(*) filter (where entrypoint = '" + EntryPoint.GUI.name() + "') as gui, " +
                "count(*) filter (where entrypoint = '" + EntryPoint.WS.name() + "') as ws, " +
                "count(*) filter (where status = '" + ValidationStatus.DONE_PASSED.name() + "') as passed, " +
                "count(*) filter (where status = '" + ValidationStatus.DONE_FAILED.name() + "') as failed, " +
                "count(*) filter (where status = '" + ValidationStatus.DONE_UNDEFINED.name() + "') as undefined " +
                "from evsc_processing p " +
                "join evsc_caller_metadata c on c.id = p.caller_id " +
                "join evsc_validation v on p.id = v.id " +
                "join evsc_validation_evsc_referenced_standard_ref vrs on vrs.evsc_validation_id = v.id " +
                "join evsc_referenced_standard_ref rsf on vrs.referencedstandards_Id = rsf.id " +
                "join evsc_referenced_standard rs on rsf.referenced_standard_id = rs.id " +
                "left join evsc_menu_standard mrs on mrs.referenced_standard_id = rs.id " +
                "left join evsc_menu_group mg on mg.id = mrs.menu_group_id " +
                "group by mg.label, rs.id, rs.display_name");
        List<Object[]> dataRows = query.getResultList();
        List<StatisticsDataRow> statisticDataRows = new ArrayList<>();
        for (Object[] dataRow : dataRows) {
            statisticDataRows.add(new StatisticsDataRow((String) dataRow[0], (int) dataRow[1], (String) dataRow[2], (BigInteger) dataRow[3],
                    (BigInteger) dataRow[4],(BigInteger) dataRow[5], (BigInteger) dataRow[6],(BigInteger) dataRow[7],(BigInteger) dataRow[8]));
        }
        return statisticDataRows;
    }

    @Override
    public List<Validation> findAllValidationByStandardId(int id) {
        EntityManager entityManager = getEntityManager();
        ValidationQuery query = new ValidationQuery(entityManager);
        query.referencedStandards().referencedStandardId().eq(id);
        return query.getList();
    }

    @Override
    public List<Object[]> countPerValidationServiceByReferencedStandardId(int referencedStandardId) {
        EntityManager entityManager = getEntityManager();
        Query query = entityManager.createNativeQuery("select v.validation_service, count(*) as total " +
                "from evsc_processing p " +
                "join evsc_caller_metadata c on c.id = p.caller_id " +
                "join evsc_validation v on p.id = v.id " +
                "join evsc_validation_evsc_referenced_standard_ref vrs on vrs.evsc_validation_id = v.id " +
                "join evsc_referenced_standard_ref rsf on vrs.referencedstandards_Id = rsf.id " +
                "join evsc_referenced_standard rs on rsf.referenced_standard_id = rs.id " +
                "where rs.id = " + referencedStandardId + " " +
                "group by v.validation_service " +
                "order by total desc;");
        List<Object[]> dataRows = query.getResultList();
        return dataRows;
    }

    @Override
    public List<Object[]> countPerValidatorByReferencedStandardId(int referencedStandardId) {
        EntityManager entityManager = getEntityManager();
        Query query = entityManager.createNativeQuery("select v.validator_keyword, count(*) as total " +
                "from evsc_processing p " +
                "join evsc_caller_metadata c on c.id = p.caller_id " +
                "join evsc_validation v on p.id = v.id " +
                "join evsc_validation_evsc_referenced_standard_ref vrs on vrs.evsc_validation_id = v.id " +
                "join evsc_referenced_standard_ref rsf on vrs.referencedstandards_Id = rsf.id " +
                "join evsc_referenced_standard rs on rsf.referenced_standard_id = rs.id " +
                "where rs.id = " + referencedStandardId + " " +
                "group by v.validator_keyword " +
                "order by total desc;");
        List<Object[]> dataRows = query.getResultList();
        return dataRows;
    }

    @Override
    public List<Object[]> countPerDateIntervalByReferencedStandardId(int referencedStandardId, IntervalForStatistics interval) {
        EntityManager entityManager = getEntityManager();
        Query query = entityManager.createNativeQuery("select to_char(p.validation_date, '" + interval.getValue() + "') as date, " +
                "count(*) as total " +
                "from evsc_processing p " +
                "join evsc_caller_metadata c on c.id = p.caller_id " +
                "join evsc_validation v on p.id = v.id " +
                "join evsc_validation_evsc_referenced_standard_ref vrs on vrs.evsc_validation_id = v.id " +
                "join evsc_referenced_standard_ref rsf on vrs.referencedstandards_Id = rsf.id " +
                "join evsc_referenced_standard rs on rsf.referenced_standard_id = rs.id " +
                "where rs.id = " + referencedStandardId + " " +
                "group by to_char(p.validation_date, '" + interval.getValue() + "') " +
                "order by date asc;");
        List<Object[]> dataRows = query.getResultList();
        return dataRows;
    }

    @Override
    public List<Object[]> countPerCountry() {
        EntityManager entityManager = getEntityManager();
        Query query = entityManager.createNativeQuery("select l.country country, " +
                "count(*) as validation_count, " +
                "count(distinct l.ip) as ip_count " +
                "from evsc_processing p " +
                "join evsc_validation v on p.id = v.id " +
                "join evsc_owner_metadata o on o.id = p.owner_id " +
                "join evsc_ip_loc l on l.ip = o.user_ip " +
                "group by l.country;");
        List<Object[]> dataRows = query.getResultList();
        return dataRows;
    }


}
