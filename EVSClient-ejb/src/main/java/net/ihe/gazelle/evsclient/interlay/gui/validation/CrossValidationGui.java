package net.ihe.gazelle.evsclient.interlay.gui.validation;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.common.report.ReportExporterManager;
import net.ihe.gazelle.evsclient.application.CallerMetadataFactory;
import net.ihe.gazelle.evsclient.application.interfaces.ForbiddenAccessException;
import net.ihe.gazelle.evsclient.application.interfaces.NotFoundException;
import net.ihe.gazelle.evsclient.application.interfaces.UnauthorizedException;
import net.ihe.gazelle.evsclient.application.notification.EmailNotificationManager;
import net.ihe.gazelle.evsclient.application.validation.ValidationServiceFacade;
import net.ihe.gazelle.evsclient.domain.processing.HandledObject;
import net.ihe.gazelle.evsclient.domain.validation.Validation;
import net.ihe.gazelle.evsclient.domain.validation.ValidationStatus;
import net.ihe.gazelle.evsclient.domain.validation.ValidationType;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.ReferencedStandard;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.ValidationServiceConf;
import net.ihe.gazelle.evsclient.interlay.util.MustacheTemplate;
import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import net.ihe.gazelle.xvalidation.core.model.GazelleCrossValidatorType;
import net.ihe.gazelle.xvalidation.core.model.ValidatorInput;
import net.ihe.gazelle.xvalidation.dao.GazelleCrossValidatorTypeDAO;
import net.ihe.gazelle.xvalidation.dao.ValidatorInputDAO;
import org.apache.commons.lang.StringUtils;
import org.richfaces.event.FileUploadEvent;

import javax.xml.bind.DatatypeConverter;
import java.util.*;

public class CrossValidationGui extends ValidationBeanGui {

    private List<GazelleCrossValidatorType> availableValidators;
    private GazelleCrossValidatorType selectedValidator;
    private CrossValidatorInputsGui inputsGui;

    public CrossValidationGui(ValidationServiceFacade validationServiceFacade,
                              CallerMetadataFactory callerMetadataFactory,
                              GazelleIdentity identity,
                              ApplicationPreferenceManager applicationPreferenceManager,
                              EmailNotificationManager emailNotificationManager) {
        super(validationServiceFacade, callerMetadataFactory, identity, applicationPreferenceManager, emailNotificationManager);
        this.cls = CrossValidationGui.class;
    }

    @Override
    public void init() throws UnauthorizedException, NotFoundException, ForbiddenAccessException {
        super.init();

        inputsGui = new CrossValidatorInputsGui() {
            @Override
            protected Validation getValidation() {
                return selectedObject;
            }
            @Override
            public boolean isValidationDone() {
                return CrossValidationGui.this.isValidationDone();
            }
            @Override
            public GazelleCrossValidatorType getValidator() {
                return selectedValidator;
            }
            @Override
            public boolean isValidatorSelected() {
                return StringUtils.isNotEmpty(selectedValidatorName);
            }
        };
    }

    @Override
    public String getStandardDescription() {
        if (referencedStandard==null) {
            return "";
        }
        return this.referencedStandard.getDescription();
    }

    @Override
    public void setReferencedStandard(ReferencedStandard referencedStandard) {
        super.setReferencedStandard(referencedStandard);
        if (this.referencedStandard!=null) {
            availableValidators = GazelleCrossValidatorTypeDAO.instanceWithDefaultEntityManager()
                    .getValidatorsAvailableForTesting(referencedStandard.getValidatorFilter());
        }
    }

    @Override
    protected Set<String> getDistinctValidationServiceName() {
        Set<String> distinctValidationServiceName = new HashSet<>();
        for (Map.Entry<String, ValidationServiceConf> entry: validatorNameValidationServiceConfMap.entrySet()) {
            if (entry.getValue().getValidationType().equals(ValidationType.XVAL)) {
                distinctValidationServiceName.add(entry.getValue().getName());
            }
        }
        return distinctValidationServiceName;
    }

    @Override
    public void uploadListener(final FileUploadEvent event) {
        super.uploadListener(event);
        selectedObject.getObjects().add(new HandledObject(this.document.getContent(), event.getUploadedFile().getName(), inputsGui.getSelectedInput().getKeyword()));
    }

    public List<ValidatorInput> getValidatorInputs() {
        GazelleCrossValidatorType validator = findValidatorByName(selectedValidatorName);
        return ValidatorInputDAO.instanceWithDefaultEntityManager().getInputsForGazelleCrossValidator(validator);
    }

    @Override
    public void setSelectedValidatorName(String selectedValidatorName) {
        super.setSelectedValidatorName(selectedValidatorName);
        changeSelectedValidator(selectedValidatorName);
    }

    private void changeSelectedValidator(String name) {
        if (name!=null){
            if (selectedValidator==null || !selectedValidator.getName().equals(name)) {
                selectedValidator = findValidatorByName(name);
                if (getSelectedObject()==null || !name.equals(getSelectedObject().getValidationService().getValidatorKeyword())) {
                    setSelectedObject(new Validation(new ArrayList<HandledObject>(), null));
                }
            }
        } else {
            selectedValidator = null;
            setSelectedObject(null);
        }
    }

    private GazelleCrossValidatorType findValidatorByName(String name) {
        if (name!=null){
            for (GazelleCrossValidatorType v : availableValidators) {
                if (name.equals(v.getName())) {
                    return v;
                }
            }
        }
        return null;
    }


    public CrossValidatorInputsGui getInputsGui() {
        return inputsGui;
    }




    @Override
    public void downloadWebServiceBody(){
        final String serviceName = validatorNameValidationServiceConfMap.get(selectedValidatorName).getName();
        final List<Object> tmp = new ArrayList<>();
        for (final HandledObject o:selectedObject.getObjects()) {
            tmp.add(new Object() {
                        String fileName = o.getOriginalFileName();
                        String content = DatatypeConverter.printBase64Binary(o.getContent());
                        String role = o.getRole();
                    });
        }
        Object context = new Object() {
            String service = serviceName;
            String validator = selectedValidatorName;
            List<Object> objects = tmp;
        };
        String body = new MustacheTemplate("validation-for-creation")
                .execute(context);
        StringBuilder fileNameBuilder = new StringBuilder();
        fileNameBuilder.append(getUploadedFileNameWithoutExtension());
        fileNameBuilder.append("_");
        fileNameBuilder.append(selectedValidatorName);
        fileNameBuilder.append(".xml");
        ReportExporterManager.exportToFile("text/xml", body, fileNameBuilder.toString());
    }

    @Override
    public void validate() {
        Validation validation = selectedObject;
        if (ValidationStatus.PENDING.equals(selectedObject.getValidationStatus())) {
            // if not revalidate
            selectedObject = null;
        }
        validate(
                validation.getObjects(),
                super.getEvsCallerMetadata(),
                super.getOwnerMetadata()
        );
    }

    @Override
    public void setSelectedObject(Validation so) {
        // do not set HandledDocuments
        selectedObject = so;
    }
}
