package net.ihe.gazelle.evsclient.interlay.adapters.validationservice;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.application.validation.ReportParser;
import net.ihe.gazelle.evsclient.domain.processing.HandledObject;
import net.ihe.gazelle.evsclient.domain.validation.ValidationType;
import net.ihe.gazelle.evsclient.domain.validation.types.ValidationProperties;
import net.ihe.gazelle.evsclient.domain.validationservice.ValidationServiceOperationException;
import net.ihe.gazelle.evsclient.domain.validationservice.Validator;
import net.ihe.gazelle.evsclient.domain.validationservice.WebValidationService;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.WebValidationServiceConf;
import net.ihe.gazelle.evsclient.interlay.adapters.validation.GitbReportMapper;
import org.jboss.resteasy.client.ClientRequest;
import org.jboss.resteasy.client.ClientResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.DatatypeConverter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.*;

@ValidationProperties(
        name = "GITB",
        types = ValidationType.DEFAULT)
@ReportParser(GitbReportMapper.class)
public class GitbRestValidationServiceClient implements WebValidationService {

    private static final Logger LOGGER = LoggerFactory.getLogger(GitbRestValidationServiceClient.class);
    private final ObjectMapper mapper = new ObjectMapper();

    protected final ApplicationPreferenceManager applicationPreferenceManager;

    protected WebValidationServiceConf configuration;

    public GitbRestValidationServiceClient(ApplicationPreferenceManager applicationPreferenceManager,
                                           WebValidationServiceConf configuration) {
        this.applicationPreferenceManager = applicationPreferenceManager;
        this.configuration = configuration;
    }

    @Override
    public String about() {
        return "GITB";
    }

    @Override
    public String getName() {
        return configuration.getName();
    }

    @Override
    public List<String> getSupportedMediaTypes() {
        return Arrays.asList("application/xml", "application/json");
    }

    @Override
    public WebValidationServiceConf getConfiguration() {
        return this.configuration;
    }

    @Override
    public String getUrl() {
        return configuration.getUrl();
    }

    @Override
    public boolean isZipTransport() {
        return false;
    }

    @Override
    public List<Validator> getValidators() {
        List<Validator> validators = runGitbInfoRequest();
        Collections.reverse(validators);
        return validators;
    }

    @Override
    public List<Validator> getValidators(String filter) {
        List<Validator> filteredValidators = runGitbInfoRequest(filter);
        if (filter != null && !filter.trim().isEmpty()) {
            for (Validator validator : filteredValidators) {
                if (!filter.equals(validator.getDomain())) {
                    filteredValidators.remove(validator);
                    Collections.reverse(filteredValidators);
                }
            }
        } else {
            filteredValidators = runGitbInfoRequest();
            Collections.reverse(filteredValidators);
        }
        return filteredValidators;
    }

    @Override
    public byte[] validate(HandledObject[] objects, String validator) throws ValidationServiceOperationException {
        Validator gitbValidator = findValidator(validator);
        return this.validateGitbRequest(DatatypeConverter.printBase64Binary(objects[0].getContent()),
                gitbValidator.getName(), gitbValidator.getDomain());
    }

    private Validator findValidator(String validator) {
        List<Validator> validators = getValidators();
        for (Validator gitbValidator : validators) {
            if (gitbValidator.getKeyword().equals(validator)) {
                return gitbValidator;
            }
        }
        throw new GitbValidationServiceException("Unable to find type for description: " + validator);
    }

    private List<Validator> runGitbInfoRequest(String domain) {
        if (configuration.getUrl() != null) {
            String url = getUrlDomain(domain);
            try {
                return parseApiInfoResponseByDomain(runHttpGet(url));
            } catch (IOException e) {
                throw new GitbValidationServiceException(e);
            }
        } else {
            throw new GitbValidationServiceException("Url is not defined for service " + configuration.getName());
        }
    }

    private String getUrlDomain(String domain) {
        if (domain.isEmpty()) {
            return removeSlash(configuration.getUrl()) + "/api/info";
        }
        return removeSlash(configuration.getUrl()) + "/" + domain + "/api/info";
    }


    private List<Validator> runGitbInfoRequest() {
        if (configuration.getUrl() != null) {
            String url = configuration.getUrl() + "/api/info";
            try {
                return parseApiInfoResponse(runHttpGet(url));
            } catch (IOException e) {
                throw new GitbValidationServiceException(e);
            }
        } else {
            throw new GitbValidationServiceException("Url is not defined for service " + configuration.getName());
        }
    }

    private static byte[] runHttpGet(String url) {
        byte[] responseBody;
        final ClientRequest request = new ClientRequest(url);
        request.header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON);
        ClientResponse<byte[]> response;
        try {
            response = request.get(byte[].class);
        } catch (Exception e) {
            throw new GitbValidationServiceException(e);
        }
        Response.Status status = response.getResponseStatus();
        if (status.getStatusCode() == 200) {
            responseBody = response.getEntity();
        } else {
            throw new GitbValidationServiceException("Validation service error reponse: HTTP " + status.getStatusCode() + " " + status.getReasonPhrase() + "\n" + new String(response.getEntity(), StandardCharsets.UTF_8));
        }
        return responseBody;
    }

    private List<Validator> parseApiInfoResponse(byte[] responseBytes) throws IOException {
        List<Validator> validators = new ArrayList<>();
        JsonNode json = mapper.readTree(responseBytes);
        Iterator<JsonNode> jsonNodeIterator = json.elements();
        while (jsonNodeIterator.hasNext()) {
            JsonNode domainNode = jsonNodeIterator.next();
            if (domainNode != null) {
                JsonNode validationTypes = domainNode.findValue("validationTypes");
                if (validationTypes != null) {
                    parseValidationTypes(
                            validators,
                            validationTypes,
                            domainNode.findValue("domain").asText()
                    );
                }
            }
        }
        return validators;
    }

    private List<Validator> parseApiInfoResponseByDomain(byte[] responseStream) throws IOException {
        List<Validator> validators = new ArrayList<>();
        JsonNode json = mapper.readTree(responseStream);
        JsonNode validationTypes = json.findValue("validationTypes");
        if (validationTypes != null) {
            parseValidationTypes(
                    validators,
                    validationTypes,
                    json.findValue("domain").asText()
            );
        }
        return validators;
    }

    private void parseValidationTypes(List<Validator> validators, JsonNode validationTypes, String domain) {
        Iterator<JsonNode> validationTypeIterator = validationTypes.elements();
        while (validationTypeIterator.hasNext()) {
            JsonNode validationType = validationTypeIterator.next();
            if (validationType.findValue("type") != null) {
                validators.add(new Validator(
                        validationType.findValue("description").asText(),
                        validationType.findValue("type").asText(),
                        domain
                ));
            }
        }
    }

    private byte[] validateGitbRequest(String contentToValidate, String validationType, String domain) {
        int status;
        byte[] bytes = null;
        String url = configuration.getUrl();
        url = url + "/" + domain + "/api/validate";
        final ClientRequest request = new ClientRequest(url);
        Map<String, Object> requestBody = new HashMap<>();
        request.header(HttpHeaders.ACCEPT, MediaType.APPLICATION_XML);
        request.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON);
        requestBody.put("contentToValidate", contentToValidate);
        requestBody.put("embeddingMethod", "BASE64");
        requestBody.put("validationType", validationType);
        requestBody.put("locationAsPath", String.valueOf(true));
        requestBody.put("addInputToReport", String.valueOf(false));
        requestBody.put("wrapReportDataInCDATA", String.valueOf(false));
        requestBody.put("locale", "en");
        String json = null;
        try {
            json = mapper.writeValueAsString(requestBody);
        } catch (JsonProcessingException e) {
            throw new GitbValidationServiceException(e);
        }
        request.body(MediaType.APPLICATION_JSON, json);
        ClientResponse<byte[]> response;
        try {
            response = request.post(byte[].class);
        } catch (Exception e) {
            throw new GitbValidationServiceException(e);
        }
        status = response.getStatus();
        if (status == 200 && response.getEntity() != null) {
            return response.getEntity();
        } else {
            throw new ValidationErrorException("Validation service error reponse: HTTP " + response.getResponseStatus().getStatusCode() + " " + response.getResponseStatus().getReasonPhrase() + "\n" + new String(response.getEntity(), StandardCharsets.UTF_8));
        }
    }

    private String removeSlash(String value) {
        if(value.endsWith("/")) {
            return value.substring(0, value.length()-1);
        } else {
            return value;
        }
    }

    public static class GitbValidationServiceException extends RuntimeException {
        public GitbValidationServiceException() {
        }

        public GitbValidationServiceException(String s) {
            super(s);
        }

        public GitbValidationServiceException(String s, Throwable throwable) {
            super(s, throwable);
        }

        public GitbValidationServiceException(Throwable throwable) {
            super(throwable);
        }
    }
}