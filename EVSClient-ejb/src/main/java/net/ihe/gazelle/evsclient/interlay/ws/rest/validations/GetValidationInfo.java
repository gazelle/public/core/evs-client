/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evsclient.interlay.ws.rest.validations;

import net.ihe.gazelle.evsclient.domain.validation.ValidationStatus;
import net.ihe.gazelle.evsclient.interlay.factory.ApplicationFactory;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;

import javax.ejb.Stateless;
import javax.ws.rs.*;

@Stateless
@Name("getValidationInfo")
@Path("/")
public class GetValidationInfo {

   public static final String CACHE = "cache";
   public static final String OID = "oid";
   public static final String TEXT_PLAIN = "text/plain";
   public static final String TOOL_OID = "toolOid";

   @In(value = "evsApplicationFactory", create = true)
   private ApplicationFactory applicationFactory;

   @GET
   @Path("/GetValidationDate")
   @Produces(GetValidationInfo.TEXT_PLAIN)
   public String getValidationDate(@QueryParam(GetValidationInfo.OID) final String oid,
                                   @DefaultValue("") @QueryParam(GetValidationInfo.CACHE) final String queryCache) {
      return applicationFactory.getValidationsCacheManager().getProcessingDateService(oid, null, null, queryCache);
   }

   @GET
   @Path("/GetValidationPermanentLink")
   @Produces(GetValidationInfo.TEXT_PLAIN)
   public String getValidationPermanentLink(@QueryParam(GetValidationInfo.OID) final String oid,
                                            @DefaultValue("") @QueryParam(GetValidationInfo.CACHE) final String queryCache) {
      return applicationFactory.getValidationsCacheManager()
            .getProcessingPermanentLinkService(oid, null, null, queryCache);
   }

   @GET
   @Path("/GetValidationStatus")
   @Produces(GetValidationInfo.TEXT_PLAIN)
   public String getValidationStatus(@QueryParam(GetValidationInfo.OID) final String oid,
                                     @DefaultValue("") @QueryParam(GetValidationInfo.CACHE) final String queryCache) {
      return validationStatusToInfoStatus(
            applicationFactory.getValidationsCacheManager().getProcessingStatusService(oid, null, null, queryCache)
      );
   }

   @GET
   @Path("/GetLastResultStatusByExternalId")
   @Produces(GetValidationInfo.TEXT_PLAIN)
   public String getLastResultStatusByExternalId(@QueryParam("externalId") final String externalId,
                                                 @QueryParam(GetValidationInfo.TOOL_OID) final String toolOid,
                                                 @DefaultValue("") @QueryParam(GetValidationInfo.CACHE) final String queryCache) {
      return validationStatusToInfoStatus(
            applicationFactory.getValidationsCacheManager()
                  .getProcessingStatusService(null, externalId, toolOid, queryCache)
      );
   }

   @GET
   @Path("/GetValidationPermanentLinkByExternalId")
   @Produces(GetValidationInfo.TEXT_PLAIN)
   public String getValidationPermanentLinkByExternalId(@QueryParam("externalId") final String externalId,
                                                        @QueryParam(GetValidationInfo.TOOL_OID) final String toolOid,
                                                        @DefaultValue("") @QueryParam(GetValidationInfo.CACHE) final String queryCache) {
      return applicationFactory.getValidationsCacheManager()
            .getProcessingPermanentLinkService(null, externalId, toolOid, queryCache);
   }

   private String validationStatusToInfoStatus(String processingStatusService) {
      if(processingStatusService != null) {
         switch (ValidationStatus.valueOf(processingStatusService)) {
            case DONE_PASSED:
               return "PASSED";
            case DONE_FAILED:
               return "FAILED";
            case DONE_UNDEFINED:
               return "ABORTED";
            case PENDING:
            case IN_PROGRESS:
               return "NOT_PERFORMED";
            default:
               return "UNKNOWN";
         }
      }
      return null;
   }

}