package net.ihe.gazelle.evsclient.interlay.adapters.statistics;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.ihe.gazelle.common.application.action.ApplicationPreferenceManagerImpl;
import net.ihe.gazelle.evsclient.application.statistics.IpCountryProvider;
import net.ihe.gazelle.evsclient.application.statistics.LocalizationException;
import org.jboss.resteasy.client.ClientRequest;
import org.jboss.resteasy.client.ClientResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.MessageFormat;

public class IpStack implements IpCountryProvider {
    private static final Logger LOGGER = LoggerFactory.getLogger(IpStack.class);
    public static final String DEFAULT_PATTERN = "http://api.ipstack.com/{0}?access_key=c6bf326c110a49c544a400ed98704601&output=xml";

    private String pattern;

    public IpStack(String pattern) {
        this.pattern = pattern;
    }

    public IpStack() {
        this(new ApplicationPreferenceManagerImpl().getStringValue("ip_loc_service_configuration") == null
                ? DEFAULT_PATTERN
                : new ApplicationPreferenceManagerImpl().getStringValue("ip_loc_service_configuration"));
    }

    public String findCountry(String ipAddress) throws LocalizationException {
        String url = MessageFormat.format(pattern, ipAddress);
        if (LOGGER.isInfoEnabled()) LOGGER.info(MessageFormat.format("find-loc.url {0}", url));
        final ClientRequest request = new ClientRequest(url);
        try {
            final ClientResponse<String> response = request.get(String.class);
            if (LOGGER.isDebugEnabled()) LOGGER.debug(MessageFormat.format("find-loc.response {0}", new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(response)));
            if (response.getStatus() == 200 && response.getEntity() != null && !response.getEntity().isEmpty()) {
                final String info = response.getEntity();
                String countryName = null;
                final int pos = info.indexOf("<country_name>");
                if (pos > 0) {
                    final int end = info.indexOf("</country_name");
                    countryName = info.substring(pos + 14, end);
                    if (countryName.contains("Korea")) {
                        countryName = "Republic of Korea";
                    }
                    if (LOGGER.isInfoEnabled()) LOGGER.info(MessageFormat.format("find-loc.country {0}", countryName));
                    return countryName;
                }
            }
            throw new LocalizationException("Quota might been reached ; IP address will not be computed.");
        } catch (final Exception e) {
            throw new LocalizationException(MessageFormat.format("An error occurred when retrieving the country for ip address: {}", ipAddress), e);
        }

    }
}
