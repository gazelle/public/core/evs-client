package net.ihe.gazelle.evsclient.interlay.adapters.validationservice;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.application.validation.ReportParser;
import net.ihe.gazelle.evsclient.domain.processing.HandledObject;
import net.ihe.gazelle.evsclient.domain.validation.ValidationType;
import net.ihe.gazelle.evsclient.domain.validation.request.ValidationItem;
import net.ihe.gazelle.evsclient.domain.validation.request.ValidationRequest;
import net.ihe.gazelle.evsclient.domain.validation.types.ValidationProperties;
import net.ihe.gazelle.evsclient.domain.validationservice.ValidationServiceOperationException;
import net.ihe.gazelle.evsclient.domain.validationservice.Validator;
import net.ihe.gazelle.evsclient.domain.validationservice.WebValidationService;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.WebValidationServiceConf;
import net.ihe.gazelle.evsclient.interlay.adapters.metadata.ServiceMetadataMapper;
import net.ihe.gazelle.evsclient.interlay.adapters.validation.GazelleValidation10ReportMapper;
import net.ihe.gazelle.evsclient.interlay.dto.rest.validation.request.ValidationRequestDTO;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


@ValidationProperties(
        name = "Gazelle Validation Service 1.0",
        types = {
                ValidationType.DEFAULT
        })
@ReportParser(GazelleValidation10ReportMapper.class)
public class GazelleValidationServiceClient implements WebValidationService {

    private final static ObjectMapper mapper = new ObjectMapper();

    private final ServiceMetadataMapper metadataMapper;

    private List<Validator> validators;

    private WebValidationServiceConf configuration;

    private final ApplicationPreferenceManager applicationPreferenceManager;



    public GazelleValidationServiceClient(ApplicationPreferenceManager applicationPreferenceManager,
                                          WebValidationServiceConf configuration) {
        // applicationPreferenceManager is not used
        this.configuration = configuration;
        this.metadataMapper = new ServiceMetadataMapper();
        this.applicationPreferenceManager = applicationPreferenceManager;
    }

    @Override
    public String about() {
        return configuration.getUrl() + "/about";
    }

    @Override
    public String getName() {
        return configuration.getName();
    }

    @Override
    public List<Validator> getValidators() {
        if(validators == null) {
            try {
                String validationProfilesJson = sendValidationProfilesRequest(this.configuration.getUrl()+"/validation/profiles");
                return this.validators = metadataMapper.getValidators(validationProfilesJson);
            } catch (ValidationServiceOperationException e) {
                throw new IllegalStateException("Unable to get metadata from validation service "+configuration.getName(), e);
            } catch (IOException e) {
                throw new IllegalStateException("Unable to parse metadata for validation service  "+configuration.getName(), e);
            }
        }
        return validators;
    }

    @Override
    public List<Validator> getValidators(String filter) {
        List<Validator> filteredValidators = new ArrayList<>();
        for(Validator validator : getValidators()) {
            if(validator.getDomain().toLowerCase().contains(filter.toLowerCase())) {
                filteredValidators.add(validator);
            }
        }
        return filteredValidators;
    }

    @Override
    public List<String> getSupportedMediaTypes() {
        return Collections.singletonList("application/xml, application/json");
    }

    @Override
    public byte[] validate(HandledObject[] objects, String validator) throws ValidationServiceOperationException {
        ValidationRequest validationRequest = new ValidationRequest();
        validationRequest.setValidationServiceName(configuration.getName());
        validationRequest.setValidationProfileId(validator);
        for(HandledObject object : objects) {
            validationRequest.addValidationItem(
                    new ValidationItem()
                            .setContent(object.getContent())
                            .setItemId(String.valueOf(object.getId()))
                            .setRole(object.getRole())
            );
        }
        String validationResponse = sendValidationRequest(configuration.getUrl() + "/validation/validate", new ValidationRequestDTO(validationRequest));
        return validationResponse.getBytes(StandardCharsets.UTF_8);
//        try {
//            ValidationReport validationReport = reportMapper.transformToRetro(validationResponse.getBytes(StandardCharsets.UTF_8));
//            ValidationReportDTO validationReportDTO = new ValidationReportDTO(validationReport, SeverityLevel.INFO);
//            return mapper.writeValueAsBytes(validationReportDTO);
//
//        } catch (ReportTransformationException | JsonProcessingException e) {
//            throw new ValidationServiceOperationException(e);
//        }
    }


//    @Override
//    protected List<String> getValidatorNames(String validatorFilter) {
//        List<String> validatorNames = new ArrayList<>();
//        for(Validator validator : getValidators()) {
//            if(validator.getName().contains(validatorFilter)) {
//                validatorNames.add(validator.getName());
//            }
//        }
//        return validatorNames;
//    }

    private String sendValidationProfilesRequest(String url) throws ValidationServiceOperationException {
        try {
            URL obj = new URL(url);
            int validationTimeout = applicationPreferenceManager.getIntegerValue("validation_timeout");
            HttpURLConnection connection = (HttpURLConnection) obj.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Accept-Language", "en;q=0.8");
            connection.setConnectTimeout(validationTimeout);
            connection.setReadTimeout(validationTimeout);
            return extractResponse(connection);
        } catch (IOException | ValidationServiceOperationException e) {
            throw new ValidationServiceOperationException(e);
        }
    }


    private String sendValidationRequest(String url, ValidationRequest validationRequest) throws ValidationServiceOperationException {
        try{
            URL obj = new URL(url);
            HttpURLConnection connection = (HttpURLConnection) obj.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
            connection.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
            wr.writeBytes(mapper.writeValueAsString(validationRequest));
            wr.flush();
            wr.close();
            return extractResponse(connection);

        } catch (IOException e) {
            throw new ValidationServiceOperationException("Error while calling validation service " + getName() + " : " + e.getMessage(),e);
        }

    }


    private String extractResponse(HttpURLConnection connection) throws IOException, ValidationServiceOperationException {
        int responseCode = connection.getResponseCode();
        if(responseCode == HttpURLConnection.HTTP_OK) {
            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuilder response = new StringBuilder();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            return response.toString();
        } else {
            throw new ValidationServiceOperationException("Error while calling validation service " + getName() + " : " + responseCode);
        }
    }


    @Override
    public WebValidationServiceConf getConfiguration() {
        return configuration;
    }

    @Override
    public String getUrl() {
        return configuration.getUrl();
    }

    @Override
    public boolean isZipTransport() {
        return false;
    }
}
