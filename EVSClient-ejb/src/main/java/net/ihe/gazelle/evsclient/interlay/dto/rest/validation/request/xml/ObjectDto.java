package net.ihe.gazelle.evsclient.interlay.dto.rest.validation.request.xml;

import javax.xml.bind.annotation.*;
import java.util.Arrays;

@XmlRootElement(name = "object")
@XmlAccessorType(XmlAccessType.FIELD)
public class ObjectDto {

    @XmlAttribute(name = "originalFileName")
    private String originalFileName;

    @XmlElement(namespace = "http://evsobjects.gazelle.ihe.net/", name = "content")
    private byte[] content;

    public String getOriginalFileName() {
        return originalFileName;
    }

    public void setOriginalFileName(String originalFileName) {
        this.originalFileName = originalFileName;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "ObjectDto{" +
                "originalFileName='" + originalFileName + '\'' +
                ", content=" + Arrays.toString(content) +
                '}';
    }
}
