package net.ihe.gazelle.evsclient.interlay.dto.rest.validation;

import io.swagger.v3.oas.annotations.media.Schema;
import net.ihe.gazelle.evsclient.domain.validation.ValidationServiceRef;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Objects;

@Schema(description = "Service d'extension demandé.  ")
@XmlRootElement(name = "extensionService")
@XmlAccessorType(XmlAccessType.FIELD)
public class ExtensionService extends AbstractService<ValidationServiceRef> {

    public ExtensionService() {
        super();
    }

    public ExtensionService(ValidationServiceRef domain) {
        super(domain);
    }

    /**
     * Return the profile to invoke when calling an extension service (aka Procesing Service in GITB meaning)
     * In an extension, there can be more than validator to specify the context fo the call. This can be called a
     * profile.
     * @return
     */
    public String getProfile() {
        return getValidator();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ExtensionService validationService = (ExtensionService) o;
        return Objects.equals(getName(), validationService.getName()) &&
                Objects.equals(getValidator(), validationService.getProfile());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getProfile());
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class ExtensionService {\n");
        sb.append("    name: ").append(toIndentedString(getName())).append("\n");
        sb.append("    profile: ").append(toIndentedString(getProfile())).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}

