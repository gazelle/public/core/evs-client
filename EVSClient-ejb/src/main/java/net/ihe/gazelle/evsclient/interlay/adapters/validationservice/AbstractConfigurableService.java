package net.ihe.gazelle.evsclient.interlay.adapters.validationservice;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.domain.validationservice.ConfigurableService;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.ProcessingConf;
import org.apache.commons.lang.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractConfigurableService<T extends ProcessingConf> implements ConfigurableService<T> {
    protected static final Logger LOGGER = LoggerFactory.getLogger(AbstractConfigurableService.class);

    protected final ApplicationPreferenceManager applicationPreferenceManager;

    protected T configuration;

    protected AbstractConfigurableService(ApplicationPreferenceManager applicationPreferenceManager, T configuration) {
        this.applicationPreferenceManager = applicationPreferenceManager;
        this.configuration = configuration;
    }

    public T getConfiguration() {
        return configuration;
    }

    public void setConfiguration(T configuration) {
        this.configuration = configuration;
    }

    // FIXME move this mehod to the configuration.
    protected Long getValidationTimeout() {
        String timeout = applicationPreferenceManager.getStringValue("validation_timeout");
        Long timeoutlg;
        if (NumberUtils.isNumber(timeout)) {
            timeoutlg = Long.parseLong(timeout);
            LOGGER.debug("timeoutlg : " + timeoutlg);
            return Long.parseLong(timeout);
        } else {
            return null;
        }
    }
}
