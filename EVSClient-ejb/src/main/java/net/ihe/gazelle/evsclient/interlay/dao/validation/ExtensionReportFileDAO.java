package net.ihe.gazelle.evsclient.interlay.dao.validation;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.application.interfaces.adapters.ProcessingNotSavedException;
import net.ihe.gazelle.evsclient.domain.validation.extension.ValidationExtension;
import net.ihe.gazelle.evsclient.interlay.dao.Repository;

class ExtensionReportFileDAO extends ReportFileDAO {

   private static final String EXTENSION_REPORT_PREFIX = "extension-report-";

   ExtensionReportFileDAO(ApplicationPreferenceManager applicationPreferenceManager) {
      super(applicationPreferenceManager);
   }

   /**
    * Save content of the original report on File System.
    *
    * @param extension Validation extension containing the originalReport to save.
    * @param repository Repository to save the file.
    *
    * @return the extension
    *
    * @throws ProcessingNotSavedException in case of file I/O exception or repository definition error.
    */
   ValidationExtension saveOriginalReportInFile(ValidationExtension extension,Repository type) {
      OriginalReportFile report = extension.getExtensionReport();
      saveReportInFile(report, new ReportFileDAO.ReportFileRepository(type,EXTENSION_REPORT_PREFIX), extension.getId().toString());
      return extension;
   }

}
