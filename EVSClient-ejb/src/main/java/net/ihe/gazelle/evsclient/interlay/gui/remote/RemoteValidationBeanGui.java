/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evsclient.interlay.gui.remote;

import net.ihe.gazelle.common.cache.GazelleCacheManager;
import net.ihe.gazelle.common.pages.Page;
import net.ihe.gazelle.common.pages.menu.Menu;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.ReferencedStandard;
import net.ihe.gazelle.evsclient.interlay.gui.GuiMessage;
import net.ihe.gazelle.evsclient.interlay.gui.QueryParam;
import net.ihe.gazelle.evsclient.interlay.gui.QueryParamEvs;
import net.ihe.gazelle.evsclient.interlay.gui.document.ContentConverter;
import net.ihe.gazelle.evsclient.interlay.gui.menu.EVSMenu;
import net.ihe.gazelle.evsclient.interlay.gui.menu.PageValidate;
import net.ihe.gazelle.evsclient.interlay.gui.processing.FileProcessingBeanGui;
import net.ihe.gazelle.evsclient.interlay.gui.validation.GuiComponentType;
import net.ihe.gazelle.evsclient.interlay.servlet.Upload;
import net.ihe.gazelle.mca.contentanalyzer.adapters.config.QueryParamMca;
import net.ihe.gazelle.mca.contentanalyzer.application.converters.Base64Converter;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.jboss.seam.core.ResourceBundle;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.faces.model.SelectItemGroup;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class RemoteValidationBeanGui extends FileProcessingBeanGui implements Serializable {

    private static final long serialVersionUID = 7920478668366260753L;

    private static final Logger LOGGER = LoggerFactory.getLogger(RemoteValidationBeanGui.class);

    private static final String FILE_DESCRIPTION = "Uploaded.xml";


    private int startOffset;
    private int endOffset;

    private boolean lucky;

    private String proxyType;

    private String externalId;

    private String toolOid;

    private String messageStr;

    private boolean base64Decoded;

    private String validationType = "";
    private boolean filterByValidationType;

    private String calledPrivacyKey;


    private String key; // key to get fileContent for remote validation
    private String fileExtension;

    private Integer selectedReferencedStandardId;

    // used to store standards to avoid request to database after user selection
    private Map<Integer, ReferencedStandard> referencedStandardMap = new HashMap<>();


    public RemoteValidationBeanGui() {
        super(RemoteValidationBeanGui.class);
        init();
    }

    public void init() {
        final Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        final String sOffset = params.get(QueryParamMca.START_OFFSET);
        final String eOffset = params.get(QueryParamMca.END_OFFSET);

        this.key = params.get(QueryParam.KEY);
        this.toolOid = params.get(QueryParam.TOOL_OID);
        this.externalId = params.get(QueryParam.EXTERNAL_ID);
        this.proxyType = params.get(QueryParam.PROXY_TYPE);
        this.lucky = Boolean.parseBoolean(params.get(QueryParam.LUCKY));
        this.validationType = params.get(QueryParamMca.VALIDATION_TYPE);
        this.filterByValidationType = !StringUtils.isEmpty(validationType);
        this.calledPrivacyKey = params.get(QueryParam.CALLER_PRIVACY_KEY);

        if(sOffset != null && !sOffset.isEmpty()){
            this.setStartOffset(Integer.valueOf(sOffset));
        }
        if(eOffset != null && !eOffset.isEmpty()) {
            this.setEndOffset(Integer.valueOf(eOffset));
        }

        // Here we are coming from Message Content Analyzer, proxy or TM
        this.uploadedFileName = Upload.getFilePath(key);

        try {
            if (this.uploadedFileName != null) {
                // Detection of message type when validating using Proxy->EVSClient
                this.fileExtension = this.uploadedFileName.substring(this.uploadedFileName.lastIndexOf('.') + 1);

                Path path = Paths.get(this.uploadedFileName);
                byte[] content = Files.readAllBytes(path);

                // Split file if offset
                if (this.getStartOffset() != 0 || this.getEndOffset() != 0) {
                    content = new ContentConverter().toByteArray(content,this.getStartOffset(), this.getEndOffset());
                }

                File decoded;
                if (new Base64Converter().base64Detection(content)) {
                    decoded = File.createTempFile("decoded_remote_", "."+fileExtension);
                    content = Base64.decodeBase64(content);
                    this.setBase64Decoded(true);
                } else {
                    decoded = File.createTempFile("remote_", "."+fileExtension);
                }
                FileUtils.writeByteArrayToFile(decoded,content);
                this.uploadedFileName = decoded.getName();
                this.key = Upload.putFile(decoded.getAbsolutePath());

                setDocumentContent(content);

            } else {
                GuiMessage.logMessage(StatusMessage.Severity.ERROR, "No file provided or file size is &gt; 15Mo");
                LOGGER.info("No file provided or file size is &gt; 15Mo");
            }
        } catch (final IOException e) {
            LOGGER.error("error to decoded file : {}", e.getMessage());
        }
    }

    public List<SelectItem> getReferencedStandardAsSelectItems() {
        List<SelectItem> options = new ArrayList<>();
        addRootSelectItem(options);
        addReferencedStandardByMenuGroup(options,validationType);
        return options;
    }

    private void addReferencedStandardByMenuGroup(List<SelectItem> options, String validationType) {
        List<Menu> evsMenuBar = EVSMenu.getMenu().getItems(); // get menu bar with referenced standard items, addons, etc..
        selectedReferencedStandardId=null;
        // iterate over Menu to find referenced standards associated to menu.
        for (Menu menu : evsMenuBar) {
            // create selectItemsGroup for standards
            SelectItemGroup menuEntryGroup = new SelectItemGroup(menu.getPage().getLabel());
            List<SelectItem> selectItemsForCurrentMenuEntryGroup = new ArrayList<>();
            for (Menu internalMenu : menu.getItems()) { // iterate over items inside menu entries
                if (!internalMenu.getItems().isEmpty()){ // to be sure that this is a validation menu
                    Page page = internalMenu.getItems().get(0).getPage();
                    if (page instanceof PageValidate) { // return only validation pages. ie : get(0) return validation page and is sufficient
                        ReferencedStandard rs = ((PageValidate) page).getStandard();
                        if (rs != null && (StringUtils.isEmpty(validationType) || !isFilterByValidationType() || validationType.equalsIgnoreCase(rs.getValidationType().name()))){
                            SelectItem selectItem = new SelectItem();
                            selectItem.setValue(rs.getId());
                            selectItem.setLabel(rs.getName());
                            selectItemsForCurrentMenuEntryGroup.add(selectItem);
                            referencedStandardMap.put(rs.getId(), rs);
                            if (selectedReferencedStandardId==null) {
                                selectedReferencedStandardId = rs.getId();
                            }
                        }
                    }
                }
            }
            menuEntryGroup.setSelectItems(selectItemsForCurrentMenuEntryGroup.toArray(new SelectItem[0]));
            if(menuEntryGroup.getSelectItems().length > 0){ // verification to only add entries with associated standards
                options.add(menuEntryGroup);
            }
        }
    }

    private void addRootSelectItem(List<SelectItem> options) {
        SelectItem rootItem = new SelectItem();
        rootItem.setValue(null);
        rootItem.setNoSelectionOption(true);
        rootItem.setDisabled(true);
        rootItem.setLabel(ResourceBundle.instance().getString("gazelle.common.PleaseSelect"));
        options.add(rootItem);
    }


    public void forwardToValidation() {

        if(selectedReferencedStandardId != null){
            cleanCacheForRemoteValidation();

            FacesMessages.instance().clear();
            StringBuilder validationUrl = new StringBuilder();
            // initialize URL
            if (GuiComponentType.getValidatorViewByValidationType(referencedStandardMap.get(selectedReferencedStandardId).getValidationType()).contains("?")) {
                validationUrl.append(GuiComponentType.getValidatorViewByValidationType(referencedStandardMap.get(selectedReferencedStandardId).getValidationType()).concat("&"));
            } else {
                validationUrl.append(GuiComponentType.getValidatorViewByValidationType(referencedStandardMap.get(selectedReferencedStandardId).getValidationType()).concat("?"));
            }
            validationUrl.append(QueryParamEvs.REF_STANDARD_ID + "=" + selectedReferencedStandardId);
            validationUrl.append("&" + QueryParam.KEY + "=" + this.key);

            if(this.toolOid != null){
                validationUrl.append("&" + QueryParam.TOOL_OID + "=" + this.toolOid);
            }
            if(this.externalId != null){
                validationUrl.append("&" + QueryParam.EXTERNAL_ID + "=" + this.externalId);
            }
            if(this.proxyType != null){
                validationUrl.append("&" + QueryParam.PROXY_TYPE + "=" + this.proxyType);
            }
            if(this.calledPrivacyKey != null){
                validationUrl.append("&" + QueryParam.CALLER_PRIVACY_KEY + "=" + this.calledPrivacyKey);
            }

            final ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
            try {
                ec.redirect(ec.getRequestContextPath() + validationUrl);
            } catch (IOException e) {
                GuiMessage.logMessage(StatusMessage.Severity.ERROR, "Could not redirect to " + validationUrl, e);
            }
        } else {
            GuiMessage.logMessage(StatusMessage.Severity.INFO, "Please select a referenced standard");
        }


    }

    private void cleanCacheForRemoteValidation() {
        //clean cache for remote validation
        final String VALIDATION_DATE = "EVSClient:GetValidationDate";
        final String VALIDATION_STATUS = "EVSClient:GetValidationStatus";
        final String VALIDATION_PERMANENTLINK = "EVSClient:GetValidationPermanentLink";
        final String VALIDATION_STATUS_AND_PERMANENTLINK = "EVSClient:GetValidationStatusAndPermanentLink";
        List<String> cacheNameList = Arrays.asList(VALIDATION_DATE,VALIDATION_PERMANENTLINK,VALIDATION_STATUS,VALIDATION_STATUS_AND_PERMANENTLINK);
        for (String cacheName : cacheNameList) {
            GazelleCacheManager.clearElement(toolOid + cacheName, externalId);
        }
    }

    public boolean isBase64Decoded() {
        return this.base64Decoded;
    }

    public void setBase64Decoded(final boolean base64Decoded) {
        this.base64Decoded = base64Decoded;
    }

    public int getStartOffset() {
        return this.startOffset;
    }

    public void setStartOffset(final int startOffset) {
        this.startOffset = startOffset;
    }

    public int getEndOffset() {
        return this.endOffset;
    }

    public void setEndOffset(final int endOffset) {
        this.endOffset = endOffset;
    }

    public boolean isFilterByValidationType() {
        return filterByValidationType;
    }

    public void setFilterByValidationType(boolean filterByValidationType) {
        this.filterByValidationType = filterByValidationType;
    }

    public String getProxyType() {
        return proxyType;
    }

    public String getExternalId() {
        return externalId;
    }

    public String getToolOid() {
        return toolOid;
    }

    public Integer getSelectedReferencedStandardId() {
        return selectedReferencedStandardId;
    }

    public void setSelectedReferencedStandardId(Integer selectedReferencedStandardId) {
        this.selectedReferencedStandardId = selectedReferencedStandardId;
    }

    public String getMessageStr() {
        return new String(presenter.getDocument().getContent());
    }
}
