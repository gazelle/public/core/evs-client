package net.ihe.gazelle.evsclient.domain.validation.report;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Sub Validation Report
 *
 * @author fde
 * @version $Id : $Id
 */
public class ValidationSubReport implements Serializable {
   private static final long serialVersionUID = 1622712017135790598L;

   private String name;

   private List<String> standards = new ArrayList<>();

   private final ValidationCounters subCounters = new ValidationCounters();

   private final List<UnexpectedError> unexpectedErrors = new ArrayList<>();

   private final List<ConstraintValidation> constraints = new ArrayList<>();

   private ValidationTestResult subReportResult = ValidationTestResult.UNDEFINED;

   private final List<ValidationSubReport> subReports = new ArrayList<>();

   /**
    * Constructor with all mandatory parameters
    *
    * @param name      a {@link String} object,
    * @param standards a {@link List} object,
    *
    * @throws IllegalArgumentException if name is null
    */
   public ValidationSubReport(String name, List<String> standards) {
      setName(name);
      setStandards(standards);
   }

   /**
    * Copy constructor
    *
    * @param validationSubReport {@link ValidationSubReport}
    */
   public ValidationSubReport(ValidationSubReport validationSubReport) {
      setName(validationSubReport.getName());
      setStandards(validationSubReport.getStandards());
      setConstraints(validationSubReport.getConstraints());
      addUnexpectedErrors(validationSubReport.getUnexpectedErrors());
      setSubReports(validationSubReport.getSubReports());
   }

   /**
    * Add a Throwable as an UnexpectedError to the ValidationSubReport
    *
    * @param throwable Throwable to add to the report
    *
    * @throws IllegalArgumentException if throwable is null
    */
   public void addUnexpectedError(Throwable throwable) {
      addUnexpectedError(new UnexpectedError(throwable));
   }

   /**
    * Add {@link UnexpectedError} to the ValidationSubReport
    *
    * @param error {@link UnexpectedError} to add to the report.
    *
    * @throws IllegalArgumentException if {@link UnexpectedError} is null.
    */
   public void addUnexpectedError(UnexpectedError error) {
      if (error != null) {
         this.unexpectedErrors.add(error);
         setSubReportResult(ValidationTestResult.UNDEFINED);
      } else {
         throw new IllegalArgumentException("cannot add a null exception to the validation sub report");
      }
   }

   /**
    * Increment Counters when a constraint is added
    *
    * @param severity             {@link SeverityLevel} object
    * @param constraintTestResult {@link ValidationTestResult}
    */
   private void incrementSubCounter(SeverityLevel severity, ValidationTestResult constraintTestResult) {
      switch (severity) {
         case ERROR:
            subCounters.incrementNumberOfErrors();
            break;
         case WARNING:
            subCounters.incrementNumberOfWarnings();
            break;
         default:
            if (ValidationTestResult.FAILED.equals(constraintTestResult)) {
               subCounters.incrementFailedWithInfoNumber();
            } else {
               subCounters.incrementNumberOfConstraints();
            }
            break;
      }
   }

   /**
    * <p>Getter of the field <code>unexpectedErrors</code></p>
    *
    * @return a {@link List} object
    */
   public List<UnexpectedError> getUnexpectedErrors() {
      return new ArrayList<>(unexpectedErrors);
   }

   /**
    * <p>add a list of unexpected errors to sub-report</p>
    *
    * @param unexpectedErrors List of unexpectedErrors to add to the report
    *
    * @throws IllegalArgumentException if unexpectedErrors list is null.
    */
   public void addUnexpectedErrors(List<UnexpectedError> unexpectedErrors) {
      if (unexpectedErrors != null) {
         for (UnexpectedError unexpectedError : unexpectedErrors) {
            addUnexpectedError(unexpectedError);
         }
      } else {
         throw new IllegalArgumentException("unexpectedErrors list to add cannot be null");
      }
   }

   /**
    * <p>Getter of the field <code>constraints</code></p>
    *
    * @return a {@link List} object
    */
   public List<ConstraintValidation> getConstraints() {
      return new ArrayList<>(constraints);
   }

   /**
    * <p>Setter of the field <code>constraints</code></p>
    * <p>For internal constructor usage only. Otherwise it will mess with counters and validation status</p>
    *
    * @param constraints a {@link List} object, can be null
    */
   private void setConstraints(List<ConstraintValidation> constraints) {
      for (ConstraintValidation constraintValidation : constraints) {
         addConstraintValidation(constraintValidation);
      }
   }

   /**
    * Add a constraintValidation to Sub report
    *
    * @param constraintValidation {@link ConstraintValidation} object
    *
    * @throws IllegalArgumentException is constraintValidation is Null
    */
   public void addConstraintValidation(ConstraintValidation constraintValidation) {
      if (constraintValidation == null) {
         throw new IllegalArgumentException("Constraint Validation can not be null");
      }
      ValidationTestResult testResult = constraintValidation.getTestResult();

      if ((constraints.isEmpty()) && (subReports.isEmpty())) {
         switch (testResult) {
            case PASSED:
               setSubReportResult(ValidationTestResult.PASSED);
               break;

            case FAILED:
               setSubReportResult((ConstraintPriority.MANDATORY.equals(
                     constraintValidation.getPriority())) ? ValidationTestResult.FAILED :
                     ValidationTestResult.PASSED);
               break;
            default:
               setSubReportResult(ValidationTestResult.UNDEFINED);
               break;
         }
      } else {
         switch (testResult) {
            case UNDEFINED:
               setSubReportResult(ValidationTestResult.UNDEFINED);
               break;
            case FAILED:
               setSubReportResult(((ConstraintPriority.MANDATORY.equals(
                     constraintValidation.getPriority())) && (ValidationTestResult.PASSED
                     .equals(subReportResult))) ? ValidationTestResult.FAILED :
                     subReportResult);
               break;
            default:
               setSubReportResult(subReportResult);
               break;
         }
      }
      this.constraints.add(new ConstraintValidation(constraintValidation));
      incrementSubCounter(constraintValidation.getSeverity(), constraintValidation.getTestResult());

   }

   /**
    * <p>Getter of the field <code>subReportResult</code></p>
    *
    * @return a {@link ValidationTestResult} object
    */
   public ValidationTestResult getSubReportResult() {
      return subReportResult;
   }

   /**
    * <p>Setter for the field <code>subReportResult</code></p>
    * subReportResult is automatically setted by constraint results
    *
    * @param subReportResult a {@link ValidationTestResult} object, can not be null
    */
   private void setSubReportResult(ValidationTestResult subReportResult) {
      this.subReportResult = subReportResult;
   }

   /**
    * <p>Getter of the field <code>subCounters</code></p>
    *
    * @return a {@link ValidationCounters} object
    */
   public ValidationCounters getSubCounters() {

      return new ValidationCounters(subCounters);
   }

   /**
    * <p>Getter of the field <code>subReports</code></p>
    *
    * @return a {@link List} object
    */
   public List<ValidationSubReport> getSubReports() {
      return new ArrayList<>(subReports);
   }

   /**
    * <p>Setter of the field <code>subReports</code></p>
    * <p>For internal constructor usage only, the list cannot be modified through the setter afterwards, otherwise it
    * will mess with the validation
    * status and the counters.</p>
    *
    * @param subReports subReports, must not be null.
    */
   private void setSubReports(List<ValidationSubReport> subReports) {
      for (ValidationSubReport subReport : subReports) {
         addSubReport(subReport);
      }
   }

   /**
    * Add a sub report to a sub report, it will upgrade subReportResult and Counters
    *
    * @param validationSubReport {@link ValidationSubReport} can not be null
    *
    * @throws IllegalArgumentException if validationSubReport is null
    */
   public void addSubReport(ValidationSubReport validationSubReport) {
      if (validationSubReport == null) {
         throw new IllegalArgumentException("validateSubReport can not be null");
      }

      if (subReports.isEmpty() && constraints.isEmpty()) {
         setSubReportResult(validationSubReport.getSubReportResult());
      } else {
         switch (validationSubReport.getSubReportResult()) {
            case FAILED:
               setSubReportResult(
                     (ValidationTestResult.PASSED.equals(getSubReportResult())) ? ValidationTestResult.FAILED :
                           getSubReportResult());
               break;
            case UNDEFINED:
               setSubReportResult(ValidationTestResult.UNDEFINED);
               break;
            default:
               setSubReportResult(getSubReportResult());
               break;
         }
      }
      this.subReports.add(new ValidationSubReport(validationSubReport));
      this.subCounters.addNumbersFromSubCounters(validationSubReport.getSubCounters());

   }

   /**
    * <p>Getter for the field <code>name</code>.</p>
    *
    * @return name a {@link String} object.
    */
   public String getName() {

      return name;
   }

   /**
    * <p>Setter for the field <code>name</code>.</p>
    *
    * @param name a {@link String} object.
    *
    * @throws IllegalArgumentException if name is null
    */
   public void setName(String name) {
      if (name == null) {
         throw new IllegalArgumentException("Name can not be null");
      }
      this.name = name;
   }

   /**
    * <p>Getter for the field <code>standards</code>.</p>
    *
    * @return standards a {@link List} object.
    */
   public List<String> getStandards() {
      return new ArrayList<>(standards);
   }

   /**
    * <p>Setter of the field <code>standards</code></p>
    *
    * @param standards if parameter is null, the list is initialized
    */
   public void setStandards(List<String> standards) {
      this.standards = (standards != null) ? new ArrayList<String>(standards) : new ArrayList<String>();
   }

   /**
    * Identity Equals Method
    *
    * @param o a {@link Object} object,
    *
    * @return {@link Boolean} object
    */
   public boolean identityEquals(Object o) {
      if (this == o) {
         return true;
      }
      if (o == null || getClass() != o.getClass()) {
         return false;
      }

      ValidationSubReport that = (ValidationSubReport) o;

      return (name.equals(that.name));

   }

   /**
    * {@inheritDoc}
    */
   @Override
   public boolean equals(Object o) {
      if (this == o) {
         return true;
      }
      if (o == null || getClass() != o.getClass()) {
         return false;
      }

      ValidationSubReport that = (ValidationSubReport) o;

      if (!standards.equals(that.standards)) {
         return false;
      }
      if (subReportResult != that.subReportResult) {
         return false;
      }
      if (!subCounters.equals(that.subCounters)) {
         return false;
      }
      if (!unexpectedErrors.equals(that.unexpectedErrors)) {
         return false;
      }

      if (!constraints.equals(that.constraints)) {
         return false;
      }

      return subReports.equals(that.subReports);
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public int hashCode() {
      int result = standards.hashCode();
      result = 31 * result + subCounters.hashCode();
      result = 31 * result + unexpectedErrors.hashCode();
      result = 31 * result + constraints.hashCode();
      result = 31 * result + subReportResult.hashCode();
      result = 31 * result + subReports.hashCode();
      return result;
   }

}
