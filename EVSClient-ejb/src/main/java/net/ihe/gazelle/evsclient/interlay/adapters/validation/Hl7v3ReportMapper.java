package net.ihe.gazelle.evsclient.interlay.adapters.validation;

import net.ihe.gazelle.evsclient.application.validation.ValidationReportMapper;
import net.ihe.gazelle.evsclient.domain.validation.report.ValidationOverview;
import net.ihe.gazelle.validation.ValidationResultsOverview;

public class Hl7v3ReportMapper extends MbvReportMapper{


    private static final String HL7v3_SERVICE_NAME = "GazelleHL7Validator";

    @Override
    protected ValidationOverview parseSpecificOverview(ValidationResultsOverview originalOverview) {
        String oldValidationServiceName = originalOverview.getValidationServiceName();
        String oldValidatorID = originalOverview.getValidationEngine();

        String validationServiceName = null;
        String validationServiceVersion = null;
        String validatorID = null;

        if(oldValidationServiceName != null){
            String[] extracted = oldValidationServiceName.split(":");
            if (extracted.length == 2){
                validationServiceName = extracted[0].trim();
                validatorID = extracted[1].trim();
            }
        }
        if(oldValidatorID != null){
            String[] extracted = oldValidatorID.split(":");
            if(extracted.length == 2){
                validationServiceVersion = extracted[1].trim();
            }
        }

        ValidationOverview overview = new ValidationOverview(ValidationReportMapper.DEFAULT_DISCLAIMER,
                parseValidationDate(originalOverview.getValidationDate(), originalOverview.getValidationTime()),
                validationServiceName != null ? validationServiceName : HL7v3_SERVICE_NAME,
                validationServiceVersion != null ? validationServiceVersion : ValidationReportMapper.UNKNOWN,
                validatorID != null ? validatorID : ValidationReportMapper.UNKNOWN
        );
        overview.setValidatorVersion(originalOverview.getValidationServiceVersion());
        return overview;
    }

}
