package net.ihe.gazelle.evsclient.interlay.factory;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.application.security.ApiKeyManager;
import net.ihe.gazelle.evsclient.interlay.ws.rest.validations.ValidationsApiService;
import net.ihe.gazelle.evsclient.interlay.ws.rest.validations.ValidationsApiServiceImpl;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.*;

@Name("evsApiFactory")
@Scope(ScopeType.APPLICATION)
@AutoCreate
public class ApiFactory {

    @In(value = "evsApplicationFactory")
    private ApplicationFactory evsApplicationFactory;

    @In(value = "evsCommonApplicationFactory")
    private EvsCommonApplicationFactory evsCommonApplicationFactory;

    @In(value = "apiKeyManager", create = true)
    private ApiKeyManager apiKeyManager;

    @In(value = "applicationPreferenceManager", create = true)
    private ApplicationPreferenceManager applicationPreferenceManager;

    @Factory(value = "validationsApiService", scope = ScopeType.EVENT)
    public ValidationsApiService getValidationsApiService() {
        return new ValidationsApiServiceImpl(evsApplicationFactory, evsCommonApplicationFactory, apiKeyManager, applicationPreferenceManager);
    }

}
