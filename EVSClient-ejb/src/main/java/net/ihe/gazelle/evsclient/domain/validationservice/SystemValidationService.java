package net.ihe.gazelle.evsclient.domain.validationservice;

import net.ihe.gazelle.evsclient.domain.validationservice.configuration.SystemValidationServiceConf;

public interface SystemValidationService extends ConfigurableValidationService<SystemValidationServiceConf>, VersionedValidationService {
}
