package net.ihe.gazelle.evsclient.domain.validationservice;

import net.ihe.gazelle.evsclient.domain.validationservice.configuration.EmbeddedValidationServiceConf;

public interface EmbeddedValidationService extends ConfigurableValidationService<EmbeddedValidationServiceConf>,VersionedValidationService {
}
