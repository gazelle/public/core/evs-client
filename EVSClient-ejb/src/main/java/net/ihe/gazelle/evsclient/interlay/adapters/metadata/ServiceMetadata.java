package net.ihe.gazelle.evsclient.interlay.adapters.metadata;

import net.ihe.gazelle.evsclient.domain.validationservice.Validator;

import java.util.ArrayList;
import java.util.List;

public class ServiceMetadata {

    private String serviceUrl;

    private String serviceName;

    private final List<Validator> validators = new ArrayList<>();

    public String getServiceUrl() {
        return serviceUrl;
    }

    public ServiceMetadata setServiceUrl(String serviceUrl) {
        this.serviceUrl = serviceUrl;
        return this;
    }

    public String getServiceName() {
        return serviceName;
    }

    public ServiceMetadata setServiceName(String serviceName) {
        this.serviceName = serviceName;
        return this;
    }

    public List<Validator> addValidator(Validator validator) {
        this.validators.add(validator);
        return validators;
    }

    public List<Validator> getValidators() {
        return validators;
    }
}
