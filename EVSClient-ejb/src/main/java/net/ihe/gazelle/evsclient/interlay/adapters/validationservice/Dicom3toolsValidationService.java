package net.ihe.gazelle.evsclient.interlay.adapters.validationservice;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.application.validation.ReportParser;
import net.ihe.gazelle.evsclient.domain.processing.HandledObject;
import net.ihe.gazelle.evsclient.domain.validation.ValidationType;
import net.ihe.gazelle.evsclient.domain.validation.report.SeverityLevel;
import net.ihe.gazelle.evsclient.domain.validation.report.ValidationReport;
import net.ihe.gazelle.evsclient.domain.validation.types.ValidationProperties;
import net.ihe.gazelle.evsclient.domain.validationservice.ValidationServiceOperationException;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.SystemValidationServiceConf;
import net.ihe.gazelle.evsclient.interlay.adapters.validation.PlainReportMapper;
import net.ihe.gazelle.evsclient.interlay.dto.rest.validation.report.XMLValidationReportMarshaller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@ValidationProperties(
        name="Dicom3tools Check",
        types =ValidationType.DICOM)
@ReportParser(PlainReportMapper.class)
public class Dicom3toolsValidationService extends AbstractSystemValidationService {

    public static final String NO_PROFILE = "DICOM Standard Conformance";
    private String command;
    private String profiles;

    public Dicom3toolsValidationService(ApplicationPreferenceManager applicationPreferenceManager, SystemValidationServiceConf configuration) {
        super(applicationPreferenceManager, configuration);
        this.command = this.executor.getBinaryLocation(configuration.getBinaryPath(),"/opt/dicom3tools");
    }

    @Override
    public String getName() {
        return "DICOM PS3";
    }

    @Override
    protected List<String> getValidatorNames(String validatorFilter) {
        List<String> validators = new ArrayList<>();
        validators.add(NO_PROFILE);
        try {
            Pattern profile = Pattern.compile("^\\s+(\\w+)\\s+\\(.*$");
            String[] lines = getProfiles().split("\n");
            for (String line:lines) {
                Matcher m = profile.matcher(line);
                if (m.find()) {
                    validators.add(m.group(1));
                }
            }
        } catch (Exception e) {

        }
        return validators;
    }

    @Override
    public String about() {
        StringBuilder sb = new StringBuilder();
        try {
            sb.append(executor.exec("man -f "+command,false));
        } catch (Exception e) {
            sb.append(command);
        }
        try {
            sb.append("\nPROFILES:\n"+this.getProfiles());
        } catch (Exception e) {
        }
        return sb.toString();
    }

    private String getProfiles() throws IOException, InterruptedException {
        if (this.profiles==null) {
            Pattern start = Pattern.compile("(?m)(?s).*^       -profile .*?$");
            Pattern end = Pattern.compile("(?m)(?s)(.*?)^       -\\w.*");
            String stdout = executor.exec("man -P cat -E UTF-8 " + command, false);
            stdout = start.matcher(stdout).replaceAll("");
            this.profiles = end.matcher(stdout).replaceAll("$1");
        }
        return this.profiles;
    }


    @Override
    public List<String> getSupportedMediaTypes() {
        return Arrays.asList("application/dicom");
    }

    @Override
    public byte[] validate(HandledObject[] objects, String validator) throws ValidationServiceOperationException {
        try {
            File dcmFile = File.createTempFile("dicom3tools_", ".dcm");
            FileOutputStream outputStream = new FileOutputStream(dcmFile);
            try {
                outputStream.write(objects[0].getContent());
            } finally {
                outputStream.close();
            }
            try {
                String result = executor.exec(
                        command
                                +" -ignoreoutofordertags"
                                +(NO_PROFILE.equals(validator)?"":" -profile "+validator)
                                +" "+dcmFile.getAbsolutePath(),true);
                ValidationReport report = new DicomReportBuilder().build(
                        this,
                        validator,
                        this.getServiceVersion(),
                        result
                );
                return new XMLValidationReportMarshaller(SeverityLevel.INFO)
                        .marshal(report).getBytes(StandardCharsets.UTF_8);
            } finally {
                dcmFile.delete();
            }
        } catch (Exception e) {
            LOGGER.error(String.format("an unexpected error occurred during the validation: %s", e.getMessage()));
            throw new ValidationServiceOperationException(e);
        }
    }

    @Override
    public String getServiceVersion() {
        try {
            return executor.exec(command+" -version",true).replace('\n',' ').replaceFirst(".*? Version:\\s*([\\.0-9a-zA-Z]+).*$","$1");
        } catch (Exception e) {
            return "";
        }
    }
}
