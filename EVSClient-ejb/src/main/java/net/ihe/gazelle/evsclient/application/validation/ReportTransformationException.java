package net.ihe.gazelle.evsclient.application.validation;

public class ReportTransformationException extends Exception {
   private static final long serialVersionUID = 7895091481512940532L;

   public ReportTransformationException(String s) {
      super(s);
   }

   public ReportTransformationException(String s, Throwable throwable) {
      super(s, throwable);
   }
}
