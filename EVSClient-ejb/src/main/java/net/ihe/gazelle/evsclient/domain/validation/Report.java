/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.ihe.gazelle.evsclient.domain.validation;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Inheritance(strategy= InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="discriminator", discriminatorType = DiscriminatorType.STRING)
@Table(name = "evsc_report")
@SequenceGenerator(name = "reportSequence", sequenceName = "evsc_report_id_seq", allocationSize = 1)
public class Report<T extends Serializable> implements Serializable {
    private static final long serialVersionUID = 5073579434201288373L;

    @Id
    @GeneratedValue(generator = "reportSequence", strategy = GenerationType.SEQUENCE)
    @Column(name = "id")
    private Integer id;

    @Transient
    private T content;

    // For JPA Only
    protected Report() {
    }

    public Report(T content) {
        setContent(content);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public T getContent() {
        return content;
    }

    public void setContent(T content) {
        if(content != null) {
            this.content = content;
        } else {
            throw new IllegalArgumentException("Content report must be defined");
        }
    }

}
