package net.ihe.gazelle.evsclient.application.validation;

import net.ihe.gazelle.evsclient.domain.validation.Report;

import java.io.IOException;

public interface ValidatorReportDao {

    Report load(Report r) throws IOException;
    Report save(Report r) throws IOException;
    void delete(Report r) throws IOException;

}
