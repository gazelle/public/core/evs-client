package net.ihe.gazelle.evsclient.application.validation;

import net.ihe.gazelle.evsclient.domain.validation.report.ValidationReport;

import java.io.IOException;

public interface ValidationReportDao {
    void persist(ValidationReport validationReport) throws IOException;
}
