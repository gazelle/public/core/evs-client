package net.ihe.gazelle.evsclient.interlay.adapters.validation;

import net.ihe.gazelle.evsclient.application.validation.OriginalReportParser;
import net.ihe.gazelle.evsclient.application.validation.ReportTransformationException;
import net.ihe.gazelle.evsclient.application.validation.ValidationReportMapper;
import net.ihe.gazelle.evsclient.domain.validation.report.ValidationReport;
import net.ihe.gazelle.evsclient.interlay.dto.rest.validation.report.ValidationReportDTO;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.ByteArrayInputStream;
import java.io.IOException;

public class PlainReportMapper extends AbstractReportMapper implements ValidationReportMapper, OriginalReportParser<ValidationReport> {

   @Override
   public ValidationReport transform(byte[] originalReport) throws ReportTransformationException {
      return parseOriginalReport(originalReport);
   }

   @Override
   public ValidationReport parseOriginalReport(byte[] originalReport) throws ReportTransformationException {
      if (originalReport!=null && originalReport.length > 0) {
         try (ByteArrayInputStream reportInputStream = new ByteArrayInputStream(originalReport)) {
            final JAXBContext jax = JAXBContext.newInstance(ValidationReportDTO.class);
            final Unmarshaller unm = jax.createUnmarshaller();
            return ((ValidationReportDTO) unm.unmarshal(reportInputStream)).toDomain();
         } catch (IOException | IllegalArgumentException | JAXBException e) {
            throw new ReportTransformationException("Unable to parse original report", e);
         }
      } else {
         throw new ReportTransformationException("Empty original report");
      }
   }
}
