package net.ihe.gazelle.evsclient.application.validation;

import net.ihe.gazelle.evsclient.application.interfaces.adapters.ProcessingDao;
import net.ihe.gazelle.evsclient.domain.validation.extension.ValidationExtension;

public interface ExtensionDao extends ProcessingDao<ValidationExtension> {

    ValidationExtension findByValidationOid(String oid);
}
