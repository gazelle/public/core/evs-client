package net.ihe.gazelle.evsclient.interlay.gui.validation;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.application.CallerMetadataFactory;
import net.ihe.gazelle.evsclient.application.interfaces.GuiPermanentLink;
import net.ihe.gazelle.evsclient.application.notification.EmailNotificationManager;
import net.ihe.gazelle.evsclient.application.validation.ValidationServiceFacade;
import net.ihe.gazelle.evsclient.domain.validation.Validation;
import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import org.richfaces.event.FileUploadEvent;

// FIXME To rework completely. Signable Validation should not be in inheritance tree !
// include this feature by creating a real validation service. Aggregating several validation together is not the
// responsability of EVS :'(
public abstract class AbstractSignableValidationBeanGui extends AbstractValidationBeanGui {

    protected boolean dsSignaturePresent;
    protected boolean validateSignature;

    protected AbstractSignableValidationBeanGui(Class<?> cls, ValidationServiceFacade validationServiceFacade,
                                                CallerMetadataFactory callerMetadataFactory,
                                                GazelleIdentity identity,
                                                ApplicationPreferenceManager applicationPreferenceManager,
                                                GuiPermanentLink<Validation> permanentLinkGui,
                                                EmailNotificationManager emailNotificationManager) {
        super(cls, validationServiceFacade, callerMetadataFactory, identity, applicationPreferenceManager, permanentLinkGui, emailNotificationManager);
    }

    public boolean isDsSignaturePresent() {
        return dsSignaturePresent;
    }

    public boolean getValidateSignature() {
        return validateSignature;
    }

    public void setValidateSignature(boolean validateSignature) {
        this.validateSignature = validateSignature;
    }

    @Override
    public void uploadListener(final FileUploadEvent event) {
        super.uploadListener(event);
        if (applicationPreferenceManager.getBooleanValue("enable_dsig_validation").booleanValue()) {
            dsSignaturePresent = processingManager.isDsSignaturePresent(getUploadedFileContent());
        }
    }

}
