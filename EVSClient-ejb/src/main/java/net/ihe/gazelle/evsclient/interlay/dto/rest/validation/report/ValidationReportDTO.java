package net.ihe.gazelle.evsclient.interlay.dto.rest.validation.report;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import net.ihe.gazelle.evsclient.domain.validation.report.*;
import net.ihe.gazelle.evsclient.interlay.dto.rest.DataTransferObject;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Schema(description = "Rapport détaillé de validation. ")
@XmlRootElement(name = "validationReport")
@XmlAccessorType(XmlAccessType.PROPERTY)
@XmlType(propOrder = {"validationOverview", "counters", "subReports"})
public class ValidationReportDTO implements DataTransferObject<ValidationReport> {

   private String uuid;
   private ValidationOverviewDTO validationOverview;
   private ValidationCountersDTO validationCounters;
   private List<ValidationSubReportDTO> validationSubReports;

   public ValidationReportDTO() {
   }

   public ValidationReportDTO(ValidationReport domain,SeverityLevel severity) {
      this.uuid = domain.getUuid();
      this.validationOverview = new ValidationOverviewDTO(
              domain.getValidationOverview(),
              severity.equals(SeverityLevel.INFO) ? null : Arrays.asList(new Metadata[] {
                      new Metadata("filtered-report-disclaimer","This report is a partial report ; only constraints of severity higher or equals to "+severity.name()+" are included. The full report can be requested using severity parameter at INFO level.")
              })
      );
      this.validationCounters = new ValidationCountersDTO(domain.getCounters());
      this.validationSubReports = domain.getSubReports() != null ? mapSubReports(domain.getSubReports(),severity) : null ;
   }

   @Schema(name = "uuid", description = "") // TODO
   @JsonProperty("uuid")
   @XmlAttribute(name = "uuid")
   public String getUuid() {
      return uuid;
   }

   public void setUuid(String uuid) {
      this.uuid = uuid;
   }

   @Schema(name = "result", description = "") // TODO
   @JsonProperty("result")
   @XmlAttribute(name = "result")
   public ValidationTestResult getResult() {
      return validationOverview.getValidationOverallResult();
   }

   public void setResult(ValidationTestResult result) {
      // Do nothing when unmarshalling.
      // Result attribute is a cloned of the value in .validationOverview@overallResult.
   }

   @Schema(name = "validationOverview", description = "") // TODO
   @JsonProperty("validationOverview")
   @XmlElement(name = "validationOverview")
   public ValidationOverviewDTO getValidationOverview() {
      return validationOverview;
   }

   public void setValidationOverview(
         ValidationOverviewDTO validationOverview) {
      this.validationOverview = validationOverview;
   }

   @Schema(name = "counters", description = "") // TODO
   @JsonProperty("counters")
   @XmlElement(name = "counters")
   public ValidationCountersDTO getCounters() {
      return validationCounters;
   }

   public void setCounters(
         ValidationCountersDTO validationCountersDTO) {
      this.validationCounters = validationCountersDTO;
   }

   @Schema(name = "subReports", description = "") // TODO
   @JsonProperty("subReports")
   @XmlElement(name = "subReport")
   public List<ValidationSubReportDTO> getSubReports() {
      return validationSubReports;
   }

   public void setSubReports(
         List<ValidationSubReportDTO> validationSubReportDTOs) {
      this.validationSubReports = validationSubReportDTOs;
   }

   @Override
   public ValidationReport toDomain() {
      ValidationReport domain = new ValidationReport(uuid, validationOverview.toDomain());
      if(validationSubReports != null) {
         for(ValidationSubReportDTO subReportDTO : validationSubReports) {
            domain.addSubReport(subReportDTO.toDomain());
         }
      }
      return domain;
   }

   private List<ValidationSubReportDTO> mapSubReports(List<ValidationSubReport> subReports, SeverityLevel severity) {
      List<ValidationSubReportDTO> dtoSubReports = new ArrayList<>();
      for (ValidationSubReport vsr : subReports) {
         dtoSubReports.add(new ValidationSubReportDTO(vsr,severity));
      }
      return dtoSubReports;
   }

}
