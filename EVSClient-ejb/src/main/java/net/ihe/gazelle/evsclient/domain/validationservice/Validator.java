package net.ihe.gazelle.evsclient.domain.validationservice;

import java.util.Objects;

public class Validator {

    private String keyword;
    private String name;
    private String domain;

    /**
     * Constructor of Validator with minimal <pre>keyword</pre> required parameter
     *
     * @param keyword the keyword (or id, oid) of the validator, used to programatically reference it.
     */
    public Validator(String keyword) {
        setKeyword(keyword);
    }

    /**
     * Constructor of Validator with keyword, name and domain.
     *
     * @param keyword the keyword (or id, oid) of the validator, used to programatically reference it (mandatory).
     * @param name    the human name of the validator (may be displayed in GUI)
     * @param domain  the domain which the validator is associated with (may be used to filtered the list of Validator in a Validation Service).
     */
    public Validator(String keyword, String name, String domain) {
        setKeyword(keyword);
        this.name = name;
        this.domain = domain;
    }

    /**
     * Get the keyword of the Validator
     *
     * @return the keyword of the Validator
     */
    public String getKeyword() {
        return keyword;
    }

    /**
     * Set the keyword of the Validator
     *
     * @param keyword the keyword of the Validator, must be unique per Validation Service.
     *
     * @throws IllegalArgumentException if the keyword is null or blank.
     */
    public void setKeyword(String keyword) {
        if (keyword != null && !keyword.trim().isEmpty()) {
            this.keyword = keyword;
        } else {
            throw new IllegalArgumentException("Validator keyword must be defined");
        }
    }

    /**
     * Get the name of the Validator
     *
     * @return the name of the Validator or <pre>null</pre>.
     */
    public String getName() {
        return name;
    }

    /**
     * Set the name of the Validator (may be displayed in GUI).
     *
     * @param name the human name of the Validator
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Get the domain the Validator is associated with. This domain may be used to filtered the list of Validator in a Validation Service.
     * <p>
     * <i>It is intended that the domain of the Validator is refering to a Domain in the interoperability model of IHE (defined in GMM), but be aware
     * that some Validation Service or Test Designers may twist this field to enter other granularity level of filtering.</i>
     *
     * @return the domain the Validator is associated with or <pre>null</pre>.
     */
    public String getDomain() {
        return domain;
    }

    /**
     * Set the domain to associate with the Validator. This domain may be used to filtered the list of Validator in a Validation Service.
     *
     * @param domain the domain the validator will be associated with.
     */
    public void setDomain(String domain) {
        this.domain = domain;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Validator validator = (Validator) o;
        return Objects.equals(keyword, validator.keyword);
    }

    @Override
    public int hashCode() {
        return Objects.hash(keyword);
    }
}
