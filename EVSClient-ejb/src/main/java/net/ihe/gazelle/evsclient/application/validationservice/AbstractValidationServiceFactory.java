package net.ihe.gazelle.evsclient.application.validationservice;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.domain.validationservice.ValidationService;
import net.ihe.gazelle.evsclient.domain.validationservice.ValidationServiceFactory;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.ValidationServiceConf;

import java.lang.reflect.InvocationTargetException;

public abstract class AbstractValidationServiceFactory<T extends ValidationService, C extends ValidationServiceConf>
   extends AbstractServiceFactory<T,C> implements ValidationServiceFactory<T,C> {


   // FIXME configuration for ValidationService Instance should be a parameter of getValidationService and not of the
   //  factory constructor.
   protected AbstractValidationServiceFactory(ApplicationPreferenceManager applicationPreferenceManager, C configuration,
                                           Class<T> validationServiceClass) {
      super(applicationPreferenceManager,configuration,validationServiceClass);
   }

   public T getService() {
      try {
         return serviceClass
                 .getDeclaredConstructor(ApplicationPreferenceManager.class, getConfigurationClass())
                 .newInstance(applicationPreferenceManager, configuration);
      } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
         throw new RuntimeException(
                 "Unable To instantiate validation service for class " + serviceClass, e);
      }
   }
}
