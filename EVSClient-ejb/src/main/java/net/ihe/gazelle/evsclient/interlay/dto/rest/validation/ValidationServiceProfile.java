package net.ihe.gazelle.evsclient.interlay.dto.rest.validation;

import java.util.Objects;

public class ValidationServiceProfile {

    private String profileID;

    private String serviceName;


    public String getProfileID() {
        return profileID;
    }

    public ValidationServiceProfile setProfileID(String profileID) {
        this.profileID = profileID;
        return this;
    }

    public String getServiceName() {
        return serviceName;
    }

    public ValidationServiceProfile setServiceName(String serviceName) {
        this.serviceName = serviceName;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ValidationServiceProfile)) return false;
        ValidationServiceProfile that = (ValidationServiceProfile) o;
        return Objects.equals(profileID, that.profileID);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(profileID);
    }
}
