package net.ihe.gazelle.evsclient.interlay.dao.validation;

import net.ihe.gazelle.evsclient.domain.validation.Report;
import net.ihe.gazelle.evsclient.interlay.dao.FileReadException;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.Serializable;

@MappedSuperclass
abstract class ReportFile<T extends Serializable> extends Report<T> {

   private static final long serialVersionUID = -1022064616574620525L;

   @Column(name = "archive_path")
   private String archivePath;

   // For JPA Only
   protected ReportFile() {
      super();
   }

   ReportFile(Report<T> report) {
      setId(report.getId());
      if (report instanceof ReportFile<?>) { // TODO verify "if condition" regarding sub-types
         // if is instance of ReportFile, then content should NOT be copied for performance issue as a
         // call to getContent() will trigger a read from the file system.
         this.archivePath = ((ReportFile<?>) report).getArchivePath();
      } else {
         setContent(report.getContent());
      }
   }

   public String getArchivePath() {
      return archivePath;
   }

   public void setArchivePath(String filePath) {
      this.archivePath = filePath;
   }

   @Override
   public T getContent() {
      if (super.getContent() == null && archivePath != null) {
         try (ReportZipInputStream fis = new ReportZipInputStream(new FileInputStream(archivePath))) {
            setContent(unmarshall(fis));
         } catch (IOException e) {
            throw new FileReadException("Unable to load content from file.", e);
         }
      }
      return super.getContent();
   }

   abstract T unmarshall(ReportZipInputStream inputStream) throws IOException;

   abstract void marshall(T content, ReportZipOutputStream outputStream) throws IOException;

}
