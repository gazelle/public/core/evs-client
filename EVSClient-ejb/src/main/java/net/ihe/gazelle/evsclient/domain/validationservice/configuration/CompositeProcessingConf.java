/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.ihe.gazelle.evsclient.domain.validationservice.configuration;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


@MappedSuperclass
public abstract class CompositeProcessingConf<T extends ProcessingConf> extends ProcessingConf implements Serializable {

    protected CompositeProcessingConf() {
    }

    @ManyToMany
    @JoinTable(name = "evsc_conf_composition",
            joinColumns = @JoinColumn(name = "composed_id"),
            inverseJoinColumns = @JoinColumn(name = "component_id"),
            uniqueConstraints = @UniqueConstraint(columnNames = {"composed_id", "component_id"}))
    private List<T> configurations = new ArrayList<>();

    public List<T> getConfigurations() {
        return configurations;
    }


    @Transient
    public List<String> getConfigurationProcessingName() {
        List<String> names = new ArrayList<>();
        for (T configuration : configurations) {
            names.add(configuration.getName());
        }
        return names;
    }

    public void setConfigurations(List<T> validationServices) {
        this.configurations = validationServices;
    }

}
