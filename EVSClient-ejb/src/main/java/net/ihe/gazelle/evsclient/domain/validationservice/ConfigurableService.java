package net.ihe.gazelle.evsclient.domain.validationservice;

import net.ihe.gazelle.evsclient.domain.validationservice.configuration.ProcessingConf;

public interface ConfigurableService<T extends ProcessingConf> extends ProcessingService {

    T getConfiguration();
}
