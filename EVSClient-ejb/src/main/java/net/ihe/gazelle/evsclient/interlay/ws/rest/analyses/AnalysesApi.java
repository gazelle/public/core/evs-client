package net.ihe.gazelle.evsclient.interlay.ws.rest.analyses;

import net.ihe.gazelle.evsclient.interlay.dto.rest.analysis.AnalysisForCreation;
import net.ihe.gazelle.evsclient.interlay.ws.rest.NotFoundException;
import org.jboss.seam.annotations.In;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

@Path("/analyses")


//@io.swagger.annotations.Api(description = "the analyses API")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaResteasyServerCodegen", date = "2021-08-27T13:39:11.206Z")
public class AnalysesApi  {

    @In(value = "analysesApiService", create = true)
    AnalysesApiService service;

    @POST
    
    @Consumes({ "application/json", "application/xml" })
    @Produces({ "application/xml", "application/json" })
//    @io.swagger.annotations.ApiOperation(value = "Crée une analyse", notes = "*[EVS-76]* Pour créer une **Analysis** et exécuter une analyse, une demande HTTP POST doit être envoyée sur {base}/analyses avec une ressource dans le corps du message (body).  *[EVS-77]* Il doit être possible d’envoyer une demande de création d'**Analysis** au format XML, le champ d’en-tête HTTP `ContentType` doit avoir la valeur `application/xml`.  *[EVS-78]* Il devrait être possible d’envoyer une demande de création d'**Analysis** au format JSON, le champ d’en-tête HTTP `ContentType` doit avoir la valeur `application/json`.  *[EVS-81]* Si la demande de création de la ressource **Analysis** inclut un objet, alors la ressource doit être créée et l’analyse déclenchée.  *[EVS-83]* Si l’en-tête `Authorization` est présent avec une clé API valide, **Analysis** et le ou les résultats doivent être rendus privés à l’utilisateur propriétaire de la clé API.  *[EVS-88]* Si la demande de l’analyse de la ressource **Analysis** a été précisée asynchrone (`Prefer: respond-async`), EVSClient doit créer l'**Analysis**, déclencher l’analyse de l’objet et répondre immédiatement selon [EVS-89] sans attendre la fin de l’analyse. ", response = Void.class, tags={ "Analysis", })
//    @io.swagger.annotations.ApiResponses(value = {
//        @io.swagger.annotations.ApiResponse(code = 201, message = "**Created**  *[EVS-86]* Une fois que la ressource **Analysis** est créée et que l’analyse est terminée, EVSClient doit répondre avec HTTP 201 Created et l’en-tête `Content-Location` doit contenir l’URL de la nouvelle **Analysis**. ", response = Void.class),
//
//        @io.swagger.annotations.ApiResponse(code = 202, message = "**Accepted**  *[EVS-89]* Si la demande de l’analyse de la ressource **Analysis** a été précisée asynchrone (`Prefer: respond-async`), la réponse d’EVSClient doit être HTTP 202 Accepted et le champ `Location` de l’en-tête est renseigné avec l’URL de récupération de l'**Analysis**. *Voir la section GET /analyses/{oid} pour plus d'information sur les lectures d'**Analysis** dont le traitement n'est pas encore terminé.* ", response = Void.class),
//
//        @io.swagger.annotations.ApiResponse(code = 400, message = "**Bad Request**  [EVS-82] Si la demande de création d'une **Analysis** n’inclut pas un objet, EVSClient doit répondre avec HTTP 400 Bad Request.  [EVS-84] Si l’en-tête authorization est présent mais ne respecte pas le format `GazelleAPIKey <key>`, EVSClient doit répondre par HTTP 400 Bad Request. ", response = Void.class),
//
//        @io.swagger.annotations.ApiResponse(code = 401, message = "**Unauthorized**  [EVS-85] Si l’analyse n’est autorisée qu’aux utilisateurs connectés, et si l’en-tête `Authorization` est manquante dans la demande ou si la clé API donnée est invalide, EVSClient doit répondre avec HTTP 401 Unauthorized. ", response = Void.class) })
    public Response createAnalysis(
            //@ApiParam(value = "Objet à analyser  Dans toutes les demandes de création de ressource **Analysis**, le contenu de l’objet binaire doit être présent et codé en base 64.  *[EVS-90]* Pour créer une ressource **Analysis**, exécuter une analyse, puis effectuer la validation du contenu détecté, une demande HTTP POST doit être envoyée sur {base}/analyses avec une **Analysis** dans le corps du message et dont l’attribut `autoValidation` est défini à `true`.  *[EVS-91]* Si `autoValidation` est défini à `true` et une fois l’analyse terminée, EVSClient doit effectuer la validation de tous les éléments du contenu qui ont été détectés et qui peuvent être validés *(c-à-d. la configuration MCA correspondante se réfère à un ObjectType qui est couvert par un validateur ou contient une référence à un service de validation et son validateur)*.  *[EVS-92]* Si `autoValidation` est défini à `true` et pour valider chaque contenu détecté validable, EVSClient doit créer une ressource **Validation**, déclancher la validation et l’associer au contenu de l’**Analysis**. " ,required=true)
            AnalysisForCreation analysis,
            // @ApiParam(value = "*[EVS-80]* Pour authentifier l’utilisateur effectuant la création d'**Analysis**, le champ d’en-tête HTTP `Authorization` prend la valeur `GazelleAPIKey <key>`. " )
            @HeaderParam("Authorization") String authorization,
            //@ApiParam(value = "[EVS-87] Les réponses asynchrones peuvent également être demandées par le client en spécifiant `respond-async` dans le champ `Prefer` de l’entête. Voir RFC7240. " , allowableValues="respond-async")
    @HeaderParam("Prefer") String prefer,@Context SecurityContext securityContext)
    throws net.ihe.gazelle.evsclient.interlay.ws.rest.NotFoundException {
        return service.createAnalysis(analysis,authorization,prefer,securityContext);
    }
    @GET
    @Path("/{oid}")
    @Consumes({ "application/json", "application/xml" })
    @Produces({ "application/json", "application/xml" })
//    @io.swagger.annotations.ApiOperation(value = "Récupère un objet analysé", notes = "*[EVS-93]* Pour lire une ressource **Analysis**, la demande HTTP GET doit être envoyée sur {base}/analyses/{oid}.  *[EVS-100]* Si l'**Analysis** demandée est publique, EVSClient peut répondre à n’importe quelle demande de lecture.  *[EVS-102]* Si l’en-tête `Authorization` est présente avec une clé API valide et si l'**Analysis** demandée est privée et appartient à l’organisation associée à la clé API, alors EVSClient peut répondre à la demande de lecture de la ressource. ", response = Void.class, tags={ "Analysis", })
//    @io.swagger.annotations.ApiResponses(value = {
//        @io.swagger.annotations.ApiResponse(code = 200, message = "**HTTP OK**  *[EVS-96]* Lors de la réception d’une demande de lecture d’une ressource **Analysis**, si l'`autoValidation` n’a pas été demandée lors de la création de la ressource et que l’analyse est terminée, EVSClient doit répondre avec HTTP 200 OK et le contenu **Analysis** dans le corps de la réponse (body).  *[EVS-97]* Lors de la réception d’une demande de lecture d’une ressource **Analysis**, si l'`autoValidation` a été demandée lors de la création de la ressource et que l’analyse et toutes les validations sont terminées, EVSClient doit répondre avec HTTP 200 OK et le contenu **Analysis** dans le corps de la réponse. ", response = Void.class),
//
//        @io.swagger.annotations.ApiResponse(code = 202, message = "**HTTP 202 Accepted**  *[EVS-98]* Lors de la réception d’une demande de lecture d’une ressource **Analysis**, si l’analyse ou des validations ne sont pas encore terminées, EVSClient doit répondre avec HTTP 202 Accepted, le champ d’en-tête Location prend la valeur de l’URL de l'**Analysis**, le champ d’en-tête X-Progress prend la valeur IN_PROGRESS et ne contient pas de body. ", response = Void.class),
//
//        @io.swagger.annotations.ApiResponse(code = 400, message = "**Bad Request**  *[EVS-101]* Si l’en-tête `Authorization` est présent mais n'est pas au format `GazelleAPIKey <key>`, EVSClient doit répondre par  HTTP 400 Bad Request. ", response = Void.class),
//
//        @io.swagger.annotations.ApiResponse(code = 401, message = "**Unauthorized**  *[EVS-104]* Si l'**Analysis** demandée est privée mais que l’en-tête `Authorization` est manquante, EVSClient doit répondre avec HTTP 401 Unauthorized. ", response = Void.class),
//
//        @io.swagger.annotations.ApiResponse(code = 403, message = "**Forbidden**  *[EVS-103]* Si l’en-tête Authorization est présente avec une clé API valide et si l'**Analysis** demandée est privée mais n’appartient PAS à l’organisation associée à la clé API, alors EVSClient doit répondre avec HTTP 403 Forbidden. ", response = Void.class),
//
//        @io.swagger.annotations.ApiResponse(code = 404, message = "**Not Found**  Si l'OID demandé ne correspond à aucune **Analysis** connue. ", response = Void.class) })
    public Response getAnalysisByOid( @PathParam("oid") String oid,  @QueryParam("privacyKey") String privacyKey,
                                      //@ApiParam(value = "[EVS-99] `GazelleAPIKey <key>` doit être pris en charge comme valeur d’en-tête de `Authorization`. " )
                                      @HeaderParam("Authorization") String authorization,

                                      //@ApiParam(value = "*[EVS-94]* Pour demander la consultation d’une **Analysis** au format XML, le champ d’en-tête HTTP `Accept` doit avoir la valeur `application/xml`.  *[EVS-95]* Pour demander la consultation d’une **Analysis** au format JSON, le champ d’en-tête HTTP `Accept` doit avoir la valeur `application/json`. " , allowableValues="application/xml, application/json", defaultValue="application/xml")
                                      @HeaderParam("Accept") String accept,@Context SecurityContext securityContext)
    throws NotFoundException {
        return service.getAnalysisByOid(oid,privacyKey,authorization,accept,securityContext);
    }
}
