package net.ihe.gazelle.evsclient.interlay.dao.validation;

public class ReportNotSavedException extends RuntimeException {
   private static final long serialVersionUID = 8678146932148497995L;

   public ReportNotSavedException(String s, Throwable throwable) {
      super(s, throwable);
   }
}
