package net.ihe.gazelle.evsclient.domain.validationservice.configuration.reports;

import net.ihe.gazelle.evsclient.domain.validationservice.configuration.stylesheets.Stylesheet;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name = "evsc_validation_report_type", schema = "public")
@SequenceGenerator(name = "evsc_validation_report_type_sequence", sequenceName = "evsc_validation_report_type_id_seq", allocationSize = 1)
public class ValidationReportType implements Serializable {

    private static final long serialVersionUID = 5496592283991917352L;

    @Id
    @NotNull
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "evsc_validation_report_type_sequence")
    private Integer id;

    @OneToOne(cascade = CascadeType.ALL)
    private Stylesheet stylesheet;

    @Enumerated(EnumType.STRING)
    @Column(name= "report_format")
    private ReportFormat reportFormat;

    public ValidationReportType() {
    }

    public ValidationReportType(Stylesheet stylesheet, ReportFormat reportFormat) {
        this.stylesheet = stylesheet;
        this.reportFormat = reportFormat;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Stylesheet getStylesheet() {
        return stylesheet;
    }

    public void setStylesheet(Stylesheet stylesheet) {
        this.stylesheet = stylesheet;
    }

    public ReportFormat getReportFormat() {
        return reportFormat;
    }

    public void setReportFormat(ReportFormat reportFormat) {
        this.reportFormat = reportFormat;
    }

}
