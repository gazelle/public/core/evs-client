package net.ihe.gazelle.evsclient.application.statistics;

import net.ihe.gazelle.evsclient.domain.statistics.IpLoc;

import java.io.IOException;

public interface IpLocDao {
    IpLoc findLastCheckedLocByIp(String ip);
    IpLoc save(IpLoc ipLoc) throws IOException;
}
