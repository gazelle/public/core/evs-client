package net.ihe.gazelle.evsclient.interlay.dto.rest.validation;

import net.ihe.gazelle.evsclient.domain.validation.Validation;
import net.ihe.gazelle.evsclient.interlay.dto.rest.AbstractEvsMarshaller;
import net.ihe.gazelle.evsclient.interlay.dto.rest.MarshallingException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.InputStream;
import java.io.OutputStream;

public class XMLValidationMarshaller extends AbstractEvsMarshaller<Validation> {

   private static final JAXBContext JAXB_CONTEXT = instantiateContext();

   private static JAXBContext instantiateContext() {
      try {
         return JAXBContext.newInstance(ValidationDTO.class);
      } catch (JAXBException e) {
         throw new RuntimeException("Unable to create JAXB context for ValidationDTO.class", e);
      }
   }

   @Override
   public void marshal(Validation validation, OutputStream outputStream) throws MarshallingException {
      try {
         Marshaller jaxbMarshaller = JAXB_CONTEXT.createMarshaller();
         jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, isIndentation());
         jaxbMarshaller.setProperty(Marshaller.JAXB_FRAGMENT, !isCompleteDocument());
         jaxbMarshaller.marshal(new ValidationDTO(validation), outputStream);
      } catch (JAXBException e) {
         throw new MarshallingException("Unable to marshal Validation", e);
      }
   }

   @Override
   public Validation unmarshal(InputStream inputStream) throws MarshallingException {
      throw new UnsupportedOperationException("Not implemented");
   }
}
