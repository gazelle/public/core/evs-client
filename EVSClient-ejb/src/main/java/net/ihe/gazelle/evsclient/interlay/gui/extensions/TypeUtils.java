package net.ihe.gazelle.evsclient.interlay.gui.extensions;

import gnu.trove.map.hash.THashMap;

import java.lang.reflect.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class TypeUtils {
    private static Class<?> getClass(final Type type) {
        if (type instanceof Class) {
            return (Class<?>) type;
        } else if (type instanceof ParameterizedType) {
            return getClass(((ParameterizedType) type).getRawType());
        } else if (type instanceof GenericArrayType) {
            final Type componentType = ((GenericArrayType) type).getGenericComponentType();
            final Class<?> componentClass = getClass(componentType);
            if (componentClass != null) {
                return Array.newInstance(componentClass, 0).getClass();
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * Gets the first type argument from the childClass.
     *
     * @param <T>        the generic type of the baseClass
     * @param baseClass  the base class
     * @param childClass the child class
     * @return the first type argument
     */
    public static <T> Class<?> getFirstTypeArgument(final Class<T> baseClass,
                                                    final Class<? extends T> childClass) {
        return getTypeArgument(baseClass, childClass, 0);
    }

    /**
     * Gets the type argument from the childClass at the given index or null if it does not exists.
     *
     * @param <T>        the generic type of the baseClass
     * @param baseClass  the base class
     * @param childClass the child class
     * @param index      the index of the type argument
     * @return the type argument from the childClass at the given index or null if it does not
     * exists.
     */
    public static <T> Class<?> getTypeArgument(final Class<T> baseClass,
                                               final Class<? extends T> childClass, final int index) {
        final List<Class<?>> typeArguments = getTypeArguments(baseClass, childClass);
        if (typeArguments != null && !typeArguments.isEmpty() && index < typeArguments.size()) {
            return typeArguments.get(index);
        }
        return null;
    }

    /**
     * Get the actual type arguments a child class has used to extend a generic base class.
     *
     * @param <T>        the generic type of the baseClass
     * @param baseClass  the base class
     * @param childClass the child class
     * @return a list of the raw classes for the actual type arguments.
     */
    public static <T> List<Class<?>> getTypeArguments(final Class<T> baseClass,
                                                      final Class<? extends T> childClass) {

        final Map<Type, Type> resolvedTypes = new THashMap<>();
        Type type = childClass;
        // start walking up the inheritance hierarchy until we hit baseClass
        while (!getClass(type).equals(baseClass)) {
            if (type instanceof Class) {
                // there is no useful information for us in raw types, so just
                // keep going.
                type = ((Class<?>) type).getGenericSuperclass();
            } else {
                final ParameterizedType parameterizedType = (ParameterizedType) type;
                final Class<?> rawType = (Class<?>) parameterizedType.getRawType();

                resolvedTypes.putAll(getTypeArgumentsAndParameters(type));

                if (!rawType.equals(baseClass)) {
                    type = rawType.getGenericSuperclass();
                }
            }
        }

        // finally, for each actual type argument provided to baseClass,
        // determine (if possible)
        // the raw class for that type argument.
        Type[] actualTypeArguments;
        if (type instanceof Class) {
            actualTypeArguments = ((Class<?>) type).getTypeParameters();
        } else {
            actualTypeArguments = ((ParameterizedType) type).getActualTypeArguments();
        }
        final List<Class<?>> typeArgumentsAsClasses = new ArrayList<>();
        // resolve types by chasing down type variables.
        for (Type baseType : actualTypeArguments) {
            while (resolvedTypes.containsKey(baseType)) {
                baseType = resolvedTypes.get(baseType);
            }
            typeArgumentsAsClasses.add(getClass(baseType));
        }
        return typeArgumentsAsClasses;
    }

    /**
     * Gets the type arguments and parameters.
     *
     * @param type the type
     * @return the type arguments and parameters
     */
    private static Map<Type, Type> getTypeArgumentsAndParameters(final Type type) {
        final ParameterizedType parameterizedType = (ParameterizedType) type;
        final Class<?> rawType = (Class<?>) parameterizedType.getRawType();
        final Map<Type, Type> resolvedTypes = new THashMap<>();
        final Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
        final TypeVariable<?>[] typeParameters = rawType.getTypeParameters();
        for (int i = 0; i < actualTypeArguments.length; i++) {
            resolvedTypes.put(typeParameters[i], actualTypeArguments[i]);
        }
        return resolvedTypes;
    }
}
