package net.ihe.gazelle.evsclient.interlay.dto.rest;


import sun.reflect.generics.reflectiveObjects.ParameterizedTypeImpl;

import java.lang.reflect.Type;

public abstract class AbstractDTO<T> implements DataTransferObject<T> {

    protected T domain;

    protected AbstractDTO(T domain) {
        this.domain = domain;
    }

    protected AbstractDTO() {
        super();
        domain = createDomainIntance();
    }

    @Override
    public T toDomain() {
        return domain;
    }

    protected T createDomainIntance() {
        try {
            return getDomainClass().newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    private Class<T> getDomainClass() {
        return (Class<T>) getParameterizedType((Class<? extends AbstractDTO<?>>) this.getClass()).getActualTypeArguments()[0];
    }

    private ParameterizedTypeImpl getParameterizedType(Class<? extends AbstractDTO<?>> cls) {
        Type c = cls.getGenericSuperclass();
        if (c instanceof ParameterizedTypeImpl) {
            return (ParameterizedTypeImpl) c;
        } else {
            if (Object.class==c) {
                throw new RuntimeException();
            }
            return getParameterizedType((Class<? extends AbstractDTO<?>>)c);
        }
    }

}
