package net.ihe.gazelle.evsclient.interlay.ws.rest.validations;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import net.ihe.gazelle.evsclient.interlay.dto.rest.validation.ValidationDTO;
import net.ihe.gazelle.evsclient.interlay.dto.rest.validation.ValidationServiceProfile;
import net.ihe.gazelle.evsclient.interlay.factory.ApiFactory;
import net.ihe.gazelle.evsclient.interlay.ws.rest.NotFoundException;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;

import javax.ejb.Stateless;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

@Stateless
@Name("validationsApi")
@Path("/validations")
@OpenAPIDefinition(info = @Info(title = "Validations API", version = "1.0", description = "TODO")) // TODO
public class ValidationsApi {

    @In(value = "evsApiFactory", create = true)
    private ApiFactory apiFactory;

    @POST

    @Consumes({"application/xml", "application/json"})

    @Operation(summary = "Crée une Validation", description = "*[EVS-41]* Pour créer une ressource **Validation** et exécuter une validation, la demande HTTP POST doit être envoyée sur {base}/validations avec la ressource dans le corps du message (body).  *[EVS-46]* Si `validationService` et `validator` sont présents, ou si `objectType` est présent et qu'un service de validation est configuré dans EVS pour couvrir cet ObjectType, alors EVS doit créer la ressource et déclencher la validation.  *[EVS-51]* Si l’en-tête authorization est présent avec une clé API valide, la ressource **Validation** et le rapport de validation doivent être rendus privés à l’utilisateur propriétaire de la clé API. ", tags = {"Validation",},
            responses = {
                    @ApiResponse(responseCode = "201", description = "**HTTP 201 Created** *[EVS-54]* Une fois la ressource créée et la validation terminée, EVSClient doit répondre avec HTTP 201 Created et l’en-tête `Content-Location` doit contenir l’URL d’accès de la nouvelle **Validation** créée, avec son OID. "),

                    @ApiResponse(responseCode = "202", description = "**HTTP 202 Accepted** *[EVS-57]* Si la resource a été créée de manière asynchrone, la réponse d'EVSClient doit être HTTP 202 Accepted et l'entête HTTP `Location` doit contenir l'URL de lecture de la **Validation**. *Voir la section GET /validations/{oid} pour plus d'information sur les lectures de ressources dont la validation n'est pas encore terminée.* "),

                    @ApiResponse(responseCode = "400", description =
                            "**HTTP 400 Bad Request**\n" +
                            " *[EVS-47]* Si l’objet binaire est manquant dans la demande de création, EVSClient doit répondre avec HTTP 400 Bad Request.\n" +
                            " *[EVS-48]* Si le couple validationService/validator et l'objectType sont manquant dans la demande de création, EVSClient doit répondre avec HTTP 400 Bad Request\n" +
                            " *[EVS-52]* Si l’en-tête `Authorization` est présent mais le contenu est vide ou ne respecte pas le format `GazelleAPIKey <key>`, EVSClient doit répondre par HTTP 400 Bad Request."),
                    @ApiResponse(responseCode = "401", description = "**HTTP 401 Unauthorized** *[EVS-53]* Si les utilisateurs non-connectés  ne sont pas autorisés à lancer des validations, et si l’en-tête Authorization est manquante dans la demande ou si la clé API donnée est invalide, EVSClient doit répondre avec HTTP 401 Unauthorized. ")})
    public Response createValidation(
            @Parameter(description = "Objet à valider ainsi que les paramètres de validation  *[EVS-42]* Il doit être possible de faire une création de **Validation** en XML, alors le `ContentType` header doit avoir la valeure `application/xml`.  *[EVS-43]* Il doit être possible de faire une création de **Validation** en JSON, alors le `ContentType` header doit avoir la valeure `application/json`.  *[EVS-45]* Dans toutes les demandes de création de ressource **Validation**, le contenu de l’objet binaire doit être présent et encodé en base 64.  *[EVS-46]* Si `validationService` et `validator` sont présents, ou si `objectType` est présent et qu'un service de validation est configuré dans EVS pour couvrir cet ObjectType, alors EVS doit créer la ressource et déclancher la validation. ", required = true)
                  ValidationDTO validation,
            @Parameter(description = "*[EVS-50]* Pour authentifier l’utilisateur effectuant la demande de création de ressource **Validation**, le champ d’en-tête HTTP Authorization prend la valeur de `GazelleAPIKey <key>`. ")
            @HeaderParam("Authorization") String authorization,
            @Parameter(description = "*[EVS-55]* Les réponses asynchrones peuvent également être demandées par le client en spécifiant `respond-async` dans le champ Prefer de l’entête. Voir [RFC7240](https://tools.ietf.org/html/rfc7240).  *[EVS-56]* Si la ressource est créée de manière asynchrone, EVSClient doit créer la ressource, déclancher la validation de l'objet et répondre immédiatement sans attendre la fin de la validation selon *[EVS-57]*. ",
                    content = @Content(schema = @Schema(pattern="respond-async")))
            @HeaderParam("Prefer") String prefer,
            @Context SecurityContext securityContext,
            @Context HttpServletRequest request)
            throws net.ihe.gazelle.evsclient.interlay.ws.rest.NotFoundException {
        return apiFactory.getValidationsApiService().createValidation(validation, authorization, prefer, securityContext, request);
    }

    @GET
    @Path("/{oid}")

    @Produces({"application/xml", "application/json"})
    @Operation(summary = "Récupère une Validation",
            description = "*[EVS-58]* Pour lire une ressource **Validation**, la demande HTTP GET doit être envoyée sur {base}/validations/{oid}.  *[EVS-64]* Si la **Validation** demandé est publique, EVSClient peut répondre à toute requête de lecture.  *[EVS-66]* Si l'entête Authorization est présente, contient une clé API valide et que la clé est associée à l'organisation qui détient la **Validation** privée, alors EVSClient peut répondre à la demande de lecture de la ressource. ",
            tags = {"Validation",},
            responses = {
                    @ApiResponse(responseCode = "200", description = "**HTTP 200 OK** *[EVS-61]* Si la ressource **Validation** a terminé sa validation (validationStatus est DONE_FAILED, DONE_PASSED ou DONE_UNDEFINED), EVSClient doit répondre avec HTTP 200 OK et avec la ressource dans le corps de la réponse. "),

                    @ApiResponse(responseCode = "202", description = "**HTTP 202 Accepted** *[EVS-62]* Si la ressource **Validation** n'a pas terminé sa validation (validationStatus est PENDING ou IN_PROGRESS), EVSClient doit répondre avec le code HTTP 202 Accepted, le champs d'entête `Location` est renseigné avec l'URL de la **Validation**, le champs `X-Progress` de l'entête doit être renseigné avec le `validationStatus` de la **Validation** et sans contenu dans le corps de la réponse. "),

                    @ApiResponse(responseCode = "400", description = "**HTTP 400 Bad Request** *[EVS-65]* Si l’en-tête `Authorization` est présent mais le contenu ne respecte pas le format `GazelleAPIKey <key>`, EVSClient doit répondre par HTTP 400 Bad Request. "),

                    @ApiResponse(responseCode = "401", description = "**HTTP 401 Unauthorized** *[EVS-68]* Si la **Validation** demandée est privée, mais que l'en-tête Authorization est manquante, alors EVSClient doit répondre avec HTTP 401 Unauthorized. "),

                    @ApiResponse(responseCode = "403", description = "**HTTP 403 Forbidden** *[EVS-67]* Si l'en-tête Authorization est présente avec une clé API valide et si la **Validation** privée n'appartient pas à l'organisation associé avec la clé API, alors EVSClient doit répondre avec HTTP 403 Forbidden. "),

                    @ApiResponse(responseCode = "404", description = "**HTTP 404 Not Found** Si l'OID demandé ne correspond à aucune **Validation** connue. ")})
    public Response getValidationByOid(
            @PathParam("oid") String oid,
            @QueryParam("privacyKey") String privacyKey,
            @Parameter(description = "[EVS-63] `GazelleAPIKey <key>` doit être supporté comme valeur du champ Authorization dans l'entête. ")
            @HeaderParam("Authorization") String authorization,
            @Parameter(description = "*[EVS-59]* Pour lire une **Validation** au format XML, le champ de l’en-tête HTTP `Accept` doit avoir la valeur `application/xml`.  *[EVS-60]* Pour lire une **Validation** au format JSON, le champ de l’en-tête HTTP `Accept` doit avoir la valeur `application/json`. ",
                    content = @Content(schema = @Schema(pattern="(application/(xml|json))", defaultValue = "application/xml")))
            @HeaderParam("Accept") String accept,
            @Context SecurityContext securityContext,
            @Context HttpServletRequest request)
            throws net.ihe.gazelle.evsclient.interlay.ws.rest.NotFoundException {
        return apiFactory.getValidationsApiService().getValidationByOid(oid, privacyKey, authorization, accept, securityContext, request);
    }

    @GET
    @Path("/{oid}/report")

    @Produces({"application/xml", "application/gzl.validation.report+xml", "application/json", "application/gzl.validation.report+json", "application/junit+xml", "application/svrl+xml"})
    @Operation(summary = "Récupère un rapport détaillé de validation", description = "*Le rapport de validation est une sous-ressource de la **Validation**. Le format par défaut doit être **Gazelle Validation Report**, mais il pourrait être demandé dans d’autres formats tels que JUnit ou SVRL.*  *[EVS-69]* Pour lire un rapport de validation d’une ressource **Validation**, la demande HTTP GET doit être envoyée sur {base}/validations/{oid}/report. ", tags = {"Validation",},
            responses = {
                    @ApiResponse(responseCode = "200", description = "**HTTP 200 OK** [EVS-74] Si la validation est terminée et que le rapport de validation est présent, EVSClient doit répondre avec HTTP 200 OK et le rapport de validation complet en pièce jointe. "),

                    @ApiResponse(responseCode = "400", description = "**HTTP 400 Bad Request** Si l’en-tête `Authorization` est présent mais le contenu ne respecte pas le format `GazelleAPIKey <key>`, EVSClient doit répondre par HTTP 400 Bad Request. "),

                    @ApiResponse(responseCode = "401", description = "**HTTP 401 Unauthorized** Si la **Validation** qui contient le rapport demandé est privée, mais que l'entête Authorization est manquante, alors EVSClient doit répondre avec HTTP 401 Unauthorized. "),

                    @ApiResponse(responseCode = "403", description = "**HTTP 403 Forbidden** Si l'entête Authorization est présente avec une clé API valide et si la **Validation** qui contient le rapport demandé est privée et n'appartient pas à l'organisation associée avec la clé API, alors EVSClient doit répondre avec HTTP 403 Forbidden. "),

                    @ApiResponse(responseCode = "404", description = "**HTTP 404 NotFound\"** Si l'OID demandé ne correspond à aucune **Validation** connue.  [EVS-75] Si le `validationStatus` de la **Validation** est toujours `PENDING` ou `IN_PROGRESS` ou si le rapport de validation n’est pas présent, EVSClient doit répondre par HTTP 404 Not Found. ")})
    public Response getValidationReportByOid(
            @PathParam("oid")
                    String oid,
            @QueryParam("privacyKey")
                    String privacyKey,
            @QueryParam("severityThreshold")
                    String severityThreshold,
            @Parameter(description = "Pour récuperer un rapport de validation associé à une **Validation** privée. Une clé API doit être donnée. Le champs prend la valeur `GazelleAPIKey <key>`.")
            @HeaderParam("Authorization") String authorization,
            @Parameter(description = "*[EVS-70]* Si l’entête `Accept` est `application/xml` ou `application/gzl.validation.report+xml`, EVSClient doit retourner le Gazelle Validation Report au format XML.  *[EVS-71]* Si l’entête `Accept` est `application/json` ou `application/gzl.validation.report+json`, EVSClient pourrait retourner le Gazelle Validation Report au format JSON.  *[EVS-72]* Si l’entête `Accept` est `application/junit+xml`, EVSClient pourrait retourner le rapport JUnit au format XML.  *[EVS-73]* Si l’entête `Accept` est `application/svrl+xml`, EVSClient pourrait retourner le rapport Schematron SVRL au format XML. ",
                    content = @Content(schema = @Schema(pattern="application/(xml|json|gzl.validation.report+xml|gzl.validation.report+json|junit+xml|svrl+xml)", defaultValue = "application/xml")))
            @HeaderParam("Accept")
                    String accept,
            @Context SecurityContext
                    securityContext,
            @Context HttpServletRequest
                    request)
            throws NotFoundException {
        return apiFactory.getValidationsApiService().getValidationReportByOid(oid, privacyKey, severityThreshold, authorization, accept, securityContext, request);
    }

    @GET
    @Path("/profiles")
    @Produces("application/json")
    @Operation(summary = "Retrieve validation profiles", description = "[EVSCLT-1217] Cette API récupère tous les profils de validation disponibles pour un service de validation configuré dans une instance EVSClient ou pour tous les services de validation si aucun nom de service n'est fourni", tags = {"Validation",},
            responses = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "**HTTP 200 OK** Si la requête est réussie, EVSClient doit répondre avec HTTP 200 OK et la liste des profils de validation dans le corps de la réponse.",
                            content = @Content(schema = @Schema(type = "array", implementation = ValidationServiceProfile.class))
                    ),
                    @ApiResponse(responseCode = "500", description = "**HTTP 500 Internal Server Error**  Si une erreur inattendue se produit lors de la récupération des profils de validation, EVSClient doit répondre avec HTTP 500 Internal Server Error. ")})

    public Response getValidationProfiles(@QueryParam("serviceName") String serviceName) {
        if (serviceName != null) {
            return apiFactory.getValidationsApiService().getValidationProfilesByServiceName(serviceName);
        }
        return apiFactory.getValidationsApiService().getValidationProfiles();
    }

}
