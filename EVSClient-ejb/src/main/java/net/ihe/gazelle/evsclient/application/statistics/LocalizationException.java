package net.ihe.gazelle.evsclient.application.statistics;

public class LocalizationException extends Exception {
    public LocalizationException(String s) {
        super(s);
    }

    public LocalizationException(String message, Exception e) {
        super(message, e);
    }
}
