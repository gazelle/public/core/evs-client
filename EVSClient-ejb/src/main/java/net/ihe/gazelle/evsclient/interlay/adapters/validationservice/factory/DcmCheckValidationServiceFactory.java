package net.ihe.gazelle.evsclient.interlay.adapters.validationservice.factory;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.application.validationservice.AbstractValidationServiceFactory;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.SystemValidationServiceConf;
import net.ihe.gazelle.evsclient.interlay.adapters.validationservice.DcmCheckValidationService;

public class DcmCheckValidationServiceFactory extends AbstractValidationServiceFactory<DcmCheckValidationService,SystemValidationServiceConf> {

    public DcmCheckValidationServiceFactory(ApplicationPreferenceManager applicationPreferenceManager, SystemValidationServiceConf configuration) {
        super(applicationPreferenceManager, configuration, DcmCheckValidationService.class);
    }
}