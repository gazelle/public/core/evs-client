/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evsclient.interlay.gui.validation;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.Segment;
import ca.uhn.hl7v2.parser.PipeParser;
import ca.uhn.hl7v2.util.Terser;
import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.evsclient.application.CallerMetadataFactory;
import net.ihe.gazelle.evsclient.application.interfaces.ForbiddenAccessException;
import net.ihe.gazelle.evsclient.application.interfaces.GuiPermanentLink;
import net.ihe.gazelle.evsclient.application.interfaces.NotFoundException;
import net.ihe.gazelle.evsclient.application.interfaces.UnauthorizedException;
import net.ihe.gazelle.evsclient.application.notification.EmailNotificationManager;
import net.ihe.gazelle.evsclient.application.validation.ValidationServiceFacade;
import net.ihe.gazelle.evsclient.domain.validation.Validation;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.ReferencedStandard;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.WebValidationServiceConf;
import net.ihe.gazelle.evsclient.interlay.factory.DocumentBuilderFactoryException;
import net.ihe.gazelle.evsclient.interlay.factory.XmlDocumentBuilderFactory;
import net.ihe.gazelle.evsclient.interlay.gui.QueryParam;
import net.ihe.gazelle.evsclient.interlay.gui.QueryParamEvs;
import net.ihe.gazelle.evsclient.interlay.gui.document.ContentConverter;
import net.ihe.gazelle.hl7.validator.client.ValidationContextBuilder;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.criterion.QueryModifier;
import net.ihe.gazelle.hql.restrictions.HQLRestrictionLikeMatchMode;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import net.ihe.gazelle.tf.model.Hl7MessageProfile;
import net.ihe.gazelle.tf.model.Hl7MessageProfileQuery;
import org.apache.commons.lang.StringUtils;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage.Severity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.faces.context.FacesContext;
import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class HL7ValidationBeanGui extends AbstractSignableValidationBeanGui implements QueryModifier<Hl7MessageProfile> {
    private static final Logger LOGGER = LoggerFactory.getLogger(HL7ValidationBeanGui.class);
    private static final long serialVersionUID = -3826607970222174718L;

    private static final String GAZELLE = "HL7v2_GAZELLE";
    private static final int ALL = 1;
    private static final int GUESS = 2;

    private WebValidationServiceConf validationService;
    private String hl7VersionFromMessage;
    private String messageTypeFromMessage;
    private Hl7MessageProfile selectedMessageProfile;
    private Filter<Hl7MessageProfile> filter;
    private int filterMode = HL7ValidationBeanGui.ALL;
    private String affinityDomain;

    private String messageType;
    private String hl7Version;
    private Integer profileId;
    private Integer referencedStandardId;
    private ReferencedStandard standard;

    public HL7ValidationBeanGui(ValidationServiceFacade validationServiceFacade,
                                CallerMetadataFactory callerMetadataFactory,
                                GazelleIdentity identity,
                                ApplicationPreferenceManager applicationPreferenceManager,
                                GuiPermanentLink<Validation> permanentLinkGui,
                                EmailNotificationManager emailNotificationManager) {
        super(HL7ValidationBeanGui.class, validationServiceFacade, callerMetadataFactory, identity, applicationPreferenceManager, permanentLinkGui, emailNotificationManager);
    }

    public String getHl7VersionFromMessage() {
        return hl7VersionFromMessage;
    }

    public String getMessageTypeFromMessage() {
        return messageTypeFromMessage;
    }

    private void extractInfoFromMessage(final String message) {
        this.hl7VersionFromMessage = null;
        this.messageTypeFromMessage = null;
        if (!HL7v2FormatAdapter.isXmlWellFormed(message)){
            extractInfoFromER7Message(message);
        } else {
            extractInfoFromXMLMessage(message);
        }
    }

    private void extractInfoFromER7Message(String message){
        if (message.startsWith("MSH")) {
            try {
                final PipeParser parser = new PipeParser();
                final Message hapiMessage = parser.parse(message);
                final Terser terser = new Terser(hapiMessage);
                final Segment msh = terser.getSegment("/.MSH");
                this.messageTypeFromMessage = msh.getField(9, 0).encode();
                this.hl7VersionFromMessage = terser.get("/.MSH-12-1");
                HL7ValidationBeanGui.LOGGER.info("version:{}", this.hl7VersionFromMessage);
                HL7ValidationBeanGui.LOGGER.info("message type:{}", this.messageTypeFromMessage);
            } catch (final HL7Exception e) {
                HL7ValidationBeanGui.LOGGER.error(e.getMessage());
            }
        }
    }

    private void extractInfoFromXMLMessage(String message){
        try{
            Document document;
            final DocumentBuilder builder = new XmlDocumentBuilderFactory()
                    .setNamespaceAware(true)
                    .setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true)
                    .getBuilder();
            document = builder.parse(
                    new InputSource(
                        new ContentConverter().toInputStream(message)));

            Element rootElement = document.getDocumentElement();
            NodeList messageTypeElements = rootElement.getElementsByTagName("MSH.9");
            if (messageTypeElements != null && messageTypeElements.getLength()==1){
                StringBuilder mType = new StringBuilder();
                Node msh9 = messageTypeElements.item(0);
                for (int i = 0 ; i < msh9.getChildNodes().getLength() ; i++){
                    if (msh9.getChildNodes().item(i).getNodeType() == 1){
                        mType.append(msh9.getChildNodes().item(i).getTextContent());
                        if (i != msh9.getChildNodes().getLength() -1){
                            mType.append("^");
                        }
                    }
                }
                String messageTypeString = mType.toString();
                messageTypeFromMessage = messageTypeString.endsWith("^")?messageTypeString.substring(0, messageTypeString.length()-1):messageTypeString;
            }

            NodeList versionElements = rootElement.getElementsByTagName("VID.1");
            if (versionElements != null && versionElements.getLength()==1){
                hl7VersionFromMessage = versionElements.item(0).getTextContent();
            }
        } catch (SAXException| DocumentBuilderFactoryException |IOException e){
            LOGGER.error("Error extracting info from XML document to filter message profiles", e);
        }
    }

    public Hl7MessageProfile getSelectedMessageProfile() {
        return this.selectedMessageProfile;
    }

    public String profileLink(final Hl7MessageProfile profile) {
        if (profile != null) {
            final String gazelleHL7v2ValidatorUrl = PreferenceService.getString("gazelle_hl7v2_validator_url");
            return gazelleHL7v2ValidatorUrl.concat("/viewProfile.seam?")
                    .concat(QueryParam.PROCESSING_OID).concat("=").concat(profile.getProfileOid());
        } else {
            return null;
        }
    }

    @Override
    public void initFromUrl() throws UnauthorizedException, NotFoundException, ForbiddenAccessException {
        super.initFromUrl();
        final Map<String, String> urlParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        if (urlParams != null && !urlParams.isEmpty()) {
            if (urlParams.containsKey(QueryParamEvs.EXTENSION)) {
                this.affinityDomain = urlParams.get(QueryParamEvs.EXTENSION);
                this.resetFilter();
            }
            if(urlParams.containsKey(QueryParamEvs.REF_STANDARD_ID)) {
                this.referencedStandardId = Integer.valueOf(urlParams.get(QueryParamEvs.REF_STANDARD_ID));
                this.standard = processingManager.getReferencedStandardById(referencedStandardId);
                if (StringUtils.isEmpty(this.affinityDomain)) {
                    affinityDomain = standard.getValidatorFilter();
                    this.resetFilter();
                }
            }
        }
    }

    @Override
    public void init() throws UnauthorizedException, NotFoundException, ForbiddenAccessException {
        super.init();
        if (this.selectedObject != null && this.selectedObject.isLucky()) {
            this.guessProfile(true);
            if (this.getMessageProfiles().size() == 1) {
                this.selectedMessageProfile = this.getMessageProfiles().getAllItems(FacesContext.getCurrentInstance())
                        .get(0);
                this.validateMessage(selectedMessageProfile);
            }
        }
    }

    private void resetFilter() {
        this.filter = null;
    }

    @Override
    public void reset() {
        super.reset();
        this.selectedObject = null;
        this.selectedMessageProfile = null;
        this.selectedValidatorName = null;
        this.filterMode = HL7ValidationBeanGui.ALL;
        if (this.filter != null) {
            this.filter.clear();
        }
    }

    public void guessProfile(final boolean guess) {
        if (this.filter != null) {
            this.filter.clear();
        }
        this.selectedMessageProfile = null;
        this.selectedValidatorName = null;
        if (guess && this.document !=null && this.document.getContent() != null) {
            this.extractInfoFromMessage(this.document.toString());
            this.filterMode = HL7ValidationBeanGui.GUESS;
        } else {
            this.filterMode = HL7ValidationBeanGui.ALL;
            this.hl7VersionFromMessage = null;
            this.messageTypeFromMessage = null;
        }
    }

    public void validateMessage(final Hl7MessageProfile messageProfile) {
        this.selectedMessageProfile = messageProfile;
        if (this.selectedMessageProfile != null){
            HL7ValidationBeanGui.LOGGER.info("selected profile: {}", this.selectedMessageProfile.getProfileOid());
            if (this.document !=null && this.document.toString() != null && !this.document.toString().isEmpty()) {
                selectedValidatorName = selectedMessageProfile.getProfileOid();
                validate();
            }
        }
        else {
            FacesMessages.instance().addFromResourceBundle(Severity.ERROR, "net.ihe.gazelle.evs.NoHL7MessageNoProfileErrorMessage");
            HL7ValidationBeanGui.LOGGER.debug("No profile has been selected");
        }
    }

    public FilterDataModel<Hl7MessageProfile> getMessageProfiles() {
        this.messageType = null;
        this.hl7Version = null;
        this.profileId = null;

        if (this.selectedMessageProfile != null) {
            this.profileId = this.selectedMessageProfile.getId();
        } else if (this.filterMode == HL7ValidationBeanGui.GUESS) {
            this.messageType = this.messageTypeFromMessage;
            this.hl7Version = this.hl7VersionFromMessage;
            FilterDataModel<Hl7MessageProfile> profiles = new FilterDataModel<Hl7MessageProfile>(
                    HL7ValidationBeanGui.this.getFilter()) {
                @Override
                protected Object getId(final Hl7MessageProfile t) {
                    // TODO Auto-generated method stub
                    return t.getId();
                }
            };
            if (profiles.getRowCount() > 0) {
                return profiles;
            } else {
                this.filterMode = HL7ValidationBeanGui.ALL;
            }
        }
        return new FilterDataModel<Hl7MessageProfile>(HL7ValidationBeanGui.this.getFilter()) {
            @Override
            protected Object getId(final Hl7MessageProfile t) {
                // TODO Auto-generated method stub
                return t.getId();
            }
        };
    }

    public void setFilter(final Filter<Hl7MessageProfile> filter) {
        this.filter = filter;
    }

    public Filter<Hl7MessageProfile> getFilter() {
        if (this.filter == null) {
            this.filter = new Filter<>(this.getCriterions());
        }
        return this.filter;
    }

    private HQLCriterionsForFilter<Hl7MessageProfile> getCriterions() {
        final Hl7MessageProfileQuery query = new Hl7MessageProfileQuery();
        final HQLCriterionsForFilter<Hl7MessageProfile> result = query.getHQLCriterionsForFilter();

        result.addPath("transaction", query.transaction());
        result.addPath("domain", query.domain());
        result.addPath("hl7Version", query.hl7Version());
        result.addPath("messageType", query.messageType());
        result.addPath("actor", query.actor());
        result.addPath("affinityDomain", query.affinityDomains().labelToDisplay(), null, this.affinityDomain);
        result.addQueryModifier(this);
        return result;
    }

    public void setFilterMode(final int filterMode) {
        this.filterMode = filterMode;
    }

    public int getFilterMode() {
        return this.filterMode;
    }

    public int getALL() {
        return HL7ValidationBeanGui.ALL;
    }

    public int getGUESS() {
        return HL7ValidationBeanGui.GUESS;
    }

    @Override
    public void modifyQuery(final HQLQueryBuilder<Hl7MessageProfile> queryBuilder, final Map<String, Object> filterValuesApplied) {
        final Hl7MessageProfileQuery hl7MessageProfileQuery = new Hl7MessageProfileQuery(queryBuilder);
        if (this.messageType != null && !this.messageType.isEmpty()) {
            hl7MessageProfileQuery.messageType().like(this.messageType, HQLRestrictionLikeMatchMode.ANYWHERE);
        }
        if (this.hl7Version != null && !this.hl7Version.isEmpty()) {
            hl7MessageProfileQuery.hl7Version().eq(this.hl7Version);
        }
        if (this.profileId != null) {
            hl7MessageProfileQuery.id().eq(this.profileId);
        }
    }

    private Map<String, ValidationContextBuilder.FaultLevel> retrieveFaultLevels() {
        final Map<String, ValidationContextBuilder.FaultLevel> validationOptions = new HashMap<>();
        validationOptions.put(ValidationContextBuilder.LENGTH,
                ValidationContextBuilder.FaultLevel.getFaultLevelByLabel(PreferenceService.getString("hl7v2_length_fault_level")));
        validationOptions.put(ValidationContextBuilder.MESSAGE_STRUCTURE,
                ValidationContextBuilder.FaultLevel.getFaultLevelByLabel(PreferenceService.getString("hl7v2_structure_fault_level")));
        validationOptions.put(ValidationContextBuilder.DATATYPE,
                ValidationContextBuilder.FaultLevel.getFaultLevelByLabel(PreferenceService.getString("hl7v2_datatype_fault_level")));
        validationOptions.put(ValidationContextBuilder.DATAVALUE,
                ValidationContextBuilder.FaultLevel.getFaultLevelByLabel(PreferenceService.getString("hl7v2_value_fault_level")));
        return validationOptions;
    }

    @Override
    protected String getValidatorUrl() {
        return null;
    }

    @Override
    public String performAnotherValidation() {
        return null;
    }

    @Override
    public String revalidate() {
        return null;
    }

    @Override
    public String getStandardDescription() {
        return standard.getDescription();
    }


    public void downloadWebServiceBody(final Hl7MessageProfile messageProfile){
        this.selectedValidatorName = messageProfile.getProfileOid();
        super.downloadWebServiceBody();
    }

}
