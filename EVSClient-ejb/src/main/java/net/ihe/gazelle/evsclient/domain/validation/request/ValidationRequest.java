package net.ihe.gazelle.evsclient.domain.validation.request;

import java.util.ArrayList;
import java.util.List;

public class ValidationRequest {


    public static String API_VERSION = "0.1";

    private String apiVersion = API_VERSION;

    private String validationServiceName; // given In URL

    private String validationProfileId;

    private List<ValidationItem> validationItems;

    public ValidationRequest() {
        this.validationItems = new ArrayList<>();
    }

    public String getApiVersion() {
        return apiVersion;
    }

    public ValidationRequest setApiVersion(String apiVersion) {
        this.apiVersion = apiVersion;
        return this;
    }

    public String getValidationServiceName() {
        return validationServiceName;
    }

    public ValidationRequest setValidationServiceName(String validationServiceName) {
        this.validationServiceName = validationServiceName;
        return this;
    }

    public String getValidationProfileId() {
        return validationProfileId;
    }

    public ValidationRequest setValidationProfileId(String validationProfileId) {
        this.validationProfileId = validationProfileId;
        return this;
    }

    public List<ValidationItem> getValidationItems() {
        return validationItems;
    }

    public ValidationRequest setValidationItems(List<ValidationItem> validationItems) {
        this.validationItems = validationItems;
        return this;
    }
    public ValidationRequest addValidationItem(ValidationItem validationItem){
        this.validationItems.add(validationItem);
        return this;
    }

    public boolean isValidationProfileIdValid(){
        return validationProfileId != null && !validationProfileId.isEmpty();
    }

    public boolean isValidationServiceNameValid(){
        return validationServiceName != null && !validationServiceName.isEmpty();
    }

    public boolean isValidationItemsValid(){
         if(validationItems.size()>0){
             if(validationItems.size() > 1){
                 for (ValidationItem item : validationItems) {
                     if(!item.isRoleDefined()){
                         return false;
                     }
                 }
             }
             return true;
         }
         return false;
    }

    public boolean isValid(){
        return isValidationItemsValid() && isValidationProfileIdValid() && isValidationServiceNameValid();
    }

}
