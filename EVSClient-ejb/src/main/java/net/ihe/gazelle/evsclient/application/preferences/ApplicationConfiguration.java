/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evsclient.application.preferences;

import net.ihe.gazelle.common.Preferences.PreferencesKey;
import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.common.model.ApplicationPreference;
import net.ihe.gazelle.common.model.ApplicationPreferenceQuery;
import net.ihe.gazelle.common.pages.menu.Menu;
import net.ihe.gazelle.common.servletfilter.CSPHeaderFilter;
import net.ihe.gazelle.common.servletfilter.SQLinjectionFilter;
import net.ihe.gazelle.evsclient.interlay.gui.GuiMessage;
import net.ihe.gazelle.evsclient.interlay.gui.menu.EVSMenu;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.sf.ehcache.CacheException;
import net.sf.ehcache.CacheManager;
import org.jboss.seam.Component;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.jboss.seam.security.Identity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


public class ApplicationConfiguration implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -7870510996996859377L;

    private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationConfiguration.class);

    private ApplicationPreference selectedPreference;

    private volatile List<ApplicationPreference> applicationPreferences;
    private Filter filter;

    private ApplicationPreferenceManager applicationPreferenceManager;

    public ApplicationConfiguration(ApplicationPreferenceManager applicationPreferenceManager) {
        this.applicationPreferenceManager = applicationPreferenceManager;
        listApplicationPreferences();
    }

    public void listApplicationPreferences() {
        final ApplicationPreferenceQuery query = new ApplicationPreferenceQuery();
        query.preferenceName().order(true);
        applicationPreferences = query.getList();
    }

    public void setSelectedPreference(final ApplicationPreference selectedPreference) {
        this.selectedPreference = selectedPreference;
    }

    public FilterDataModel<ApplicationPreference> getApplicationPreferences() {
        return new FilterDataModel<ApplicationPreference>(ApplicationConfiguration.this.getFilter()) {
            @Override
            protected Object getId(final ApplicationPreference t) {
                return t.getId();
            }
        };
    }

    private Filter<ApplicationPreference> getFilter() {

        if (this.filter == null) {
            final ApplicationPreferenceQuery query = new ApplicationPreferenceQuery();
            final HQLCriterionsForFilter<ApplicationPreference> result = query.getHQLCriterionsForFilter();
            this.filter = new Filter<>(result);
        }
        return this.filter;

    }

    public String createNewApplicationPreference() {

        return "/administration/displayApplicationPreference.seam";
    }

    public String displayApplicationPreference(final ApplicationPreference preference) {
        return "/administration/displayApplicationPreference.seam?id=" + preference.getId();
    }

    public String backToPreferenceList() {
        return "/administration/applicationConfiguration.seam";
    }



    public boolean isCheckCDAPIDs() {
        return applicationPreferenceManager.getBooleanValue("check_CDA_PIDs");
    }

    public String getLocalizationCDAPIDs() {
        return applicationPreferenceManager.getStringValue("check_CDA_PIDs_script_localization");
    }


    public boolean isCas() {
        return applicationPreferenceManager.getBooleanValue("cas_enabled");
    }

    public static boolean isTemplateScanned() {
        final ApplicationPreference res = ApplicationPreference.get_preference_from_db("templates_scanned");
        return "true".equals(res.getPreferenceValue());
    }

    public String loginByIP() {
        final Identity identity = (Identity) Component.getInstance("org.jboss.seam.security.identity");
        if ("loggedIn".equals(identity.login())) {
            return "/home.xhtml";
        } else {
            return null;
        }
    }

    public void initPreferenceApplicationDisplay() {
        final Map<String, String> urlParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        if (urlParams.containsKey("id")) {
            try {
                final EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
                this.selectedPreference = entityManager
                        .find(ApplicationPreference.class, Integer.valueOf(urlParams.get("id")));
            } catch (final NumberFormatException e) {
                this.selectedPreference = null;
            }
        } else {
            this.selectedPreference = new ApplicationPreference();
            this.selectedPreference.setClassName("java.lang.String");
        }
    }

    public void save() {
        if (this.selectedPreference != null) {
            final EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
            this.selectedPreference = entityManager.merge(this.selectedPreference);
            entityManager.flush();
            FacesMessages.instance().addFromResourceBundle(StatusMessage.Severity.INFO,
                    "net.ihe.gazelle.evs.ApplicationPreferenceSavedWithSuccess");
            CSPHeaderFilter.clearCache();
            SQLinjectionFilter.resetInjectionFilter();
        }
    }

    public void createMissingKeys() {
        ApplicationConfiguration.LOGGER.debug("Create missing keys");
        ApplicationConfiguration.LOGGER.info("Create missing keys");
        final EntityManager entityManager = EntityManagerService.provideEntityManager();

        ApplicationPreferenceQuery applicationPreferenceQuery;
        ApplicationPreference ap;
        for (final PreferencesKey preference : PreferencesKey.values()) {
            applicationPreferenceQuery = new ApplicationPreferenceQuery();
            final String friendlyName = preference.getFriendlyName();
            applicationPreferenceQuery.preferenceName().eq(friendlyName);
            ap = applicationPreferenceQuery.getUniqueResult();

            if (ap == null) {
                ap = new ApplicationPreference();
                ap.setClassName(preference.getType());
                ap.setDescription(preference.getDescription());
                ap.setPreferenceName(friendlyName);
                ap.setPreferenceValue(preference.getDefaultValue());
                entityManager.merge(ap);
                entityManager.flush();
            }
        }
        CSPHeaderFilter.clearCache();
        SQLinjectionFilter.resetInjectionFilter();
        GuiMessage.logMessage(StatusMessage.Severity.INFO, "Missing keys are now created !");
    }

    public ApplicationPreference getSelectedPreference() {
        return this.selectedPreference;
    }

    /**
     * Get the application Database initialization flag (true if DB is well initialized) from the database
     *
     * @return Boolean : true if DB is well populated/initialized
     */
    public Boolean getApplicationDatabaseInitializationFlag() {
        final String value = applicationPreferenceManager
                .getStringValue("application_database_initialization_flag");
        if (!"database_successfully_initialized".equals(value)) {
            ApplicationConfiguration.LOGGER.error("");
            ApplicationConfiguration.LOGGER.error(
                    "@@@@@@@@@@@@@  DATABASE : ERROR DURING SQL POPULATING/INITIALIZATION !!!!!!!!!!!!!!!    @@@@@@@@@@@@");
            ApplicationConfiguration.LOGGER.error(
                    "@@@@@@@@@@@@@  DATABASE : ERROR DURING SQL POPULATING/INITIALIZATION !!!!!!!!!!!!!!!    @@@@@@@@@@@@");
            ApplicationConfiguration.LOGGER.error(
                    "@@@@@@@@@@@@@  DATABASE : ERROR DURING SQL POPULATING/INITIALIZATION !!!!!!!!!!!!!!!    @@@@@@@@@@@@");
            ApplicationConfiguration.LOGGER.error(
                    "@@@@@@@@@@@@@  DATABASE : ERROR DURING SQL POPULATING/INITIALIZATION !!!!!!!!!!!!!!!    @@@@@@@@@@@@");
            ApplicationConfiguration.LOGGER.error(
                    "@@@@@@@@@@@@@  DATABASE : ERROR DURING SQL POPULATING/INITIALIZATION !!!!!!!!!!!!!!!    @@@@@@@@@@@@");
            ApplicationConfiguration.LOGGER.error("");
            ApplicationConfiguration.LOGGER.error("Please check in the logs ABOVE why your database is not initialized...");
            ApplicationConfiguration.LOGGER.error("");
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }

    public Menu getMenu() {
        return EVSMenu.getMenu();
    }

    public void resetCache() {
        final List<CacheManager> allCacheManagers = new ArrayList<>(CacheManager.ALL_CACHE_MANAGERS);
        for (final CacheManager cacheManager : allCacheManagers) {
            try {
                final String[] cacheNames = cacheManager.getCacheNames();
                for (final String cacheName : cacheNames) {
                    if ("resteasyCache".equals(cacheName)) {
                        cacheManager.getEhcache(cacheName).removeAll();
                        GuiMessage.logMessage(StatusMessage.Severity.INFO, "Cache is cleared !");
                    }
                }
            } catch (final CacheException e) {
                ApplicationConfiguration.LOGGER.error("Failed to clear cache for {}", cacheManager);
                GuiMessage.logMessage(StatusMessage.Severity.ERROR, "Failed to clear cache for " + cacheManager);
            }
        }
    }

    public void resetHttpHeaders() {
        applicationPreferenceManager.resetHttpHeaders();
        GuiMessage.logMessage(StatusMessage.Severity.INFO, "Http headers are reset !");
    }

    public void deleteSelectedPreference() {
        final String preferenceName;
        if (this.selectedPreference != null) {
            final EntityManager entityManager = (EntityManager) Component.getInstance("entityManager");
            this.selectedPreference = entityManager.find(ApplicationPreference.class, this.selectedPreference.getId());
            if (this.selectedPreference != null) {
                preferenceName = this.selectedPreference.getPreferenceName();
                entityManager.remove(this.selectedPreference);
                entityManager.flush();
                GuiMessage.logMessage(StatusMessage.Severity.INFO, "Preference '" + preferenceName + "' is deleted.");
            }
        } else {
            GuiMessage.logMessage(StatusMessage.Severity.ERROR, "There was no Preference selected for deletion!");
        }
    }

    public boolean checkPreferenceKeyIsUnique(final String label) {
        final Iterator<ApplicationPreference> iterator = applicationPreferences.iterator();
        while (iterator.hasNext()) {
            final ApplicationPreference tmp = iterator.next();
            if (tmp.getPreferenceName().compareTo(label) == 0) {
                return false;
            }
        }
        return true;
    }


}
