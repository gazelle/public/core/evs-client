package net.ihe.gazelle.evsclient.domain.validationservice.configuration.extension;

import net.ihe.gazelle.evsclient.domain.validation.extension.ValidationExtension;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.WebServiceConf;
import net.ihe.gazelle.evsclient.domain.validationservice.extension.ExtensionService;
import net.ihe.gazelle.evsclient.domain.validationservice.extension.ExtensionServiceFactory;

import javax.persistence.*;

@Entity
@Table(name = "evsc_web_extension_service_conf", schema = "public")
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class WebExtensionServiceConf<E extends ValidationExtension, S extends ExtensionService<E>, C extends ExtensionServiceConf, T extends ExtensionServiceFactory<S,C>> extends ExtensionServiceConf<E,S,C,T> implements WebServiceConf {
    private static final long serialVersionUID = 6067224015483089438L;

    @Column(name = "url")
    private String url;

    @Column(name = "zip_transport")
    private boolean zipTransport;

    protected WebExtensionServiceConf(Integer id, String description, String name, String factoryClassname, boolean available, String url, boolean zipTransport) {
        super(id, description, name, factoryClassname, available);
        this.url = url;
        this.zipTransport = zipTransport;
    }

    protected WebExtensionServiceConf() {
        super();
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isZipTransport() {
        return zipTransport;
    }

    public void setZipTransport(boolean zipTransport) {
        this.zipTransport = zipTransport;
    }


}
