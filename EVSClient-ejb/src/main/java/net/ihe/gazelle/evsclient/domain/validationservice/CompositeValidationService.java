package net.ihe.gazelle.evsclient.domain.validationservice;

import net.ihe.gazelle.evsclient.domain.validationservice.configuration.CompositeProcessingConf;

public interface CompositeValidationService<T extends CompositeProcessingConf> extends ConfigurableValidationService<T> {
}
