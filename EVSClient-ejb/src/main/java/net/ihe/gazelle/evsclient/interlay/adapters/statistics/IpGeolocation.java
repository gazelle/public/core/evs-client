package net.ihe.gazelle.evsclient.interlay.adapters.statistics;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.common.application.action.ApplicationPreferenceManagerImpl;
import net.ihe.gazelle.evsclient.application.statistics.IpCountryProvider;
import net.ihe.gazelle.evsclient.application.statistics.LocalizationException;
import net.ihe.gazelle.evsclient.interlay.factory.DefaultImplementation;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.*;
import java.text.MessageFormat;

@DefaultImplementation(IpCountryProvider.class)
public class IpGeolocation implements IpCountryProvider {
    private static final Logger LOGGER = LoggerFactory.getLogger(IpGeolocation.class);
    public static final String DEFAULT_PATTERN = "https://api.ipgeolocation.io/ipgeo?apiKey=7854d7fe1d7b460980d57fdec5261062&ip={0}";

    private static ApplicationPreferenceManager prefs = new ApplicationPreferenceManagerImpl();
    private static String getPreference(String name, String defaultValue) {
        String value = prefs.getStringValue(name);
        return StringUtils.isEmpty(value)? defaultValue : value;
    }

    private String pattern;
    private String localCountry;
    private ObjectMapper om = new ObjectMapper();
    public IpGeolocation(String pattern, String localCountry) {
        this.pattern = pattern;
        this.localCountry = localCountry;
    }

    public IpGeolocation() {
        this(
                getPreference("ip_loc_service_configuration", DEFAULT_PATTERN),
                getPreference("ip_loc_service_local_country", "France")
        );
    }


    public String findCountry(String ipAddress) throws LocalizationException {
        try {
            if (isLocalIpAddress(InetAddress.getByName(ipAddress))) {
                return localCountry;
            }
            URL url = new URL(MessageFormat.format(pattern, ipAddress));
            if (LOGGER.isInfoEnabled()) LOGGER.info(MessageFormat.format("ip-geolocalisation.find-country.url {0}", url));
            String countryName = om.readTree(url)
                    .get("country_name")
                    .asText();
            if (LOGGER.isInfoEnabled()) LOGGER.info(MessageFormat.format("ip-geolocalisation.find-country.country {0}", countryName));
            return countryName;
        } catch (final Exception e) {
            throw new LocalizationException(MessageFormat.format("An error occurred when retrieving the country for ip address: {0}", ipAddress), e);
        }

    }

    public static boolean isLocalIpAddress(InetAddress addr) {
        // Check if the address is a valid special local or loop back
        if (addr.isAnyLocalAddress() || addr.isLoopbackAddress())
            return true;

        // Check if the address is defined on any interface
        try {
            return NetworkInterface.getByInetAddress(addr) != null;
        } catch (SocketException e) {
            return false;
        }
    }
}
