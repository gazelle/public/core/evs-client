package net.ihe.gazelle.evsclient.application.validation;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.evsclient.application.AbstractProcessingManagerImpl;
import net.ihe.gazelle.evsclient.application.interfaces.OidGeneratorManager;
import net.ihe.gazelle.evsclient.application.interfaces.adapters.NotLoadedException;
import net.ihe.gazelle.evsclient.application.interfaces.adapters.ProcessingNotSavedException;
import net.ihe.gazelle.evsclient.application.job.ExtensionJob;
import net.ihe.gazelle.evsclient.application.statistics.IpLocService;
import net.ihe.gazelle.evsclient.application.validationservice.GatewayTimeoutException;
import net.ihe.gazelle.evsclient.application.validationservice.ValidationServiceNotAvailableException;
import net.ihe.gazelle.evsclient.application.validationservice.configuration.ReferencedStandardNotFoundException;
import net.ihe.gazelle.evsclient.application.validationservice.configuration.ServiceConfNotFoundException;
import net.ihe.gazelle.evsclient.application.validationservice.configuration.ValidationServiceConfManager;
import net.ihe.gazelle.evsclient.domain.processing.*;
import net.ihe.gazelle.evsclient.domain.validation.*;
import net.ihe.gazelle.evsclient.domain.validation.report.*;
import net.ihe.gazelle.evsclient.domain.validationservice.ValidationService;
import net.ihe.gazelle.evsclient.domain.validationservice.Validator;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.ProcessingConf;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.ReferencedStandard;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.ServiceConfException;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.ValidationServiceConf;
import net.ihe.gazelle.evsclient.interlay.adapters.validationservice.AbstractConfigurableValidationService;
import net.ihe.gazelle.evsclient.interlay.dto.rest.validation.ValidationServiceProfile;
import net.ihe.gazelle.evsclient.interlay.factory.XmlDocumentBuilderFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import java.io.ByteArrayInputStream;
import java.util.*;
import java.util.concurrent.Callable;

public class ValidationServiceFacade extends AbstractProcessingManagerImpl<Validation> {

   private static final Logger LOGGER = LoggerFactory.getLogger(ValidationServiceFacade.class);

   private final ValidationDao validationDao;
   private final ValidationServiceConfManager validationServiceConfManager;
   private final IpLocService ipLocService;
   private ValidationExtensionServiceFacade validationExtensionServiceFacade;

   private static Set<ValidationServiceProfile> allValidationServiceProfiles;

   public ValidationServiceFacade(ValidationDao validationDao,
                                  ApplicationPreferenceManager applicationPreferenceManager,
                                  OidGeneratorManager oidGenerator,
                                  ValidationServiceConfManager validationServiceConfManager,
                                  ExtensionJob extensionJob,
                                  IpLocService ipLocService) {
      super(validationDao, applicationPreferenceManager, oidGenerator);

      this.validationDao = validationDao;
      this.validationServiceConfManager = validationServiceConfManager;
      this.ipLocService = ipLocService;
      this.validationExtensionServiceFacade = new ValidationExtensionServiceFacade(applicationPreferenceManager,oidGenerator,extensionJob, validationServiceConfManager);
   }

   public ValidationExtensionServiceFacade getValidationExtensionServiceFacade() {
      return validationExtensionServiceFacade;
   }

   public Validation validate(List<HandledObject> objects, EVSCallerMetadata caller, OwnerMetadata ownerMetadata,
                              Directive directive, boolean async)
         throws ServiceConfNotFoundException, ValidationServiceNotAvailableException, UnexpectedProcessingException,GatewayTimeoutException {

      ValidationServiceConf validationServiceConf = getValidationServiceByKeyword(directive.serviceName);
      if (validationServiceConf.isAvailable()) {
         try {
            Validation validation = instantiateAndPersistValidation(directive, objects, caller, ownerMetadata,
                  validationServiceConf.getValidationType());
            computeCountryStatistics(ownerMetadata);

            return executeValidationJob(directive, async, validationServiceConf, validation);
         } catch (NotLoadedException | ProcessingNotSavedException e) {
            throw new UnexpectedProcessingException("Unable to save validation", e);
         }
      } else {
         throw new ValidationServiceNotAvailableException("Validation Service is disabled");
      }
   }

   /**
    * @deprecated this function is doubtful ; it's not normal to change de result of a validation.
    * Whatever made a validation eventually failed, a new validation should be created to revalidate a document.
    */
   @Deprecated
   public Validation revalidate(String oid, OwnerMetadata ownerMetadata, Directive directive, boolean async)
         throws Exception {

      ValidationServiceConf validationServiceConf = getValidationServiceByKeyword(directive.serviceName);

      Validation validation = validationDao.getObjectByOid(oid);

      if (validation == null) {
         throw new NotLoadedException(oid);
      }

      validation.setValidationService(new ValidationServiceRef(directive.serviceName, directive.profileName));
      validation.setDate(new Date());
      validation.getCaller().setEntryPoint(EntryPoint.GUI);
      return executeValidationJob(directive, async, validationServiceConf, validation);
   }

   public List<ReferencedStandard> inferReferencedStandards(String validationServiceName, String validatorName)
         throws ServiceConfNotFoundException, ReferencedStandardNotFoundException {
      return validationServiceConfManager.inferReferencedStandards(validationServiceName, validatorName);
   }

   /**
    * Is Ds Signature Present
    *
    * @param fileContent the file upload content in the validator
    */
   // FIXME move to a signature processing service (Or MCA ?)
   public boolean isDsSignaturePresent(byte[] fileContent) {
      try {

         DocumentBuilder builder = new XmlDocumentBuilderFactory()
                 .setNamespaceAware(true)
                 .getBuilder();
         Document document = builder.parse(new ByteArrayInputStream(fileContent));
         NodeList signatureNodes = document.getElementsByTagNameNS("http://www.w3.org/2000/09/xmldsig#", "Signature");
         if (signatureNodes != null && signatureNodes.getLength() > 0) {
            return true;
         }
      } catch (Exception e) {
         LOGGER.error("Issue when trying to parse the document to extract ds:Signature");
      }
      return false;
   }


   public ReferencedStandard getReferencedStandardById(int id) {
      return validationServiceConfManager.getReferencedStandardById(id);
   }

   public List<ReferencedStandard> getReferencedStandardsByValidationServiceConf(ValidationServiceConf vsc) {
      return validationServiceConfManager.getReferencedStandardsByValidationServiceId(vsc.getId());
   }

   public ProcessingConf getReferencedStandardByName(String name) throws Exception {
      List<ReferencedStandard> results = validationServiceConfManager.getReferencedStandardFiltered(name, null, null);
      if (results == null || results.isEmpty()) {
         throw new Exception("referenced-standard-not-found " + name);
      }
      return results.iterator().next();
   }

   public ValidationServiceConf getValidationServiceByKeyword(String keyword) throws ServiceConfNotFoundException {
      return validationServiceConfManager.getValidationServiceConfByKeyword(keyword);
   }

   public synchronized Set<ValidationServiceProfile> getValidationProfiles() {
     return getProfiles();
   }

   public List<ValidationServiceProfile> getValidationProfilesByServiceName(String serviceName) {
      List<ValidationServiceProfile> profiles = new ArrayList<>();
      for (ValidationServiceProfile profile : getProfiles()) {
         if (profile.getServiceName().equals(serviceName)) {
            profiles.add(profile);
         }
      }
      return profiles;
   }


   public FilterDataModel<Validation> getValidationsFilteredDataModel(Filter<Validation> filter) {
      return validationDao.getFilteredDataModel(filter);
   }

   public ValidationService getValidationService(ValidationServiceConf validationServiceConf)
         throws ServiceConfException {
      return validationServiceConfManager.getFactory(validationServiceConf).getService();
   }

   /**
    * @deprecated Original report should disappeared in future validator version that should send standard report
    * @param originalReport
    * @param validationServiceRef
    * @return
    * @throws ServiceConfException
    * @throws ReportTransformationException
    */
   @Deprecated
   public Object asOriginalReport(Report<byte[]> originalReport, ValidationServiceRef validationServiceRef)
         throws ServiceConfException, ReportTransformationException {
      if (originalReport != null) {
         return validationServiceConfManager.getOriginalReportParser(validationServiceRef)
               .parseOriginalReport(originalReport.getContent());
      } else {
         throw new ReportTransformationException("Original report missing");
      }
   }

   private synchronized Set<ValidationServiceProfile> getProfiles(){
      if(allValidationServiceProfiles == null){
         List<ReferencedStandard> referencedStandards = validationServiceConfManager.getAllReferencedStandard();
         allValidationServiceProfiles = new TreeSet<>(new ValidationProfilesComparator());
         for(ReferencedStandard referencedStandard : referencedStandards){
            List<ValidationServiceConf> validationServices = referencedStandard.getValidationServiceConfs();
            for(ValidationServiceConf conf : validationServices){
               if(conf.isAvailable()){
                  try {
                     ValidationService service = getValidationService(conf);
                     List<Validator> validators = service.getValidators(referencedStandard.getValidatorFilter());
                     for(Validator validator : validators){
                        allValidationServiceProfiles.add(new ValidationServiceProfile().setServiceName(conf.getName()).setProfileID(validator.getKeyword()));
                     }
                  } catch (Exception e) {
                     LOGGER.error("Could not get validators for service " + conf.getName(), e);
                  }
               }
            }
         }
      }
      return allValidationServiceProfiles;
   }

   private Validation instantiateAndPersistValidation(Directive directive, List<HandledObject> objects,
                                                      EVSCallerMetadata evsCallerMetadata, OwnerMetadata ownerMetadata,
                                                      ValidationType validationType)
         throws UnexpectedProcessingException, NotLoadedException, ProcessingNotSavedException {
      Validation validation = new Validation(objects, evsCallerMetadata, ownerMetadata);
      super.init(validation);
      validation.setValidationType(validationType);
      validation.setValidationService(new ValidationServiceRef(directive.serviceName, directive.profileName));
      validation.setReferencedStandards(toReferencedStandardRefs(directive.referencedStandards));
      return super.create(validation);
   }

   private List<ReferencedStandardRef> toReferencedStandardRefs(List<ReferencedStandard> referencedStandards) {
      List<ReferencedStandardRef> referencedStandardRefs = new ArrayList<>();
      for (ReferencedStandard referencedStandard : referencedStandards) {
         referencedStandardRefs.add(new ReferencedStandardRef(referencedStandard.getId()));
      }
      return referencedStandardRefs;
   }



   private Validation executeValidationJob(Directive directive, boolean async, final ValidationServiceConf serviceConf,
                                           final Validation validation) throws GatewayTimeoutException {
      if (async) {
         throw new UnsupportedOperationException("Asynchronous validation is not yet implemented");
      } else {
         doValidation(validation, serviceConf);
      }
      return validation;
   }


   private ValidationStatus toValidationStatus(ValidationTestResult validationOverallResult) {
      switch (validationOverallResult) {
         case FAILED:
            return ValidationStatus.DONE_FAILED;
         case PASSED:
            return ValidationStatus.DONE_PASSED;
         case UNDEFINED:
         default:
            return ValidationStatus.DONE_UNDEFINED;
      }
   }


   // This method should avoid throwing checked exception. This will allow to move it in a Runnable or Worker once
   // asynchronous calls will be available.
   private void doValidation(final Validation validation, final ValidationServiceConf validationServiceConf) throws GatewayTimeoutException {
      try {
         Exception exception = ValidationExecutionQueue.run(
                 validationServiceConf,
                 new Callable<Exception>() {
                     @Override
                     public Exception call() throws Exception {
                        try {
                           ValidationService validationService = getValidationService(validationServiceConf);

                           validation.setValidationStatus(ValidationStatus.IN_PROGRESS);
                           byte[] originalReportBytes = validationService.validate(validation.getObjects().toArray(new HandledObject[0]),
                                   validation.getValidationService().getValidatorKeyword());

                           validation.setOriginalReport(new Report<>(originalReportBytes));
                           validation.setValidationReport(new Report<>(
                                   validationServiceConfManager.getReportMapper(validation.getValidationService()).transform(originalReportBytes)
                           ));

                           validation.setValidationStatus(
                                   toValidationStatus(validation.getValidationReport().getContent().getValidationOverallResult()));

                           fillOutVersions(validation);
                        } catch (Exception e) {
                           return e;
                        }
                        return null;
                     }
                  },
                 "doValidation");

         if (exception!=null) {
            AbstractConfigurableValidationService.clearValidatorsCache(); // reset cache because it a may be the sign that service configuration has changed.
            validation.setValidationStatus(ValidationStatus.DONE_UNDEFINED);
            validation.setValidationReport(
                    generateReportForException(
                            exception,
                            validation.getValidationService().getName(),
                            validation.getValidationService().getValidatorKeyword()));
            if (InterruptedException.class.isInstance(exception)) {
               throw new GatewayTimeoutException(validation.getOid(),exception);
            } else if (GatewayTimeoutException.class.isInstance(exception)) {
               throw (GatewayTimeoutException)exception;
            }
         }
      } finally {
         validationDao.merge(validation);
      }
   }

   private void fillOutVersions(Validation validation) {
      if (validation.getValidationReport() != null && validation.getValidationReport().getContent() != null) {
         ValidationOverview validationOverview = validation.getValidationReport().getContent().getValidationOverview();

         validation.getValidationService().setVersion(validationOverview.getValidationServiceVersion());
         validation.getValidationService().setValidatorVersion(validationOverview.getValidatorVersion());
      }
   }

   private Report<ValidationReport> generateReportForException(Exception e, String validationServiceName,
                                                               String validatorId) {
      ValidationReport validationReport = new ValidationReport(
            UUID.randomUUID().toString(),
            new ValidationOverview(
                  ValidationReportMapper.DEFAULT_DISCLAIMER,
                  validationServiceName,
                  ValidationReportMapper.UNKNOWN,
                  validatorId));

      ValidationSubReport validationSubReport = new ValidationSubReport("Unexpected error", null);
      validationSubReport.addUnexpectedError(new UnexpectedError(e));

      validationReport.addSubReport(validationSubReport);
      return new Report<>(validationReport);
   }

   private void computeCountryStatistics(OwnerMetadata ownerMetadata) {
      if (ownerMetadata != null && ownerMetadata.getUserIp() != null &&
            applicationPreferenceManager.getBooleanValue("include_country_statistics").booleanValue()) {
         try {
            ipLocService.computeLoc(ownerMetadata.getUserIp());
         } catch (RuntimeException e) {
            LOGGER.error("Could not retrieve IP location. Geolocalized statistics may be inaccurate", e);
         }
      }
   }

   private class ValidationProfilesComparator implements Comparator<ValidationServiceProfile> {
      @Override
      public int compare(ValidationServiceProfile o1, ValidationServiceProfile o2) {
         int serviceNameComparison = o1.getServiceName().compareTo(o2.getServiceName());
         if(serviceNameComparison == 0){
            return o1.getProfileID().compareTo(o2.getProfileID());
         } else {
            return serviceNameComparison;
         }
      }
   }

}
