package net.ihe.gazelle.evsclient.interlay.dto.rest.validation;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import net.ihe.gazelle.common.application.action.ApplicationPreferenceManagerImpl;
import net.ihe.gazelle.evsclient.interlay.dto.rest.AbstractDTO;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import java.net.URI;
import java.text.MessageFormat;

@Schema(description="URL de récupération du rapport détaillé de validation. ")
@XmlRootElement(name = "validationReportRef")
@XmlAccessorType(XmlAccessType.FIELD)
public class ValidationReportRef extends AbstractDTO<URI> {


    public ValidationReportRef(URI domain) {
        super(domain);
    }

    public ValidationReportRef() {
        this(URI.create(""));
    }

    public ValidationReportRef(net.ihe.gazelle.evsclient.domain.validation.Validation validation) {
        this(URI.create(
                MessageFormat.format(
                        "{0}/rest/validations/{1}/report",
                        ApplicationPreferenceManagerImpl.instance().getApplicationUrl().replaceFirst("/$",""),
                        validation.getOid())
        ));
    }

    @Schema(name= "location",example = "", description = ".") // TODO
    @JsonProperty("location")
    @XmlAttribute(name = "location")
    public String getLocation() {
        return domain.toASCIIString();
    }

}
