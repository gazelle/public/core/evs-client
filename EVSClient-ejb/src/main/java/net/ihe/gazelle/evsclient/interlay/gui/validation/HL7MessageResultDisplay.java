/*
 * EVS Client is part of the Gazelle Test Bed
 * Copyright (C) 2006-2016 IHE
 * mailto :eric DOT poiseau AT inria DOT fr
 *
 * See the NOTICE file distributed with this work for additional information
 * regarding copyright ownership.  This code is licensed
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package net.ihe.gazelle.evsclient.interlay.gui.validation;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.common.application.action.ApplicationPreferenceManagerImpl;
import net.ihe.gazelle.evsclient.application.CallerMetadataFactory;
import net.ihe.gazelle.evsclient.application.interfaces.ForbiddenAccessException;
import net.ihe.gazelle.evsclient.application.interfaces.NotFoundException;
import net.ihe.gazelle.evsclient.application.interfaces.UnauthorizedException;
import net.ihe.gazelle.evsclient.application.validation.ValidationServiceFacade;
import net.ihe.gazelle.evsclient.interlay.dao.HandledObjectFile;
import net.ihe.gazelle.evsclient.interlay.dao.validationservice.validator.HL7ProfileDaoImpl;
import net.ihe.gazelle.evsclient.interlay.gui.GuiMessage;
import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import org.apache.commons.lang.StringUtils;
import org.jboss.seam.international.StatusMessage;
import org.richfaces.model.TreeNode;

import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Paths;



public class HL7MessageResultDisplay extends ValidationResultBeanGui implements Serializable {
    private static final long serialVersionUID = 7920472368366520753L;

    private String messageContent;
    private transient TreeNode treeMessageContent;
    private boolean isXmlMessage;
    private String packageName;

    public HL7MessageResultDisplay(ValidationServiceFacade validationServiceFacade, CallerMetadataFactory callerMetadataFactory, GazelleIdentity identity, ApplicationPreferenceManager applicationPreferenceManager) {
        super(validationServiceFacade, callerMetadataFactory, identity, applicationPreferenceManager);
        this.cls = HL7MessageResultDisplay.class;
    }

    @Override
    public void initFromUrl() throws UnauthorizedException, NotFoundException, ForbiddenAccessException {
        super.initFromUrl();
        this.initMessageContent();
    }

    public boolean isXmlMessage() {
        if (messageContent==null) {
            initMessageContent(); // initialize isXmlMessage
        }
        return isXmlMessage;
    }

    public void setXmlMessage(boolean xmlMessage) {
        isXmlMessage = xmlMessage;
    }

    public String permanentLinkToProfile() {
        ApplicationPreferenceManager applicationPreferenceManager = new ApplicationPreferenceManagerImpl();
        String url = applicationPreferenceManager.getStringValue("gazelle_hl7v2_validator_url");
        if (url != null) {
            url = url.concat("/viewProfile.seam?oid=");
            url = url.concat(this.selectedObject.getValidationService().getValidatorKeyword());
        }
        return url;
    }

    public String getHighlightedER7() {
        return HL7v2FormatAdapter.getHighlightedER7(messageContent, isXmlMessage);
    }

    public String getER7() {
        return HL7v2FormatAdapter.getDisplayER7(this.messageContent, isXmlMessage);
    }

    public String getXML() {
        return HL7v2FormatAdapter.getXML(messageContent==null?initMessageContent():messageContent, isXmlMessage, packageName);
    }

    public TreeNode getTree() {
        if (this.treeMessageContent == null) {
            this.treeMessageContent = HL7v2FormatAdapter.getTree(this.getXML());
        }
        return this.treeMessageContent;

    }

    private String initMessageContent(){
        try {
            if (this.selectedObject != null){

                // TODO Verify if selectedObject.getContent() is not enought
                messageContent =
                      new String(Files.readAllBytes(Paths.get(((HandledObjectFile)selectedObject.getObject()).getFilePath())));
                if (StringUtils.isNotEmpty(messageContent)) {
                    this.isXmlMessage = HL7v2FormatAdapter.isXmlWellFormed(messageContent);
                    if (!this.isXmlMessage) {
                        this.messageContent = HL7v2FormatAdapter.trunkHL7MessageContentIfTooBig(messageContent);
                    }
                    packageName = new HL7ProfileDaoImpl().findPackageNameWithProfileOID(this.selectedObject.getValidationService().getValidatorKeyword());
                }
                return messageContent;
            }
        } catch (IOException e) {
            GuiMessage.logMessage(StatusMessage.Severity.ERROR, "Can not read the message file : " + e.getMessage());

        }
        return "";
    }
}
