package net.ihe.gazelle.evsclient.interlay.gui.statistics;

import net.ihe.gazelle.evsclient.application.statistics.StatisticsManager;
import net.ihe.gazelle.evsclient.application.validationservice.configuration.ValidationServiceConfManager;
import net.ihe.gazelle.evsclient.interlay.gui.GuiMessage;
import net.ihe.gazelle.evsclient.interlay.gui.QueryParamEvs;
import org.jboss.seam.faces.FacesManager;
import org.jboss.seam.international.StatusMessage;

import javax.faces.context.FacesContext;
import java.util.List;
import java.util.Map;

public class StatisticsByStandardBeanGui {

    private final StatisticsManager statisticsManager;

    private final ValidationServiceConfManager validationServiceConfManager;

    private int standardId;

    public StatisticsByStandardBeanGui(StatisticsManager statisticsManager, ValidationServiceConfManager validationServiceConfManager) {
        this.statisticsManager = statisticsManager;
        this.validationServiceConfManager = validationServiceConfManager;
        init();
    }

    private void init() {
        final Map<String, String> urlParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        try {
            standardId = Integer.parseInt(urlParams.get(QueryParamEvs.REF_STANDARD_ID));
        } catch (final NumberFormatException e) {
            FacesManager.instance().redirect("/error.seam");
            GuiMessage.logMessage(StatusMessage.Severity.ERROR, "Standard id is not an integer");
        }
    }

    public String getStandardName() {
        return validationServiceConfManager.getReferencedStandardById(standardId).getName();
    }

    public List<Object[]> getStatisticsPerValidationService() {
        return statisticsManager.countPerValidationServiceByReferencedStandardId(standardId);
    }

    public List<Object[]> getStatisticsPerValidator() {
        return statisticsManager.countPerValidatorByReferencedStandardId(standardId);
    }

    public List<Object[]> getStatisticsPerStatus() {
        return statisticsManager.countPerStatusByReferencedStandardId(standardId);
    }

    public List<Object[]> countPerDateMonthIntervalByReferencedStandardId(){
        return statisticsManager.countPerDateMonthIntervalByReferencedStandardId(standardId);
    }

    public List<Object[]> countPerDateYearIntervalByReferencedStandardId(){
        return statisticsManager.countPerDateYearIntervalByReferencedStandardId(standardId);
    }

}
