package net.ihe.gazelle.evsclient.integration;

import junit.framework.TestCase;
import net.ihe.gazelle.mb.validator.client.MBValidator;
import net.ihe.gazelle.mb.validator.client.ServiceConnectionError;
import net.ihe.gazelle.mb.validator.client.ServiceOperationError;

import javax.xml.bind.DatatypeConverter;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

/**
 * Class Description : Integration testing related to webservices used on the validation of CDA Documents and XD* metadatas.
 * All model based webservice validator has the same JAVA interface, which is @net.ihe.gazelle.simulator.ws.ModelBasedValidationRemote.
 * It contains 3 methods :
 * <ul>
 * <li>validateDocument(String document, String validator)</li>
 * <li>validateBase64Document(String base64Document, String validator)</li>
 * <li>List<String> getListOfValidators(String descriminator)</li>
 * </ul>
 * The aim of this class is to test the reachability of each method using the webservice of XDS and CDA
 *
 * @author abderrazek boufahja / Kereval / IHE-Europe
 * @version 1.0 - 15 jul 2013
 * @see > abderrazek.boufahja@ihe-europe.com  -  http://www.ihe-europe.org
 */
public class MBValidatorTest extends TestCase {

    private static final String CDA_ENDPOINT = "https://gazelle.ihe.net/CDAGenerator-ejb/ModelBasedValidationWSService/ModelBasedValidationWS?wsdl";

    private static final String XDS_ENDPOINT = "https://gazelle.ihe.net/XDStarClient-ejb/ModelBasedValidationWSService/ModelBasedValidationWS?wsdl";

    private static final String WADO_ENDPOINT = "https://gazelle.ihe.net/XDStarClient-ejb/WADOModelBasedWSService/WADOModelBasedWS?wsdl";

    /**
     * test the possibility to retrieve all availables validators' name from the CDA endpoint
     */
    public void testCDAValidatorListAvailableValidators() throws ServiceConnectionError, ServiceOperationError {
        MBValidator mbValidator = new MBValidator(CDA_ENDPOINT);
        List<String> liss = mbValidator.listAvailableValidators("IHE");
        assertTrue(!liss.isEmpty());
        assertTrue(liss.contains("HL7 - CDA Release 2"));
    }

    /**
     * test the possibility to validate a CDA document against a kind of CDA validator.
     */
    public void testCDAValidatorTestValidation() throws IOException, ServiceConnectionError, ServiceOperationError {
        String document = readDoc("src/test/resources/cda/sample1.xml");
        MBValidator mbValidator = new MBValidator(CDA_ENDPOINT);
        String liss = mbValidator.validate(document, "HL7 - CDA Release 2", false);
        assertTrue(liss != null && !liss.isEmpty());
        assertTrue(liss.contains("<Result>PASSED</Result>"));
    }

    /**
     * test the possibility to validate a CDA document against a kind of CDA validator by
     * sending the document on base64 format
     */
    public void testCDAValidatorTestValidationBase64() throws IOException, ServiceConnectionError, ServiceOperationError {
        String document = readDoc("src/test/resources/cda/sample1.xml");
        MBValidator mbValidator = new MBValidator(CDA_ENDPOINT);
//        String liss = mbValidator.validate(Base64.encodeBytes(document.getBytes()), "HL7 - CDA Release 2", true);
        String liss = mbValidator.validate(DatatypeConverter.printBase64Binary(document.getBytes()), "HL7 - CDA Release 2", true);
        assertTrue(liss != null && !liss.isEmpty());
        assertTrue(liss.contains("<Result>PASSED</Result>"));
    }

    /**
     * test the possibility to retrieve all availables validators' name from the XDS endpoint
     */
    public void testXDSValidatorListAvailableValidators() throws ServiceConnectionError, ServiceOperationError {
        MBValidator mbValidator = new MBValidator(XDS_ENDPOINT);
        List<String> liss = mbValidator.listAvailableValidators("IHE");
        assertTrue(!liss.isEmpty());
        assertTrue(liss.contains("IHE XCA ITI-38 Cross Gateway Query - response"));
    }

    /**
     * test the possibility to validate a XDS metadata against a kind of XDS validator.
     */
    public void testXDSValidatorTestValidation() throws IOException, ServiceConnectionError, ServiceOperationError {
        String document = readDoc("src/test/resources/xds/sample1.xml");
        MBValidator mbValidator = new MBValidator(XDS_ENDPOINT);
        String liss = mbValidator.validate(document, "IHE Retrieve Document Set - request", false);
        assertTrue(liss != null && !liss.isEmpty());
        assertTrue(liss.contains("<Result>PASSED</Result>"));
    }

    /**
     * test the possibility to validate an XDS metadatas against a kind of XDS validator by
     * sending the document on base64 format
     */
    public void testXDSValidatorTestValidationBase64() throws IOException, ServiceConnectionError, ServiceOperationError {
        String document = readDoc("src/test/resources/xds/sample1.xml");
        MBValidator mbValidator = new MBValidator(XDS_ENDPOINT);
 //       String liss = mbValidator
 //               .validate(Base64.encodeBytes(document.getBytes()), "IHE Retrieve Document Set - request", true);
        String liss = mbValidator
                .validate(DatatypeConverter.printBase64Binary(document.getBytes()), "IHE Retrieve Document Set - request", true);
        assertTrue(liss != null && !liss.isEmpty());
        assertTrue(liss.contains("<Result>PASSED</Result>"));
    }

    private static String readDoc(String name) throws IOException {
        BufferedReader scanner = new BufferedReader(new InputStreamReader(new FileInputStream(name)));
        StringBuilder res = new StringBuilder();
        try {
            String line = scanner.readLine();
            while (line != null) {
                res.append(line + "\n");
                line = scanner.readLine();
            }
        } finally {
            scanner.close();
        }
        return res.toString();
    }

    /**
     * test the possibility to retrieve all available validators' name from the WADO endpoint
     */
    public void testWADOValidatorListAvailableValidator() throws ServiceConnectionError, ServiceOperationError {
        MBValidator wadoValidator = new MBValidator(WADO_ENDPOINT);
        List<String> validatorList = wadoValidator.listAvailableValidators("IHE");
        assertTrue("The list of available WADO validator must not be empty.", !validatorList.isEmpty());
        assertTrue("The list of available WADO validator must contains IHE- WADO",
                validatorList.contains("IHE - WADO"));
    }

    /**
     * Test the possibility to validate a wado request with IHE WADO validator.
     */
    public void testWADOValidatorValidateRightDocument() throws ServiceConnectionError, ServiceOperationError {
        String rlt;
        MBValidator wadoValidator = new MBValidator(WADO_ENDPOINT);
        String url = "https://gazelle/wadoValid?requestType=WADO" + "&studyUID=1.2.250.1.59.40211.12345678.678910"
                + "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
                + "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2"
                + "&contentType=image%2Fjp2%3Blevel%3D1,image%2Fjpeg%3Bq%3D0.5" + "&annotation=patient,technique"
                + "&columns=400" + "&rows=300" + "&region=0.3,0.4,0.5,0.5" + "&windowCenter=-1000"
                + "&windowWidth=%2B2.5E3";
        rlt = wadoValidator.validate(url, "IHE - WADO", false);
        assertTrue("The result must ne be null/empty", rlt != null && !rlt.isEmpty());
        assertTrue("The result must be PASSED", rlt.contains("<Result>PASSED</Result>"));
    }

    /**
     * Test the possibility to reject a wado request with IHE WADO validator.
     */
    public void testWADOValidatorValidateWrongDocument() throws ServiceConnectionError, ServiceOperationError {
        String rlt;
        MBValidator wadoValidator = new MBValidator(WADO_ENDPOINT);
        String url = "https://gazelle/wadoValid?requestType=WADO" + "&studyUID=1.2.250.1.59.40211.12345678.678910"
                + "&seriesUID=1.2.250.1.59.40211.789001276.14556172.67789"
                + "&objectUID=1.2.250.1.59.40211.2678810.87991027.899772.2"
                + "&contentType=image%2Fjp2%3Blevel%3D1,image%2Fjpeg%3Bq%3D0.5" + "&annotation=technique"
                + "&columns=400" + "&rows=300" + "&region=0.3,0.4,0.5,0.5"
                + "&presentationUID=1.2.250.1.F0.40211.111A13.1D1C16"
                + "&presentationSeriesUID=1.2.250.1.59.40211.222120.191817";
        rlt = wadoValidator.validate(url, "IHE - WADO", false);
        assertTrue("The result must ne be null/empty", rlt != null && !rlt.isEmpty());
        assertTrue("The result must be FAILED", rlt.contains("<Result>FAILED</Result>"));
    }

}
