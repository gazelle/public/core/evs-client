package net.ihe.gazelle.evsclient.application;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.application.interfaces.OidGeneratorManager;
import net.ihe.gazelle.evsclient.application.validation.Directive;
import net.ihe.gazelle.evsclient.application.validation.ValidationDao;
import net.ihe.gazelle.evsclient.application.validation.ValidationServiceFacade;
import net.ihe.gazelle.evsclient.application.validationservice.GatewayTimeoutException;
import net.ihe.gazelle.evsclient.application.validationservice.ValidationServiceNotAvailableException;
import net.ihe.gazelle.evsclient.application.validationservice.configuration.ServiceConfNotFoundException;
import net.ihe.gazelle.evsclient.application.validationservice.configuration.ValidationServiceConfDao;
import net.ihe.gazelle.evsclient.application.validationservice.configuration.ValidationServiceConfManager;
import net.ihe.gazelle.evsclient.domain.processing.*;
import net.ihe.gazelle.evsclient.domain.validation.Validation;
import net.ihe.gazelle.evsclient.domain.validation.ValidationStatus;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.EmbeddedValidationServiceConf;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.ReferencedStandard;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.ValidationServiceConf;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author ceoche
 */
public class ValidationServiceFacadeTest {

   private static final Logger LOG = LoggerFactory.getLogger(ValidationServiceFacadeTest.class);

   private static final String UNAVAILABLE_SRV_NAME = "Unavailable";
   private static final String UNKNOWN_SRV_NAME = "Unknown";
   private static final String OK_SRV_NAME = "service4Test";

   private ValidationServiceFacade facade;
   private ValidationDao validationDao;
   private ApplicationPreferenceManager applicationPreferenceManager;
   private OidGeneratorManager oidGenerator;
   private ValidationServiceConfDao validationServiceConfDao;
   private ValidationServiceConfManager validationServiceConfManager;

   @Before
   public void setUp() {
      validationDao = newValidationDaoMock();
      oidGenerator = newOidGeneratorMock();
      validationServiceConfDao = newServiceConfDaoMock();
      applicationPreferenceManager = newApplicationPreferenceManagerMock();
      validationServiceConfManager = new ValidationServiceConfManager(validationServiceConfDao, applicationPreferenceManager);
      facade = new ValidationServiceFacade(validationDao, applicationPreferenceManager, oidGenerator,
              validationServiceConfManager, null,null);
   }

   @Test
   public void testSimpleValidation()
         throws ValidationServiceNotAvailableException, ServiceConfNotFoundException, UnexpectedProcessingException, GatewayTimeoutException {
      Validation validation = facade.validate(getHandledObject(), getEvsCallerMetadata(), buildOwnerMetadata(),
            new Directive(OK_SRV_NAME, "validator4Test", new ArrayList<ReferencedStandard>()),
            false);
      Assert.assertEquals(ValidationStatus.DONE_PASSED, validation.getValidationStatus());
   }

   @Test(expected = ServiceConfNotFoundException.class)
   public void testServiceNotFoundError()
           throws ValidationServiceNotAvailableException, ServiceConfNotFoundException, UnexpectedProcessingException, GatewayTimeoutException {
      Directive directive = new Directive(UNKNOWN_SRV_NAME, "any validator", new ArrayList<ReferencedStandard>());
      facade.validate(getHandledObject(), new EVSCallerMetadata(), new OwnerMetadata(), directive, false);
   }

   @Test(expected = ValidationServiceNotAvailableException.class)
   public void testServiceNotAvailableError()
           throws ValidationServiceNotAvailableException, ServiceConfNotFoundException, UnexpectedProcessingException, GatewayTimeoutException {
      Directive directive = new Directive(UNAVAILABLE_SRV_NAME, "any validator", new ArrayList<ReferencedStandard>());
      facade.validate(getHandledObject(), new EVSCallerMetadata(), new OwnerMetadata(), directive, false);
   }

   private OwnerMetadata buildOwnerMetadata() {
      return new OwnerMetadata("127.0.0.1", "ceoche", "Kereval");
   }

   private EVSCallerMetadata getEvsCallerMetadata() {
      return new EVSCallerMetadata(EntryPoint.WS);
   }

   private List<HandledObject> getHandledObject() {
      return Arrays.asList(new HandledObject("<doc>mydocument</doc>".getBytes(StandardCharsets.UTF_8)));
   }

   private ValidationDao newValidationDaoMock() {
      ValidationDao validationDao = new ValidationDaoTestMock();
      return validationDao;
   }

   private ValidationServiceConfDao newServiceConfDaoMock() {
      ValidationServiceConfDao validationServiceConfDao = Mockito.mock(ValidationServiceConfDao.class);
      Mockito.doReturn(getUnavailableService()).when(validationServiceConfDao)
            .getValidationServiceByKeyword(UNAVAILABLE_SRV_NAME);
      Mockito.doReturn(getService4Test()).when(validationServiceConfDao).getValidationServiceByKeyword(OK_SRV_NAME);
      return validationServiceConfDao;
   }

   private OidGeneratorManager newOidGeneratorMock() {
      OidGeneratorManager oidGenerator = Mockito.mock(OidGeneratorManager.class);
      Mockito.doReturn("1.2.3.4").when(oidGenerator).getNewOid();
      return oidGenerator;
   }

   private ApplicationPreferenceManager newApplicationPreferenceManagerMock() {
      ApplicationPreferenceManager applicationPreferenceManager = Mockito.mock(ApplicationPreferenceManager.class);
      Mockito.doReturn(false).when(applicationPreferenceManager).getBooleanValue("include_country_statistics");
      return applicationPreferenceManager;
   }

   private ValidationServiceConf getUnavailableService() {
      ValidationServiceConf unavailableService = new EmbeddedValidationServiceConf();
      unavailableService.setName(UNAVAILABLE_SRV_NAME);
      unavailableService.setAvailable(false);
      return unavailableService;
   }

   private ValidationServiceConf getService4Test() {
      ValidationServiceConf service4Test = new EmbeddedValidationServiceConf();
      service4Test.setName(OK_SRV_NAME);
      service4Test.setAvailable(true);
      service4Test.setFactoryClassname("net.ihe.gazelle.evsclient.application.ValidationServiceFactoryTestMock");
      return service4Test;
   }

}
