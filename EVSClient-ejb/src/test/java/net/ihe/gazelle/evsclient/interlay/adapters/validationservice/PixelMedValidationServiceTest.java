package net.ihe.gazelle.evsclient.interlay.adapters.validationservice;

import junit.framework.TestCase;
import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.domain.processing.HandledObject;
import net.ihe.gazelle.evsclient.domain.validation.ValidationType;
import net.ihe.gazelle.evsclient.domain.validationservice.ValidationServiceOperationException;
import net.ihe.gazelle.evsclient.domain.validationservice.Validator;
import net.ihe.gazelle.evsclient.domain.validationservice.configuration.EmbeddedValidationServiceConf;
import net.ihe.gazelle.evsclient.interlay.adapters.validationservice.factory.PixelMedValidationServiceFactory;
import net.ihe.gazelle.evsclient.interlay.factory.DocumentBuilderFactoryException;
import net.ihe.gazelle.evsclient.interlay.factory.XmlDocumentBuilderFactory;
import org.apache.commons.io.FileUtils;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.modules.junit4.PowerMockRunner;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.List;

import static net.ihe.gazelle.files.FilesUtils.loadFile;

@RunWith(PowerMockRunner.class)
public class PixelMedValidationServiceTest extends TestCase {

    private static String[] xpathJdkConfig;

    @Mock
    ApplicationPreferenceManager applicationPreferenceManager;

    PixelMedValidationService service;

    @BeforeClass
    public static void systemSetUp() {
        // backup XMl xPath JDK config.
        xpathJdkConfig = new String[] {
                System.getProperty("jdk.xml.xpathExprGrpLimit", "10"),
                System.getProperty("jdk.xml.xpathExprOpLimit", "100"),
                System.getProperty("jdk.xml.xpathTotalOpLimit", "10000")
        };

        // Tune XML xPath engine to avoid javax.xml.transform.TransformerConfigurationException: JAXP0801003
        // https://stackoverflow.com/questions/72401149/limit-set-by-feature-secure-processing
        System.setProperty("jdk.xml.xpathExprGrpLimit", "0");
        System.setProperty("jdk.xml.xpathExprOpLimit", "0");
        System.setProperty("jdk.xml.xpathTotalOpLimit", "0");
    }

    @AfterClass
    public static void systemTearDown() {
        // Restore JDK config
        System.setProperty("jdk.xml.xpathExprGrpLimit", xpathJdkConfig[0]);
        System.setProperty("jdk.xml.xpathExprOpLimit", xpathJdkConfig[1]);
        System.setProperty("jdk.xml.xpathTotalOpLimit", xpathJdkConfig[2]);
    }

    @Before
    public void setUp() {
        EmbeddedValidationServiceConf conf = new EmbeddedValidationServiceConf();
        conf.setValidationType(ValidationType.DICOM);
        conf.setAvailable(true);
        conf.setName("PixelMed Validation Service");
        conf.setId(1);
        conf.setFactoryClassname(PixelMedValidationServiceFactory.class.getCanonicalName());
        service = new PixelMedValidationService(applicationPreferenceManager, conf);
    }

    @Test
    public void name() {
        Assert.assertEquals(service.getName(),"PixelMed Validation Service");
    }

    @Test
    public void getValidatorNames() {
        List<Validator> validators = service.getValidators();
        Assert.assertEquals(1,validators.size());
    }

    @Test
    public void about() {
        String about = service.about();
        Assert.assertNotNull(about);
        Assert.assertNotEquals("",about);
    }

    @Test
    public void validate() throws ValidationServiceOperationException {
        final File f = loadFile("/dcm/176F66C6.dcm");
        byte[] fileContent;
        try {
            fileContent = FileUtils.readFileToByteArray(f);
        } catch (IOException e) {
            fail("Unable to read file");
            return;
        }
        try {
            HandledObject handledObject = new HandledObject(fileContent, "176F66C6.dcm");

            byte[] report = service.validate(new HandledObject[]{handledObject}, PixelMedValidationService.DICOM_SR_CONFORMANCE);

            DocumentBuilder builder = new XmlDocumentBuilderFactory().getBuilder();
            Document xml = builder.parse(new ByteArrayInputStream(report));
            XPath xPath = XPathFactory.newInstance().newXPath();
            Assert.assertTrue((boolean)xPath.compile("boolean(//validationReport[@result='PASSED'])").evaluate(xml, XPathConstants.BOOLEAN));
            Assert.assertTrue((boolean)xPath.compile("boolean(//counters[@numberOfWarnings='89'])").evaluate(xml, XPathConstants.BOOLEAN));
            Assert.assertTrue((boolean)xPath.compile("boolean(//counters[@numberOfErrors='0'])").evaluate(xml, XPathConstants.BOOLEAN));
            Assert.assertTrue((boolean)xPath.compile("boolean(//counters[@numberOfConstraints='91'])").evaluate(xml, XPathConstants.BOOLEAN));
            Assert.assertTrue((boolean)xPath.compile("boolean(//counters[@failedWithInfoNumber='0'])").evaluate(xml, XPathConstants.BOOLEAN));
        } catch (ValidationServiceOperationException | XPathExpressionException | DocumentBuilderFactoryException | SAXException | IOException e) {
            fail("No exception is supposed to be raised : " + e.getMessage());
        }
    }

    @Test
    public void getServiceVersion() {
        Assert.assertNotEquals("",service.getServiceVersion());
    }
}
