package net.ihe.gazelle.evsclient.statistics;

import net.ihe.gazelle.evsclient.application.validation.ValidationDao;
import net.ihe.gazelle.evsclient.domain.statistics.StatisticsDataRow;
import net.ihe.gazelle.evsclient.interlay.dao.validation.ValidationDaoImpl;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.math.BigInteger;
import java.util.List;

import static org.junit.Assert.*;

public class ValidationDaoImplTest {

    private static EntityManagerFactory entityManagerFactory;

    private final ValidationDao validationDao;

    private List<StatisticsDataRow> statisticsDataRowList;

    @BeforeClass
    public static void init(){
        entityManagerFactory = Persistence.createEntityManagerFactory("PersistenceUnitTestForStatistics");
    }

    public ValidationDaoImplTest() {
        validationDao = new ValidationDaoImpl(entityManagerFactory, null);
        statisticsDataRowList = validationDao.countTotalDataRowGroupByReferencedStandard();
    }

    @Test
    public void getTotalDataRow() {
        StatisticsDataRow statisticsDataRow = validationDao.countTotalDataRow();
        assertEquals(BigInteger.valueOf(119), statisticsDataRow.getNumberOfValidations());
        assertEquals(BigInteger.valueOf(5), statisticsDataRow.getNumberOfGuiValidations());
        assertEquals(BigInteger.valueOf(114), statisticsDataRow.getNumberOfWebServiceValidations());
        assertEquals(BigInteger.valueOf(111), statisticsDataRow.getNumberOfPassedValidations());
        assertEquals(BigInteger.valueOf(5), statisticsDataRow.getNumberOfFailedValidations());
        assertEquals(BigInteger.valueOf(3), statisticsDataRow.getNumberOfUndefinedValidations());
    }

    @Test
    public void getTotalDataRowByGroupByReferencedStandardNotEmpty() {
        assertTrue(statisticsDataRowList.size() == 13);
    }

    @Test
    public void getTotalDataRowByGroupByHL7v3Ihe() throws Exception {
        String standardToTest = "HL7v3 (IHE)";
        String menu = "IHE";
        assertEquals(getStatisticsDataRowFromList(standardToTest, menu).getNumberOfValidations(), BigInteger.valueOf(13));
        assertEquals(getStatisticsDataRowFromList(standardToTest, menu).getNumberOfGuiValidations(), BigInteger.valueOf(0));
        assertEquals(getStatisticsDataRowFromList(standardToTest, menu).getNumberOfWebServiceValidations(), BigInteger.valueOf(13));
        assertEquals(getStatisticsDataRowFromList(standardToTest, menu).getNumberOfPassedValidations(), BigInteger.valueOf(11));
        assertEquals(getStatisticsDataRowFromList(standardToTest, menu).getNumberOfFailedValidations(), BigInteger.valueOf(2));
        assertEquals(getStatisticsDataRowFromList(standardToTest, menu).getNumberOfUndefinedValidations(), BigInteger.valueOf(0));
    }

    @Test
    public void getTotalDataRowByGroupByHL7v3Test() throws Exception {
        String standardToTest = "HL7v3 (IHE)";
        String menu = "TEST";
        assertEquals(getStatisticsDataRowFromList(standardToTest, menu).getNumberOfValidations(), BigInteger.valueOf(13));
        assertEquals(getStatisticsDataRowFromList(standardToTest, menu).getNumberOfGuiValidations(), BigInteger.valueOf(0));
        assertEquals(getStatisticsDataRowFromList(standardToTest, menu).getNumberOfWebServiceValidations(), BigInteger.valueOf(13));
        assertEquals(getStatisticsDataRowFromList(standardToTest, menu).getNumberOfPassedValidations(), BigInteger.valueOf(11));
        assertEquals(getStatisticsDataRowFromList(standardToTest, menu).getNumberOfFailedValidations(), BigInteger.valueOf(2));
        assertEquals(getStatisticsDataRowFromList(standardToTest, menu).getNumberOfUndefinedValidations(), BigInteger.valueOf(0));
    }

    @Test
    public void getTotalDataRowByGroupByCdaModelBasedAsip() throws Exception {
        List<StatisticsDataRow> statisticsDataRowList = validationDao.countTotalDataRowGroupByReferencedStandard();
        String standardToTest = "CDA ModelBased (ASIP)";
        String menu = "TEST";
        assertEquals(getStatisticsDataRowFromList(standardToTest, menu).getNumberOfValidations(), BigInteger.valueOf(15));
        assertEquals(getStatisticsDataRowFromList(standardToTest, menu).getNumberOfGuiValidations(), BigInteger.valueOf(0));
        assertEquals(getStatisticsDataRowFromList(standardToTest, menu).getNumberOfWebServiceValidations(), BigInteger.valueOf(15));
        assertEquals(getStatisticsDataRowFromList(standardToTest, menu).getNumberOfPassedValidations(), BigInteger.valueOf(12));
        assertEquals(getStatisticsDataRowFromList(standardToTest, menu).getNumberOfFailedValidations(), BigInteger.valueOf(2));
        assertEquals(getStatisticsDataRowFromList(standardToTest, menu).getNumberOfUndefinedValidations(), BigInteger.valueOf(1));
    }

    @Test
    public void getTotalDataRowByGroupByFhir() throws Exception {
        List<StatisticsDataRow> statisticsDataRowList = validationDao.countTotalDataRowGroupByReferencedStandard();
        String standardToTest = "FHIR";
        String menu = "TEST";
        assertEquals(getStatisticsDataRowFromList(standardToTest, menu).getNumberOfValidations(), BigInteger.valueOf(22));
        assertEquals(getStatisticsDataRowFromList(standardToTest, menu).getNumberOfGuiValidations(), BigInteger.valueOf(0));
        assertEquals(getStatisticsDataRowFromList(standardToTest, menu).getNumberOfWebServiceValidations(), BigInteger.valueOf(22));
        assertEquals(getStatisticsDataRowFromList(standardToTest, menu).getNumberOfPassedValidations(), BigInteger.valueOf(22));
        assertEquals(getStatisticsDataRowFromList(standardToTest, menu).getNumberOfFailedValidations(), BigInteger.valueOf(0));
        assertEquals(getStatisticsDataRowFromList(standardToTest, menu).getNumberOfUndefinedValidations(), BigInteger.valueOf(0));
    }

    @Test
    public void getTotalDataRowByGroupByHL7v2Ihe() throws Exception {
        List<StatisticsDataRow> statisticsDataRowList = validationDao.countTotalDataRowGroupByReferencedStandard();
        String standardToTest = "HL7v2.x (IHE)";
        String menu = "TEST";
        assertEquals(getStatisticsDataRowFromList(standardToTest, menu).getNumberOfValidations(), BigInteger.valueOf(11));
        assertEquals(getStatisticsDataRowFromList(standardToTest, menu).getNumberOfGuiValidations(), BigInteger.valueOf(0));
        assertEquals(getStatisticsDataRowFromList(standardToTest, menu).getNumberOfWebServiceValidations(), BigInteger.valueOf(11));
        assertEquals(getStatisticsDataRowFromList(standardToTest, menu).getNumberOfPassedValidations(), BigInteger.valueOf(11));
        assertEquals(getStatisticsDataRowFromList(standardToTest, menu).getNumberOfFailedValidations(), BigInteger.valueOf(0));
        assertEquals(getStatisticsDataRowFromList(standardToTest, menu).getNumberOfUndefinedValidations(), BigInteger.valueOf(0));
    }

    @Test
    public void getTotalDataRowByGroupByHL7v2Test() throws Exception {
        List<StatisticsDataRow> statisticsDataRowList = validationDao.countTotalDataRowGroupByReferencedStandard();
        String standardToTest = "HL7v2.x (IHE)";
        String menu = "IHE";
        assertEquals(getStatisticsDataRowFromList(standardToTest, menu).getNumberOfValidations(), BigInteger.valueOf(11));
        assertEquals(getStatisticsDataRowFromList(standardToTest, menu).getNumberOfGuiValidations(), BigInteger.valueOf(0));
        assertEquals(getStatisticsDataRowFromList(standardToTest, menu).getNumberOfWebServiceValidations(), BigInteger.valueOf(11));
        assertEquals(getStatisticsDataRowFromList(standardToTest, menu).getNumberOfPassedValidations(), BigInteger.valueOf(11));
        assertEquals(getStatisticsDataRowFromList(standardToTest, menu).getNumberOfFailedValidations(), BigInteger.valueOf(0));
        assertEquals(getStatisticsDataRowFromList(standardToTest, menu).getNumberOfUndefinedValidations(), BigInteger.valueOf(0));
    }

    @Test
    public void getTotalDataRowByGroupByCdaAsip() throws Exception {
        List<StatisticsDataRow> statisticsDataRowList = validationDao.countTotalDataRowGroupByReferencedStandard();
        String standardToTest = "CDA Schematron (ASIP)";
        String menu = "TEST";
        assertEquals(getStatisticsDataRowFromList(standardToTest, menu).getNumberOfValidations(), BigInteger.valueOf(12));
        assertEquals(getStatisticsDataRowFromList(standardToTest, menu).getNumberOfGuiValidations(), BigInteger.valueOf(0));
        assertEquals(getStatisticsDataRowFromList(standardToTest, menu).getNumberOfWebServiceValidations(), BigInteger.valueOf(12));
        assertEquals(getStatisticsDataRowFromList(standardToTest, menu).getNumberOfPassedValidations(), BigInteger.valueOf(11));
        assertEquals(getStatisticsDataRowFromList(standardToTest, menu).getNumberOfFailedValidations(), BigInteger.valueOf(1));
        assertEquals(getStatisticsDataRowFromList(standardToTest, menu).getNumberOfUndefinedValidations(), BigInteger.valueOf(0));
    }

    @Test
    public void getTotalDataRowByGroupByXDSAsip() throws Exception {
        List<StatisticsDataRow> statisticsDataRowList = validationDao.countTotalDataRowGroupByReferencedStandard();
        String standardToTest = "XD* metadata (ASIP)";
        String menu = "TEST";
        assertEquals(getStatisticsDataRowFromList(standardToTest, menu).getNumberOfValidations(), BigInteger.valueOf(11));
        assertEquals(getStatisticsDataRowFromList(standardToTest, menu).getNumberOfGuiValidations(), BigInteger.valueOf(0));
        assertEquals(getStatisticsDataRowFromList(standardToTest, menu).getNumberOfWebServiceValidations(), BigInteger.valueOf(11));
        assertEquals(getStatisticsDataRowFromList(standardToTest, menu).getNumberOfPassedValidations(), BigInteger.valueOf(11));
        assertEquals(getStatisticsDataRowFromList(standardToTest, menu).getNumberOfFailedValidations(), BigInteger.valueOf(0));
        assertEquals(getStatisticsDataRowFromList(standardToTest, menu).getNumberOfUndefinedValidations(), BigInteger.valueOf(0));
    }

    @Test
    public void getTotalDataRowByGroupByFhirUrl() throws Exception {
        List<StatisticsDataRow> statisticsDataRowList = validationDao.countTotalDataRowGroupByReferencedStandard();
        String standardToTest = "FHIR URL";
        String menu = "TEST";
        assertEquals(getStatisticsDataRowFromList(standardToTest, menu).getNumberOfValidations(), BigInteger.valueOf(11));
        assertEquals(getStatisticsDataRowFromList(standardToTest, menu).getNumberOfGuiValidations(), BigInteger.valueOf(0));
        assertEquals(getStatisticsDataRowFromList(standardToTest, menu).getNumberOfWebServiceValidations(), BigInteger.valueOf(11));
        assertEquals(getStatisticsDataRowFromList(standardToTest, menu).getNumberOfPassedValidations(), BigInteger.valueOf(11));
        assertEquals(getStatisticsDataRowFromList(standardToTest, menu).getNumberOfFailedValidations(), BigInteger.valueOf(0));
        assertEquals(getStatisticsDataRowFromList(standardToTest, menu).getNumberOfUndefinedValidations(), BigInteger.valueOf(0));
    }

    @Test
    public void getTotalDataRowByGroupByHprimIheFrance() throws Exception {
        List<StatisticsDataRow> statisticsDataRowList = validationDao.countTotalDataRowGroupByReferencedStandard();
        String standardToTest = "Hprim XML (IHE France)";
        String menu = "IHE France";
        assertEquals(getStatisticsDataRowFromList(standardToTest, menu).getNumberOfValidations(), BigInteger.valueOf(11));
        assertEquals(getStatisticsDataRowFromList(standardToTest, menu).getNumberOfGuiValidations(), BigInteger.valueOf(0));
        assertEquals(getStatisticsDataRowFromList(standardToTest, menu).getNumberOfWebServiceValidations(), BigInteger.valueOf(11));
        assertEquals(getStatisticsDataRowFromList(standardToTest, menu).getNumberOfPassedValidations(), BigInteger.valueOf(11));
        assertEquals(getStatisticsDataRowFromList(standardToTest, menu).getNumberOfFailedValidations(), BigInteger.valueOf(0));
        assertEquals(getStatisticsDataRowFromList(standardToTest, menu).getNumberOfUndefinedValidations(), BigInteger.valueOf(0));
    }

    @Test
    public void getTotalDataRowByGroupByHL7v2IheFrance() throws Exception {
        List<StatisticsDataRow> statisticsDataRowList = validationDao.countTotalDataRowGroupByReferencedStandard();
        String standardToTest = "HL7v2.x (IHE France)";
        String menu = "IHE France";
        assertEquals(getStatisticsDataRowFromList(standardToTest, menu).getNumberOfValidations(), BigInteger.valueOf(11));
        assertEquals(getStatisticsDataRowFromList(standardToTest, menu).getNumberOfGuiValidations(), BigInteger.valueOf(0));
        assertEquals(getStatisticsDataRowFromList(standardToTest, menu).getNumberOfWebServiceValidations(), BigInteger.valueOf(11));
        assertEquals(getStatisticsDataRowFromList(standardToTest, menu).getNumberOfPassedValidations(), BigInteger.valueOf(11));
        assertEquals(getStatisticsDataRowFromList(standardToTest, menu).getNumberOfFailedValidations(), BigInteger.valueOf(0));
        assertEquals(getStatisticsDataRowFromList(standardToTest, menu).getNumberOfUndefinedValidations(), BigInteger.valueOf(0));
    }

    private StatisticsDataRow getStatisticsDataRowFromList(String name, String menu) throws Exception {
        for (StatisticsDataRow statisticsDataRow : statisticsDataRowList) {
            if(statisticsDataRow.getName().equalsIgnoreCase(name) && statisticsDataRow.getMenu().equalsIgnoreCase(menu)) {
                return statisticsDataRow;
            }
        }
        throw new Exception(menu + " - " + name + " not found in data");
    }


}
