package net.ihe.gazelle.evsclient.statistics;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.application.statistics.StatisticsManager;
import net.ihe.gazelle.evsclient.factory.ApplicationFactoryTest;
import net.ihe.gazelle.evsclient.factory.DaoFactoryTest;
import net.ihe.gazelle.evsclient.interlay.gui.menu.EVSMenu;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import static org.junit.Assert.assertEquals;

/**
 * Integration tests for csv export
 */
@RunWith(MockitoJUnitRunner.class)
public class StatisticsManagerTest {

   private static EntityManagerFactory entityManagerFactory;

   private final ApplicationFactoryTest applicationFactoryTest;

   @InjectMocks
   private final StatisticsManager statisticsManager;

   @Mock
   private EVSMenu evsMenu;

   @Mock
   private ApplicationPreferenceManager applicationPreferenceManager;

   private final String expectedData;

   private final String header = "OID;Date de la validation;Menu - Références aux standards;Service de validation;" +
         "Version du service de validation;Validateur;Version du validateur;Statut de la validation;Interface;" +
         "Nom d'utilisateur;Organisation de l'utilisateur;Privé;Lien permanent";

   @BeforeClass
   public static void init() {
      entityManagerFactory = Persistence.createEntityManagerFactory("PersistenceUnitTestForStatistics");
   }

   /*
    * Here we use test factories to use in memory database. Real Dao implementations are used and not mocked because
    * generate mock for ValidationDao with enough data is pretty long.
    * So, be careful if headers or data-model change, you must update /test/resources/statistics/export_example.csv
    */
   public StatisticsManagerTest() throws Exception {
      MockitoAnnotations.initMocks(this);

      applicationFactoryTest = new ApplicationFactoryTest(
            new DaoFactoryTest(entityManagerFactory, applicationPreferenceManager), applicationPreferenceManager);

      mockEvsMenu();
      mockApplicationPreferenceManager();

      statisticsManager = applicationFactoryTest.getStatisticsManagerTest(evsMenu);

      expectedData = new String(
            Files.readAllBytes(
                  Paths.get(StatisticsManagerTest.class.getResource("/statistics/export_example.csv").toURI())),
            StandardCharsets.UTF_8);
   }

   private void mockEvsMenu() throws Exception {
      Mockito.when(evsMenu.getMenuNameByReferencedStandard(21)).thenReturn(Arrays.asList(new String[]{"TEST", "IHE"}));
      Mockito.when(evsMenu.getMenuNameByReferencedStandard(22)).thenReturn(Arrays.asList(new String[]{"IHE"}));
      Mockito.when(evsMenu.getMenuNameByReferencedStandard(11)).thenReturn(Arrays.asList(new String[]{"TEST"}));
      Mockito.when(evsMenu.getMenuNameByReferencedStandard(12)).thenReturn(Arrays.asList(new String[]{"TEST"}));
      Mockito.when(evsMenu.getMenuNameByReferencedStandard(19)).thenReturn(Arrays.asList(new String[]{"TEST"}));
      Mockito.when(evsMenu.getMenuNameByReferencedStandard(25)).thenReturn(Arrays.asList(new String[]{"IHE France"}));
      Mockito.when(evsMenu.getMenuNameByReferencedStandard(26)).thenReturn(Arrays.asList(new String[]{"IHE France"}));
      Mockito.when(evsMenu.getMenuNameByReferencedStandard(13)).thenReturn(Arrays.asList(new String[]{"ANS"}));
      Mockito.when(evsMenu.getMenuNameByReferencedStandard(14)).thenReturn(Arrays.asList(new String[]{"TEST"}));
      Mockito.when(evsMenu.getMenuNameByReferencedStandard(15)).thenReturn(Arrays.asList(new String[]{"ANS"}));
      Mockito.when(evsMenu.getMenuNameByReferencedStandard(16)).thenReturn(Arrays.asList(new String[]{"IHE"}));
      Mockito.when(evsMenu.getMenuNameByReferencedStandard(17)).thenReturn(Arrays.asList(new String[]{"IHE"}));
      Mockito.when(evsMenu.getMenuNameByReferencedStandard(20)).thenReturn(Arrays.asList(new String[]{"IHE"}));
      Mockito.when(evsMenu.getMenuNameByReferencedStandard(23)).thenReturn(Arrays.asList(new String[]{"IHE France"}));
      Mockito.when(evsMenu.getMenuNameByReferencedStandard(24)).thenReturn(Arrays.asList(new String[]{"TEST","IHE"}));
      Mockito.when(evsMenu.getMenuNameByReferencedStandard(27)).thenReturn(Arrays.asList(new String[]{"TEST"}));
      Mockito.when(evsMenu.getMenuNameByReferencedStandard(28)).thenReturn(Arrays.asList(new String[]{"TEST"}));
   }

   private void mockApplicationPreferenceManager() {
      Mockito.when(applicationPreferenceManager.getApplicationUrl()).thenReturn("http://localhost:8380/evs");
   }

   @Test
   public void createCsvDataWithNoValidations() throws Exception {
      Date startDate = new SimpleDateFormat("dd/MM/yyyy").parse("01/09/2021");
      Date endDate = new SimpleDateFormat("dd/MM/yyyy").parse("30/09/2021");
      String csvData = statisticsManager.createCsvData(startDate, endDate, header.split(","),
            applicationFactoryTest.getValidationGuiPermanentLinkTest());
      assertEquals(header.concat(";"), csvData.trim()); // we expect ";" at the end
   }


   @Test
   public void createCsvData() throws Exception {
      Date startDate = new SimpleDateFormat("dd/MM/yyyy").parse("01/12/2021");
      Date endDate = new SimpleDateFormat("dd/MM/yyyy").parse("30/12/2021");
      String csvData = statisticsManager.createCsvData(startDate, endDate, header.split(","),
            applicationFactoryTest.getValidationGuiPermanentLinkTest());
      assertEquals(expectedData, csvData);
   }

}
