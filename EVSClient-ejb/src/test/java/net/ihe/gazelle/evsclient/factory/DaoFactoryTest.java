package net.ihe.gazelle.evsclient.factory;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.application.validation.ValidationDao;
import net.ihe.gazelle.evsclient.application.validationservice.configuration.ValidationServiceConfDao;
import net.ihe.gazelle.evsclient.interlay.dao.validation.ValidationDaoImpl;
import net.ihe.gazelle.evsclient.interlay.dao.validationservice.configuration.ValidationServiceConfDaoImpl;

import javax.persistence.EntityManagerFactory;

public class DaoFactoryTest {

    private final EntityManagerFactory entityManagerFactory;

    private final ApplicationPreferenceManager applicationPreferenceManager;

    public DaoFactoryTest(EntityManagerFactory entityManagerFactory, ApplicationPreferenceManager applicationPreferenceManager) {
        this.entityManagerFactory = entityManagerFactory;
        this.applicationPreferenceManager = applicationPreferenceManager;
    }

    public ValidationDao getValidationDaoTest() {
        return new ValidationDaoImpl(entityManagerFactory, applicationPreferenceManager);
    }

    public ValidationServiceConfDao getServiceConfDaoTest() {
        return new ValidationServiceConfDaoImpl(entityManagerFactory);
    }
}
