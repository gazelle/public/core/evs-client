package net.ihe.gazelle.evsclient.interlay.ws.rest.validations;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.evsclient.application.security.ApiKeyIdentity;
import net.ihe.gazelle.evsclient.application.security.ApiKeyManager;
import net.ihe.gazelle.evsclient.application.security.InvalidApiKeyException;
import net.ihe.gazelle.evsclient.domain.processing.OwnerMetadata;
import net.ihe.gazelle.evsclient.domain.security.ApiKey;
import net.ihe.gazelle.evsclient.interlay.factory.ApplicationFactory;
import net.ihe.gazelle.evsclient.interlay.factory.EvsCommonApplicationFactory;
import net.ihe.gazelle.ssov7.authn.domain.GazelleIdentity;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import javax.activation.MimeType;
import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.assertEquals;

@RunWith(PowerMockRunner.class)
public class ValidationsApiServiceImplTest {

    ValidationsApiService validationsApiService;

    @Mock
    ApplicationFactory evsApplicationFactory;

    @Mock
    EvsCommonApplicationFactory evsCommonApplicationFactory;

    @Mock
    ApiKeyManager apiKeyManager;

    @Mock
    ApplicationPreferenceManager applicationPreferenceManager;

    @Before
    public void setUp() {
        validationsApiService = new ValidationsApiServiceImpl(evsApplicationFactory, evsCommonApplicationFactory, apiKeyManager,applicationPreferenceManager);
    }

    private ApiKey getValidApiKey(String apiKey) throws ParseException {
        return new ApiKey(apiKey, "ADMIN", "ADMIN_ORG", new Date(), new SimpleDateFormat( "yyyyMMdd" ).parse( "20990820" ));
    }

    @Test(expected = ValidationsApiServiceImpl.InvalidAuthorizationHeaderException.class)
    public void testAuthenticateInvalidAuthnHeaderError() throws Exception {
        String authorization = "bad key";
        Whitebox.invokeMethod(validationsApiService, "authenticate",
                authorization);
    }

    @Test(expected = InvalidApiKeyException.class)
    public void testAuthenticateWrongKeyError() throws Exception {
        String apiKey = "unknownKey";
        String authorization = "GazelleAPIKey " + apiKey;

        Mockito.doThrow(InvalidApiKeyException.class).when(apiKeyManager).authenticate(apiKey);
        Whitebox.invokeMethod(validationsApiService, "authenticate", authorization);
    }

    @Test
    public void parseOwnerMetadataWithUserNeedsToBeLoggedInAndGoodApiKey() throws Exception {
        // GIVEN
        HttpServletRequest request = Mockito.mock(HttpServletRequest.class);;
        String apiKey = "goodkey";
        String authorization = "GazelleAPIKey " + apiKey;
        String userIp = "0.0.0.0";

        Mockito.when(request.getHeader("X-Forwarded-For")).thenReturn(userIp);
        Mockito.when(apiKeyManager.authenticate(apiKey)).thenReturn(new ApiKeyIdentity("ADMIN", "ADMIN_ORG"));

        // WHEN
        GazelleIdentity identity = Whitebox.invokeMethod(validationsApiService, "authenticate", authorization);
        OwnerMetadata ownerMetadata = Whitebox.invokeMethod(validationsApiService, "buildOwnerMetadata", identity, request);

        //THEN
        assertEquals("ADMIN", ownerMetadata.getUsername());
        assertEquals("ADMIN_ORG", ownerMetadata.getOrganization());
        assertEquals(userIp, ownerMetadata.getUserIp());
    }

    @Test(expected = InvalidMimeTypeException.class)
    public void parseMimeTypeWithNoAcceptedContentType() throws Exception {
        Whitebox.invokeMethod(validationsApiService, "parseMimeType",
                new Class[] {String.class, (new MimeType[] {}).getClass()},
                null, new MimeType[] {});
    }

    @Test
    public void parseMimeTypeWithAcceptedContentType() throws Exception {
        MimeType mimetype = Whitebox.invokeMethod(validationsApiService, "parseMimeType",
                new Class[] {String.class, (new MimeType[] {}).getClass()},
                "application/xml", new MimeType[] {new MimeType("application/xml"),
                        new MimeType("application/json")});
        assertEquals(mimetype.getPrimaryType(), "application");
        assertEquals(mimetype.getSubType(), "xml");
    }
}
