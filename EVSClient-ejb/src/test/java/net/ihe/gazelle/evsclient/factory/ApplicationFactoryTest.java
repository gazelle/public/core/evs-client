package net.ihe.gazelle.evsclient.factory;

import net.ihe.gazelle.common.application.action.ApplicationPreferenceManager;
import net.ihe.gazelle.common.application.action.ApplicationPreferenceManagerImpl;
import net.ihe.gazelle.evsclient.application.AbstractGuiPermanentLink;
import net.ihe.gazelle.evsclient.application.interfaces.GuiPermanentLink;
import net.ihe.gazelle.evsclient.application.statistics.StatisticsManager;
import net.ihe.gazelle.evsclient.domain.validation.Validation;
import net.ihe.gazelle.evsclient.interlay.factory.DaoFactory;
import net.ihe.gazelle.evsclient.interlay.gui.Pages;
import net.ihe.gazelle.evsclient.interlay.gui.menu.EVSMenu;
import net.ihe.gazelle.evsclient.interlay.util.CsvExportMapperImpl;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Factory;

public class ApplicationFactoryTest {

    private final ApplicationPreferenceManager applicationPreferenceManager;

    private final DaoFactoryTest daoFactory;

    public ApplicationFactoryTest(DaoFactoryTest daoFactory, ApplicationPreferenceManager applicationPreferenceManager) {
        this.daoFactory = daoFactory;
        this.applicationPreferenceManager = applicationPreferenceManager;
    }

    public StatisticsManager getStatisticsManagerTest(EVSMenu evsMenu) {
        return new StatisticsManager(daoFactory.getValidationDaoTest(), daoFactory.getServiceConfDaoTest(), new CsvExportMapperImpl(), evsMenu);
    }

    public GuiPermanentLink<Validation> getValidationGuiPermanentLinkTest() {
        return new AbstractGuiPermanentLink<Validation>(applicationPreferenceManager) {
            @Override
            public String getResultPageUrl() {
                return Pages.DETAILED_RESULT.getMenuLink();
            }
        };
    }
}
