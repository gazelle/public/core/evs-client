<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:fo="http://www.w3.org/1999/XSL/Format" exclude-result-prefixes="fo">
    <xsl:output/>
    <xsl:template match="detailedResult">
        <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">

            
            <fo:layout-master-set>
                <fo:simple-page-master master-name="A4-portrait"
                    page-height="29.7cm" page-width="21.0cm" margin-top="1cm"
                    margin-left="1.5cm" margin-right="1cm" margin-bottom="1cm">
                    <fo:region-body />
                    <fo:region-after region-name="xsl-region-after" extent="10mm"/>
                    <fo:region-start region-name="xsl-region-start" extent="10mm"/>
                </fo:simple-page-master>
            </fo:layout-master-set>
            <fo:page-sequence master-reference="A4-portrait" initial-page-number="1">
           
                <fo:static-content flow-name="xsl-region-after">
                    <fo:block text-align="center">
                        Page <fo:page-number/> of <fo:page-number-citation ref-id="end"/>
                    </fo:block>
                </fo:static-content>
                
                <fo:flow flow-name="xsl-region-body">
                    <fo:block font-size="16pt" font-weight="bold" space-after="5mm">Company Name:
                    </fo:block>
                    <fo:block font-size="10pt">
                        <fo:table table-layout="fixed" width="100%" border-collapse="separate">
                            <fo:table-column column-width="2cm"/>
                            <fo:table-column column-width="8cm"/>
                            <fo:table-body>
                                <xsl:apply-templates select="//Info"/>
                            </fo:table-body>
                        </fo:table>
                    </fo:block>
                    <fo:block id="end"/>
                </fo:flow>
                
              
                
            </fo:page-sequence>
        </fo:root>
    </xsl:template>
    
    <xsl:template match="Info">
        <fo:table-row  border="solid 1mm black">
            <fo:table-cell>
                <fo:block>
                    Test
                </fo:block>
            </fo:table-cell>

            <fo:table-cell>
                <fo:block>
                    <xsl:value-of select="Test"/>
                </fo:block>
            </fo:table-cell>
        </fo:table-row>
    </xsl:template>
</xsl:stylesheet>