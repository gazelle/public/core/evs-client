<?xml version="1.0" encoding="UTF-8"?>
<!--
  ~ EVS Client is part of the Gazelle Test Bed
  ~ Copyright (C) 2006-2016 IHE
  ~ mailto :eric DOT poiseau AT inria DOT fr
  ~
  ~ See the NOTICE file distributed with this work for additional information
  ~ regarding copyright ownership.  This code is licensed
  ~ to you under the Apache License, Version 2.0 (the
  ~ "License"); you may not use this file except in compliance
  ~ with the License.  You may obtain a copy of the License at
  ~
  ~   http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~ Unless required by applicable law or agreed to in writing,
  ~ software distributed under the License is distributed on an
  ~ "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
  ~ KIND, either express or implied.  See the License for the
  ~ specific language governing permissions and limitations
  ~ under the License.
  -->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="1.0">
    <xsl:output encoding="UTF-8" indent="yes" method="html"
                media-type="text/html"/>
    <xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" scope="stylesheet">
        <xd:desc>
            <xd:p>
                <xd:b>Created on:</xd:b>
                June 30, 2011
            </xd:p>
            <xd:p>
                <xd:b>Author:</xd:b>
                Anne-Gaëlle BERGE, IHE Development, INRIA Rennes
            </xd:p>
            <xd:p></xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template match="/">
        <div>
            <table border="0">
                <xsl:if test="count(detailedResult/DocumentWellFormed) = 1">
                    <tr>
                        <td>
                            <b>
                                <a href="#wellformed">XML</a>
                            </b>
                        </td>
                        <td>
                            <xsl:choose>
                                <xsl:when
                                        test="contains(detailedResult/DocumentWellFormed/Result, 'PASSED')">
                                    <div class="PASSED">
                                        PASSED
                                    </div>
                                </xsl:when>
                                <xsl:otherwise>
                                    <div class="FAILED">
                                        FAILED
                                    </div>
                                </xsl:otherwise>
                            </xsl:choose>
                        </td>
                    </tr>
                </xsl:if>
                <xsl:if test="count(detailedResult/DocumentValidCDA) = 1">
                    <tr>
                        <td>
                            <b>
                                <a href="#xsd">CDA</a>
                            </b>
                            (
                            <a href="http://gazelle.ihe.net/xsd/CDA.xsd">CDA.xsd</a>
                            )
                        </td>
                        <td>
                            <xsl:choose>
                                <xsl:when
                                        test="contains(detailedResult/DocumentValidCDA/Result, 'PASSED')">
                                    <div class="PASSED">
                                        PASSED
                                    </div>
                                </xsl:when>
                                <xsl:otherwise>
                                    <div class="FAILED">
                                        FAILED
                                    </div>
                                </xsl:otherwise>
                            </xsl:choose>
                        </td>
                    </tr>
                </xsl:if>
                <xsl:if test="count(detailedResult/DocumentValidEpsos) = 1">
                    <tr>
                        <td>
                            <b>
                                <a href="#xsd">epSOS</a>
                            </b>
                            (
                            <a href="http://gazelle.ihe.net/xsd/CDA_extended.xsd">CDA_extended.xsd</a>
                            )
                        </td>
                        <td>
                            <xsl:choose>
                                <xsl:when
                                        test="contains(detailedResult/DocumentValidEpsos/Result, 'PASSED')">
                                    <div class="PASSED">
                                        PASSED
                                    </div>
                                </xsl:when>
                                <xsl:otherwise>
                                    <div class="FAILED">
                                        FAILED
                                    </div>
                                </xsl:otherwise>
                            </xsl:choose>
                        </td>
                    </tr>
                </xsl:if>
                <xsl:if test="count(detailedResult/DocumentValid) = 1">
                    <tr>
                        <td>
                            <b>
                                <a href="#xsd">XSD</a>
                            </b>
                        </td>
                        <td>
                            <xsl:choose>
                                <xsl:when
                                        test="contains(detailedResult/DocumentValid/Result, 'PASSED')">
                                    <div class="PASSED">
                                        PASSED
                                    </div>
                                </xsl:when>
                                <xsl:otherwise>
                                    <div class="FAILED">
                                        FAILED
                                    </div>
                                </xsl:otherwise>
                            </xsl:choose>
                        </td>
                    </tr>
                </xsl:if>
                <xsl:if test="count(detailedResult/MIFValidation) = 1">
                    <tr>
                        <td>
                            <b>
                                <a href="#mif">MIF</a>
                            </b>
                        </td>
                        <td>
                            <xsl:choose>
                                <xsl:when
                                        test="contains(detailedResult/MIFValidation/Result, 'PASSED')">
                                    <div class="PASSED">
                                        PASSED
                                    </div>
                                </xsl:when>
                                <xsl:otherwise>
                                    <div class="FAILED">
                                        FAILED
                                    </div>
                                </xsl:otherwise>
                            </xsl:choose>
                        </td>
                    </tr>
                </xsl:if>
                <xsl:if test="count(detailedResult/SchematronValidation) = 1">
                    <tr>
                        <td>
                            <b>
                                <a href="#schematron">Schematron</a>
                            </b>
                        </td>
                        <td>
                            <xsl:if
                                    test="contains(detailedResult/SchematronValidation/Result, 'PASSED')">
                                <div class="PASSED">
                                    <xsl:value-of select="detailedResult/SchematronValidation/Result"/>
                                </div>
                            </xsl:if>
                            <xsl:if
                                    test="contains(detailedResult/SchematronValidation/Result, 'FAILED')">
                                <div class="FAILED">
                                    <xsl:value-of select="detailedResult/SchematronValidation/Result"/>
                                </div>
                            </xsl:if>
                            <xsl:if
                                    test="contains(detailedResult/SchematronValidation/Result, 'ABORTED')">
                                <div class="ABORTED">
                                    <xsl:value-of select="detailedResult/SchematronValidation/Result"/>
                                </div>
                            </xsl:if>
                        </td>
                    </tr>
                </xsl:if>
            </table>
        </div>
    </xsl:template>
</xsl:stylesheet>
                