<?xml version="1.0" encoding="UTF-8"?>
<!--
  ~ EVS Client is part of the Gazelle Test Bed
  ~ Copyright (C) 2006-2016 IHE
  ~ mailto :eric DOT poiseau AT inria DOT fr
  ~
  ~ See the NOTICE file distributed with this work for additional information
  ~ regarding copyright ownership.  This code is licensed
  ~ to you under the Apache License, Version 2.0 (the
  ~ "License"); you may not use this file except in compliance
  ~ with the License.  You may obtain a copy of the License at
  ~
  ~   http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~ Unless required by applicable law or agreed to in writing,
  ~ software distributed under the License is distributed on an
  ~ "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
  ~ KIND, either express or implied.  See the License for the
  ~ specific language governing permissions and limitations
  ~ under the License.
  -->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="1.0">
    <xsl:output encoding="UTF-8" indent="yes" method="xml"
                omit-xml-declaration="yes"/>
    <xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" scope="stylesheet">
        <xd:desc>
            <xd:p>
                <xd:b>Created on:</xd:b>
                Aug 31, 2010
            </xd:p>
            <xd:p>
                <xd:b>Author:</xd:b>
                Anne-Gaëlle BERGE, IHE Development, INRIA Rennes
            </xd:p>
            <xd:p></xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template match="/">
        <div>
            <xsl:if
                    test="count(SummaryResults/ValidationCounters/NrOfValidationErrors) = 1">
                <xsl:choose>
                    <xsl:when
                            test="SummaryResults/ValidationCounters/NrOfValidationErrors &gt; 0">
                        <a href="#errors">
                            <xsl:value-of
                                    select="SummaryResults/ValidationCounters/NrOfValidationErrors"/>
                            error(s)
                        </a>
                    </xsl:when>
                    <xsl:otherwise>
                        No error
                    </xsl:otherwise>
                </xsl:choose>
                <br/>
            </xsl:if>
            <xsl:if
                    test="count(SummaryResults/ValidationCounters/NrOfValidationWarnings) = 1">
                <xsl:choose>
                    <xsl:when
                            test="SummaryResults/ValidationCounters/NrOfValidationWarnings &gt; 0">
                        <a href="#warnings">
                            <xsl:value-of
                                    select="SummaryResults/ValidationCounters/NrOfValidationWarnings"/>
                            warning(s)
                        </a>
                    </xsl:when>
                    <xsl:otherwise>
                        No warning
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:if>
        </div>
    </xsl:template>
</xsl:stylesheet>