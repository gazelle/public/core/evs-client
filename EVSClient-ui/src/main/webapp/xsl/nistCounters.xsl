<?xml version="1.0" encoding="UTF-8"?>
<!--
  ~ EVS Client is part of the Gazelle Test Bed
  ~ Copyright (C) 2006-2016 IHE
  ~ mailto :eric DOT poiseau AT inria DOT fr
  ~
  ~ See the NOTICE file distributed with this work for additional information
  ~ regarding copyright ownership.  This code is licensed
  ~ to you under the Apache License, Version 2.0 (the
  ~ "License"); you may not use this file except in compliance
  ~ with the License.  You may obtain a copy of the License at
  ~
  ~   http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~ Unless required by applicable law or agreed to in writing,
  ~ software distributed under the License is distributed on an
  ~ "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
  ~ KIND, either express or implied.  See the License for the
  ~ specific language governing permissions and limitations
  ~ under the License.
  -->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="1.0">
    <xsl:output encoding="UTF-8" indent="yes" method="xml" omit-xml-declaration="yes"/>
    <xd:doc xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" scope="stylesheet">
        <xd:desc>
            <xd:p>
                <xd:b>Created on:</xd:b>
                Aug 31, 2010
            </xd:p>
            <xd:p>
                <xd:b>Author:</xd:b>
                Anne-Gaëlle BERGE, IHE Development, INRIA Rennes
            </xd:p>
            <xd:p></xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:template match="/">
        <div>
            <xsl:choose>
                <xsl:when
                        test="count(ValidationReport/ReportHeader/ErrorCount) &gt; 0">
                    <a href="#errors">
                        <xsl:value-of
                                select="ValidationReport/ReportHeader/ErrorCount"/> error(s)
                    </a>
                </xsl:when>
                <xsl:otherwise>
                    No error
                </xsl:otherwise>
            </xsl:choose>
            <br/>
            <xsl:choose>
                <xsl:when
                        test="ValidationReport/ReportHeader/WarningCount &gt; 0">
                    <a href="#warnings">
                        <xsl:value-of
                                select="ValidationReport/ReportHeader/WarningCount"/> warning(s)
                    </a>
                </xsl:when>
                <xsl:otherwise>
                    No warning
                </xsl:otherwise>
            </xsl:choose>
            <br/>
            <xsl:if test="count(ValidationReport/ReportHeader/UserCount) = 1">
                <xsl:choose>
                    <xsl:when
                            test="ValidationReport/ReportHeader/UserCount &gt; 0">
                        <a href="#users">
                            <xsl:value-of
                                    select="ValidationReport/ReportHeader/UserCount"/> user failure(s)
                        </a>
                    </xsl:when>
                    <xsl:otherwise>
                        No user failure
                    </xsl:otherwise>
                </xsl:choose>
                <br/>
            </xsl:if>
            <xsl:if test="count(ValidationReport/ReportDetails/Hl7v2ValidationReport/MessageValidation/Result/@IgnoreCount) = 1">
                <xsl:choose>
                    <xsl:when
                            test="ValidationReport/ReportDetails/Hl7v2ValidationReport/MessageValidation/Result/@IgnoreCount &gt; 0">
                        <a href="#ignores">
                            <xsl:value-of
                                    select="ValidationReport/ReportDetails/Hl7v2ValidationReport/MessageValidation/Result/@IgnoreCount"/>
                            ignore failure(s)
                        </a>
                    </xsl:when>
                    <xsl:otherwise>
                        No ignore failure
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:if>
        </div>
    </xsl:template>
</xsl:stylesheet>