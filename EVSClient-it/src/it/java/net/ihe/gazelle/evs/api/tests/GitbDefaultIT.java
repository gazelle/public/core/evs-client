package net.ihe.gazelle.evs.api.tests;

import net.ihe.gazelle.evs.api.config.validation.ValidationRequest;
import org.junit.Test;

public class GitbDefaultIT extends DefaultAdapterIT {

    @Override
    protected ValidationRequest getRequest() {
        return given().gitb().oots();
    }

    @Test
    @SuppressWarnings("java:S2699") // assertion is in called methods
    public void validationWrongEncodingFile() {
        validationWrongContent(
                getRequest(),
                "GITB-wrong-encoding.base64",
                false
        );
    }

    @Test
    @SuppressWarnings("java:S2699") // assertion is in called methods
    public void validationWrongEncoding() {
        validationWrongEncodingFile(
                getRequest()
        );
    }

    protected void validationWrongEncodingFile(ValidationRequest request) {
        publicValidationFailed(request
                .modifyContent("......$","XXXXXX"));
    }
}
