package net.ihe.gazelle.evs.api.config.gherkin;

import net.ihe.gazelle.evs.api.config.validation.Validation;

public abstract class ThenAccessValidation<T> {


    protected Validation validationResult;

    public ThenAccessValidation(Validation validationResult) {
        this.validationResult = validationResult;
    }

    public T success() {
        validationResult.getResponse().then().statusCode(200);
        return (T) this;
    }

    public T notFound() {
        validationResult.getResponse().then().statusCode(404);
        return (T) this;
    }

    public T notAcceptable() {
        validationResult.getResponse().then().statusCode(406);
        return (T) this;
    }

    public T unauthorized() {
        validationResult.getResponse().then().statusCode(401);
        return (T) this;
    }
    public T forbidden() {
        validationResult.getResponse().then().statusCode(403);
        return (T) this;
    }

    public T badRequest() {
        validationResult.getResponse().then().statusCode(400);
        return (T)this;
    }
    public WhenAccessValidation when() {
        return new WhenAccessValidation(validationResult.getValidationResponse());
    }

}
