package net.ihe.gazelle.evs.api.config.gherkin;

import net.ihe.gazelle.evs.api.config.validation.Validation;
import net.ihe.gazelle.evs.api.config.validation.ValidationResult;

import static org.hamcrest.Matchers.equalTo;

public class ThenAccessValidationResult extends ThenAccessValidation<ThenAccessValidationResult> {

    public ThenAccessValidationResult(Validation validationResult) {
        super(validationResult);
    }

    public void validationPassed() {
        validationResult.getResponse().then().body("validation.status",equalTo("DONE_PASSED"));
    }

    public void validationFailed() {
        validationResult.getResponse().then().body("validation.status",equalTo("DONE_FAILED"));
    }

    public void validationUndefined() {
        validationResult.getResponse().then().body("validation.status",equalTo("DONE_UNDEFINED"));
    }


}
