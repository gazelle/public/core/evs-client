package net.ihe.gazelle.evs.api.config;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import io.restassured.RestAssured;
import io.restassured.config.LogConfig;
import io.restassured.config.RestAssuredConfig;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import net.ihe.gazelle.evs.api.IT;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.URI;
import java.util.Map;

public class RestApiConnector {

    private static Logger logger = Logger.getLogger(RestApiConnector.class);
    private static RestApiConnector instance = null;

    private final String VALIDATION_PATH_PREFIX = "validations/";

    protected RestAssuredConfig config;
    private LoggingOutputStream los;
    private URI validationUri;
    private String apiKeyTokenName;

    public static RestApiConnector getInstance() {
        if(instance == null) {
            instance = new RestApiConnector();
        }
        return instance;
    }

    private RestApiConnector() {
        initApiConfig();

        RestAssured.reset();

        los = new LoggingOutputStream(logger, Level.INFO);
        config = RestAssuredConfig.newConfig().logConfig(new LogConfig(new PrintStream(los),true));
    }

    private void initApiConfig() {
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        try {
            JsonNode apiConfig;
            try {
                apiConfig = mapper.readTree(IT.class.getResourceAsStream("config/"+System.getProperty("api-it.configuration","dev/"+System.getProperty("user.name")+"/api-it.yml")));
            } catch (IOException e) {
                apiConfig = mapper.readTree(IT.class.getResourceAsStream("config/api-it.yml"));
            }
            String host = apiConfig.at("/it/evs/ws/host").asText();
            String path = apiConfig.at("/it/evs/ws/path").asText();

            apiKeyTokenName = apiConfig.at("/it/evs/ws/api-key-token-name").asText();
            validationUri = URI.create(System.getProperty("it.evs-ws-host", host).replaceFirst("(.*?)[/]*$","$1/")).resolve(System.getProperty("it.evs-ws-path", path)).resolve(VALIDATION_PATH_PREFIX);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public URI getValidationUri() {
        return validationUri;
    }

    public Response publicValidation(String body) {
        return postRequest(body, null, true);
    }

    public Response privateValidation(String body, String apiKey, boolean wellFormed) {
        return postRequest(body, apiKey, wellFormed);
    }

    private Response postRequest(String body, String apiKey, boolean wellFormed) {
        RequestSpecification request = request(RestAssured.given()
                .contentType(ContentType.XML)
                .body(body));

        // send request
        request = appendApiKey(apiKey, wellFormed, request);
        return response(request
                .when()
                .post(validationUri));
    }

    public Response getRequest(String uri) {
        return getRequest(uri, ContentType.XML);
    }

    public Response getRequest(String uri, ContentType acceptedContentType) {
        return getRequest(uri,acceptedContentType, null, true, null);
    }

    public Response getRequest(String uri, String apiKey, boolean wellFormed, String privacyKey) {
        return getRequest(uri, ContentType.XML, apiKey, wellFormed, privacyKey);
    }

    private Response getRequest(String uri, ContentType acceptedContentType, String apiKey, boolean wellFormed, String privacyKey) {
        RequestSpecification request = request(RestAssured.given());
        request = appendApiKey(apiKey, wellFormed, request);
        request = appendAcceptedContentType(acceptedContentType, request);
        request = appendPrivacyKey(privacyKey, request);
        return response(request
                .when()
                .get(URI.create(uri)));
    }

    public Response getParameterizedRequest(String uri, ContentType acceptedContentType, String apiKey, boolean wellFormed, String privacyKey,Map<String,String> params) {
        RequestSpecification request = request(RestAssured.given());
        request = appendApiKey(apiKey, wellFormed, request);
        request = appendAcceptedContentType(acceptedContentType, request);
        request = appendPrivacyKey(privacyKey, request);
        if (params!=null) {
            for (Map.Entry<String,String> param:params.entrySet()) {
                appendParameter(param.getKey(),param.getValue(),request);
            }
        }
        return response(request
                .when()
                .get(URI.create(uri)));
    }

    private RequestSpecification appendParameter(String key, String value, RequestSpecification request) {
        if (key != null && value!=null) {
            request = request.queryParam(key, value);
        }
        return request;
    }

    private RequestSpecification appendApiKey(String apiKey, boolean wellFormed, RequestSpecification request) {
        if(apiKey != null){
            String authorizationHeader = wellFormed ? apiKeyTokenName + " " + apiKey : apiKey;
            // token name missing if not well formed
            request = request.header("Authorization", authorizationHeader);
        }
        return request;
    }

    private RequestSpecification appendPrivacyKey(String privacyKey, RequestSpecification request) {
        if (privacyKey != null) {
            request = request.queryParam("privacyKey",privacyKey);
        }
        return request;
    }

    private RequestSpecification appendAcceptedContentType(ContentType acceptedContentType, RequestSpecification request) {
        if (acceptedContentType != null) {
            request = request.accept(acceptedContentType.getAcceptHeader());
        }
        return request;
    }

    private RequestSpecification request(RequestSpecification request) {
        logger.info(" <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< ");
        logger.info(" <<<<<<<<<< "+Thread.currentThread().getStackTrace()[2].getMethodName());
        request.config(config).log().all();
        logger.info(" <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< ");
        los.flush();
        return request;
    }

    private Response response(Response response) {
        response.then().log().all();
        los.flush();
        logger.info(" >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> ");
        logger.info("");
        logger.info("");
        return response;
    }

    class LoggingOutputStream extends OutputStream {

        /**
         * Default number of bytes in the buffer.
         */
        private static final int DEFAULT_BUFFER_LENGTH = 2048;

        /**
         * Indicates stream state.
         */
        private boolean hasBeenClosed = false;

        /**
         * Internal buffer where data is stored.
         */
        private byte[] buf;

        /**
         * The number of valid bytes in the buffer.
         */
        private int count;

        /**
         * Remembers the size of the buffer.
         */
        private int curBufLength;

        /**
         * The logger to write to.
         */
        private Logger log;

        /**
         * The log level.
         */
        private Level level;

        /**
         * Creates the Logging instance to flush to the given logger.
         *
         * @param log         the Logger to write to
         * @param level       the log level
         * @throws IllegalArgumentException in case if one of arguments is  null.
         */
        public LoggingOutputStream(final Logger log,
                                   final Level level)
                throws IllegalArgumentException {
            if (log == null || level == null) {
                throw new IllegalArgumentException(
                        "Logger or log level must be not null");
            }
            this.log = log;
            this.level = level;
            curBufLength = DEFAULT_BUFFER_LENGTH;
            buf = new byte[curBufLength];
            count = 0;
        }

        /**
         * Writes the specified byte to this output stream.
         *
         * @param b the byte to write
         * @throws IOException if an I/O error occurs.
         */
        public void write(final int b) throws IOException {
            if (hasBeenClosed) {
                throw new IOException("The stream has been closed.");
            }
            // don't log nulls
            if (b == 0) {
                return;
            }
            // would this be writing past the buffer?
            if (count == curBufLength) {
                // grow the buffer
                final int newBufLength = curBufLength +
                        DEFAULT_BUFFER_LENGTH;
                final byte[] newBuf = new byte[newBufLength];
                System.arraycopy(buf, 0, newBuf, 0, curBufLength);
                buf = newBuf;
                curBufLength = newBufLength;
            }

            buf[count] = (byte) b;
            count++;
        }

        /**
         * Flushes this output stream and forces any buffered output
         * bytes to be written out.
         */
        public void flush() {
            if (count == 0) {
                return;
            }
            final byte[] bytes = new byte[count];
            System.arraycopy(buf, 0, bytes, 0, count);
            String str = new String(bytes);
            log.log(level, str);
            count = 0;
        }

        /**
         * Closes this output stream and releases any system resources
         * associated with this stream.
         */
        public void close() {
            flush();
            hasBeenClosed = true;
        }
    }
}
