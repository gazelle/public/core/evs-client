package net.ihe.gazelle.evs.api.config.gherkin;

import net.ihe.gazelle.evs.api.config.validation.*;

public class Given {

    public ModelBased modelbased() {
        return new ModelBased();
    }

    public Schematron schematron() {
        return new Schematron();
    }

    public Hl7v2 hl7v2() {
        return new Hl7v2();
    }

    public Embedded embedded() {
        return new Embedded();
    }

    public SystemBased systemBased() {
        return new SystemBased();
    }

    public XmlRpc xmlRpc() {
        return new XmlRpc();
    }

    public TLS tls() {
        return new TLS();
    }

    public Gitb gitb() { return new Gitb(); }
}
