package net.ihe.gazelle.evs.api.config.validation;

import io.restassured.response.Response;
import net.ihe.gazelle.evs.api.config.gherkin.ThenAccessValidationResult;

public class ValidationResult extends Validation {

    public ValidationResult(ValidationResponse validationResponse, Response response) {
        super(validationResponse, response);
    }

    public ThenAccessValidationResult then(){
        return new ThenAccessValidationResult(this);
    }

}
