package net.ihe.gazelle.evs.api.tests;

import net.ihe.gazelle.evs.api.config.validation.ValidationRequest;
import org.junit.Test;

public class PdfIT extends DefaultAdapterIT
{
    @Override
    @SuppressWarnings("java:S2699") // assertion is in called methods
    protected ValidationRequest getRequest() {
        return given().embedded().pdf();
    }

    @Test
    @SuppressWarnings("java:S2699") // assertion is in called methods
    public void publicValidationFailed() {
        whenPublicValidation(getRequest().fileName("document.pdf"))
                .accessValidationReport()
                .then()
                .success()
                .when()
                .accessValidation()
                .then()
                .success()
                .validationFailed();
    }

}
