package net.ihe.gazelle.evs.api.tests;

import net.ihe.gazelle.evs.api.config.gherkin.Given;
import net.ihe.gazelle.evs.api.config.gherkin.WhenAccessValidation;
import net.ihe.gazelle.evs.api.config.gherkin.WhenWithoutGiven;
import net.ihe.gazelle.evs.api.config.validation.ValidationRequest;

public abstract class AbstractIT {

    public Given given() {
        return new Given();
    }

    public WhenWithoutGiven when() {
        return new WhenWithoutGiven();
    }

    protected WhenAccessValidation whenPrivateValidation(ValidationRequest request) {
        return request
                .when()
                .privateValidation()
                .then()
                .created()
                .when();
    }

    protected WhenAccessValidation whenPublicValidation(ValidationRequest request) {
        return request
                .when()
                .publicValidation()
                .then()
                .created()
                .when();
    }


    protected void publicValidationFailed(ValidationRequest request) {
        request
                .when()
                    .publicValidation()
                .then()
                    .created()
                .when()
                    .accessValidationReport()
                .then()
                    .success()
                .when()
                    .accessValidation()
                .then()
                    .success()
                    .validationFailed();
    }


    protected void accessPrivateValidationReportWithEmptyAuthorizationHeader(ValidationRequest request) {
        request
                .when()
                .privateValidation()
                .then()
                .created()
                .when()
                .accessPrivateValidationReportWithEmptyAuthorizationHeader()
                .then()
                .unauthorized();
    }


    protected void validationWrongMimeType(ValidationRequest request) {
        request
                .when()
                    .publicValidation()
                .then()
                    .created()
                .when()
                    .accessValidationWithWrongAcceptedContentType()
                .then()
                    .notAcceptable();
    }

    protected void validationReportWrongMimeType(ValidationRequest request) {
        request
                .when()
                    .publicValidation()
                .then()
                    .created()
                .when()
                    .accessValidationReportWithWrongAcceptedContentType()
                .then()
                    .notAcceptable();
    }

    protected void validationWrongContent(ValidationRequest request, String filename, boolean encode) {
        request.wrongFilename()
                .fileName(filename,encode)
                .when()
                .publicValidation()
                .then()
                .badRequest();
    }


    protected void validationUnparseableFile(ValidationRequest request, String filename) {
        request
                .fileName(filename,true)
                .when()
                .publicValidation()
                .then()
                .created()
                .when()
                .accessValidationReport()
                .then()
                .success()
                .when()
                .accessValidation()
                .then()
                .success()
                .validationFailed();
    }

    protected void validationUndefined(ValidationRequest request) {
        request
                .when()
                .publicValidation()
                .then()
                .created()
                .when()
                .accessValidationReport()
                .then()
                .success()
                .when()
                .accessValidation()
                .then()
                .success()
                .validationUndefined();
    }
}
