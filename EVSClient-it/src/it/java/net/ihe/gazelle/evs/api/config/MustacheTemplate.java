package net.ihe.gazelle.evs.api.config;

import com.samskivert.mustache.Mustache;
import com.samskivert.mustache.MustacheException;
import com.samskivert.mustache.Template;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;

public class MustacheTemplate {

    private static Logger logger = Logger.getLogger(MustacheTemplate.class);

    Mustache.Compiler compiler = Mustache.compiler()
                .withLoader(new Mustache.TemplateLoader() {
                    public Reader getTemplate(String name) {
                        try {
                            return reader(name);
                        } catch (Exception e) {
                            logger.error("Mustache.TemplateLoader", e);
                            return null;
                        }
                    }
                });

        Template template;

        public MustacheTemplate(String tplname) {
            template = compiler.compile(reader(tplname));
        }

        public String execute(Object context) throws MustacheException {
            return template.execute(context);
        }

        private Reader reader(String name) {
            return new BufferedReader(new InputStreamReader(this.getClass().getResourceAsStream(name + ".mustache"), StandardCharsets.UTF_8));
        }

        private Class<?> getRootClass() {
            return MustacheTemplate.this.getClass();
        }
}
