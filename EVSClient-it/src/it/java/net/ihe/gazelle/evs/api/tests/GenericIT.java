package net.ihe.gazelle.evs.api.tests;

import org.junit.Test;

public class GenericIT extends AbstractIT {

    @Test
    @SuppressWarnings("java:S2699") // assertion is in called methods
    public void accessValidationThatDoesNotExist() {
        when()
                .accessValidationThatDoesNotExist()
        .then()
                .notFound();
    }

    @Test
    @SuppressWarnings("java:S2699") // assertion is in called methods
    public void accessValidationReportThatDoesNotExist() {
        when()
                .accessValidationReportThatDoesNotExist()
        .then()
                .notFound();
    }
}
