package net.ihe.gazelle.evs.api.config.gherkin;

import net.ihe.gazelle.evs.api.config.validation.ValidationResponse;

public class Then {

    private ValidationResponse validationResponse;

    public Then(ValidationResponse validationResponse) {
        this.validationResponse = validationResponse;
    }

    public Then created() {
        validationResponse.getResponse().then().statusCode(201);
        return this;
    }

    public Then unauthorized() {
        validationResponse.getResponse().then().statusCode(401);
        return this;
    }

    public Then badRequest() {
        validationResponse.getResponse().then().statusCode(400);
        return this;
    }
    public Then forbidden() {
        validationResponse.getResponse().then().statusCode(403);
        return this;
    }
    public WhenAccessValidation when() {
        return new WhenAccessValidation(validationResponse);
    }
}
