package net.ihe.gazelle.evs.api.config.validation;

public class Embedded {

    public ValidationRequest xval() {
        return new ValidationRequest("/it/evs/ws/services/embedded/xval/service",
                "/it/evs/ws/services/embedded/xval/validator",
                "/it/evs/ws/services/embedded/xval/objects");
    }

    public ValidationRequest pdf() {
        return new ValidationRequest("/it/evs/ws/services/embedded/pdf/service",
                "/it/evs/ws/services/embedded/pdf/validator",
                "/it/evs/ws/services/embedded/pdf/filename");
    }

    public ValidationRequest pixelmed() {
        return new ValidationRequest("/it/evs/ws/services/embedded/pixelmed/service",
                "/it/evs/ws/services/embedded/pixelmed/validator",
                "/it/evs/ws/services/embedded/pixelmed/filename");
    }

}
