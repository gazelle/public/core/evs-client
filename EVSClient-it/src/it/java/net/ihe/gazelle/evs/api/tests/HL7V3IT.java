package net.ihe.gazelle.evs.api.tests;

import net.ihe.gazelle.evs.api.config.validation.ValidationRequest;
import org.junit.Test;

public class HL7V3IT extends DefaultAdapterIT
{
    @Override
    protected ValidationRequest getRequest() {
        return given().modelbased().hl7v3();
    }

    @Test
    @SuppressWarnings("java:S2699") // assertion is in called methods
    public void publicValidationFailed(){
        publicValidationFailed(getRequest().fileName("MyCDA2.xml"));
    }

    @Test
    @SuppressWarnings("java:S2699") // assertion is in called methods
    public void publicValidationFailedWithOtherValidator(){
        publicValidationFailed(getRequest().validator("[ITI-44] Patient Identity Feed HL7V3 - Add Patient Record"));
    }
}
