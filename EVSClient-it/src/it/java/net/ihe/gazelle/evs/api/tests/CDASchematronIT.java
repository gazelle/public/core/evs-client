package net.ihe.gazelle.evs.api.tests;

import net.ihe.gazelle.evs.api.config.validation.ValidationRequest;
import org.junit.Test;

public class CDASchematronIT extends DefaultAdapterIT {

    @Override
    protected ValidationRequest getRequest() {
        return given().schematron().cda();
    }

    protected void validationWrongEncodingFile(ValidationRequest request) {
        publicValidationFailed(request
                .modifyContent("......$","XXXXXX"));
    }

    @Test
    @SuppressWarnings("java:S2699") // assertion is in called methods
    public void validationWrongEncodingFile() {
        validationWrongEncodingFile(
                getRequest()
        );
    }

}
