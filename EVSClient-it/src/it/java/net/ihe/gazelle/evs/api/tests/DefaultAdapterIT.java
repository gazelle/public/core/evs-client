package net.ihe.gazelle.evs.api.tests;

import net.ihe.gazelle.evs.api.config.validation.ValidationRequest;
import org.junit.Test;

public abstract class DefaultAdapterIT extends AbstractIT {

    protected abstract ValidationRequest getRequest();

    @Test
    @SuppressWarnings("java:S2699") // assertion is in called methods
    public void publicValidationPassed() {
                whenPublicValidation(getRequest())
                        .accessValidationReport()
                        .then()
                        .success()
                        .when()
                        .accessValidation()
                        .then()
                        .success()
                        .validationPassed();
    }

    @Test
    @SuppressWarnings("java:S2699") // assertion is in called methods
    public void privateValidationPassed() {
                whenPrivateValidation(getRequest())
                        .accessPrivateValidationReport()
                        .then()
                        .success()
                        .when()
                        .accessPrivateValidation()
                        .then()
                        .success()
                        .validationPassed();
    }

    @Test
    @SuppressWarnings("java:S2699") // assertion is in called methods
    public void privateValidationWithExpiredKey() {
                getRequest()
                        .when()
                        .privateValidationWithExpiredKey()
                        .then()
                        .unauthorized();
    }

    @Test
    @SuppressWarnings("java:S2699") // assertion is in called methods
    public void privateValidationWithWrongKey() {
                getRequest()
                        .when()
                        .privateValidationWithWrongKey()
                        .then()
                        .unauthorized();
    }

    @Test
    @SuppressWarnings("java:S2699") // assertion is in called methods
    public void privateValidationWithEmptyAuthorizationHeader() {
                getRequest()
                        .when()
                        .privateValidationWithEmptyAuthorizationHeader()
                        .then()
                        .badRequest();
    }

    @Test
    @SuppressWarnings("java:S2699") // assertion is in called methods
    public void privateValidationWithBadAuthorizationHeader() {
                getRequest()
                        .when()
                        .privateValidationWithBadAuthorizationHeader()
                        .then()
                        .badRequest();
    }

    @Test
    @SuppressWarnings("java:S2699") // assertion is in called methods
    public void validationWrongService() {
                getRequest()
                        .wrongService()
                        .when()
                        .publicValidation()
                        .then()
                        .badRequest();
    }

    @Test
    @SuppressWarnings("java:S2699") // assertion is in called methods
    public void validationWrongValidator() {
                getRequest()
                        .wrongValidator()
                        .when()
                        .publicValidation()
                        .then()
                        .badRequest();
    }

    @Test
    @SuppressWarnings("java:S2699") // assertion is in called methods
    public void validationEmptyFile() {
        validationWrongContent(getRequest(),"empty-file.xml", true);
    }


    @Test
    @SuppressWarnings("java:S2699") // assertion is in called methods
    public void accessPrivateValidationWithWrongApiKey() {
                whenPrivateValidation(getRequest())
                        .accessPrivateValidationWithWrongApiKey()
                        .then()
                        .unauthorized();
    }

    @Test
    @SuppressWarnings("java:S2699") // assertion is in called methods
    public void accessPrivateValidationWithExpiredApiKey() {
                whenPrivateValidation(getRequest())
                        .accessPrivateValidationWithExpiredApiKey()
                        .then()
                        .unauthorized();
    }

    @Test
    @SuppressWarnings("java:S2699") // assertion is in called methods
    public void accessValidationNoAcceptedContentType() {
        whenPublicValidation(getRequest())
                    .accessValidationWithNoAcceptedContentType()
                    .then()
                    .notAcceptable();
    }

    @Test
    @SuppressWarnings("java:S2699") // assertion is in called methods
    public void accessPrivateValidationWithoutAuthorizationHeader() {
                whenPrivateValidation(getRequest())
                        .accessValidation()
                        .then()
                        .unauthorized();
    }

    @Test
    @SuppressWarnings("java:S2699") // assertion is in called methods
    public void accessPrivateValidationReportWithWrongApiKey() {
                whenPrivateValidation(getRequest())
                        .accessPrivateValidationReportWithWrongApiKey()
                        .then()
                        .unauthorized();
    }

    @Test
    @SuppressWarnings("java:S2699") // assertion is in called methods
    public void accessPrivateValidationReportWithExpiredApiKey() {
                whenPrivateValidation(getRequest())
                        .accessPrivateValidationReportWithExpiredApiKey()
                        .then()
                        .unauthorized();
    }

    @Test
    @SuppressWarnings("java:S2699") // assertion is in called methods
    public void accessValidationReportNoAcceptedContentType() {
        whenPublicValidation(getRequest())
                    .accessValidationReportWithNoAcceptedContentType()
                    .then()
                    .notAcceptable();
    }

    @Test
    @SuppressWarnings("java:S2699") // assertion is in called methods
    public void accessValidationWrongAcceptedContentType() {
        whenPublicValidation(getRequest())
                .accessValidationWithWrongAcceptedContentType()
                .then()
                .notAcceptable();
    }

    @Test
    @SuppressWarnings("java:S2699") // assertion is in called methods
    public void accessValidationReportWrongAcceptedContentType() {
        whenPublicValidation(getRequest())
                .accessValidationReportWithWrongAcceptedContentType()
                .then()
                .notAcceptable();
    }
    
    @Test
    @SuppressWarnings("java:S2699") // assertion is in called methods
    public void accessPrivateValidationWithWrongOrganizationApiKey() {
        whenPrivateValidation(getRequest())
                    .accessValidationWithWrongOrganizationApiKey()
                .then()
                    .forbidden();
    }
    @Test
    @SuppressWarnings("java:S2699") // assertion is in called methods
    public void accessPrivateValidationReportWithoutAuthorizationHeader() {
            whenPrivateValidation(getRequest())
                    .accessValidationReport()
                    .then()
                    .unauthorized();
    }
}
