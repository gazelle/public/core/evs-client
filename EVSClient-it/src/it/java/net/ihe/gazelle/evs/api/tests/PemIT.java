package net.ihe.gazelle.evs.api.tests;

import net.ihe.gazelle.evs.api.config.validation.ValidationRequest;
import org.junit.Test;

public class PemIT extends DefaultAdapterIT{

    @Override
    @SuppressWarnings("java:S2699") // assertion is in called methods
    protected ValidationRequest getRequest() {
        return given().tls().pem();
    }

    @Test
    @SuppressWarnings("java:S2699") // assertion is in called methods
    public void publicValidationFailed() {
        whenPublicValidation(getRequest().fileName("tls-server-failed.crt"))
                .accessValidationReport()
                .then()
                .success()
                .when()
                .accessValidation()
                .then()
                .success()
                .validationFailed();
    }
}
