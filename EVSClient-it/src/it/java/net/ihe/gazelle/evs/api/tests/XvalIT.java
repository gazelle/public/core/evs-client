package net.ihe.gazelle.evs.api.tests;

import net.ihe.gazelle.evs.api.config.validation.ValidationRequest;
import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;

public class XvalIT extends DefaultAdapterIT {
   @Override
   protected ValidationRequest getRequest() {
      return given().embedded().xval();
   }

   @Test
   @SuppressWarnings("java:S2699") // assertion is in called methods
   public void validationEmptyObjects() {
      ValidationRequest request = getRequest();
      request.getObjects().clear();
      request.when()
            .publicValidation()
            .then()
            .badRequest();
   }

   @Test
   @SuppressWarnings("java:S2699") // assertion is in called methods
   public void validationWrongObjectsCardinality() {
      ValidationRequest request = getRequest();
      request.getObjects().remove(0);
      validationUndefined(request);
   }

   @Test
   @SuppressWarnings("java:S2699") // assertion is in called methods
   public void validationWrongObjectRole() {
      ValidationRequest request = getRequest();
      request.getObjects().get(0).setRole("wrong");
      validationUndefined(request);
   }

   @Test
   @SuppressWarnings("java:S2699") // assertion is in called methods
   public void validationEmptyObjectRole() {
      ValidationRequest request = getRequest();
      request.getObjects().get(0).setRole(null);
      whenPublicValidation(request)
            .accessValidationReport()
            .getResponse()
            .then()
            .body("validationReport.@result", equalTo("UNDEFINED"))
            .body("validationReport.subReport.unexpectedError.cause.cause.message", equalTo("invalid-input-role null"));
   }
}
