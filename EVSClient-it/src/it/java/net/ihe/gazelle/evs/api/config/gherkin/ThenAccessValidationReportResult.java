package net.ihe.gazelle.evs.api.config.gherkin;

import net.ihe.gazelle.evs.api.config.validation.Validation;
import net.ihe.gazelle.evs.api.config.validation.ValidationResult;

import static org.hamcrest.Matchers.equalTo;

public class ThenAccessValidationReportResult extends ThenAccessValidation<ThenAccessValidationReportResult> {

    public ThenAccessValidationReportResult(Validation validationReportResult) {
        super(validationReportResult);
    }

}
