package net.ihe.gazelle.evs.api.config.validation;

public class ModelBased {

    public ValidationRequest hl7v3() {
        return new ValidationRequest("/it/evs/ws/services/model-based/hl7v3/service",
                "/it/evs/ws/services/model-based/hl7v3/validator",
                "/it/evs/ws/services/model-based/hl7v3/filename");
    }

    public ValidationRequest cda() {
        return new ValidationRequest("/it/evs/ws/services/model-based/cda/service",
                "/it/evs/ws/services/model-based/cda/validator",
                "/it/evs/ws/services/model-based/cda/filename");
    }

    public ValidationRequest xdstar() {
        return new ValidationRequest("/it/evs/ws/services/model-based/xdstar/service",
                "/it/evs/ws/services/model-based/xdstar/validator",
                "/it/evs/ws/services/model-based/xdstar/filename");
    }

    public ValidationRequest fhirJson() {
        return new ValidationRequest("/it/evs/ws/services/model-based/fhir-json/service",
                "/it/evs/ws/services/model-based/fhir-json/validator",
                "/it/evs/ws/services/model-based/fhir-json/filename");
    }

    public ValidationRequest fhirXml() {
        return new ValidationRequest("/it/evs/ws/services/model-based/fhir-xml/service",
                "/it/evs/ws/services/model-based/fhir-xml/validator",
                "/it/evs/ws/services/model-based/fhir-xml/filename");
    }

    public ValidationRequest fhirUrl() {
        return new ValidationRequest("/it/evs/ws/services/model-based/fhir-url/service",
                "/it/evs/ws/services/model-based/fhir-url/validator",
                "/it/evs/ws/services/model-based/fhir-url/filename");
    }

    public ValidationRequest atna(){
        return new ValidationRequest("/it/evs/ws/services/model-based/atna/service",
                "/it/evs/ws/services/model-based/atna/validator",
                "/it/evs/ws/services/model-based/atna/filename");
    }

    public ValidationRequest dsub(){
        return new ValidationRequest("/it/evs/ws/services/model-based/dsub/service",
                "/it/evs/ws/services/model-based/dsub/validator",
                "/it/evs/ws/services/model-based/dsub/filename");
    }

    public ValidationRequest hpd(){
        return new ValidationRequest("/it/evs/ws/services/model-based/hpd/service",
                "/it/evs/ws/services/model-based/hpd/validator",
                "/it/evs/ws/services/model-based/hpd/filename");
    }

    public ValidationRequest saml(){
        return new ValidationRequest("/it/evs/ws/services/model-based/saml/service",
                "/it/evs/ws/services/model-based/saml/validator",
                "/it/evs/ws/services/model-based/saml/filename");
    }

    public ValidationRequest svs(){
        return new ValidationRequest("/it/evs/ws/services/model-based/svs/service",
                "/it/evs/ws/services/model-based/svs/validator",
                "/it/evs/ws/services/model-based/svs/filename");
    }

    public ValidationRequest dicomWeb(){
        return new ValidationRequest("/it/evs/ws/services/model-based/dicom-web/service",
                "/it/evs/ws/services/model-based/dicom-web/validator",
                "/it/evs/ws/services/model-based/dicom-web/filename");
    }

    public ValidationRequest xdw() {
        return new ValidationRequest("/it/evs/ws/services/model-based/xdw/service",
                "/it/evs/ws/services/model-based/xdw/validator",
                "/it/evs/ws/services/model-based/xdw/filename");
    }

    public ValidationRequest iua() {
        return new ValidationRequest("/it/evs/ws/services/model-based/iua/service",
                "/it/evs/ws/services/model-based/iua/validator",
                "/it/evs/ws/services/model-based/iua/filename");
    }
}
