package net.ihe.gazelle.evs.api.config.validation;

import io.restassured.internal.util.IOUtils;
import net.ihe.gazelle.evs.api.IT;
import org.apache.commons.codec.binary.Base64;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

public class HandledObject {
    protected String fileName;
    protected String role;
    protected String content;

    protected String getFileName() {
        return fileName;
    }

    protected void setFileName(String fileName) {
        setFileName(fileName, true);
    }

    protected void setFileName(String fileName, boolean encode) {
        this.fileName = fileName;
        setContent(loadContent(this.fileName, encode));
    }

    private String loadContent(String filename, boolean encode) {
        try {
            InputStream is = IT.class.getResourceAsStream("data/" + filename);
            return encode
                    ? Base64.encodeBase64String(IOUtils.toByteArray(is))
                    : new String(IOUtils.toByteArray(is), StandardCharsets.UTF_8);
        } catch (IOException ioe) {
          throw new RuntimeException(ioe);
        }
    }

    public HandledObject modifyContent(String pattern, String replacement) {
        content = content.replaceAll(pattern, replacement);
        return this;
    }

    protected String getContent() {
        return content;
    }

    protected void setContent(String content) {
        this.content = content;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
