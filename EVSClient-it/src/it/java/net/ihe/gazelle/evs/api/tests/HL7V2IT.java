package net.ihe.gazelle.evs.api.tests;

import net.ihe.gazelle.evs.api.config.validation.ValidationRequest;

public class HL7V2IT extends DefaultAdapterIT {

    @Override
    protected ValidationRequest getRequest() {
        return given().hl7v2().passed();
    }

}
