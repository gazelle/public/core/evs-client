package net.ihe.gazelle.evs.api.config.gherkin;

import io.restassured.response.Response;
import net.ihe.gazelle.evs.api.config.RestApiConnector;
import net.ihe.gazelle.evs.api.config.validation.ValidationResult;

public class WhenWithoutGiven {

    private RestApiConnector restApiConnector;

    public WhenWithoutGiven() {
        restApiConnector = RestApiConnector.getInstance();
    }

    public ValidationResult accessValidationThatDoesNotExist() {
        Response response = restApiConnector.getRequest(restApiConnector.getValidationUri().toString() + "0.0.0.0.0.0.0");
        ValidationResult validationResult = new ValidationResult(null, response);
        return validationResult;
    }

    public ValidationResult accessValidationReportThatDoesNotExist() {
        Response response = restApiConnector.getRequest(restApiConnector.getValidationUri().toString() + "0.0.0.0.0.0.0/report");
        ValidationResult validationResult = new ValidationResult(null, response);
        return validationResult;
    }

    public ValidationResult accessValidationReport(String oid) {
        Response response = restApiConnector.getRequest(restApiConnector.getValidationUri().toString() + oid +"/report");
        ValidationResult validationResult = new ValidationResult(null, response);
        return validationResult;
    }
}
