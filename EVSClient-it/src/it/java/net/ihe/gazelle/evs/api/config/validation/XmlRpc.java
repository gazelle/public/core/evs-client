package net.ihe.gazelle.evs.api.config.validation;

public class XmlRpc {

    public ValidationRequest dccheck() {
        return new ValidationRequest("/it/evs/ws/services/xml-rpc/dccheck/service",
                "/it/evs/ws/services/xml-rpc/dccheck/validator",
                "/it/evs/ws/services/xml-rpc/dccheck/objects");
    }

}
